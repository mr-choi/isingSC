\documentclass[twocolumn, 10pt, aps, floatfix, showpacs, prl,
  citeautoscript, superscriptaddress]{revtex4-1} % notitlepage
\usepackage{graphicx}
\usepackage[caption=false]{subfig}
\usepackage{amsmath, amssymb}
\usepackage{bm}
\usepackage{xcolor}
\usepackage{upgreek}
\usepackage[colorlinks, citecolor={blue!50!black}, urlcolor={blue!50!black}, linkcolor={red!50!black}]{hyperref}
\usepackage{bookmark}
\usepackage[version=4]{mhchem}
\usepackage{microtype}
\usepackage{colordvi}
\usepackage[load=physical,load=abbr, range-units=single]{siunitx}

\setcounter{secnumdepth}{4}
\setcounter{tocdepth}{4}

\newcommand{\bgreek}[1]{\mbox{\boldmath$#1$\unboldmath}}
\newcommand{\pmat}[1]{\begin{pmatrix}#1\end{pmatrix}}
\newcommand{\avg}[1]{\langle#1\rangle}
\newcommand{\comment}[1]{}

\def\e{\varepsilon}
\renewcommand{\comment}{\paragraph}
\sisetup{range-phrase=\text{--}}

\begin{document}
\title{A New Platform for Nodal Topological Superconductors in Monolayer Molybdenum Dichalcogenides}

\author{Lin Wang}
\email[Corresponding author: ]{L.Wang-6@tudelft.nl}
\affiliation{Kavli Institute of Nanoscience, Delft University of Technology,
  P.~O.~Box 4056, 2600 GA Delft, The Netherlands}
\author{Tomas Orn Rosdahl}
\affiliation{Kavli Institute of Nanoscience, Delft University of Technology,
  P.~O.~Box 4056, 2600 GA Delft, The Netherlands}
\author{Doru Sticlet}
\affiliation{National  Institute  for  Research  and  Development of Isotopic and Molecular  Technologies,  67-103 Donat, 400293 Cluj-Napoca, Romania}
\affiliation{Kavli Institute of Nanoscience, Delft University of Technology, P.~O.~Box 4056, 2600 GA Delft, The Netherlands}

\begin{abstract}
We propose a new platform to realize nodal topological superconductors in a superconducting monolayer of \ce{MoX2} (X$=$S, Se, Te) using an in-plane magnetic field.
The bulk nodal points appear where the spin splitting due to spin-orbit coupling
vanishes near the $\pm \bm K$ valleys of the Brillouin zone, and are six or twelve per valley in total.
In the nodal topological superconducting phase, the nodal points are connected
by flat bands of zero-energy Andreev edge states. These flat bands, which are
protected by chiral symmetry, are present for all lattice-termination boundaries except zigzag.
\end{abstract}

\maketitle

\emph{Introduction.}~Fully gapped topological superconductors (TSCs), characterized by a global topological invariant in the Brillouin zone, have been the subject of investigation in recent years.
They provide a platform for the creation of Majoranas~\cite{Alicea2012, Leijnse2012, Beenakker2013}, which have promising
applications in quantum information~\cite{Kitaev2001, Bravyi2002, Nayak2008}.
Nodal superconductors, \emph{i.e.},
superconductors with nodal points or lines at the Fermi surface where the bulk gap vanishes, can also display nontrivial topological properties, becoming nodal
TSCs~\cite{Kashiwaya2000, Lofwander2001, Schnyder2015}.
Their topological invariants are defined locally in the Brillouin zone, giving rise to flat bands or arcs of surface states \cite{Ryu2002, Sato2011, Schnyder2012}.

Intrinsic nodal TSCs are predicted to exist in unconventional superconductors,
such as heavy fermion systems~\cite{Kasahara2007, Zhou2013, Allan2013}, noncentrosymmetric~\cite{Yada2011, Schnyder2011}, Weyl~\cite{Fischer2014}, or high-temperature $d$-wave superconductors~\cite{Tsuei2000}.
However, intrinsic unconventional pairing is complex, ambiguous, and not robust to disorder, making intrinsic nodal TSCs challenging experimentally.
It is therefore desirable to engineer nodal TSCs using simpler components \cite{Meng2012, Wong2013, Huang2018}, such as conventional $s$-wave spin-singlet superconductors, similar to realizing fully gapped TSCs using proximity-induced $s$-wave pairing~\cite{Fu2008, Sau2010}.

Two-dimensional monolayers of transition metal dichalcogenides (TMDs)~\cite{Wang2012} offer an opportunity to engineer nodal TSCs.
Recent experiments show that several monolayer TMDs, such as \ce{MoS2}, \ce{MoSe2}, \ce{MoTe2}, \ce{WS2}, and \ce{NbSe2}, become superconducting~\cite{Ye2012, Taniguchi2012, Lu2015, Shi2015, Saito2016, Costanzo2016, Xi2016, Zheliuk2017, Sergio2018}, with a critical temperature \emph{e.g.}~as large as $10$ K observed in \ce{MoS2}~\cite{Lu2015}.
These superconductors possess a critical in-plane magnetic field several times larger than the Pauli limit, due to a special type of Ising spin-orbit coupling (SOC)~\cite{Lu2015, Saito2015, Zhou2016, Ilic2017}.
Ising SOC results from the heavy atoms and the absence of inversion symmetry, and acts as an effective Zeeman term perpendicular to the TMD plane, with opposite orientation at opposite momenta, pinning electron spins to the out-of-plane direction~\cite{Zhu2011, Xiao2012}.
Previous work predicts that hole-doped monolayer \ce{NbSe2} with $s$-wave
superconductivity near $\bm\Gamma$ becomes a nodal TSC in an in-plane magnetic
field~\cite{He2016}. There, the bulk nodal points appear along
  $\Gamma-M$ lines where the Ising SOC vanishes because of the in-plane mirror
symmetry $M_x: x\rightarrow -x$.
However, the potential of TMD materials such as \ce{MoS2}, \ce{MoSe2},
\ce{MoTe2}, and \ce{WS2}, which are superconducting at electron doping near the
$\bm K$ valleys, to become nodal TSC, is currently not known.
Note that $M_x$ does not guarantee the vanishing of SOC near the $\bm K$ valleys.

In this Letter, we show that $s$-wave superconducting monolayers of molybdenum dichalcogenides (\ce{MoX2}, X$=$S, Se, Te) become nodal TSCs in an in-plane magnetic field.
The bulk nodal points appear near the $\bm K$ valleys at special momenta where
the SOC splitting vanishes. We find two regimes in the nodal topological phase, with six or twelve nodal points appearing near each valley respectively.
Nodal points are connected by zero-energy Andreev flat band edge states, which are protected by chiral symmetry
originating from mirror symmetry in the \ce{MoX2} plane, and present for all edges except zigzag.
Finally, we discuss possible experimental verification.

\emph{Model.}~A monolayer \ce{MX2} (\ce{MoS2}, \ce{MoSe2}, \ce{MoTe2}, or \ce{WS2}) consists of a triangular lattice of M atoms sandwiched between two triangular lattices of X atoms.
The top and bottom X atoms project onto the same position in the layer of M atoms.
Viewed from above, the monolayer has the hexagonal lattice structure shown in Fig.~\ref{fig:hex_nodal_points}(a).
In the normal state, the monolayer \ce{MX2} has a direct band gap at the $\pm \bm K$ points.
Near the $\eta \bm K$ ($\eta = \pm$) points, the point group is $C_{3h}$, and the effective Hamiltonian of the lowest
conduction band up to the third order in momentum ${\bm k} = (k_x, k_y)$ is
\begin{equation}
  H_e^{\eta}({\bm k})=\frac{k^2}{2m^*}+[\lambda\eta+A_1k^2\eta+A_2(k_x^3-3k_xk_y^2)]\sigma_z,\label{normal_H}
\end{equation}
in the basis $[c_{\eta {\bm k}\uparrow}, c_{\eta {\bm k}\downarrow}]$, with
$c_{\eta {\bm k} s}$ the annihilation operator for an electron in valley $\eta$ at momentum ${\bm k}$ with spin $s = \uparrow, \downarrow$. 
We obtain this effective Hamiltonian from the ${\bm k}\cdot {\bm p}$ Hamiltonian near the $\pm \bm K$
valleys in Ref.~\onlinecite{Fang2015} by L\"owdin partitioning~\cite{Lowdin1951,Wang_L2014}.
The $x\ (y)$-axis points along the zigzag (armchair) direction as in Fig.~\ref{fig:hex_nodal_points}(a), $m^*$ denotes the effective mass, $\lambda$ and $A_{1,2}$ are SOC strengths, and $\sigma_{x, y, z}$ Pauli matrices in spin space.
Material parameters are provided as Supplementary Material \cite{suppl}.

Including superconductivity with $s$-wave pairing and an in-plane magnetic field, the Bogoliubov-de Gennes (BdG) Hamiltonian in the basis $[c_{\eta {\bm k}\uparrow},c_{\eta {\bm
      k}\downarrow},c^{\dagger}_{-\eta-{\bm k}\uparrow},c^{\dagger}_{-\eta-{\bm
    k}\downarrow}]$ is
\begin{eqnarray}
  H_{\rm BdG}^{\eta}({\bm k})
  &=&(\frac{k^2}{2m^*}-\mu)\tau_z+[\lambda\eta+A_1k^2\eta+A_2(k_x^3-3k_x\nonumber\\
    &&\mbox{}\times k_y^2)]\sigma_z+V_x\sigma_x\tau_z+V_y\sigma_y+\Delta\sigma_y\tau_y,
    \label{eq:H_BdG}
\end{eqnarray}
where $\mu$, $\tau_{x, y, z}$, $\Delta$ and $V_{x,y}$ are the chemical
potential, Pauli matrices in particle-hole space, the superconducting gap, and
the Zeeman energy terms due to the magnetic field, respectively.

The BdG Hamiltonian $H_{\rm BdG}^{\eta}({\bm k})$ has a particle-hole symmetry
(PHS) $\mathcal{P}H_{\rm
  BdG}^{\eta}({\bm k}){\mathcal{P}}^{-1}=-H_{\rm BdG}^{-\eta}(-{\bm k})$ where
$\mathcal{P}=\tau_x\mathcal{K}$, with complex conjugation $\mathcal{K}$.
Although time-reversal symmetry (TRS) $\mathcal{T}=i\sigma_y\mathcal{K}$ is broken by the magnetic field, $H_{\rm
  BdG}^{\eta}({\bm k})$ respects an effective TRS $\tilde{\mathcal{T}}H_{\rm
  BdG}^{\eta}({\bm k}){\tilde{\mathcal{T}}}^{-1}=H_{\rm BdG}^{-\eta}(-{\bm k})$ where
$\tilde{\mathcal{T}}=M_{xy}\mathcal{T}$, with $M_{xy}=-i\sigma_z\tau_z$ the mirror symmetry in the monolayer plane.
Therefore, $H_{\rm BdG}^{\eta}({\bm k})$ has the chiral symmetry $\mathcal{C}H_{\rm BdG}^{\eta}({\bm k}){\mathcal{C}}^{-1}=-H_{\rm BdG}^{\eta}({\bm k})$ with
$\mathcal{C}=\mathcal{P}\tilde{\mathcal{T}} = \sigma_x\tau_y$.
As a result, $H_{\rm BdG}^{\eta}({\bm k})$ is in class BDI, which is trivial in
two dimensions for gapped systems~\cite{Schnyder2008,Chiu2016}, but can be
nontrivial for nodal systems.
We reduce the dimension to one by fixing two orthogonal directions
${\bm k}_\parallel$ and ${\bm k}_\perp$ in momentum space, and considering each
$H_{\rm BdG}^{\eta}({\bm k}_\perp, {\bm k}_\parallel)$ at a fixed ${\bm
  k}_\parallel$ separately~\cite{Sato2011}.
Although $\mathcal{P}$ and $\tilde{\mathcal{T}}$ are generally not symmetries of the one-dimensional (1D) Hamiltonian $H_{\rm BdG}^{\eta}({\bm k}_\perp, {\bm k}_\parallel)$ at a fixed ${\bm k}_\parallel$ \cite{Varjas2018}, $\mathcal{C}$ is a symmetry for any choice of ${\bm k}_\parallel$ \footnote{The 1D PHS or TRS that only flip ${\bm k}_\perp$ require an extra unitary symmetry that maps ${\bm k}_\parallel \rightarrow -{\bm k}_\parallel$, 
but we find that no such symmetry exists for generic $({\bm k}_\perp, {\bm k}_\parallel)$.}.
Therefore the 1D Hamiltonians $H_{\rm
  BdG}^{\eta}({\bm k}_\perp, {\bm k}_\parallel)$ at a fixed ${\bm
  k}_\parallel$ belong to class AIII \footnote{For the armchair direction ${\bm k}_\parallel
  = k_y \hat{\bm y}$, although the 1D Hamiltonian
  of the continuum model belongs to class BDI, it turns out to be in class AIII in
  the tight-binding models, due to the absence of a point group symmetry that maps $y \rightarrow -y$.}, and are characterized the winding number
topological invariant \cite{Schnyder2008}.

\begin{figure}[!tbh]
\includegraphics[width=0.9\columnwidth]{combisketch.pdf}
\caption{(a) Top view of monolayer \ce{MX2} lattice structure with primitive lattice vectors ${\bm a}_1$ and
  ${\bm a}_2$.
  (b) Phase diagram of the gap-closing condition as a
  function of $\mu$ and $\sqrt{V_x^2-\Delta^2}$.
  Nodal points appear in regions where the gap closes, colored yellow (regime I) and green
  (regime II),
  with the phase boundaries given by $\mu=\mu_{1,2}\pm
  \sqrt{V_x^2-\Delta^2}$. III represents the boundary between regimes I and II. (c) Sketch of nodal points near $\bm K$ valley. 
The chirality of nodal points with $+ (\star)$ is $1 (-1)$, and diamond denotes two overlapping nodal points of opposite chirality.
Nodal point projections on the $k_y$-axis determine nontrivial phases with nonzero winding number (solid green lines).}
\label{fig:hex_nodal_points}
\end{figure}
%

\emph{Bulk nodal points.}~We begin investigating the topological phases of $H_{\rm BdG}^{\eta}({\bm k})$ by finding the gap-closing conditions, which determine the bulk nodal points.
Due to chiral symmetry, $H_{\rm BdG}^{\eta}({\bm k})$ can be brought to a block
off-diagonal form \cite{Beri2010, Sato2011},
with the upper off-diagonal element
\begin{eqnarray}
  A_{\eta}({\bm
    k})&=&-(\frac{k^2}{2m^*}-\mu)+[\lambda\eta+A_1k^2\eta+A_2(k_x^3-3k_xk_y^2)]\sigma_z\nonumber\\
  &&\mbox{}-V_x\sigma_x+V_y\sigma_y+i\Delta\sigma_z.
\end{eqnarray}
The gap-closing condition ${\rm det}[A_{\eta}({\bm k})]=0$ gives rise to two requirements:
\begin{subequations}
\begin{align}
\lambda\eta+A_1k^2\eta+A_2(k_x^3-3k_xk_y^2)&=0, \label{nodal}\\
\mu\pm \sqrt{V_x^2+V_y^2-\Delta^2}&=\frac{k^2}{2m^*}. \label{mom_circ}
\end{align}
\label{eq:gap_closing}
\end{subequations}
\noindent The first is the vanishing of SOC splitting [see Eq.~(\ref{normal_H})], and the
second is the magnetic field closing the bulk gap at the Fermi circle without SOC.
Closing the gap with the magnetic field brings together bands coupled by SOC.
The bands thus repulse, except at momentum points where the SOC vanishes and the gap closes.
Such points manifest as crossings between the spin-split conduction bands in the
normal-state dispersion, which are present near $\pm \bm K$ valleys in monolayer
\ce{MoX2} (X=S, Se, Te) but not \ce{WS2}, due to the relative strengths of SOC
  contributions from the $d$ orbitals on the M atoms and the $p$
  orbitals on the X atoms~\cite{Liu2013, Kosmider2013,
  Kormanyos2015}. 
Therefore, the requirement~\eqref{nodal} is not met in \ce{WS2}, and we focus on \ce{MoX2} in the following.
Because the gap-closing requirements \eqref{eq:gap_closing} are independent of the in-plane magnetic field orientation, we fix $V_y  = 0$.
Solving Eq.~\eqref{nodal} limits $k$ to $k_{c1}\le k\le k_{c2}$ with $k_{c1,c2}=k_0\pm k_0^2/(2A_0)$, $k_0=\sqrt{-\lambda/A_1}$ and $A_0=A_1/A_2$~\cite{suppl}.
Figure~\ref{fig:hex_nodal_points}(b) shows a phase diagram of the gap-closing conditions as a function of $\mu$ and $\sqrt{V_x^2-\Delta^2}$.
The four phase boundaries $\mu=\mu_{1,2}\pm \sqrt{V_x^2-\Delta^2}$ with
$\mu_{1,2}=k^2_{c1,c2}/(2m^*)$ divide the diagram into regimes, with nodal points and therefore possible nontrivial phases in the colored regions (I and II).

\emph{Topological phases.}~In the gapless regimes of the phase diagram, Fig.~\ref{fig:hex_nodal_points}(c) sketches the nodal points near the $\bm K$ valley along with their chirality $w({\bm k}^i)$.
The chirality of the nodal point at ${\bm k}^i = (k_\perp^i, k_\parallel^i)$ is the winding number around it, and is $\pm 1$ \cite{Beri2010, Sato2011, Schnyder2011}.
The nodal point chirality relates to the winding number $W$ of the
1D Hamiltonian at a fixed $k_\parallel$ through $W(k_\parallel) =\sum_{{k^i_\parallel} < k_\parallel} w({\bm k}^i)$, so we can obtain $W(k_\parallel)$ by counting the nodal point projections onto the $k_\parallel$-axis and keeping track of their chirality.
For the zigzag direction ${\bm k}_\parallel = k_x \hat{\bm x}$, the nodal point projections cancel exactly, because the nodal points come in pairs with opposite chirality at each $k_x$, and hence $W(k_x) = 0$ always.
For any other direction, the nodal points do not cancel, and nontrivial phases thus exist for all directions ${\bm k}_\parallel$ other than zigzag.
We show the projections of the nodal points on the armchair direction ${\bm k}_\parallel = k_y \hat{\bm y}$, and the corresponding segments of the $k_y$-axis where $W(k_y) \neq 0$(solid green lines).
In regime I, there are two momentum circles \eqref{mom_circ} near the $\bm K$ valley, with six nodal points each for a total of twelve.
The nodal points divide the $k_y$-axis into thirteen segments, with six segments nontrivial.
In regime II, there is only one momentum circle with six nodal points, and the $k_y$-axis separates into seven parts, with three nontrivial.
At the boundary between regimes I and II (marked as III in the figure), pairs of nodal points of opposite chirality overlap on one momentum circle, such that only the other circle contributes to the winding number $W$, similar to regime II.
The nodal points near the $-\bm K$ valley are symmetric to the ones near $\bm K$ in $k_x$ [see also Fig.~\ref{fig:other_edge_cut}(a)].
The preceding analysis applies equally to all three \ce{MoX2} monolayers.
In the following, we further explore the topological phases, focusing on nodal point projections on the armchair direction.
We show examples for specific materials, and have verified that the physics is qualitatively the same for all three~\cite{suppl}.

To complement the analysis of nodal point projections,
Figs.~\ref{fig:winding_gap}(a) and (b) show computed phase diagrams of the winding number as a function of $k_y$ and $\sqrt{V_x^2-\Delta^2}$ at two chemical potentials, $\mu_1<\mu<(\mu_1+\mu_2)/2$ in (a) and $\mu<\mu_1$ in (b), respectively representative of regimes I and II of Fig.~\ref{fig:hex_nodal_points}.
The phase diagrams are even in $k_y$, and the winding number is $\pm 2$ due to equal contributions from the $\pm\bm K$ valleys.
The phase boundaries in Fig.~\ref{fig:hex_nodal_points}(b) determine the range of the nontrivial regions in $\sqrt{V_x^2-\Delta^2}$, while the maximum extent along $k_y$ is bounded from above by $|k_y| \leq k_0$, independent of $\mu$ and $\sqrt{V_x^2 - \Delta^2}$ \cite{suppl}.
Sweeping over $\sqrt{V_x^2-\Delta^2}$ in (a), the phase diagram transitions from regime I to II, and the number of nontrivial segments along $k_y$ changes from six to three (also counting $-k_y$).
In contrast, (b) is exclusively in regime II.
%
\begin{figure}[!tbh]
\includegraphics[width=0.9\columnwidth]{topo_kp_MoSe2.pdf}
\caption{Topological phase diagrams for the armchair direction ${\bm k}_\parallel = k_y \hat{\bm y}$ of monolayer \ce{MoSe2}.
The winding number as a function of $k_y$ and $\sqrt{V_x^2-\Delta^2}$ with
(a) $\mu_1<\mu<(\mu_1+\mu_2)/2$ and (b) $\mu<\mu_1$, in regimes I and II of Fig.~\ref{fig:hex_nodal_points}.
The phase diagrams for $(\mu_1+\mu_2)/2<\mu<\mu_2$ and $\mu>\mu_2$ are similar to (a) and (b) respectively, but with opposite winding numbers.
(c, d) The corresponding topological excitation gap $E_{\rm gap}$ to (a) and (b) separately.
Data is obtained using the continuum model \eqref{eq:H_BdG}, and $a$ is
the lattice constant of the \ce{MX2} lattice.}
\label{fig:winding_gap}
\end{figure}
%

\begin{figure}[!tbh]
\includegraphics[width=0.9\columnwidth]{DOS_decay_2.pdf}
\caption{(a) Density of states at the armchair edge as a function of $k_y$ for monolayer \ce{MoS2}, with parameters in regime I of Fig.~\ref{fig:hex_nodal_points}(b).
Flat bands of zero-energy Andreev edge states where the winding number is nonzero between nodal point projections.
(b) Decay length of the edge states in the nontrivial phase.
The nontrivial phases are marked by the shaded regions with the nonzero winding numbers in the insets.
Data is obtained using an 11-orbital tight-binding model with $\mu = 1.8337$ eV, $\sqrt{V_x^2 - \Delta^2} = 1.5$ meV, and $\Delta=0.8\ $meV \cite{suppl}.}
\label{fig:LDOS}
\end{figure}

\emph{Excitation gap and edge states.}~Topologically nontrivial phases are
protected by the topological gap, which we define as $E_{\rm
  gap}({\bm k}_\parallel) = \mathrm{min}_{n, {\bm k}_\perp} |E_{n}({\bm
  k}_\parallel, {\bm k}_\perp)|$, with $E_{n}({\bm k})$ the spectrum of
$H_{\rm BdG}^\eta({\bm k})$, and $n$ a band index. Figures \ref{fig:winding_gap}(c) and (d) show the gap corresponding to the phase diagrams (a) and (b), respectively.
In the nontrivial phase, we see that $E_{\rm gap} \lesssim 0.1 \Delta$ for \ce{MoSe2}, and similarly find $E_{\rm gap} \lesssim 0.04\Delta$ for \ce{MoS2}, and $E_{\rm gap} \lesssim 0.2\Delta$ for \ce{MoTe2} \cite{suppl}.
Here, $\Delta$ may represent intrinsic superconductivity, such that no proximity effect is required, and interface effects that reduce the gap further are thus absent.


In a topologically nontrivial phase, edge states manifest at a monolayer edge.
We investigate the edge states at an armchair edge by calculating the local density of states (LDOS) at the boundary, $\rho(E, x_B, k_y)=-\frac{1}{\pi}{\rm Tr}[{\rm Im}G(E, x_B, k_y)]$, with $E$ the energy, $x_B$ the coordinate of the armchair edge, and $G$ the surface Green's function \cite{Datta1995}.
Figure \ref{fig:LDOS}(a) shows the LDOS obtained using parameters from regime I of Fig.~\ref{fig:hex_nodal_points}(b), \emph{i.e.}~with $12$ nodal points per valley.
There are six sections of zero-energy Andreev flat bands connecting nodal points, matching the nontrivial regions (dotted lines).
In Fig.~\ref{fig:LDOS}(b), we show the decay length of the edge states, which is of the order \SI{1}{\mu m} here.


\begin{figure}[!tbh]
\includegraphics[width=0.9\columnwidth]{edge_cut_winding.pdf}
\caption{(a) Schematic of the hexagonal first Brillouin zone of the monolayer lattice, with nodal points around the high symmetric points $\pm\bm K$.
For arbitrary edge cuts, we deform the Brillouin zone into a rectangle (dash-dotted lines, $k_\parallel$ and $k_\perp$ axes), and project the nodal points onto $k_\parallel$.
Flat bands of Andreev bound states exist for $k_\parallel$ where the winding number is nonzero (bold colored lines). 
For a generic edge cut, each nodal point generally projects onto a distinct $k_\parallel$, such that the winding number may take various values, \emph{e.g.}~$\pm 1$ (green) or $\pm 2$ (purple) in the sketch.
(b) Phase diagram of the winding number for an edge with $ \phi \approx 1.2^\circ$.
The phase diagram is rich with the winding number $\pm 1$, $\pm 2$, $\pm 3$ or $\pm 4$.
Data is obtained from an 11-orbital tight-binding model for \ce{MoS2} with $\mu=1.8390\ $eV and $\Delta=0.8\ $meV \cite{suppl}.}
\label{fig:other_edge_cut}
\end{figure}


\emph{Arbitrary edge directions.}~Although we have focused on an armchair edge, topologically nontrivial regimes exist for all edges except zigzag.
Simulating the \ce{MX2} lattice with tight-binding models [Fig.~\ref{fig:hex_nodal_points}(a)] using Kwant \cite{Groth2014}, we characterize an edge with a superlattice vector $\bm T$ at the angle $\phi$ relative to the armchair direction~\cite{suppl}.
We deform the hexagonal first Brillouin zone into the rectangle spanned by primitive reciprocal vectors $\hat{{\bm k}}_\parallel$ and $\hat{{\bm k}}_\perp$, which are parallel and, respectively, transverse to $\bm T$ \cite{Delplace2011}, and project the nodal points onto the $k_\parallel$-axis [Fig.~\ref{fig:other_edge_cut}(a)].
Flat bands exist in segments of the $k_\parallel$-axis where the winding number is nonzero.
Unlike an armchair edge, the nodal points near $\pm\bm K$ generally do not project pairwise onto the same $k_\parallel$ at a generic boundary, and the winding number can take other values than $\pm 2$, e.g., $\pm 1$ (green lines).
Figure~\ref{fig:other_edge_cut}(b) shows a phase diagram for an edge direction with $\phi \approx 1.2^\circ$, showing that the winding number can be $\pm 1$, $\pm 3$, and even $\pm 4$.
For generic edges other than armchair, nodal topological phases are thus not only present, but also manifest in rich phase diagrams with large winding numbers.


\begin{table}[ht]
\caption{Chemical potentials $\mu_{1,2}$ in meV for \ce{MoS2}, \ce{MoSe2} and \ce{MoTe2} [see also Fig.~\ref{fig:hex_nodal_points}(b)], obtained from the continuum model.}
\begin{tabular}{c | c c c}
\hline\hline
~ & \ce{MoS2} & \ce{MoSe2} & \ce{MoTe2} \\
\hline
$\mu_1$ & $32.6$& $126.7$ & $136.1$ \\
$\mu_2$ & $34.5$ & $143.0$ & $184.5$ \\
\hline\hline
\end{tabular}
\label{tab:kp_mu}
\end{table}

\emph{Summary and discussion.}~We have shown that a superconducting monolayer \ce{MoX2} (X$=$S, Se, Te) becomes a nodal TSC in an
in-plane magnetic field. The bulk nodal points occur at special momenta near
$\pm\bm K$ valleys in the Brillouin zone where SOC spin splitting vanishes, and can be $6$ or $12$ in each valley.
For all edges except zigzag, flat bands of Andreev edge states, which are protected by chiral symmetry, connect the edge projections of the nodal points.
Our conclusions are based on a study of both continuum and tight-binding models.

Finally, we address experimental feasibility.
It is possible to produce monolayer \ce{MoX2} crystals with low impurity
densities, and sizes in the tens of microns or even millimeters
\cite{Coleman2011, Chowalla2013, Wang2014, Chen2017}.
Such large samples may guarantee that the topological Andreev edge states at opposing edges are well separated.
Recent experiments show that thin films even down to monolayers of \ce{MoX2} become superconducting in the conduction band at carrier densities $\gtrsim 6\times 10^{13}\ $cm$^{-2}$ \cite{Shi2015, Lu2015, Ye2012}, which translates to a minimum chemical potential $\mu_0$ for superconductivity of $153\ $meV (\ce{MoS2}), $120\ $meV (\ce{MoSe2}) and $117\ $meV (\ce{MoTe2}).
The mismatch of $\mu_0$ and $\mu_{1,2}$ in \ce{MoS2} implies that intrinsic
superconductivity is not suitable to realize nodal TSCs in
\ce{MoS2}, but this can potentially be overcome using the proximity
effect.
Furthermore, a recent experiment indicates possible intrinsic unconventional pairing in \ce{MoS2} at very large doping \cite{Costanzo2018}.
For monolayer \ce{MoSe2} and \ce{MoTe2}, $\mu_0$ is close to $\mu_{1,2}$ in Fig.~\ref{fig:hex_nodal_points}(b) [see Table~\ref{tab:kp_mu}], and therefore these two materials are promising candidates for realizing nodal TSCs.
Aside from tunnelling measurements, the character of bulk nodal points could be probed using quasiparticle
interference or local pair-breaking measurements \cite{Hanaguri2007, Allan2013,
  Zhou2013}. Because the flat bands manifest as a zero-energy
  LDOS peak in the nontrivial parts of
  the phase diagram Fig.~\ref{fig:hex_nodal_points}(b), it is possible to discern them from other edge states \cite{Rostami2016},
  which don't stick to zero energy, by tuning the magnetic field
  and/or chemical potential. If the chiral symmetry is broken, the flat bands
  may split from zero energy. Two possible causes are a perpendicular electric
  field due to asymmetric electrostatic gating, and an out-of-plane Zeeman field. The electric
  field can be avoided by chemical doping \cite{Ye2012,Lu2015} and it is
  possible to align the magnetic field along the in-plane direction to a precision of $\lesssim 0.02^\circ$, such that the out-of-plane projection is
  negligible \cite{Saito2015}.

\acknowledgements
We thank Anton Akhmerov, Alexander Lau, and Valla Fatemi for fruitful discussions. This work was supported by ERC Starting Grant 638760, the Netherlands Organisation for Scientific Research (NWO/OCW), as part of the Frontiers of Nanoscience program,
and the US Office of Naval Research.

\emph{Author contributions:} L.W.~conceived and initiated the
project. L.W.~contributed to most of the continuum model calculations, T.O.R.~performed most
of the calculations with the $11$-orbital tight-binding model, and
D.S.~performed most of the calculations with the three-orbital tight-binding
model, and some with the continuum model. All authors contributed to writing the manuscript. 

\bibliographystyle{apsrev4-1}
\bibliography{bibl}
\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
