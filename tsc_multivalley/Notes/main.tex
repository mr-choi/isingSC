\documentclass[aps,prb,notitlepage,superscriptaddress,
onecolumn,tightenlines,12pt]{revtex4-1}
%\documentclass[10pt]{article}
%\usepackage{geometry}
\usepackage{graphicx, color}
%\usepackage[caption=false]{subfig}
\usepackage{amsmath, amsthm, amssymb}
\usepackage{siunitx}
\usepackage{float}
\usepackage{mathtools}
\usepackage{framed}
\usepackage{microtype}
\usepackage{float}
\usepackage[english]{babel}
%\usepackage{overpic}
%\usepackage{tikz}
\usepackage[version=3]{mhchem}
\usepackage{bm}
\usepackage[utf8]{inputenc}
%\usepackage[square,numbers]{natbib}
%\usepackage{showkeys}

\usepackage{hyperref}

\newcommand{\pmat}[1]{
\begin{pmatrix}#1\end{pmatrix}
}

\begin{document}
%\begin{abstract}
%\end{abstract}
\title[Notes]{Notes}
\author{Doru Sticlet}
\noaffiliation
\maketitle
\tableofcontents

\section{Tb Hamiltonian for three bands}
\subsection{Symmetries}
Using mirror symmetry we can construct effective 1D symmetries.

In 2D the model has P, T, and C symmetries:
\begin{equation}
P=\tau_1K, T=i\sigma_2\tau K
\end{equation}
The mirror symmetry $M_x$, $x\to-x$ has the form:
\begin{equation}
M_x=i\sigma_1\tau_3\textrm{diag}(1,-1,1),
\end{equation}
where the last matrix acts in orbital space.
This allows to define effective PHS and TRS in 1D:
\begin{equation}
P_1=M_xP=-\sigma_1\tau_2\textrm{diag}(1,-1,1)K,\quad
T_1=\textrm{diag}(1,-1,1)K,
\end{equation}
\begin{equation}
T_1/P_1H(k_x,k_y)=\pm H(k_x,-k_y)T_1/P_1.
\end{equation}
The symmetry is broken by magnetic field pointing away from $x$ direction.

Additionally we search for effective anti-unitary symmetries at fixed $k_y$ (armchair edges) which exist for the effective models.

\subsection{Contribution from p orbitals}
The spin-orbit crossing in the conduction band can only be obtained by considering the contribution from $d$ and $p$ orbitals.
The three band model captures only the contribution from $d$ orbitals.
Here we include the contribution of $p$ orbitals of $X$ atoms by integrating out the hopping from $d$ to $p$ orbitals.

In particular we are interested in the contribution to the $d_0$ orbital which is most important in the energy window we consider, i.e.~in the conductance band bottom.

The transition to $X$ atoms also renormalize the regular hopping between the $d$ orbitals.
Instead focus here on the contribution of spin-orbit coupling in $p$ orbitals,
\begin{equation}
\langle p_y|\bm L\cdot\bm S |p_x \rangle\approx \frac{i}{2}s_3.
\end{equation}
This contributes to the model in the form of a Kane-Mele spin-orbit coupling:
\begin{equation}
H_{KM}=i\nu_{ij}t_{so}s_3,
\end{equation}
with $\nu_{ij}=\pm 1$, whether the link between $M$ atoms has nearest an $S$ atom to the left or, respectively, to the right.
The effective hopping strength $t_{so}$ is a perturbative contribution depending quadratically on the hopping integral between $d_0$ and $p_{x,y}$ orbitals:
\begin{eqnarray}
t_{so}&=&\frac{\sqrt{3}}{16}\frac{F(\varepsilon_c)}
{(\varepsilon_c-\varepsilon_p)(\varepsilon_c-\varepsilon_d)}V_{pd\sigma}^2\notag\\
F(E)&=&
\bigg[E-\varepsilon_p-\frac{1}{4(E-\varepsilon_p)}\bigg]^{-1},
\end{eqnarray}
where $\varepsilon_c$ is the bottom of the conductance band, and $\varepsilon_{p,d}$ is the on-site energy of $p$ and $d$ orbitals.

\subsection{Bloch Hamiltonian}
The Bloch Hamiltonian for the three band nearest neighbor Hamiltonian expands the one Ref.~\onlinecite{Liu2013} by including the crossing in the conduction band due to additional KM term. 

\begin{eqnarray}
H&=&H_0\sigma_0\tau_3L_0 + \lambda \sigma_3 \tau_0 L_3
+(V_1\tau_3\sigma_1+V_2\tau_0\sigma_2)L_0
+\Delta \sigma_2\tau_2 L_0\notag\\
&&+2\beta_{so}[\sin(2\alpha)-2\sin(\alpha)\cos(\beta)]\sigma_3\tau_0L_{KM}.
\end{eqnarray}
In orbital space we have matrices $L$:
\begin{equation}
L_0=\bm 1_{3\times 3},\quad L_3=\pmat{0&0&0\\0&0&i\\0&-i&0},
\quad L_{KM}=\textrm{diag}(1, 0, 0).
\end{equation}
Since we want the model valid in $\pm\bm K$ valley in the conduction band near the crossings, we are not interested in contribution of $p$ orbitals to other $d$ orbitals, which also results in KM type terms.

\begin{equation}
H_0=\pmat{h_0 & h_1 & h_2\\
h_1^* & h_{11} & h_{12} \\
h_2^* & h_{12}^* & h_{22}
},
\end{equation}
with the following elements:
\begin{align}
h_0 &= 2t_0 (2\cos \alpha\cos\beta+\cos2 \alpha)+\varepsilon_1
-\mu
\notag\\
h_1 &=2 i t_1 (\sin 2\alpha +\sin\alpha\cos\beta)
-2\sqrt{3}t_2\sin\alpha\sin\beta,\notag\\
h_2&=2 i \sqrt3 t_1 \cos\alpha\sin\beta +
2t_2(\cos 2\alpha-\cos\alpha\cos\beta),\notag\\
h_{11}&=t_{11} (\cos\alpha\cos\beta+2 \cos2 \alpha)
+3 t_{22} \cos\alpha \cos\beta+\varepsilon_2-\mu,\notag\\
h_{22}&=3t_{11} \cos\alpha\cos\beta
+t_{22}(\cos\alpha\cos\beta +2\cos2\alpha )+\varepsilon_2-\mu,
\notag\\
h_{12}&=\sqrt3 (t_{22}-t_{11})\sin\alpha\sin\beta
+4 i t_{12} \sin\alpha(\cos\alpha-\cos\beta),
\end{align}
with $\alpha=k_xa/2$ and $\beta=\sqrt 3 k_ya/2$.


\section{k.p Hamiltonian at $\pm K$}
A $\bm{k\cdot p}$ Hamiltonian is obtained in \href{http://journals.aps.org/prb/abstract/10.1103/PhysRevB.92.205108}{Phys.~Rev.~B \textbf{92} 205108} from first principle calculations.

Perturbation theory uncovers a reduced conduction band Hamiltonian $H_c$:
\begin{equation}\label{kp}
H_c=f_0+k^2(f_2+f_3)+f_6\eta s
+\frac{f_1^2k^2+2\eta f_1f_4k_1(k_1^2-3k_2^2)}
{f_0+(f_6-f_5)\eta s},
\end{equation}
where $\eta=\pm$ is valley d.o.f. and $s=\pm$ spin.

Neglecting the cubic correction to the kinetic term, it follows a continuum Hamiltonian similar to Ref.~\onlinecite{Wang2014}:
\begin{equation}\label{lin}
H_\eta=(\frac{\hbar^2 k^2}{2m}-\mu)s_0
+[\beta_{so}\eta+A_1 k^2\eta+A_2(k_1^3-3k_1k_2^2)]s_3,
\end{equation}

The parameters in the continuum Hamiltonian are related to the $f$ parameters:
\begin{align}
\mu&=-f_0+\tilde\mu&
\frac{\hbar^2}{2m}&= a^2\bigg(f_2+f_3+\frac{f_0f_1^2}{f_0^2-(f_5-f_6)^2}\bigg) & 
\beta_{so}&=f_6\\
A_1&=a^2\frac{f_1^2(f_5-f_6)}{f_0^2-(f_5-f_6)^2} & 
A_2&=2a^3\frac{f_1f_4(f_5-f_6)}{f_0^2-(f_5-f_6)^2},
\end{align}
with $a$ the lattice constant in \ce{MoS2}.
The constant $\tilde\mu$ can be tuned.

Adding superconductivity and magnetic field in $x$ direction gives the Hamiltonian:
\begin{equation}
H_{sc}^\eta=(\frac{\hbar^2 k^2}{2m}-\mu)s_0\tau_3
+[\beta_{so}\eta
+A_1 k^2\eta+A_2(k_1^3-3k_1k_2^2)]s_3\tau_0+V_1s_1\tau_3
+\Delta s_2\tau_2.
\end{equation}

\section{Different edge directions}
\subsection{Geometry of BZ}

Let us take an edge given by a lattice vector:
\begin{equation}
\bm T=m\bm a_1+n \bm a_2,
\end{equation}
where $m$ and $n$ are coprime integers, and
\begin{equation}
\bm a_1=(1,0)a,\quad \bm a_2=(1,\sqrt 3)a/2.
\end{equation}
with $a$ the distance between adjacent Mo atoms.

The angle $\varphi$ between the $\bm T$ and the armchair direction $(m=-1, n=2)$ reads 
\begin{equation}
\tan\varphi = (1+2m/n)/\sqrt 3.
\end{equation}

Then we can define a new Brillouin zone with $k_\parallel$ along the edge direction and $k_\perp$, normal to it:
\begin{eqnarray}
k_\perp&\in& \frac{2\pi}{\sqrt 3 a}
(-\sqrt{m^2+n^2+mn},\sqrt{m^2+n^2+mn})\\
k_\parallel&\in&
\frac{\pi}{a}[-(m^2+n^2+mn)^{-1/2},(m^2+n^2+mn)^{-1/2}].
\end{eqnarray}

The $k_1$ and $k_2$ depend on $k_\parallel$ and $k_\perp$
\begin{equation}
k_1=v k_\parallel+w k_\perp,\quad
k_2=w k_\parallel-v k_\perp,
\end{equation}
with
\begin{equation}
v=\frac{m+n/2}{\sqrt{m^2+n^2+mn}},\quad
w=\frac{n\sqrt{3}/2}{\sqrt{m^2+n^2+mn}}.
\end{equation}
To compute the winding number integrate now over $k_\perp$.

The above generic procedure can be compared with the usual way where we form an edge Hamiltonian in an enlarged periodic primitive cell.
The usual way is cumbersome to treat all possible edges. We focus here only on the armchair edge.

In this case we have the edge parallel to $y$ direction and the Brillouin zone can be chosen:
\begin{equation}
k_1\in (-\pi, \pi],\quad k_2\in (-\pi, \pi]
,
\end{equation}
where $k_1=k_xa$ and $k_2=\sqrt 3 k_y a$.
For the case of the three-band Hamiltonian we have the following effective kinetic $H_k$ in the orbital part:
\begin{equation}
H_k=\pmat{H_0 & B^\dag e^{-i(k_1+k_2)/2} \\ B e^{i(k_1+k_2)/2} & H_0},
\end{equation}
with
\begin{equation}
H_0=\pmat{2t_0\cos(k_1)+\varepsilon_1-\mu & 2it_1\sin(k_1) & 2t_2\cos(k_1) \\
-2it_1\sin(k_1) & 2t_{11}\cos(k_1)+\varepsilon_2-\mu & 2it_{12}\sin(k_1) \\
2t_2\cos(k_1) & -2it_{12}\sin(k_1) & 2t_{22}\cos(k_1)+\varepsilon_2-\mu
}.
\end{equation}
The hopping matrix has elements:
\begin{eqnarray}
B_{11}&=& 4t_0\cos(k_1/2)\cos(k_2/2),\notag\\
B_{12}&=& 2\sin(k_1/2)
[it_1\cos(k_2/2)-\sqrt 3 t_2\sin(k_2/2)]\notag\\
B_{13}&=& 2\cos(k_1/2)
[i\sqrt 3 t_1 \sin(k_2/2)-t_2\cos(k_2/2)]\notag\\
B_{22}&=& (t_{11}+3t_{22})\cos(k_1/2)\cos(k_2/2)
\notag\\
B_{23}&=& \sin(k_1/2)
[\sqrt 3(t_{22}-t_{11})\sin(k_2/2)-4it_{12}\cos(k_2/2)],\notag\\
B_{33}&=& (3t_{11}+t_{22})\cos(k_1/2)\cos(k_2/2),
\end{eqnarray}
and the rest of the elements determined by the relations:
\begin{equation}
B_{21}=B_{12}^*,\quad
B_{31}=B_{13}^*,\quad
B_{32}=B_{23}^*.
\end{equation}

The Kane-Mele spin-orbit term contribution to only the $d_{z^2}$ orbital reads:
\begin{equation}
H_{KM}=2\beta_{so}\pmat{\sin(k_1) & * \\
-2e^{i(k_1+k_2)/2}\sin(k_1/2)\cos(k_2/2) & \sin(k_1)
}.
\end{equation}
The rest of the terms (Zeeman energy, atomistic SOC and SC pairing) will only affect onsite terms and are therefore trivial in the enlarged cell.

\section{Topological invariant}
From $\bm k\cdot\bm p$ Hamiltonian~\eqref{kp} we obtain the topological phases of the model.

The eigenvalues of the Hamiltonian are
\begin{equation}\label{eval}
E_\pm^2=\xi_k^2+\lambda_\eta^2+\Delta^2+V^2
\pm 2\sqrt{\xi_k^2\lambda_\eta^2+\xi_k^2V^2+\Delta^2V^2},
\end{equation}
with valley $\eta$ dependent SOI
\begin{equation}
\lambda_\eta=\eta\beta
+\eta A_1(k_1^2+k_2^2)
+A_2(k_1^3-3k_1k_2^2),
\end{equation}
and kinetic term
\begin{equation}
\xi_k=t(k_1^2+k_2^2)-\mu.
\end{equation}

Since we work at fixed $k_2$ for armchair edges, we see that information in the two valleys is redundant.
Due to the symmetry of the Hamiltonian an energy state at $k_1$ in + valley, has a partner at $-k_1$ in $-$ valley.
We will show that both of these are sources or sinks with the same winding number.

We also see that any result in one valley can be recovered for the other valley under the change $A_1\to -A_1$ and $\beta\to -\beta$. WLOG for the next part we work in + valley.

The nodal points at fixed $k_1$ occur, SOI term vanishes.
This gives us three solutions for $k_1$:
\begin{equation}
k_1\in\{q_a, q_b, q_c\}.
\end{equation}
For our set of parameters derived from first principle calculation, one $q_c$ solution occurs at unphysically large momenta, while the other two occur near the $\pm \bm K$ points, and are almost equal in magnitude, but opposite sign.

Using Vieta's formulas allows us to estimate
\begin{equation}
q_c\approx -\frac{A_1}{A_2}\approx 7.3 a^{-1}\gg |q_a|,|q_b|.
\end{equation}
Similarly we obtain immediately approximate physical solution relevant at low energy
\begin{equation}\label{p}
p=q_aq_b=k_2^2+\beta/A_1,
\end{equation}
and
\begin{equation}
s=q_a+q_b=\frac{A_2}{A_1}(4k_2^2+\frac{\beta}{A_1}).
\end{equation}

For a solution $q_a$ and $q_b$ to be nodal points, eigenvalues~\eqref{eval} have to be zero, which determines the boundaries of possible topological phases:
\begin{equation}
E_\pm^2=(\sqrt{\xi_k^2-\Delta^2}\pm V)^2=0,
\end{equation}
or
\begin{equation}\label{mu_crit}
\boxed{
\mu_j=t(q_j^2+k_2^2)\pm \sqrt{V^2-\Delta^2},\quad j\in\{a,b\}
}.
\end{equation}

The system has a chiral symmetry $C=\sigma_1\tau_2$ which allows us to off-diagonalize the Hamiltonian.

This yields 
\begin{equation}
U^\dag HU
=
\begin{pmatrix}
0 & A\\A^\dag & 0
\end{pmatrix},
\end{equation}
with $U$ the eigenvectors of the chiral operator:
\begin{equation}
U=\begin{pmatrix}
-i&0&i&0\\0&i&0&-i\\0&1&0&1\\1&0&1&0.
\end{pmatrix}
\end{equation}

\begin{equation}
\det A = \lambda_\eta^2-\xi_k^2+V^2-\Delta^2
+i\Delta\lambda_\eta.
\end{equation}
WLOG $\Delta>0$.

The winding number of the Hamiltonian is given at physical solutions $q_a$ and $q_b$:
\begin{equation}
w =\frac{1}{2}\sum_{q=q_a, q_b} \text{sgn}(\partial_{k_1}\lambda_\eta)(\lambda_\eta^2
-\xi_k^2+V^2-\Delta^2)\big|_{q}.
\end{equation} 

Since the winding number changes only when parameters pass over one of the critical lines~\eqref{mu_crit}, it is enough to pick a single point in the phase diagram and compute $w$.

Also notice that we can treat separately $a$ and $b$ points.
Let us pick for example $q_a$ case.

Simplest case is at $V^2-\Delta^2=\varepsilon>0$ ($\varepsilon\ll 1$) and the chemical potential in the middle of each respective region,
\begin{equation}
\mu_j=t(k_2^2+q^2_j).
\end{equation}
In this case
\begin{equation}
w_a=\frac{1}{2}\text{sgn}[2 A_1 q_a+3A_2(q_a^2-k_2^2)].
\end{equation}
Further simplification:
\begin{equation}
w_a=\frac{1}{2}\text{sgn}(q_a A_1)
\text{sgn}[2 + \frac{3A_2(q_a^2-k_2^2)}{A_1 q_a}].
\end{equation}
Using the estimate for $q_c$ we obtain that the last signum function is always positive since:
\begin{equation}
2 + \frac{3A_2(q_a^2-k_2^2)}{A_1 q_a}
\approx 2-3\bigg(
\frac{q_a}{q_c}-\frac{k_2^2}{q_aq_c}
\bigg)>1,
\end{equation}
since $|q_c|\gg |k_2|, |q_a|, |q_b|$.

This shows that:
\begin{equation}
\boxed{
w_j=\frac{1}{2}\text{sgn}{(q_j A_1)}
}.
\end{equation}
From the middle of the region we can vary the chemical potential until we pass over the nodal lines in \eqref{mu_crit}.
Then $w_j$ changes by $1$ when passing over the line due to the term:
\begin{equation}
\text{sgn}(V^2-\Delta^2-\xi_k^2).
\end{equation}

In + valley, topological numbers exist only if indeed $q_a$ and $q_b$ have different signs.
Therefore from Eq.~\eqref{p} we deduce that topological phases exist only in a finite region, up to
\begin{equation}
\boxed{
|k_2|<\sqrt{-\frac{\beta}{A_1}}
}
\end{equation}

Also notice that if $q_a$=$-q_b$ this gives that both nodal points occur at the same energy and annihilate when projected on the edge.
This gives another easy to check condition. There are no topological phases at:
\begin{equation}
|k_2|=\frac{1}{2}\sqrt{-\frac{\beta}{A_1}}.
\end{equation}

In the opposite valley $A_1\to -A_1$, but also $q_a\to-q_a$.
This shows that the nodal points at opposite valley occur at the same energy and are of the same character (either sink or source).
Therefore the winding number only adds in the two valleys.

These explain all the properties of the topological phases.
The width of the topological region in $(\mu, \sqrt{V^2-\Delta^2})$ space is simply:
\begin{equation}
width =t|q_a^2-q_b^2|.
\end{equation}
The topological phase has a $W$ shape.


\bibliographystyle{apsrev4-1}
\bibliography{bibl}
\end{document}