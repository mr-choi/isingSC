from types import SimpleNamespace
import tinyarray as ta
import numpy as np
import scipy
import scipy.sparse.linalg as sla
from scipy.optimize import fmin
import kwant

# parameters of the model

#LDA set
#c = SimpleNamespace(t0=-0.218, t1=0.444, t2=0.533,
#        t11=0.250, t12=0.360, t22=0.047, e1=1.238, e2=2.366)

#GGA set (onsite energies shifted to coincide with k.p in Fang paper)
c = SimpleNamespace(t0=-0.184, t1=0.401, t2=0.507,
        t11=0.218, t12=0.338, t22=0.057, e1=1.046+0.0755, e2=2.104+0.0755)

s0 = ta.array([[1,0], [0,1]])
s1 = ta.array([[0,1], [1,0]])
s2 = ta.array([[0, -1j], [1j, 0]])
s3 = ta.array([[1,0], [0, -1]])

def ham_sm(alpha, beta, p, c=c):
    """Hamiltonian in absence of SC
    d.o.f: (orb, spin)
    """
    mat = ta.array([[2*c.t0*(np.cos(2*alpha)+2*np.cos(alpha)*np.cos(beta))+c.e1-p.mu,
        2*1j*c.t1*(np.sin(2*alpha)+np.sin(alpha)*np.cos(beta)) \
        -2*np.sqrt(3)*c.t2*np.sin(alpha)*np.sin(beta),
        2*1j*np.sqrt(3)*c.t1*np.cos(alpha)*np.sin(beta) \
        +2*c.t2*(np.cos(2*alpha)-np.cos(alpha)*np.cos(beta))],
        \
        [-2*1j*c.t1*(np.sin(2*alpha)+np.sin(alpha)*np.cos(beta)) \
        -2*np.sqrt(3)*c.t2*np.sin(alpha)*np.sin(beta),
        c.t11*(np.cos(alpha)*np.cos(beta)+2*np.cos(2*alpha)) \
        +3*c.t22*np.cos(alpha)*np.cos(beta)+c.e2-p.mu,
        np.sqrt(3)*(c.t22-c.t11)*np.sin(alpha)*np.sin(beta) \
        +4*1j*c.t12*np.sin(alpha)*(np.cos(alpha)-np.cos(beta))],
        \
        [-2*1j*np.sqrt(3)*c.t1*np.cos(alpha)*np.sin(beta) \
        +2*c.t2*(np.cos(2*alpha)-np.cos(alpha)*np.cos(beta)),
        np.sqrt(3)*(c.t22-c.t11)*np.sin(alpha)*np.sin(beta) \
        -4*1j*c.t12*np.sin(alpha)*(np.cos(alpha)-np.cos(beta)),
        3*c.t11*np.cos(alpha)*np.cos(beta) \
        +c.t22*(np.cos(alpha)*np.cos(beta)+2*np.cos(2*alpha))+c.e2-p.mu]
        ])
    
    ham0 = np.kron(mat, s0)
    zeeman = np.kron(np.eye(3), s1*p.B*np.cos(p.theta)+s2*p.B*np.sin(p.theta))
    so = p.lam * np.kron(np.array([[0, 0, 0], [0, 0, 1j], [0, -1j, 0]]), s3)
    
    #split of conductance band due to K-M SOC.
    # split = 2*p.bso*(np.sin(2*alpha)-2*np.sin(alpha)*np.cos(beta))*np.kron(np.eye(3), s3)
    split = 2*p.bso*(np.sin(2*alpha)-2*np.sin(alpha)*np.cos(beta)) * \
            np.kron(np.array([[1, 0, 0], [0, 0, 0], [0, 0, 0]]), s3)
    # all corrections
    return ham0+zeeman+so+split

def ham_sc(alpha, beta, p, c=c):
    """BdG Hamiltonian 
    d.o.f.: (orb, spin, ph)
    """
    mat = ta.array([[2*c.t0*(np.cos(2*alpha)+2*np.cos(alpha)*np.cos(beta))+c.e1-p.mu,
        2*1j*c.t1*(np.sin(2*alpha)+np.sin(alpha)*np.cos(beta)) \
        -2*np.sqrt(3)*c.t2*np.sin(alpha)*np.sin(beta),
        2*1j*np.sqrt(3)*c.t1*np.cos(alpha)*np.sin(beta) \
        +2*c.t2*(np.cos(2*alpha)-np.cos(alpha)*np.cos(beta))],
        \
        [-2*1j*c.t1*(np.sin(2*alpha)+np.sin(alpha)*np.cos(beta)) \
        -2*np.sqrt(3)*c.t2*np.sin(alpha)*np.sin(beta),
        c.t11*(np.cos(alpha)*np.cos(beta)+2*np.cos(2*alpha)) \
        +3*c.t22*np.cos(alpha)*np.cos(beta)+c.e2-p.mu,
        np.sqrt(3)*(c.t22-c.t11)*np.sin(alpha)*np.sin(beta) \
        +4*1j*c.t12*np.sin(alpha)*(np.cos(alpha)-np.cos(beta))],
        \
        [-2*1j*np.sqrt(3)*c.t1*np.cos(alpha)*np.sin(beta) \
        +2*c.t2*(np.cos(2*alpha)-np.cos(alpha)*np.cos(beta)),
        np.sqrt(3)*(c.t22-c.t11)*np.sin(alpha)*np.sin(beta) \
        -4*1j*c.t12*np.sin(alpha)*(np.cos(alpha)-np.cos(beta)),
        3*c.t11*np.cos(alpha)*np.cos(beta) \
        +c.t22*(np.cos(alpha)*np.cos(beta)+2*np.cos(2*alpha))+c.e2-p.mu]
        ])
    
    ham0 = np.kron(np.kron(mat, s0), s3)
    zeeman = p.B*np.cos(p.theta)*np.kron(np.kron(np.eye(3), s1), s3) \
        + p.B*np.sin(p.theta)*np.kron(np.kron(np.eye(3), s2), s0)
    so = p.lam*np.kron(np.kron(np.array([[0, 0, 0], [0, 0, 1j], [0, -1j, 0]]), s3), s0)
    sc = p.delta*np.kron(np.kron(np.eye(3), s2), s2)
    
    # split of conductance band due to K-M SOC.
    # split = 2*p.bso*(np.sin(2*alpha)-2*np.sin(alpha)*np.cos(beta))*np.kron(np.kron(np.eye(3), s3), s0)
    split = 2*p.bso*(np.sin(2*alpha)-2*np.sin(alpha)*np.cos(beta)) * \
        np.kron(np.kron(np.array([[1, 0, 0], [0, 0, 0], [0, 0, 0]]), s3), s0)
    # all corrections
#     h = ham0+zeeman+so+split+sc
#     print(np.linalg.norm(np.conj(np.transpose(h))-h))
#    print(ham0.shape, zeeman.shape, so.shape, split.shape, sc.shape)
    return ham0+zeeman+so+split+sc

def spectrum(alpha, beta, p, sc=False):
    """Returns eigenvalues of the Hamiltonian
    """
    if sc: return np.linalg.eigvalsh(ham_sc(alpha, beta, p))
    return np.linalg.eigvalsh(ham_sm(alpha, beta, p))

def wf(alpha, beta, p, sc=False):
    """Returns eigenvalues and eigenvectors
    """
    if sc: return np.linalg.eigh(ham_sc(alpha, beta, p))
    return np.linalg.eigh(ham_sm(alpha, beta, p))

def polz(alpha, beta, p, band=2.):
    """z-spin polarization at k=(alpha, beta) point in the band 'band')
    band : int
        Defaults to the conduction band = 2.
    """
    Sz = np.kron(np.eye(3), s3)
    evals, evecs = wf(alpha, beta, sc=False)
    return evals[band], np.diag(np.transpose(np.conj(evecs)) @ Sz @ evecs)[band]

def detA(alpha, beta, p):
    """
    Returns the determinant of the off-diagonal block of chiral transformed BdG Hamiltonian
    (orb, spin, ph) d.o.f.
    """
    evals, evecs = np.linalg.eigh(np.kron(np.kron(np.eye(3), s1), s2))
#     det1 = np.linalg.det((evecs.conj().transpose() @ ham_sc(alpha, beta, p) @ evecs)[:6, 6:])
#     det2 = np.linalg.det((evecs.conj().transpose() @ ham_sc(-alpha, -beta, p) @ evecs)[:6, 6:])
#     assert np.allclose(det1, det2.conj())
    return np.linalg.det((evecs.conj().transpose() @ ham_sc(alpha, beta, p) @ evecs)[:6, 6:])

def detA_alt(alpha, beta, p, c=c):
    """
    Returns the normalized determinant of the off-diagonal block of chiral transformed BdG Hamiltonian
    """
    h0 = 2*c.t0*(np.cos(2*alpha)+2*np.cos(alpha)*np.cos(beta))+c.e1-p.mu
    h1 = 2j*c.t1*(np.sin(2*alpha)+np.sin(alpha)*np.cos(beta))-2*np.sqrt(3)*c.t2*np.sin(alpha)*np.sin(beta)
    h2 = 2j*np.sqrt(3)*c.t1*np.cos(alpha)*np.sin(beta)+2*c.t2*(np.cos(2*alpha)-np.cos(alpha)*np.cos(beta))
    h11 = c.t11*(np.cos(alpha)*np.cos(beta)+2*np.cos(2*alpha))+3*c.t22*np.cos(alpha)*np.cos(beta)+c.e2-p.mu
    h12 = np.sqrt(3)*(c.t22-c.t11)*np.sin(alpha)*np.sin(beta)+4j*c.t12*np.sin(alpha)*(np.cos(alpha)-np.cos(beta))
    h22 = 3*c.t11*np.cos(alpha)*np.cos(beta)+c.t22*(np.cos(alpha)*np.cos(beta)+2*np.cos(2*alpha))+c.e2-p.mu
    # kane-mele SOC
    sokm = 2*p.bso*(np.sin(2*alpha)-2*np.sin(alpha)*np.cos(beta))
    det = np.linalg.det(np.array([
        [p.B, -h0-sokm-1j*p.delta, 0, -h1, 0, -h2],
        [0., -np.conj(h1), p.B, -h11-1j*p.delta, 0, -h12-1j*p.lam],
        [h0-sokm-1j*p.delta, -p.B, h1, 0, h2, 0],
        [np.conj(h2), 0, 1j*p.lam+np.conj(h12), 0, h22-1j*p.delta, -p.B],
        [np.conj(h1), 0, h11-1j*p.delta, -p.B, h12-1j*p.lam, 0],
        [0, -np.conj(h2), 0, 1j*p.lam-np.conj(h12), p.B, -h22-1j*p.delta]
        ]) )
    return det/np.abs(det)

def crossings(p, beta, amin=-0.2, amax=0.2, na = 5000):
    """Obtains crossing near the K point in the 3 band model (zero magnetic field and mu=0).
    """
    K = (2*np.pi/3., 0)
    amin = K[0]+amin
    amax = K[0]+amax
    alphas = np.linspace(amin, amax, na)
    p.B = 0.
    p.mu = 0.
    
    # the split conduction bands are 2, 3
    ergs = np.asarray([spectrum(alpha, beta, p, sc=False) for alpha in alphas]).reshape((na, 6))

    # conduction bands
    c1 = ergs[:, 2]
    c2 = ergs[:, 3]

    try:
        assert np.min(c2-c1) < 1e-4
    except:
        print('There are no crossing for this choice of parameters or momentum space window.')
        return 0

    # identifying the crossings
    cross1 = np.argwhere((c2-c1) == np.min((c2-c1)[alphas < K[0]]))
    cross2 = np.argwhere((c2-c1) == np.min((c2-c1)[alphas > K[0]]))
    
    # print(c2[cross1], c2[cross2])
    # energy window for topological nontrivial phases
    # print(np.abs(c2[cross1]-c2[cross2]))
    return float(c2[cross2]), float(c2[cross1])

def min_gap(p, beta, a_min=-0.2, a_max=0.2, a_points=5000, **kwargs):
    """
    Finds minimum gap (in units of SC gap) at a given set of parameters.
    sqv = \sqrt{V^2-\Delta^2}
    """
    p.__dict__.update(kwargs)
    
    def lowest(alpha, beta, p):
        """Returns conduction band energy.
        """
        alpha = float(alpha)
        return spectrum(alpha, beta, p, sc=True)[6]

    # search near K point
    K = (2*np.pi/3., 0.)
    
    alphas = np.linspace(a_min+K[0], a_max+K[0], a_points)
        
    # lowest excited energies
    energy_tb = np.asarray([lowest(alpha, beta, p) for alpha in alphas])
    idx = np.argwhere(np.diff(np.sign(np.gradient(energy_tb))) != 0)

    a_mins = [fmin(lowest, a0, args=(beta, p), disp=False, xtol=1e-13, ftol=1e-13) for a0 in alphas[idx].ravel()]
    return np.min([lowest(alpha, beta, p)/p.delta for alpha in a_mins])

def winding(p, beta, c=c, **kwargs):
    """Returns the winding number of the 3 band model with armchair edges.
    """
    p.__dict__.update(kwargs)
    A0 = np.array([
        [p.B, -c.e1+p.mu-1j*p.delta, 0, 0, 0, 0],
        [0, 0, p.B, -c.e2+p.mu-1j*p.delta, 0, -1j*p.lam],
        [c.e1-p.mu-1j*p.delta, -p.B, 0, 0, 0, 0],
        [0, 0, 1j*p.lam, 0, c.e2-p.mu-1j*p.delta, -p.B],
        [0, 0, c.e2-p.mu-1j*p.delta, -p.B, -1j*p.lam, 0],
        [0, 0, 0, 1j*p.lam, p.B, -c.e2+p.mu-1j*p.delta] ])
    
    A1 = np.array([
      [0, -2j*(p.bso-1j*c.t0)*np.cos(beta), 0, -c.t1*np.cos(beta)-1j*np.sqrt(3)*c.t2*np.sin(beta), 0, 
       c.t2*np.cos(beta)-1j*np.sqrt(3)*c.t1*np.sin(beta)],
      [0, c.t1*np.cos(beta)-1j*np.sqrt(3)*c.t2*np.sin(beta), 0, -0.5*(c.t11+3*c.t22)*np.cos(beta), 0, 
       2*c.t12*np.cos(beta)-0.5j*np.sqrt(3)*(c.t11-c.t22)*np.sin(beta)],
      [2*(-1j*p.bso+c.t0)*np.cos(beta), 0, c.t1*np.cos(beta)+1j*np.sqrt(3)*c.t2*np.sin(beta), 0,
       -c.t2*np.cos(beta)+1j*np.sqrt(3)*c.t1*np.sin(beta), 0],
      [-c.t2*np.cos(beta)-1j*np.sqrt(3)*c.t1*np.sin(beta), 0,
       2*c.t12*np.cos(beta)+0.5j*np.sqrt(3)*(c.t11-c.t22)*np.sin(beta), 0, 0.5*(3*c.t11+c.t22)*np.cos(beta), 0],
      [-c.t1*np.cos(beta)+1j*np.sqrt(3)*c.t2*np.sin(beta), 0, 0.5*(c.t11+3*c.t22)*np.cos(beta), 0,
       -2*c.t12*np.cos(beta)+0.5j*np.sqrt(3)*(c.t11-c.t22)*np.sin(beta), 0],
      [0, c.t2*np.cos(beta)+1j*np.sqrt(3)*c.t1*np.sin(beta), 0,
       -2*c.t12*np.cos(beta)-0.5j*np.sqrt(3)*(c.t11-c.t22)*np.sin(beta), 0, -0.5*(3*c.t11+c.t22)*np.cos(beta)]])

    Am1 = np.array([
      [0, 2j*(p.bso+1j*c.t0)*np.cos(beta), 0, c.t1*np.cos(beta)+1j*np.sqrt(3)*c.t2*np.sin(beta), 0, 
       c.t2*np.cos(beta)-1j*np.sqrt(3)*c.t1*np.sin(beta)],
      [0, -c.t1*np.cos(beta)+1j*np.sqrt(3)*c.t2*np.sin(beta), 0, -0.5*(c.t11+3*c.t22)*np.cos(beta), 0, 
       -2*c.t12*np.cos(beta)+0.5j*np.sqrt(3)*(c.t11-c.t22)*np.sin(beta)],
      [2*(1j*p.bso+c.t0)*np.cos(beta), 0, -c.t1*np.cos(beta)-1j*np.sqrt(3)*c.t2*np.sin(beta), 0,
       -c.t2*np.cos(beta)+1j*np.sqrt(3)*c.t1*np.sin(beta), 0],
      [-c.t2*np.cos(beta)-1j*np.sqrt(3)*c.t1*np.sin(beta), 0,
       -2*c.t12*np.cos(beta)-0.5j*np.sqrt(3)*(c.t11-c.t22)*np.sin(beta), 0, 0.5*(3*c.t11+c.t22)*np.cos(beta), 0],
      [c.t1*np.cos(beta)-1j*np.sqrt(3)*c.t2*np.sin(beta), 0, 0.5*(c.t11+3*c.t22)*np.cos(beta), 0,
       2*c.t12*np.cos(beta)-0.5j*np.sqrt(3)*(c.t11-c.t22)*np.sin(beta), 0],
      [0, c.t2*np.cos(beta)+1j*np.sqrt(3)*c.t1*np.sin(beta), 0,
       2*c.t12*np.cos(beta)+0.5j*np.sqrt(3)*(c.t11-c.t22)*np.sin(beta), 0, -0.5*(3*c.t11+c.t22)*np.cos(beta)]])
    
    A2 = np.array([
        [0, 1j*p.bso-c.t0, 0, -c.t1, 0, -c.t2],
        [0, c.t1, 0, -c.t11, 0, -c.t12],
        [1j*p.bso+c.t0, 0, c.t1, 0, c.t2, 0],
        [c.t2, 0, -c.t12, 0, c.t22, 0],
        [-c.t1, 0, c.t11, 0, c.t12, 0],
        [0, -c.t2, 0, c.t12, 0, -c.t22]])

    Am2 = np.array([
        [0, -1j*p.bso-c.t0, 0, c.t1, 0, -c.t2],
        [0, -c.t1, 0, -c.t11, 0, c.t12],
        [-1j*p.bso+c.t0, 0, -c.t1, 0, c.t2, 0],
        [c.t2, 0, c.t12, 0, c.t22, 0],
        [c.t1, 0, c.t11, 0, -c.t12, 0],
        [0, -c.t2, 0, -c.t12, 0, -c.t22]])
    
    Id = np.eye(6)
    O = np.zeros_like(Id)
    # solving the genralized eigenvalue problem U\psi = z W\psi    
    W = np.bmat([[Am1, A0, A1, A2], [Id, O, O, O], [O, Id, O, O], [O, O, Id, O]])
    U = np.bmat([[-Am2, O, O, O], [O, Id, O, O], [O, O, Id, O], [O, O, O, Id]])

    ev = scipy.linalg.eigvals(U, W)
    # winding number (zeros - poles)
    return len(ev[np.abs(ev)<1]) - 12

#--------------------------------------------#
# Routines for generic boundary terminations #
#--------------------------------------------#
def spec_any(kperp, kpar, p):
    m = p.edge[0]
    n = p.edge[1]
    
    u = (m+0.5*n)/np.sqrt(m**2+n**2+m*n)
    w = 0.5*n*np.sqrt(3)/np.sqrt(m**2+n**2+m*n)

    alpha = 0.5*(u*kpar+w*kperp)
    beta = 0.5*np.sqrt(3)*(w*kpar-u*kperp)
    return spectrum(alpha, beta, p, sc=False)

def detA_any(kperp, kpar, p):
    """
    Returns the normalized determinant of the off-diagonal block of chiral transformed hamiltonian for any edge cut.
    """
    m = p.edge[0]
    n = p.edge[1]
    
    u = (m+0.5*n)/np.sqrt(m**2+n**2+m*n)
    w = 0.5*n*np.sqrt(3)/np.sqrt(m**2+n**2+m*n)

    alpha = 0.5*(u*kpar+w*kperp)
    beta = 0.5*np.sqrt(3)*(w*kpar-u*kperp)    
    return detA_alt(alpha, beta, p)

def cross_points(p, kpar, k_val, amin=-0.25, amax=0.25, na=5000, plot=True):
    """Cross points at valley k_val.
    """
    kmin = k_val+amin
    kmax = k_val+amax
    kperps = np.linspace(kmin, kmax, na)
    # the split conduction bands are 2, 3
    ergs = np.asarray([spec_any(kperp, kpar, p) for kperp in kperps]).reshape((na, 6))
    # conduction bands
    c1 = ergs[:, 2]
    c2 = ergs[:, 3]

    # check if crossings exist
    try:
        assert np.min(c2-c1) < 1e-4
    except:
        print('There are no crossing for this choice of parameters or momentum space window.')
        return [np.nan] * 4

    # identifying the index of momenta kperp where the crossings occur
    cross1 = np.argwhere((c2-c1) == np.min((c2-c1)[kperps < k_val]))
    cross2 = np.argwhere((c2-c1) == np.min((c2-c1)[kperps > k_val]))
    
    if plot:
        plt.plot(kperps[cross1], c1[cross1], 'ro')
        plt.plot(kperps[cross2], c2[cross2], 'bo')

    # ensure decreasing order of crossings in the negative BZ
    if k_val < 0: return float(kperps[cross2]), float(c2[cross2]), float(kperps[cross1]), float(c1[cross1])
    return float(kperps[cross1]), float(c1[cross1]), float(kperps[cross2]), float(c2[cross2])

def cross_data(p, kpar, n_val):
    """
    n_val : int
        Number of valleys in half BZ.
    Assumes knowledge of an upper bound for the number of valleys where there are crossings. 
    """
    vl = np.zeros(n_val)
    vl[0] = 1
    for i in range(0, n_val-1):
        vl[i+1] = vl[i]+1+i%2
        
    bz_lim = 2*np.pi/np.sqrt(3)*np.sqrt(p.edge[0]**2+p.edge[1]**2+p.edge[0]*p.edge[1])
    
    vl = 4*np.pi/3*vl
    
    dR = []
    dL = []
    for k_val in vl[vl < bz_lim]:
        dR.append(cross_points(p, kpar, k_val, plot=False))
        dL.append(cross_points(p, kpar, -k_val, plot=False))
    
    # returns crossings in the right BZ and left BZ
    return np.asarray(dR).flatten()[1::2], np.asarray(dL).flatten()[1::2]

def winding_mesh(sqvd, mus, dR, dL):
    """Returns a meshgrid of winding numbers for drawing the phase diagram.
    sqvd : float
        Array of \sqrt(V^2-\Delta^2)
    mus : float
        Array of chemical potentials
    dR : ndarray 
        Crossings in right BZ (aranged in increasing order in momentum).
    dL : ndarray
        Crossings in left BZ (aranged in decreasing order in momentum).
    """
    def swap(a):
        for j in range(len(a)):
            if j%4 == 3: a[j], a[j-1] = a[j-1], a[j]
        return a

    def point(x, y, data):
        """Winding number at (x, y).
        """
        up = data+x
        down = data-x

        idx_up = np.argwhere(y < up)
        idx_down = np.argwhere(y < down)
        
        return np.sum(high[idx_up])+np.sum(low[idx_down])
    
    low = 1 - 2*(np.arange(len(dR)) % 2)
    high = np.roll(low, 1)
    
    dR = swap(dR[~np.isnan(dR)])
    dL = swap(dL[~np.isnan(dL)])
        
    V, M = np.meshgrid(sqvd, mus)
    return np.array([point(x, y, dR)+point(x, y, dL) for (x, y) in zip(V.ravel(), M.ravel())]).reshape(M.shape)

# Adaptative exploration of BZ
def jump(p, kperp1, kperp2, kpar, na = 300):
    """Returns determinants between 2 close points in momentum space such that the distance between the
    determinants is smaller than the desired ds.
    It does not return the determinant(kperp2).
    
    Parameters:
    -----------
    kperp1, kperp2, kpar : float
        kperp2 > kperp1, close points in perpendicular BZ.
    na: int
        Presumtive discretization on the BZ.
    """
    dets = []
    ds=2*np.pi/na
    
    det2 = detA_any(kperp2, kpar, p)
    
    while kperp1 != kperp2:
        trial = kperp2
        det1 = detA_any(kperp1, kpar, p)
        # det2 = detA_any(trial, kpar, p)
        distance = dist(det1, det2)
        while distance > ds:
            trial = 0.5*(kperp1+trial)
            det_trial = detA_any(trial, kpar, p)
            distance = dist(det1, det_trial)
            
        dets.append((np.real(det1), np.imag(det1)))
        kperp1 = trial
    return dets

def winding_adaptative(p, kpar, na=300, **kwargs):
    """Adaptative computation of winding number.
    na : int
        Presumtive discretization on the BZ.
    kpar : float
        Momentum in parallel direction.
    """
    p.__dict__.update(kwargs)
    
    # Brillouin zone
    kperp_lim=2*np.pi/np.sqrt(3)*np.sqrt(p.edge[0]**2+p.edge[1]**2+p.edge[0]*p.edge[1])
    kperps = np.linspace(-kperp_lim, kperp_lim, na, endpoint=True)
    
    data = []
    
    init = kperps[0]
    for kperp in kperps[1:]:
        dets = jump(p, init, kperp, kpar, na=na)
        data.extend(dets)
        init = kperp
        
    # adding the final point
    detFin = detA_any(kperps[-1], kpar, p)
    data.append((np.real(detFin), np.imag(detFin)))
    
    data = np.asarray(data)
    x = data[:, 0]
    y = data[:, 1]
    
    # computing invariant
    idx = np.argwhere(np.diff(np.sign(x)) != 0)
    return -0.5*np.sign(y[idx] * np.diff(x)[idx]).sum()

#-------------------------#
# Finite geometry systems #
#-------------------------#
def make_sys(p, c=c, sys_type='patch'):
    '''Tight-binding finite geometries.
    sys_type options:
        patch : finite system
        infinite : 2d infinite system
        ribbon : ribbon with edges
    d.o.f.: (ph, spin, orbital)
    '''
    def onsite(site, p):
        return p.B*np.cos(p.theta)*np.kron(s3, np.kron(s1, np.eye(3))) \
        + p.B*np.sin(p.theta)*np.kron(s0, np.kron(s2, np.eye(3)))  \
        + p.delta*np.kron(s2, np.kron(s2, np.eye(3))) \
        + np.kron(s0, p.lam*np.kron(s3, np.array([[0, 0, 0], [0, 0, 1j], [0, -1j, 0]]))) \
        + np.kron(s3, np.kron(s0, ta.array([[c.e1-p.mu, 0, 0], [0, c.e2-p.mu, 0], [0, 0, c.e2-p.mu]])))
                         
    def hopR1(site1, site2, p):
        return -1j*p.bso*np.kron(s0, np.kron(s3, ta.array([[1., 0., 0.], [0., 0., 0.], [0., 0., 0.]]))) \
        + np.kron(s3, np.kron(s0, ta.array([[c.t0, c.t1, c.t2], [-c.t1, c.t11, c.t12], [c.t2, -c.t12, c.t22]])))

    def hopR2(site1, site2, p):
        return 1j*p.bso*np.kron(s0, np.kron(s3, ta.array([[1., 0., 0.], [0., 0., 0.], [0., 0., 0.]]))) \
        + np.kron(s3,
            np.kron(s0,ta.array([[c.t0, 0.5*(c.t1-np.sqrt(3)*c.t2), -0.5*(np.sqrt(3)*c.t1+c.t2)],
            [-0.5*(c.t1+np.sqrt(3)*c.t2), 0.25*(c.t11+3*c.t22), 0.25*np.sqrt(3)*(c.t22-c.t11)-c.t12],
            [0.5*(np.sqrt(3)*c.t1-c.t2), 0.25*np.sqrt(3)*(c.t22-c.t11)+c.t12, 0.25*(3*c.t11+c.t22)]])))
                      
    def hopR6(site1, site2, p):        
        return 1j*p.bso*np.kron(s0, np.kron(s3, ta.array([[1., 0., 0.], [0., 0., 0.], [0., 0., 0.]]))) \
        + np.kron(s3,
            np.kron(s0,ta.array([[c.t0, 0.5*(c.t1+np.sqrt(3)*c.t2), 0.5*(np.sqrt(3)*c.t1-c.t2)],
            [0.5*(-c.t1+np.sqrt(3)*c.t2), 0.25*(c.t11+3*c.t22), 0.25*np.sqrt(3)*(c.t11-c.t22)-c.t12],
            [0.5*(-np.sqrt(3)*c.t1-c.t2), 0.25*np.sqrt(3)*(c.t11-c.t22)+c.t12, 0.25*(3*c.t11+c.t22)]])))

    def ribbon(pos):
        if p.edge[1] == 0 : return (-0.5*p.height <= pos[1] < 0.5*p.height)
        return (-0.5*p.width <= pos[0] < 0.5*p.width)

    def patch(pos):
        (x, y) = pos
        if p.edge[1] == 0: return (-0.5*p.width <= x < 0.5*p.width) & (-0.5*p.height <= y < 0.5*p.height)
        return (-0.5*p.height <= y < 0.5*p.height) & \
            ( y*(2*p.edge[0]+p.edge[1])/(np.sqrt(3)*p.edge[1])-0.5*p.width <= x 
                 < y*(2*p.edge[0]+p.edge[1])/(np.sqrt(3)*p.edge[1])+0.5*p.width )
        
    lat = kwant.lattice.triangular()
#     vecs = ta.array([(1, 0), (0.5, 0.5*np.sqrt(3))])
#     lat = kwant.lattice.general(prim_vecs=vecs)

    if sys_type=='patch':
        sys = kwant.Builder()
        sys[lat.shape(patch, (0,0))] = onsite
    elif sys_type=='ribbon':
        sys = kwant.Builder(kwant.TranslationalSymmetry((p.edge[0]+0.5*p.edge[1], 0.5*np.sqrt(3)*p.edge[1])))
        sys[lat.shape(ribbon, (0, 0))] = onsite
    elif sys_type=='infinite':
        sys = kwant.Builder(kwant.TranslationalSymmetry(*lat.prim_vecs))
        sys[lat.shape(lambda pos: True, (0, 0))] = onsite
    else: raise ValueError('System type unrecognized!')
    
    sys[kwant.builder.HoppingKind((1, 0), lat, lat)] = hopR1
    sys[kwant.builder.HoppingKind((0, 1), lat, lat)] = hopR6
    sys[kwant.builder.HoppingKind((1, -1), lat, lat)] = hopR2
    return sys

def make_sys_with_leads(p, c=c):
    """Creates a ribbon-shaped system with leads.
    """
    def onsite(site, p):
        return p.B*np.cos(p.theta)*np.kron(s3, np.kron(s1, np.eye(3))) \
        + p.B*np.sin(p.theta)*np.kron(s0, np.kron(s2, np.eye(3))) \
        + p.delta*np.kron(s2, np.kron(s2, np.eye(3))) \
        + p.lam*np.kron(s0, np.kron(s3, np.array([[0, 0, 0], [0, 0, 1j], [0, -1j, 0]]))) \
        + np.kron(s3, np.kron(s0, ta.array([[c.e1-p.mu, 0, 0], [0, c.e2-p.mu, 0], [0, 0, c.e2-p.mu]])))
                         
    def hopR1(site1, site2, p):
        return -1j*p.bso*np.kron(s0, np.kron(s3, ta.array([[1., 0., 0.], [0., 0., 0.], [0., 0., 0.]]))) \
        + np.kron(s3, np.kron(s0, ta.array([[c.t0, c.t1, c.t2], [-c.t1, c.t11, c.t12], [c.t2, -c.t12, c.t22]])))

    def hopR2(site1, site2, p):
        return 1j*p.bso*np.kron(s0, np.kron(s3, ta.array([[1., 0., 0.], [0., 0., 0.], [0., 0., 0.]]))) \
        + np.kron(s3,
            np.kron(s0, ta.array([[c.t0, 0.5*(c.t1-np.sqrt(3)*c.t2), -0.5*(np.sqrt(3)*c.t1+c.t2)],
            [-0.5*(c.t1+np.sqrt(3)*c.t2), 0.25*(c.t11+3*c.t22), 0.25*np.sqrt(3)*(c.t22-c.t11)-c.t12],
            [0.5*(np.sqrt(3)*c.t1-c.t2), 0.25*np.sqrt(3)*(c.t22-c.t11)+c.t12, 0.25*(3*c.t11+c.t22)]])))
                      
    def hopR6(site1, site2, p):        
        return 1j*p.bso*np.kron(s0, np.kron(s3, ta.array([[1., 0., 0.], [0., 0., 0.], [0., 0., 0.]]))) \
        + np.kron(s3,
            np.kron(s0, ta.array([[c.t0, 0.5*(c.t1+np.sqrt(3)*c.t2), 0.5*(np.sqrt(3)*c.t1-c.t2)],
            [0.5*(-c.t1+np.sqrt(3)*c.t2), 0.25*(c.t11+3*c.t22), 0.25*np.sqrt(3)*(c.t11-c.t22)-c.t12],
            [-0.5*(np.sqrt(3)*c.t1+c.t2), 0.25*np.sqrt(3)*(c.t11-c.t22)+c.t12, 0.25*(3*c.t11+c.t22)]])))
     
    def patch(pos):
        (x, y) = pos
        # zig-zag edges
        if p.edge[1] == 0: return (-0.5*p.width <= x < 0.5*p.width) & (-0.5*p.height <= y < 0.5*p.height)
        else: return (-0.5*p.height <= y < 0.5*p.height) & \
            ( y*(2*p.edge[0]+p.edge[1])/(np.sqrt(3)*p.edge[1])-0.5*p.width <= x 
                 < y*(2*p.edge[0]+p.edge[1])/(np.sqrt(3)*p.edge[1])+0.5*p.width )
    
    def lead_shape(pos):
        (x, y) = pos
        if p.edge[1] == 0: return (-0.5*p.height <= pos[1] < 0.5*p.height)
        else: return (-0.5*p.width <= pos[0] < 0.5*p.width)
        
    lat = kwant.lattice.triangular()
    
    sys = kwant.Builder()
    sys[lat.shape(patch, (0,0))] = onsite
    
    lead = kwant.Builder(kwant.TranslationalSymmetry((p.edge[0]+0.5*p.edge[1], 0.5*np.sqrt(3)*p.edge[1])))
    lead[lat.shape(lead_shape, (0, 0))] = onsite
    
    sys[kwant.builder.HoppingKind((1, 0), lat, lat)] = hopR1
    sys[kwant.builder.HoppingKind((0, 1), lat, lat)] = hopR6
    sys[kwant.builder.HoppingKind((1, -1), lat, lat)] = hopR2
            
    lead[kwant.builder.HoppingKind((1, 0), lat, lat)] = hopR1
    lead[kwant.builder.HoppingKind((0, 1), lat, lat)] = hopR6
    lead[kwant.builder.HoppingKind((1, -1), lat, lat)] = hopR2
    
    sys.attach_lead(lead)
    sys.attach_lead(lead.reversed())
    return sys

#---------------------#
# Auxiliary functions #
#---------------------#
def dist(det1, det2):
    """Distance between two complex numbers.
    """
    return np.sqrt((np.real(det1)-np.real(det2))**2+(np.imag(det1)-np.imag(det2))**2)

def sparse_eigs(ham, n_eigs, n_vec_lanczos, sigma=0):
    """Compute eigenenergies using MUMPS as a sparse solver.

    Parameters:
    ----------
    ham : coo_matrix
        The Hamiltonian of the system in sparse representation..
    n_eigs : int
        The number of energy eigenvalues to be returned.
    n_vec_lanczos : int
        Number of Lanczos vectors used by the sparse solver.
    sigma : float
        Parameter used by the shift-inverted method. See
        documentation of scipy.sparse.linalg.eig

    Returns:
    --------
    A list containing the sorted energy levels. Only positive
    energies are returned.
    """
    class LuInv(sla.LinearOperator):
        def __init__(self, A):
            inst = kwant.linalg.mumps.MUMPSContext()
            inst.factor(A, ordering='metis')
            self.solve = inst.solve
            try:
                super(LuInv, self).__init__(shape=A.shape, dtype=A.dtype,
                                            matvec=self._matvec)
            except TypeError:
                super(LuInv, self).__init__(shape=A.shape, dtype=A.dtype)
    
        def _matvec(self, x):
            return self.solve(x.astype(self.dtype))

    ev, evecs = sla.eigs(ham, k=n_eigs,
                         OPinv=LuInv(ham), sigma=sigma, ncv=n_vec_lanczos)

    energies = list(ev.real)
    return energies, evecs