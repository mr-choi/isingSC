import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import Mod.MoS2_3bnd as H

def plot_spec(p, sc=True):
    """Plots the 3 bnd MoS2 dispersion on high symmetry lines.
    """
    # trajectory
    gam = 200
    m = gam/2.
    last = int(np.sqrt(gam**2-m**2))

    GKx = np.linspace(0, 2*np.pi/3., gam, endpoint=False)
    GKy = np.zeros_like(GKx)
    KMx = np.linspace(2*np.pi/3., np.pi/2., m, endpoint=False)
    KMy = np.linspace(0., np.pi/2., m, endpoint=False)
    MGx = np.linspace(np.pi/2., 0., last)
    MGy = np.linspace(np.pi/2., 0., last)

    alpha = np.concatenate((GKx, KMx, MGx))
    beta = np.concatenate((GKy, KMy, MGy))
    
    res = [H.spectrum(alpha[j], beta[j], p, sc=sc) for j in range(len(alpha))]
        
    plt.plot(res, 'r-', lw=1.5)
    plt.ylabel('$E$ (eV)')
    plt.hlines(0, 0, len(alpha))
    plt.xlim(0, len(alpha))

    plt.xticks((0, gam, gam+m, len(alpha)),('$\Gamma$', '$K$', '$M$', '$\Gamma$'))
    plt.show()
    
def ang(edge):
    """Angle to y direction for edge given by (m,n) in
    basis: a1=(1, 0), a2=(1, \sqrt{3})/2.
    """
    return np.arctan((1+2*edge[0]/edge[1])/np.sqrt(3))

class MidpointNormalize(colors.Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=0):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax)
    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))