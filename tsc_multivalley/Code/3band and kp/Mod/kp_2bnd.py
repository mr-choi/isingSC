import numpy as np
import tinyarray as ta
from scipy.optimize import fmin

s0 = ta.array([[1,0], [0,1]])
s1 = ta.array([[0,1], [1,0]])
s2 = ta.array([[0, -1j],[1j, 0]])
s3 = ta.array([[1,0], [0, -1]])

# f params from Fang paper
f0=1.6735; f1=1.1518; f2=0.0744; f3=-0.0613
f4=-0.078; f5=0.0746; f6=-0.0015

# relation to conduction band Hamiltonian parameters
beta=f6
A1=f1**2*(f5-f6)/(f0**2-(f5-f6)**2)
A2=2*f1*f4*(f5-f6)/(f0**2-(f5-f6)**2)
t=f2+f3+(f0*f1**2)/(f0**2-(f5-f6)**2)

# Hamiltonians near K point
def H_sc(k1, k2,  mu, V, Delta):
    return (t*k1**2+t*k2**2+f0-mu)*np.kron(s0, s3) \
        + (beta+A1*(k1**2+k2**2)+A2*k1*(k1**2-3*k2**2))*np.kron(s3, s0) + V*np.kron(s1, s3) \
        + Delta*np.kron(s2, s2)
        
def H_sm(k1, k2, mu, V, eta=1):
    """Semiconducting Hamiltonian at valley eta = +/-1
    """
    return (t*k1**2+t*k2**2+f0-mu)*s0 + (eta*beta+eta*A1*(k1**2+k2**2)+A2*k1*(k1**2-3*k2**2))*s3 + V*s1

def spectrum(k1, k2, mu, V, Delta=0., eta=1, sc=False):
    if sc: return np.linalg.eigvalsh(H_sc(k1, k2, mu, V, Delta))
    return np.linalg.eigvalsh(H_sm(k1, k2, mu, V, eta=eta))

def th_cross(k2, mu):
    """Vanishing of SOI
    """
    coeff = [A2, A1, -3*k2**2*A2, A1*k2**2+beta]
    roots = np.roots(coeff)
    k1s = roots[np.abs(roots) < 1]
    return t*k1s**2+t*k2**2+f0-mu

def crossings(k2, mu, V=0., amin=-0.4, amax=0.4, na = 1000):
    """Crossings in the normal spectrum near K point
    """
    k1s = np.linspace(amin, amax, na)
    ergs = np.asarray([spectrum(k1, k2, mu, V=V, sc=False) for k1 in k1s]).reshape((na, 2))

    # conduction bands
    c1 = ergs[:, 0]
    c2 = ergs[:, 1]

    # identifying the crossings
    cross1 = np.argwhere((c2-c1) == np.min((c2-c1)[k1s < 0]))
    cross2 = np.argwhere((c2-c1) == np.min((c2-c1)[k1s > 0]))

    try:
        assert np.min(c2-c1) < 1e-4
    except:
        print('There are no crossing for this choice of parameters or momentum space window.')
        return 0

    return float(c1[cross2]), float(c1[cross1])

def lowest(k1, k2, mu, V, Delta):
    """First excited quasiparticle band.
    """
    lam = (beta+A1*(k1**2+k2**2)+A2*k1*(k1**2-3*k2**2))
    xi = t*k1**2+t*k2**2+f0-mu
    return np.sqrt(xi**2+lam**2+Delta**2+V**2-2*np.sqrt(xi**2*lam**2+xi**2*V**2+Delta**2*V**2))

def min_gap(k2, mu, sqv, Delta, k1_min=-0.25, k1_max=0.25, k1_points=5000):
    """
    Finds minimum gap (in units of SC gap) at a given set of parameters.
    sqv = \sqrt{V^2-\Delta^2}
    """
    V = np.sqrt(sqv**2+Delta**2)
    qxs = np.linspace(k1_min, k1_max, k1_points)
    
    energy_kp = np.asarray([lowest(k1, k2, mu, V, Delta) for  k1 in qxs])
    idx = np.argwhere(np.diff(np.sign(np.gradient(energy_kp))) != 0)
    
    kmin = [fmin(lowest, q, args=(k2, mu, V, Delta), disp=False, xtol=1e-13, ftol=1e-13) for q in qxs[idx].ravel()]
    # returns minimum gap in units of Delta
    return np.min([lowest(k1, k2, mu, V, Delta)/Delta for k1 in kmin])

@np.vectorize
def winding(k2, mu, v, eta=1.):
    """Winding number in the eta=+/-1 valley.
    v : float
        \sqrt{V^2-\Delta^2}
    """
    coeff = [A2, eta*A1, -3*A2*k2**2, eta*beta+eta*A1*k2**2]
    roots = np.roots(coeff)
    roots = roots[np.abs(roots) < 1.]
    k1s = roots[np.imag(roots)==0]
    return np.real(-0.5*np.sum(np.sign((2*eta*A1*k1s+3*A2*(k1s**2-k2**2)) * ((t*k1s**2+t*k2**2+f0-mu)**2-v**2))))