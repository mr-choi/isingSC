import numpy as np
import tinyarray as ta

s0 = ta.array([[1,0], [0,1]])
s1 = ta.array([[0,1], [1,0]])
s2 = ta.array([[0, -1j],[1j, 0]])
s3 = ta.array([[1,0], [0, -1]])

# f params from https://link.aps.org/doi/10.1103/PhysRevB.92.205108
f0=1.6735; f1=1.1518; f2=0.0744; f3=-0.0613
f4=-0.078; f5=0.0746; f6=-0.0015

### K.p. Hamiltonian without magnetic field or superconductivity
def H_kp(k1, k2, eta=1.):
    """
    d.o.f. (conductance-valley, spin)
    eta: +/-1
        valley index; defaults to 1.
    """
    return (0.5*f0+f2*(k1**2+k2**2))*np.kron(s0, s0)+(0.5*f0+f3*(k1**2+k2**2))*np.kron(s3, s0) \
        +(eta*f1*k1+f4*(k1**2-k2**2))*np.kron(s1, s0)+(f1*k2-2*f4*eta*k1*k2)*np.kron(s2, s0) \
        +0.5*eta*f5*np.kron(s0-s3, s3)+0.5*eta*f6*np.kron(s0+s3, s3)

def spectrum(k1, k2):
    return np.linalg.eigvalsh(H_kp(k1, k2))