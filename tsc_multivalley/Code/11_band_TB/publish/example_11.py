import kwant
import scipy
import numpy as np
from scipy.linalg import block_diag

from types import SimpleNamespace

import Modules.pauli as pauli
import Modules.TB_parameters as Tpar
import Modules.ribbon_params_11_band as rp
import Modules.Ham_MX2_11_band as Ham
import Modules.functions as func


import pickle
import matplotlib.pyplot as plt


######## Compute the winding number #####################

def winding_number_ky_Ex(t, N, p, U, phase=1.0, half_BZ=True,
                         adaptive=True, eps=0.5e-2,
                         extended_hopping=True):
    """Compute the winding number as a function of k_par and Ex. 
    
    Parameters
    -------------
    t: iterable of two floats
        iterable containing first the value of k_par, and second the value of Ex.
    N: integer
        Number of even discretization intervals for the momentum k_perp.
        For adaptive evaluation, this is the minimum number of k_perp
        intervals used.
    p: SimpleNamespace object
        SimpleNamespace object of Hamiltonian parameters used in the
        computation.
    U: numpy.array
        Unitary that block off-diagonalizes the Hamiltonian.
    phase: complex with norm 1
        Overall phase factor for the integrand in the computation
        of the winding number. Defaults to 1.
    half_BZ: bool
        Whether to restrict the integration to half of the k_perp Brillouin
        zone or not. In general, this only works for an armchair edge.
    adaptive: bool
        Whether to adaptively evaluate k_perp over the Brillouin zone or
        use an even discretization.
    eps: float
        Tolerance for winding number integrand using adaptive evaluation:
        if the integrand changes by less than eps, the k_perp points are
        considered close enough.
    extended_hopping: bool
        Whether or not to include long-range hopping in the tight binding
        model.
    
    Returns
    ---------------
    W: integer
        The winding number at the given parameters.
    """
    ky, Ex = t
    p.Ex = Ex
    return func.winding_number(N, ky, p, U, phase=phase, half_BZ=half_BZ, adaptive=adaptive, eps=eps,
                                  extended_hopping=extended_hopping)[0]



# Example of how to compute the winding number
# Set Hamiltonian parameters
p = SimpleNamespace(mu=1.8337,    # Chemical potential
                    Delta=0.8e-3,  # Superconducting pairing
                    lambda_M=0.0836,  # Spin-orbit strength on M atom
                    lambda_X=0.0556,  # Spin-orbit strength on X atoms
                    Ex=0.0, Ey=0.0,  # Zeeman energies along x and y
                    mm=0, nn=0)     # Edge lattice vector in terms of
                                    # the primitive vectors.

# Calculate for an armchair edge.
mm = 1; nn = 2
# The phase diagram in Fig 4 (b) uses
# mm = 14; nn = 27
p.mm = mm; p.nn = nn

# Chiral symmetry operator
C = Ham.C
# Unitary that block off-diagonalizes the Hamiltonian
U = np.linalg.eigh(C)[1]
phase = 1.0
N = 10000
half_BZ=True
adaptive=True
eps=1.0e-2
extended_hopping=True


# Parameters to sweep over
kys = np.linspace(0, 0.07, 300)*4*np.pi/np.sqrt(3)
sqrts = np.linspace(0, 0.015, 300)

Exs = np.sqrt(sqrts**2 + p.Delta**2)
params = [(ky, Ex) for ky in kys for Ex in Exs]


# Calculate the winding number.
# This calculation will take extremely long to complete
# on a single core!
data = [winding_number_ky_Ex(t, N, p, U, half_BZ=half_BZ,
                             phase=phase, adaptive=adaptive,
                             eps=eps,
                             extended_hopping=extended_hopping)
       for t in params]



######## Compute the gap #####################

def gap_kys_Exs(t, params, R1=2, R2=2, start_Es=(0, 1e-3),
                eps=1e-10, max_iter=200, three_d=False,
                orb_mag=False):
    """ Compute the gap as a function of k_par and Ex.

    Parameters
    -------------
    t: iterable of two floats
        iterable containing first the value of k_par, and second
        the value of Ex.
    params: dictionary
        dictionary of Hamiltonian parameters used in the
        computation.
    R1: integer
        Parameter to scale the size of an armchair unit cell used
        in the computation.
        R1 scales its extent along x.
    R2: integer
        Parameter to scale the size of an armchair unit cell used
        in the computation.
        R1 scales its extent along x.
    start_Es: tuple of two floats
        Lower and upper bounds for the search interval in
        energy for the gap, in eV.
    eps: float
        Tolerance value for the gap finder.
    max_iter: integer
        Maximum number of iterations for the gap finder.
    three_d: bool
        Whether to use a composite X atom in the tight
        binding model (False), or to split the X atoms
        into top and bottom layers (True).
    orb_mag: bool
        Whether to include orbital effects of the in-plane
        magnetic field or not.
    
    Returns
    ----------------
    gap: float
        The topological gap at the chosen parameters
        in units of eV.
        
    Note:
    For example, (R1, R2) = (1, 1) means that the translational unit cell
    of the bulk Hamiltonian which we use to compute the gap
    is chosen as a single armchair unit cell.
    (2, 1) means we double the extent of the bulk unit cell along x,
    but leave the size along y unchanged.
    This feature can be useful, because kwant cannot finalize systems
    with hoppings beyond adjacent translation unit cells.
    """

    k_y, Ex = t
    flead = func.make_AC_lead(R1=R1, R2=R2, plot_sys=False, SC=True, spin=True, finalize=True,
                              orb_mag=orb_mag, three_d=three_d)
    params['k_y'] = k_y
    params['Ex'] = Ex
    # Possible instability at band bottoms. If so, shift
    # ky slightly to avoid it.
    try:
        return func.gap_finder(flead, params, start_Es=start_Es, eps=eps, max_iter=max_iter)
    except:
        params['k_y'] += 1e-8
        return func.gap_finder(flead, params, start_Es=start_Es, eps=eps, max_iter=max_iter)


# Example of how to compute the topological gap.
params = dict(k_y=1e-8, Ex=0.0, Ey=0, Delta=0.8e-3, mu=1.8337, pot=0)
three_d = False
orb_mag = False
eps = 1e-10
max_iter = 200
start_Es = (0, 2*params['Delta'])

R1, R2 = 2, 1
params['R'] = (R1, R2)

# We use a larger bulk unit cell (R1 = 2), so we need to scale
# the ky values to make plots matching ones based on the Bloch
# Hamiltonian
kys = np.linspace(0, 0.07, 300)*4*np.pi
sqrts = np.linspace(0, 0.015, 300)

Exs = np.sqrt(sqrts**2 + params['Delta']**2)
ps = [(ky, Ex) for ky in kys for Ex in Exs]



# Calculate the gap.
# This calculation will take extremely long to complete
# on a single core!
data = [gap_kys_Exs(t, params, R1=R1, R2=R2, start_Es=start_Es,
                    eps=eps, max_iter=max_iter, three_d=three_d,
                    orb_mag=orb_mag)
        for t in ps]


######## Compute the local density of states #####################

def DOS_ky_Es(t, params, eta=5*1e-6, R1=2, R2=2, error=False,
              three_d=False, orb_mag=False):
    """Compute the density of states at the edge of a monolayer half-plane
    as a function of k_par and energy.

    Parameters
    -----------------
    t: iterable of two floats
        Iterable containing first the value of k_par, and second
        the value of energy.
    params: dict
        Dictionary of Hamiltonian parameters that are used in the computation.
    eta: float
        Broadening parameter for the surface Greens function.
    R1: integer
        Parameter to scale the size of an armchair unit cell used
        in the computation.
        R1 scales its extent along x.
    R2: integer
        Parameter to scale the size of an armchair unit cell used
        in the computation.
        R1 scales its extent along x.
    error: bool
        Whether to return an estimate of the error in the density of
        states or not.
    three_d: bool
        Whether to use a composite X atom in the tight
        binding model (False), or to split the X atoms
        into top and bottom layers (True).
    orb_mag: bool
        Whether to include orbital effects of the in-plane
        magnetic field or not.

    Returns
    ------------------
    ldos: float
        The density of states in a translation unit cell of the kwant system
        adjacent to an edge.
    """
    ky, E = t
    flead = func.make_AC_lead(R1=R1, R2=R2, plot_sys=False, SC=True, spin=True, finalize=True,
                           orb_mag=orb_mag, three_d=three_d)
    params['k_y'] = ky
    # Possible instability at band bottoms. If so, shift
    # ky slightly to avoid it.
    try:
        return func.surface_ldos_stable(flead, params, E, eta=eta, error=error)
    except:
        params['k_y'] += 1e-8
        return func.surface_ldos_stable(flead, params, E, eta=eta, error=error)


# Example of how to compute the local density of states.
params = dict(k_y=0, Ex=0, Ey=0, Delta=0.8e-3, mu=1.8337, pot=0)
eta = 1e-6
three_d = True
orb_mag = False
params['eta'] = eta


# Parameters to sweep
kys = np.linspace(-0.06, 0.06, 300)*4*np.pi/np.sqrt(3) * 2*np.sqrt(3)
Es = np.linspace(-0.2*params['Delta'], 0.2*params['Delta'], 200)

ps = [(ky, E) for ky in kys for E in Es]


sqrt = 1.5e-3   # sqrt(Ex^2 - Delta^2), eV
Ex = np.sqrt(sqrt**2 + params['Delta']**2)
params['Ex'] = Ex
# Calculate the density of states.
# This calculation will take extremely long to complete
# on a single core!
data = [DOS_ky_Es(t, params, eta=eta, three_d=three_d,
                  orb_mag=orb_mag)
        for t in ps]

