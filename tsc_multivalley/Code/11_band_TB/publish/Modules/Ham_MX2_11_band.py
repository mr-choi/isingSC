# The Bloch Hamiltonian for MX2 monolayers, from
# Fang et al., PRB 92, 205108 (2015), along with various
# helpful functions.

import numpy as np
import itertools as it

from numpy import cos, sin
from scipy.linalg import block_diag

import Modules.pauli as pauli
from Modules.TB_parameters import TB, delta, epsilon

# Chiral operator
C = np.kron(np.kron(pauli.sy, pauli.sx), np.diag([-1 for _ in range(5)] +
                                                 [1 for _ in range(6)]))
# Mirror symmetry M2, x -> -x
M2 = np.kron(np.kron(pauli.sz, pauli.sx),
             np.diag([-1, 1, 1, -1, 1, 1, -1, 1, 1, -1, 1]))
# 2D particle-hole symmetry, kx, ky -> -kx, -ky
P = np.kron(pauli.sx, np.eye(22))


def energies(kx, ky, p, extended_hopping=True, spin=True, SC=False):
    """Spectrum of the monolayer Bloch Hamiltonian at the momentum (kx, ky),
    with momenta in units of the lattice constant a.

    Parameters:
    -----------------
    kx: float
        x-component of the momentum at which the spectrum is computed.
    ky: float
        y-component of the momentum at which the spectrum is computed.
    p: SimpleNamespace object
        A SimpleNamespace object of Hamiltonian parameters.
    extended_hopping: bool
        If True, include long range hoppings in the tight binding model,
        otherwise not.
    spin: bool
        Whether to include spin or not.
    SC: bool
        Whether to include superconductivity or not. Note that
        superconductivity can only be included if spin is included aswell.

    Returns:
    ------------------
    energies: numpy.array
        The eigenvalues of the Bloch Hamiltonian at the specified momentum.
    """
    Ham = full_Hamiltonian(kx, ky, p, extended_hopping=extended_hopping,
                           spin=spin, SC=SC)
    return np.linalg.eigh(Ham)[0]
    
def eigvecs(kx, ky, p, extended_hopping=True, spin=True, SC=False):
    """Eigenstates of the monolayer Bloch Hamiltonian at the momentum
        (kx, ky), with momenta in units of the lattice constant a.

    Parameters:
    -----------------
    kx: float
        x-component of the momentum.
    ky: float
        y-component of the momentum.
    p: SimpleNamespace object
        A SimpleNamespace object of Hamiltonian parameters.
    extended_hopping: bool
        If True, include long range hoppings in the tight binding model,
        otherwise not.
    spin: bool
        Whether to include spin or not.
    SC: bool
        Whether to include superconductivity or not. Note that
        superconductivity can only be included if spin is included aswell.

    Returns:
    ------------------
    eigvecs: numpy.array
        The eigenstates of the Bloch Hamiltonian at the specified momentum.
    """
    Ham = full_Hamiltonian(kx, ky, p, extended_hopping=extended_hopping,
                           spin=spin, SC=SC)
    return np.linalg.eigh(Ham)[1]

def Ham(kx, ky, extended_hopping=True):
    """Orbital part of the Bloch Hamiltonian for monolayer MX2.

    Parameters:
    -----------------
    kx: float
        x-component of the momentum.
    ky: float
        y-component of the momentum.
    extended_hopping: bool
        If True, include long range hoppings in the tight binding model,
        otherwise not.

    Returns:
    ------------------
    Ham: numpy.array
        The orbital Bloch Hamiltonian at the specified momentum.
    """    

    H = np.zeros((11, 11), dtype=complex)
    k = np.array([kx, ky])
    # Indices
    inds = (list(zip(range(11), range(11))) +
           [(2,4), (5,7), (8,10),
            (0,1), (2,3), (3,4), (5,6), (6,7), (8,9), (9,10),
            (2,0), (4,0), (3,1), (9,5), (8,6), (10,6), (9,7),
            (3,0), (2,1), (4,1), (8,5), (10,5), (9,6), (8,7), (10,7),
            (8,5), (10,5), (9,5), (8,7), (8,6), (9,6), (10,6), (10,7),
            ])
    # Eliminate duplicates
    inds = list(set(inds))
    for i, j in inds:
        H[i, j] += H_matrix_element(i, j, k, extended_hopping=extended_hopping)
        if i != j:
            H[j, i] += H[i, j].conj()
    return H
    

def H_matrix_element(i, j, k, extended_hopping=True):
    """Matrix elements of the orbital Bloch Hamiltonian. """
    
    # Matrix elements of the minimal TB Hamiltonian.
    # Diagonal elements
    ele = 0
    if i == j:
        ele += (epsilon[i] + 2*TB(1, i, i)*cos(k.dot(delta(1))) + 
                2*TB(2, i, i)*(cos(k.dot(delta(2))) + cos(k.dot(delta(3))))
               )
        
    # MM and XX, even under M2
    if (i+1, j+1) in [(3,5), (6,8), (9,11)]:
        ele += (2*TB(1, i, j)*cos(k.dot(delta(1))) +
                TB(2, i, j)*(np.exp(-1j*k.dot(delta(2))) +
                             np.exp(-1j*k.dot(delta(3)))) +
                TB(3, i, j)*(np.exp(1j*k.dot(delta(2))) +
                             np.exp(1j*k.dot(delta(3))))
               )
    
    # MM and XX, odd under M2
    if (i+1, j+1) in [(1,2), (3,4), (4,5), (6,7), (7,8), (9,10), (10,11)]:
        ele += (-2j*TB(1, i, j)*sin(k.dot(delta(1))) + 
                TB(2, i, j)*(np.exp(-1j*k.dot(delta(2))) -
                             np.exp(-1j*k.dot(delta(3)))) +
                TB(3, i, j)*(-np.exp(1j*k.dot(delta(2))) +
                             np.exp(1j*k.dot(delta(3))))
               )

    # MX (and XM), even under M2
    if (i+1, j+1) in [(3,1), (5,1), (4,2), (10,6), (9,7), (11,7), (10,8)]:
        ele += TB(4, i, j)*(np.exp(1j*k.dot(delta(4))) -
                            np.exp(1j*k.dot(delta(6))))

    # MX (and XM), odd under M2
    if (i+1, j+1) in [(4,1), (3,2), (5,2), (9,6), (11,6), (10,7),
                      (9,8), (11,8)]:
        ele += (TB(4, i, j)*(np.exp(1j*k.dot(delta(4))) +
                             np.exp(1j*k.dot(delta(6)))) + 
                TB(5, i, j)*np.exp(1j*k.dot(delta(5)))
               )
    
    # Corrections due to extended hopping, between M and next-nearest X atoms
    if extended_hopping:
        if (i+1, j+1) == (9, 6):
            ele += TB(6, i, j)*(np.exp(1j*k.dot(delta(7))) +
                                np.exp(1j*k.dot(delta(8))) +
                                np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) == (11, 6):
            ele += TB(6, i, j)*(np.exp(1j*k.dot(delta(7))) -
                                0.5*np.exp(1j*k.dot(delta(8))) -
                                0.5*np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) == (10, 6):
            ele += np.sqrt(3.)/2*TB(6, 11-1, 6-1)*(-np.exp(1j*k.dot(delta(8))) +
                                                   np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) == (9, 8):
            ele += TB(6, i, j)*(np.exp(1j*k.dot(delta(7))) -
                                0.5*np.exp(1j*k.dot(delta(8))) -
                                0.5*np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) == (9, 7):
            ele += np.sqrt(3.)/2*TB(6, 9-1, 8-1)*(-np.exp(1j*k.dot(delta(8))) +
                                                  np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) == (10, 7):
            ele += 0.75*TB(6, 11-1, 8-1)*(np.exp(1j*k.dot(delta(8))) +
                                          np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) in [(11,7), (10,8)]:
            ele += np.sqrt(3.)/4*TB(6, 11-1, 8-1)*(np.exp(1j*k.dot(delta(8))) -
                                                   np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) == (11, 8):
            ele += TB(6, i, j)*(np.exp(1j*k.dot(delta(7))) +
                                0.25*np.exp(1j*k.dot(delta(8))) +
                                0.25*np.exp(1j*k.dot(delta(9))))
    return ele

def full_Hamiltonian(kx, ky, p, extended_hopping=True, spin=True, SC=False):
    """ Full Bloch Hamiltonian with spin and superconductivity at the
    momentum (kx, ky), with momenta in units of the lattice constant a.

    Parameters:
    -----------------
    kx: float
        x-component of the momentum.
    ky: float
        y-component of the momentum.
    p: SimpleNamespace object
        A SimpleNamespace object of Hamiltonian parameters.
    extended_hopping: bool
        If True, include long range hoppings in the tight binding model,
        otherwise not.
    spin: bool
        Whether to include spin or not.
    SC: bool
        Whether to include superconductivity or not. Note that
        superconductivity can only be included if spin is included aswell.

    Returns:
    ------------------
    full_Hamiltonian: numpy.array
        The Bloch Hamiltonian at the specified momentum.
    """
    H = Ham(kx, ky, extended_hopping=extended_hopping) - p.mu*np.eye(11)
    if spin:
        H_SOI = SOI_Ham(lambda_M=p.lambda_M, lambda_X=p.lambda_X)
        H_Z = Zeeman_Ham(Ex=p.Ex, Ey=p.Ey)
        H = np.kron(pauli.s0, H) + H_SOI + H_Z
        # For now, only include SC if spin is included as well
        if SC:
            H_neg_k = (Ham(-kx, -ky, extended_hopping=extended_hopping) -
                       p.mu*np.eye(11))
            H_neg_k = np.kron(pauli.s0, H_neg_k) + H_SOI + H_Z
            H = block_diag(H, -H_neg_k.conj()) + SC_coupling(Delta=p.Delta)
    return H

def SOI_Ham(lambda_M = 0.0836, lambda_X = 0.0556):
    """Spin-orbit interaction Hamiltonian for monolayer MX2.

    Parameters:
    ---------------
    lambda_M: float
        Strength of atomic spin-orbit coupling for M atoms [eV].
        Default value is for M = Mo.
    lambda_X: float
        Strength of atomic spin-orbit coupling for X atoms [eV].
        Default value is for X = S.

    Returns:
    ----------------
    SOI_Ham: numpy.array
        Spin-orbit interaction Hamiltonian for the monolayer.

    """
    # Block of even spin up, even spin up
    Meeuu = np.zeros((11, 11), dtype=complex)
    Meeuu[6, 7] = 1j*lambda_M
    Meeuu[7, 6] = Meeuu[6, 7].conj()
    Meeuu[9, 10] = -0.5j*lambda_X
    Meeuu[10, 9] = Meeuu[9, 10].conj()
    # Block of even spin down, even spin down
    Meedd = -Meeuu
    # Block of odd spin up, odd spin up
    Moouu = np.zeros((11, 11), dtype=complex)
    Moouu[0, 1] = -0.5j*lambda_M
    Moouu[1, 0] = Moouu[0, 1].conj()
    Moouu[3, 4] = -0.5j*lambda_X
    Moouu[4, 3] = Moouu[3, 4].conj()
    # Block of odd spin down, odd spin down
    Moodd = -Moouu
    # Block of even spin up, odd spin down
    Meoud = np.zeros((11, 11), dtype=complex)
    Meoud[5, 0] = -np.sqrt(3.)/2*lambda_M 
    Meoud[5, 1] = 1j*np.sqrt(3.)/2*lambda_M
    Meoud[6, 0] = -0.5j*lambda_M
    Meoud[6, 1] = 0.5*lambda_M
    Meoud[7, 0] = 0.5*lambda_M
    Meoud[7, 1] = 0.5j*lambda_M
    Meoud[8, 3] = -0.5*lambda_X
    Meoud[8, 4] = 0.5j*lambda_X
    Meoud[9, 2] = 0.5*lambda_X
    Meoud[10, 2] = -0.5j*lambda_X
    # Block of odd spin down, even spin up
    Moedu = Meoud.T.conj()
    # Block of even spin down, odd spin up
    Meodu = np.zeros((11, 11), dtype=complex)
    Meodu[5, 0] = np.sqrt(3.)/2*lambda_M
    Meodu[5, 1] = 1j*np.sqrt(3.)/2*lambda_M
    Meodu[6, 0] = -0.5j*lambda_M
    Meodu[6, 1] = -0.5*lambda_M
    Meodu[7, 0] = -0.5*lambda_M
    Meodu[7, 1] = 0.5j*lambda_M
    Meodu[8, 3] = 0.5*lambda_X
    Meodu[8, 4] = 0.5j*lambda_X
    Meodu[9, 2] = -0.5*lambda_X
    Meodu[10, 2] = -0.5j*lambda_X
    # Block of odd spin up, even spin down
    Moeud = Meodu.T.conj()
    
    # Construct the SOI Hamiltonian
    H_SOI = block_diag(Moouu + Meeuu, Moodd + Meedd)
    H_SOI[:11, 11:] = Moeud + Meoud
    H_SOI[11:, :11] = Moedu + Meodu
    
    return H_SOI

def SC_coupling(Delta=0.2e-3, N=11):
    """The Hamiltonian of the superconducting pairing.

    Parameters:
    --------------
    Delta: float
        s-wave pairing potential [eV].
    N: integer
        Number of orbitals in a unit cell. Included for testing,
        defaults to the correct value of 11.

    Returns:
    --------------
    SC_coupling: np.array
        BdG Hamiltonian for the pairing.
    """
    # The inner Kronecker product is the SC coupling in spin space,
    # the outer one between the electron and hole blocks.
    SC_Ham = np.kron(pauli.sy, np.kron(pauli.sy, np.eye(N)))
    return Delta*SC_Ham

def Zeeman_Ham(Ex=0, Ey=0, N=11):
    """Hamiltonian for the Zeeman terms due to an in-plane magnetic field.
    
    Parameters:
    --------------
    Ex: float
        Zeeman energy for the field along x [eV].
    Ey: float
        Zeeman energy for the field along y [eV].
    N: integer
        Number of orbitals in a unit cell. Included for testing,
        defaults to the correct value of 11.

    Returns:
    --------------
    Zeeman_Ham: np.array
        Hamiltonian for the Zeeman terms.
    """
    return np.kron(Ex*pauli.sx + Ey*pauli.sy, np.eye(N))