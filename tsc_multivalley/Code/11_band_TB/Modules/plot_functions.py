import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.gridspec import GridSpec

import Modules.TB_parameters as TB
import Modules.Ham_MX2_11_band as H

class MidpointNormalize(colors.Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=0):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax)
    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))

def plot_color_data(data, xs, ys, figsize=(14, 7), title='Winding number', xlabel=r'$E_x$ [eV]',
                      ylabel=r'$\mu$ [eV]', pcolor=False, cmap=None, half_BZ=True, transpose=False,
                   vmax=None, vmin=None, clabel=r' ', norm=1.0, normalizer=None):
    """Plot a single phase diagram. Data is a list of length len(xs)*len(ys)."""
    fig = plt.figure(figsize=figsize)
    dat = np.reshape(data, (len(xs), len(ys)))/norm
    if half_BZ:
        # If winding number integration only over half the BZ, must multiply by 2
        dat *= 2
    if not pcolor:
        if transpose:
            dat = dat.T
        plt.imshow(dat, origin = 'lower', extent=[min(xs), max(xs), min(ys), max(ys)],
                   aspect='auto', cmap=cmap, vmin=vmin, vmax=vmax, norm=normalizer)
    else:
        if transpose:
            dat = dat.T
            Xs, Ys = np.meshgrid(ys, xs, indexing='ij')
        else:
            Xs, Ys = np.meshgrid(xs, ys, indexing='ij')
        plt.pcolormesh(Xs, Ys, dat, cmap=cmap, vmin=vmin, vmax=vmax, norm=normalizer)
    plt.colorbar().set_label(label=clabel, size=20)
    plt.xlabel(xlabel, size=20)
    plt.ylabel(ylabel, size=20)
    plt.title(title, size=20)
    
def disp_slice(ks, p, bands=slice(14,16), SC=False, spin=True):
    return np.array([H.energies(kx, ky, p, SC=SC, spin=spin)[bands] for kx, ky in ks])
    
    
def subplot_disp(ax, disp, ks, title=None, xlabel=None, ylabel=None,
                 xlim=None, ylim=None, xticks=None, yticks=None):
    if ylabel is not None:
        ax.set_ylabel(ylabel, fontsize=20)
    if xlabel is not None:
        ax.set_xlabel(xlabel, fontsize=20)
    ax.plot(ks, disp, 'k-')
    if title is not None:
        ax.set_title(title, fontsize=20)
    plt.setp(ax.get_xticklabels(), fontsize=15)
    plt.setp(ax.get_yticklabels(), fontsize=15)
    if yticks is not None:
        ax.set_yticks(yticks)
    if xticks is not None:
        ax.set_xticks(xticks)
    if ylim is not None:
        ax.set_ylim(ylim[0], ylim[1])
    if xlim is not None:
        ax.set_xlim(xlim[0], xlim[1])

def topo_disp_Ex(p, K=1, suptitle=r'$K$', ylims=[(-1.2, 1.2), (-1.2, 1.2), (-1.2, 1.2)],
                 Exs = [(0, 0.9), (1.0, 1.1), (8.01, 9.0)], klimsL=(0.895, 0.925),
                 klimsR=(1.075, 1.105), nKpoints=300, xticksL = [0.90, 0.91, 0.92],
                 xticksR = [1.08, 1.09, 1.10], yticks=[(-1.0, -0.5, 0, 0.5, 1.0),
                                                       (-1.0, -0.5, 0, 0.5, 1.0),
                                                       (-1.0, -0.5, 0, 0.5, 1.0)]):
    """Panel plot of the dispersion in kx for different Ex. K=1 for the K point, -1 for -K.
    
    Default k values are good for a chemical potential matching the lower crossing of conduction bands,
    and a gap of 2 meV"""
    
    fig = plt.figure()
    fig.set_size_inches(10, 8)
    plt.suptitle(suptitle, size=25)
    
    kxsL = np.linspace(klimsL[0], klimsL[1], nKpoints)*TB.Kp[0]*K
    kxsR = np.linspace(klimsR[0], klimsR[1], nKpoints)*TB.Kp[0]*K
    ky = TB.Kp[1]
    ksL = [(kx, ky) for kx in kxsL]
    ksR = [(kx, ky) for kx in kxsR]
    
    # Left column
    gs1 = GridSpec(3, 2)
    gs1.update(left=0.05, right=0.5, hspace=0.08, wspace=0.0)
    # Right column
    gs2 = GridSpec(3, 2)
    gs2.update(left=0.53, right=0.98, hspace=0.08, wspace=0.0)
    
    Exs = [(r1*p.Delta, r2*p.Delta) for (r1, r2) in Exs]
    if xticksL is not None:
        xticksL = [K*tick for tick in xticksL]
    if xticksR is not None:
        xticksR = [K*tick for tick in xticksR]
    
    # Cover all rows
    for i, (tEx, ylim, ytick) in enumerate(zip(Exs, ylims, yticks)):
        ExL, ExR = tEx
        if i == 2:
            pxticksL = xticksL
            pxticksR = xticksR
            xlabel = r'$k_x$ [$4\pi/3a$]'
        else:
            pxticksL = []
            pxticksR = []
            xlabel = None
        # Left column
        p.Ex = ExL
        ax1 = plt.subplot(gs1[i, 0])
        disp = disp_slice(ksL, p, SC=True, bands=slice(20,24))
        subplot_disp(ax1, disp/p.Delta, kxsL/(4*np.pi/3.), title=r' ', xlabel=xlabel, ylabel=r'$E/\Delta$',
                     xlim=np.array(klimsL)*K, ylim=ylim, xticks=pxticksL,
                     yticks=ytick)

        ax2 = plt.subplot(gs1[i, 1])
        disp = disp_slice(ksR, p, SC=True, bands=slice(20,24))
        subplot_disp(ax2, disp/p.Delta, kxsR/(4*np.pi/3.), title=r' ', xlabel=xlabel, ylabel=None,
                     xlim=np.array(klimsR)*K, ylim=ylim, xticks=pxticksR, yticks=[])
        # Right column
        p.Ex = ExR
        ax3 = plt.subplot(gs2[i, 0])
        disp = disp_slice(ksL, p, SC=True, bands=slice(20,24))
        subplot_disp(ax3, disp/p.Delta, kxsL/(4*np.pi/3.), title=r' ', xlabel=xlabel, ylabel=None,
                     xlim=np.array(klimsL)*K, ylim=ylim, xticks=pxticksL, yticks=[])

        ax4 = plt.subplot(gs2[i, 1])
        disp = disp_slice(ksR, p, SC=True, bands=slice(20,24))
        subplot_disp(ax4, disp/p.Delta, kxsR/(4*np.pi/3.), title=r' ', xlabel=xlabel, ylabel=None,
                     xlim=np.array(klimsR)*K, ylim=ylim, xticks=pxticksR, yticks=[])
    plt.show()

def topo_disp_ky(p, K=1, suptitle=r'$K$', ylims=[(-1.2, 1.2), (-1.2, 1.2), (-1.2, 1.2)],
                 yticks=[(-1.0, -0.5, 0, 0.5, 1.0), (-1.0, -0.5, 0, 0.5, 1.0), (-1.0, -0.5, 0, 0.5, 1.0)],
                 kys = [(0, 0.015), (0.025, 0.03), (0.04, 0.05)],
                 klimsL=(0.895, 0.925), klimsR=(1.075, 1.105), nKpoints=300, xticksL = None,
                 xticksR = None, wkx = 0.015,
                 kx_centers = [(0.090, 0.088), (0.082, 0.077), (0.073, 0.070)]):
    """Panel plot for the dispersion in kx for different ky. K=1 for the K point, -1 for -K. """
    
    fig = plt.figure()
    fig.set_size_inches(10, 8)
    plt.suptitle(suptitle, size=25)
    
    # Left column
    gs1 = GridSpec(3, 2)
    gs1.update(left=0.05, right=0.5, hspace=0.2, wspace=0.0)
    # Right column
    gs2 = GridSpec(3, 2)
    gs2.update(left=0.53, right=0.98, hspace=0.2, wspace=0.0)
    
    kys = [(r1*4*np.pi/np.sqrt(3.), r2*4*np.pi/np.sqrt(3.)) for (r1, r2) in kys]
    if xticksL is not None:
        xticksL = [K*tick for tick in xticksL]
    if xticksR is not None:
        xticksR = [K*tick for tick in xticksR]
    
    # Cover all rows
    for i, (tky, tklims, ylim, ytick) in enumerate(zip(kys, kx_centers, ylims, yticks)):
        if i == 2:
            xlabel = r'$k_x$ [$4\pi/3a$]'
        else:
            xlabel = None
        kx_center_L, kx_center_R = tklims
        # Left column
        klimsL = (kx_center_L-wkx, kx_center_L+wkx)
        klimsR = (2.0-kx_center_L-wkx, 2.0-kx_center_L+wkx)
        kxsL = np.linspace(klimsL[0], klimsL[1], nKpoints)*TB.Kp[0]*K
        kxsR = np.linspace(klimsR[0], klimsR[1], nKpoints)*TB.Kp[0]*K
        kyL, kyR = tky
        ksL = [(kx, kyL) for kx in kxsL]
        ksR = [(kx, kyL) for kx in kxsR]
        ax1 = plt.subplot(gs1[i, 0])
        disp = disp_slice(ksL, p, SC=True, bands=slice(20,24))
        subplot_disp(ax1, disp/p.Delta, kxsL/(4*np.pi/3.), title=r' ', xlabel=xlabel, ylabel=r'$E/\Delta$',
                     xlim=np.array(klimsL)*K, ylim=ylim, xticks=xticksL, yticks=ytick)

        ax2 = plt.subplot(gs1[i, 1])
        disp = disp_slice(ksR, p, SC=True, bands=slice(20,24))
        subplot_disp(ax2, disp/p.Delta, kxsR/(4*np.pi/3.), title=r' ', xlabel=xlabel, ylabel=None,
                     xlim=np.array(klimsR)*K, ylim=ylim, xticks=xticksR, yticks=[])
        # Right column
        # Left column
        klimsL = (kx_center_R-wkx, kx_center_R+wkx)
        klimsR = (2.0-kx_center_R-wkx, 2.0-kx_center_R+wkx)
        kxsL = np.linspace(klimsL[0], klimsL[1], nKpoints)*TB.Kp[0]*K
        kxsR = np.linspace(klimsR[0], klimsR[1], nKpoints)*TB.Kp[0]*K
        ksL = [(kx, kyR) for kx in kxsL]
        ksR = [(kx, kyR) for kx in kxsR]
        ax3 = plt.subplot(gs2[i, 0])
        disp = disp_slice(ksL, p, SC=True, bands=slice(20,24))
        subplot_disp(ax3, disp/p.Delta, kxsL/(4*np.pi/3.), title=r' ', xlabel=xlabel, ylabel=None,
                     xlim=np.array(klimsL)*K, ylim=ylim, xticks=xticksL, yticks=[])

        ax4 = plt.subplot(gs2[i, 1])
        disp = disp_slice(ksR, p, SC=True, bands=slice(20,24))
        subplot_disp(ax4, disp/p.Delta, kxsR/(4*np.pi/3.), title=r' ', xlabel=xlabel, ylabel=None,
                     xlim=np.array(klimsR)*K, ylim=ylim, xticks=xticksR, yticks=[])
    plt.show()
    
def topo_disp_mu(p, ky=0, K=1, suptitle=r'$K$', ylims=[(-1.2, 1.2), (-1.2, 1.2), (-1.2, 1.2)],
                 yticks=[(-1.0, -0.5, 0, 0.5, 1.0), (-1.0, -0.5, 0, 0.5, 1.0), (-1.0, -0.5, 0, 0.5, 1.0)],
                 mus = [(1.8295, 1.8323), (1.8295, 1.8323), (1.8295, 1.8323)],
                 nKpoints=300, xticksL = None,
                 xticksR = None, wkx = 0.015,
                 kx_centers = [(0.97, 0.98), (0.98, 0.98), (0.98, 0.98)]):
    """Panel plot for the dispersion in kx for different mu. K=1 for the K point, -1 for -K. """
    
    fig = plt.figure()
    fig.set_size_inches(10, 8)
    plt.suptitle(suptitle, size=25)
    
    # Left column
    gs1 = GridSpec(3, 2)
    gs1.update(left=0.05, right=0.5, hspace=0.2, wspace=0.0)
    # Right column
    gs2 = GridSpec(3, 2)
    gs2.update(left=0.53, right=0.98, hspace=0.2, wspace=0.0)
    
    if xticksL is not None:
        xticksL = [K*tick for tick in xticksL]
    if xticksR is not None:
        xticksR = [K*tick for tick in xticksR]
    
    # Cover all rows
    for i, (tmu, tklims, ylim, ytick) in enumerate(zip(mus, kx_centers, ylims, yticks)):
        if i == 2:
            xlabel = r'$k_x$ [$4\pi/3a$]'
        else:
            xlabel = None
        kx_center_L, kx_center_R = tklims
        muL, muR = tmu
        # Left column
        p.mu = muL
        klimsL = (kx_center_L-wkx, kx_center_L+wkx)
        klimsR = (2.0-kx_center_L-wkx, 2.0-kx_center_L+wkx)
        kxsL = np.linspace(klimsL[0], klimsL[1], nKpoints)*TB.Kp[0]*K
        kxsR = np.linspace(klimsR[0], klimsR[1], nKpoints)*TB.Kp[0]*K
        ksL = [(kx, ky) for kx in kxsL]
        ksR = [(kx, ky) for kx in kxsR]
        ax1 = plt.subplot(gs1[i, 0])
        disp = disp_slice(ksL, p, SC=True, bands=slice(20, 24))
        subplot_disp(ax1, disp/p.Delta, kxsL/(4*np.pi/3.), title=r' ', xlabel=xlabel, ylabel=r'$E/\Delta$',
                     xlim=np.array(klimsL)*K, ylim=ylim, xticks=xticksL, yticks=ytick)

        ax2 = plt.subplot(gs1[i, 1])
        disp = disp_slice(ksR, p, SC=True, bands=slice(20, 24))
        subplot_disp(ax2, disp/p.Delta, kxsR/(4*np.pi/3.), title=r' ', xlabel=xlabel, ylabel=None,
                     xlim=np.array(klimsR)*K, ylim=ylim, xticks=xticksR, yticks=[])
        # Right column
        p.mu = muR
        klimsL = (kx_center_R-wkx, kx_center_R+wkx)
        klimsR = (2.0-kx_center_R-wkx, 2.0-kx_center_R+wkx)
        kxsL = np.linspace(klimsL[0], klimsL[1], nKpoints)*TB.Kp[0]*K
        kxsR = np.linspace(klimsR[0], klimsR[1], nKpoints)*TB.Kp[0]*K
        ksL = [(kx, ky) for kx in kxsL]
        ksR = [(kx, ky) for kx in kxsR]
        ax3 = plt.subplot(gs2[i, 0])
        disp = disp_slice(ksL, p, SC=True, bands=slice(20, 24))
        subplot_disp(ax3, disp/p.Delta, kxsL/(4*np.pi/3.), title=r' ', xlabel=xlabel, ylabel=None,
                     xlim=np.array(klimsL)*K, ylim=ylim, xticks=xticksL, yticks=[])
        ax4 = plt.subplot(gs2[i, 1])
        disp = disp_slice(ksR, p, SC=True, bands=slice(20, 24))
        subplot_disp(ax4, disp/p.Delta, kxsR/(4*np.pi/3.), title=r' ', xlabel=xlabel, ylabel=None,
                     xlim=np.array(klimsR)*K, ylim=ylim, xticks=xticksR, yticks=[])
    plt.show()


def winding_mesh(sqvd, mus, dR, dL):
    """Returns a meshgrid of winding number for the phase diagram.
    sqvd : float
        Array of \sqrt(V^2-\Delta^2)
    mus : float
        Array of chemical potentials
    dR : ndarray 
        Crossings in right BZ (aranged in increasing order in momentum)
    dL : ndarray
        Crossings in left BZ (aranged in decreaseing order in momentum)
    """
    def swap(a):
        for j in range(len(a)):
            if j%4 == 3: a[j], a[j-1] = a[j-1], a[j]
        return a

    def point(x, y, data):
        """Winding number at (x, y).
        """
        up = data+x
        down = data-x

        idx_up = np.argwhere(y < up)
        idx_down = np.argwhere(y < down)

        return np.sum(high[idx_up])+np.sum(low[idx_down])

    low = 1 - 2*(np.arange(len(dR)) % 2)
    high = np.roll(low, -1)

    dR = swap(dR[~np.isnan(dR)])
    dL = swap(dL[~np.isnan(dL)])

    V, M = np.meshgrid(sqvd, mus)
    return np.array([point(x, y, dR)+point(x, y, dL) for (x, y) in zip(V.ravel(), M.ravel())]).reshape(M.shape), V, M


def winding_from_cross(cross_dict, sqvd, mus):
    dR = []
    dL = []
    keys = list(cross_dict.keys())
    keys.sort()
    for key in keys:
        if cross_dict[key][0] is not None:
            if key > 0:
                dL += list(cross_dict[key])
            else:
                dR += list(cross_dict[key])
    LdR = len(dR)
    LdL = len(dL)
    if LdR > LdL:
        dL += (LdR-LdL)*[np.nan]
    if LdR < LdL:
        dR += (LdL-LdR)*[np.nan]
    dL.reverse()
    return winding_mesh(sqvd, mus, np.array(dR), np.array(dL))
    

nb_html_header = """
<script type=text/javascript>
/* Add a button for showing or hiding input */
on = "Show input";
off = "Hide input";
function onoff(){
  currentvalue = document.getElementById('onoff').value;
  if(currentvalue == off){
    document.getElementById("onoff").value=on;
      $('div.input').hide();
  }else{
    document.getElementById("onoff").value=off;
      $('div.input').show();
  }
}

/* Launch first notebook cell on start */
function launch_first_cell (evt) {
  if (!launch_first_cell.executed
      && Jupyter.notebook.kernel
  ) {
    Jupyter.notebook.get_cells()[0].execute();
    launch_first_cell.executed = true;
  }
}

$([Jupyter.events]).on('status_started.Kernel notebook_loaded.Notebook', launch_first_cell);
</script>

<p>Press this button to show/hide the code used in the notebook:
<input type="button" class="ui-button ui-widget ui-state-default \
ui-corner-all ui-button-text-only" value="Hide input" id="onoff" \
onclick="onoff();"></p>
"""
