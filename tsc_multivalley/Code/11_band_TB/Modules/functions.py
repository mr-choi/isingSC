import kwant
import scipy
import numpy as np
import itertools as it

from scipy.optimize import minimize
from scipy.linalg import block_diag

import Modules.Ham_MX2_11_band as H
import Modules.TB_parameters as TB
import Modules.ribbon_params_11_band as rp
import Modules.pauli as pauli
import Modules.constants as const

### Functions for BZ with different edge cuts ##############################

def from_11_to_other(mm, nn):
    """Transform a lattice vector from the 11 band basis to the other basis (Doru's).
    Input are mm and nn in T = mm a_1 + nn a_2 in the 11 band basis, returns
    m and n in T = m alpha_1 + n alpha_2"""
    return mm - nn, nn
    
def from_other_to_11(m, n):
    """Same as from_11_to_other but vice versa. """
    return m + n, n

def k_BZ_edges(mm, nn):
    """Lattice vector indices in the 11 band basis, T = mm*a_1 + nn*a_2. """
    # Transform to Doru's basis
    m, n = from_11_to_other(mm, nn)
    return 2*np.pi/np.sqrt(3.) * np.sqrt(m**2 + n**2 + m*n), np.pi/np.sqrt(m**2 + n**2 + m*n)

def tokxky(ks, mm, nn):
    kperp, kpar = ks
    # Transform to Doru's basis
    m, n = from_11_to_other(mm, nn)
    v = (m + n/2.)/np.sqrt(m**2 + n**2 + m*n)
    w = 0.5*n*np.sqrt(3.)/np.sqrt(m**2 + n**2 + m*n)
    return (v*kpar + w*kperp, w*kpar - v*kperp)

def angle_armchair(m, n):
    """Return the angle between the armchair edge and the edge T = ma_1 + na_2,
    where the 11 band basis is assumed for the indices m and n.
    
    Angle returned in degrees"""
    return np.arccos(np.sqrt(3)*n/(np.sqrt((2*m -n)**2 + 3*n**2)))*360/(2*np.pi)

### Functions to compute the winding number ###################################
def z(kx, ky, U, p, extended_hopping=True):
    """z = det(A(k))/|det(A(k))|, where A is the top left off-diagonal block of
    the Hamiltonian at k=(kx, ky). U is the unitary that off-block diagonalizes the
    Hamiltonian. """
    Ham = H.full_Hamiltonian(kx, ky, p, extended_hopping=extended_hopping, spin=True, SC=True)
    nHam = U.T.conj().dot(Ham).dot(U)
    # Check that the diagonal blocks indeed vanish
    assert np.allclose(nHam[:22, :22], np.zeros((22,22), dtype=complex))
    assert np.allclose(nHam[22:, 22:], np.zeros((22,22), dtype=complex))
    A = nHam[:22, 22:]
    detA = np.linalg.det(A)
    return detA/np.abs(detA)

def winding_number(ks, p, U, phase=1.0, extended_hopping=True):
    """Compute ks is a list of tuples (kx, ky), a closed path
    in the Brillouin zone over which to integrate to find
    the winding number.
    
    phase is an overall phase factor that the z values are multipled
    with. """
    assert np.allclose(np.abs(phase), 1) 
    # Get the phase at all values of k
    phis = np.angle([z(kx, ky, U, p, extended_hopping=extended_hopping)*phase for (kx, ky) in ks])
    # The phase at the two end points should be the same
    assert np.allclose(np.abs(phis[0]), np.abs(phis[-1]))
    # Since the curve is closed, the winding number is the number
    # of branch cuts.
    return branch_cuts(phis)

def branch_cuts(phis):
    """Check how often an array of phase values winds over the branch cut at the negative
    x-axis. Counterclockwise crossings are considered positive winding, clockwise ones negative.
    
    This function should be used with output from np.angle, which has the correct branch cut."""
    W = 0
    # There is a branch cut along the direction -x, where the phase jumps
    # between pi and -pi. We need to count the number of times the path
    # in the complex plane crosses this branch cut in each direction.
    # Identify where the phase changes sign. Happens at crossings
    # over the x axis.
    asign = np.sign(phis)
    # Compare adjacent elements to see when the phase changes sign.
    # If signchange[i] = 1, then phi[i] < 0 and phi[i+1] > 0 => decreases winding number.
    # If signchange[i] = -1, then phi[i] > 0 and phi[i+1] < 0 => increases winding number.
    # The winding number only changes if the crossing is over the negative part of the x-axis.
    signchange = ((np.roll(asign, -1) - asign)/2.).astype(int)
    # Get the indices of the values where the phase changes sign.
    cross_inds = it.compress(range(len(signchange)), np.abs(signchange))
    for i in cross_inds:
        # The final entry is compared to the first one.
        if i == len(phis)-1:
            phi_1 = phis[i]
            phi_2 = phis[0]
        else:
            phi_1 = phis[i]
            phi_2 = phis[i+1]
        # Crossings over the negative x-axis result in a discontinuity,
        # while crossings over the positive x-axis are continuous.
        # Pick out the discontinuous ones
        if np.abs(phi_1 - phi_2) > 0.7*2*np.pi:
            W -= signchange[i]
    return W


def new_winding_number(N, kpar, p, U, phase=1.0, half_BZ=True, adaptive=False, eps=1e-3, scale=0.1,
                       extended_hopping=True):
    """ Compute the winding number at a fixed momentum kpar parallel to the boundary.
    
    phase is an overall phase factor that the z values are multipled
    with.
    
    If adaptive = False, the winding number is calculated by discretizing the momentum
    perpendicular to the boundary into N+1 evenly spaced points. If adaptive = True,
    we use an adaptive mesh such that the real and imaginary parts of the winding number
    integrand do not change much between points. """
    
    assert np.allclose(np.abs(phase), 1)
    # Extent of the BZ for k perpendicular to the boundary
    kperp_edge = k_BZ_edges(p.mm, p.nn)[0]
    if half_BZ:  # Only integrate over half of the BZ
        init_kperp = 0
    else: # Entire BZ
        init_kperp = -kperp_edge
    kperps, dk = np.linspace(init_kperp, kperp_edge, N, retstep=True)
    # Do not use an adaptive mesh for kperp
    if not adaptive:
        # Get the phase at all values of k
        ks = [tokxky((kperp, kpar), p.mm, p.nn) for kperp in kperps]
        zs = [z(kx, ky, U, p, extended_hopping=extended_hopping)*phase for (kx, ky) in ks]
        phis = np.angle(zs)
    # Use an adaptive mesh
    else:
        # Initial k point and corresponding value of z
        kperp0 = init_kperp
        kx0, ky0 = tokxky((kperp0, kpar), p.mm, p.nn)
        z0 = z(kx0, ky0, U, p, extended_hopping=extended_hopping)*phase
        zs = [z0] # Store the first point
        # Outer loop is over the BZ - break it when we're at the edge
        orig_dk = dk  # Store the initial spacing between k points
        while True:
            # Inner loop adapts the mesh between adjacent k-points, such
            # that the real and imaginary parts change ''continuously'
            # between any two k-points, i.e. such that the real and imaginary
            # parts of z change by less than eps between k points.
            # Initial k point and corresponding value of z
            kx0, ky0 = tokxky((kperp0, kpar), p.mm, p.nn)
            z0 = z(kx0, ky0, U, p, extended_hopping=extended_hopping)*phase
            while True:
                # Next k point and the value of z
                kperp1 = kperp0 + dk
                kx1, ky1 = tokxky((kperp1, kpar), p.mm, p.nn)
                z1 = z(kx1, ky1, U, p, extended_hopping=extended_hopping)*phase
                # If both the real and imaginary parts of z change by less than eps, we
                # consider it a 'continuous' change between k points, i.e. a fine enough
                # mesh.
                if (np.abs(z0.real - z1.real) < eps) and (np.abs(z0.imag - z1.imag) < eps):
                    # If we're not at the BZ edge yet, store z and move on to the next point,
                    # i.e. set kperp0 = kperp1
                    if kperp1 < kperp_edge:
                        zs.append(z1)
                        kperp0 = kperp1
                        # Attempt to gradually scale the spacing back up
                        # between iterations, but only if the new spacing
                        # would be smaller than the initial spacing.
                        if dk/scale < orig_dk:
                            dk = dk/scale
                        else:
                            dk = orig_dk
                    # If we're at or beyond the BZ edge, we pick kperp1 = kperp_edge and exit the
                    # inner loop
                    elif kperp1 >= kperp_edge:
                        kperp1 = kperp_edge
                        kx1, ky1 = tokxky((kperp1, kpar), p.mm, p.nn)
                        zs.append(z(kx1, ky1, U, p, extended_hopping=extended_hopping)*phase)
                    break
                # If the points are not close enough, we move the next
                # k point closer to this one by making the mesh finer.
                else:
                    dk = scale*dk
                    continue
            # If we're at the BZ edge, we're done
            if np.abs(kperp1 - kperp_edge) < 1e-12:
                break
        phis = np.angle(zs)
    # The phase at the two end points should be the same
    assert np.allclose(np.abs(phis[0]), np.abs(phis[-1]))
    # Since the curve is closed, the winding number is the number
    # of branch cuts.
    return branch_cuts(phis), zs
################################################################################

### Functions to compute the topological gap ###################################
def minimize_this(kx, ky, p, SC=True, spin=True, bands=slice(20,24)):
    """Function to minimize to find the topological gap. Returns the gap
    in meV!"""
    return 1000*min(np.abs(H.energies(kx, ky, p, SC=SC, spin=spin)[bands]))

def find_gap(ky, p, check_grad=False, K=1, init_interval=0.2, points=30, grad_points=5000, tol=1e-8):
    """Estimate the gap at a fixed ky by minimizing the lowest band energy as a function of kx.
    Base the computation on several initial guesses around either the K or the -K point. The
    initial guesses are chosen from an interval [(1-init_interval)*K_x : (1+init_interval)*Kx]
    
    The gap is returned in meV!"""
    
    if check_grad:
        # Base initial guesses for k on where the derivative of the dispersion vanishes
        kxs = np.linspace(K*(1-init_interval)*TB.Kp[0], K*(1+init_interval)*TB.Kp[0], grad_points)
        # Energy of the lowest band
        lowest_band = np.array([minimize_this(kx, ky, p) for kx in kxs])
        idx = np.argwhere(np.diff(np.sign(np.gradient(lowest_band))) != 0)
        init_kxs = kxs[idx].ravel()
    else:
        # Use a grid of kx points around K or K' as initial guesses
        init_kxs = np.linspace(K*(1-init_interval)*TB.Kp[0], K*(1+init_interval)*TB.Kp[0], points)
    
    band_minima = []
    for kx0 in init_kxs:
        res = minimize(lambda kx: minimize_this(kx, ky, p), kx0, tol=tol)
        band_minimum = res.fun
        assert np.allclose(minimize_this(res.x, ky, p), band_minimum)
        band_minima.append(band_minimum)
    return np.min(band_minima)


# Better implementation
def gap_finder(lead, params, start_Es=(0, 1.0), eps=1e-10, max_iter=200):
    """Compute the induced gap from the dispersion of a lead by finding the
    smallest energy at which there are propagating modes. Positive energies
    are assumed.
    
    If there are propagating modes at the specified lower energy bound, the
    system is assumed gapless and the lower bound is returned.
    
    If there are no propagating modes at either the upper or the lower energy
    bounds, we interpret the gap as larger than the upper bound and just return
    the upper bound.

    Parameters:
    -------------------
    lead: kwant.system.InfiniteSystem object
          A finalized Kwant 1D translationally invariant system.
    params: dict
          A container with all Hamiltonian parameters.
    start_Es: tuple of two floats
          Starting guess for an energy interval around the induced gap.
          It is assumed that start_Es[1] > start_Es[0].
    eps: float
          Tolerance for the induced gap. """

    E0, E1 = start_Es
    nmodes0 = lead.modes(energy=E0, params=params)[1].nmodes
    nmodes1 = lead.modes(energy=E1, params=params)[1].nmodes
    if nmodes0 != 0:
        return E0
    if nmodes0 == 0 and nmodes1 == 0:
        return E1
    if not (nmodes0 == 0 and nmodes1 != 0):
        raise ValueError('Give initial energies such that modes are \
            active at one but not the other')
    iterations = 0
    while True:
        iterations += 1
        new_E = 0.5*(E1 + E0)
        new_nmodes = lead.modes(energy=new_E, params=params)[1].nmodes
        if new_nmodes == 0:
            previous_E = E0
            E0 = new_E
        elif new_nmodes != 0:
            previous_E = E1
            E1 = new_E
        if np.abs(new_E - previous_E) < eps:
            break
        if iterations > max_iter:
            return None
    return new_E


################################################################################

def dispersion(ks, p, bands=slice(14,16), SC=False, spin=True):
    return np.array([H.energies(kx, ky, p, SC=SC, spin=spin)[bands] for kx, ky in ks])

def dispersion_diff(kperp, kpar, p, bands=slice(14, 16)):
    ks = [tokxky((kperp, kpar), p.mm, p.nn)]
    cond_disp = dispersion(ks, p, bands=bands)
    return np.abs(cond_disp[0][0] - cond_disp[0][1])

def find_crossings(p, kpar=0, N=5, half_BZ=True, N_points=4000, search_interval=0.16, cross_tol=0.5e-5, max_E=2.0,
                  bands=slice(14, 16)):
    """Crossings returned in a dictionary, with valleys indices as keys and the two chemical potential values
    in a tuple as values, such that the left one crosses to the left of each K minimum, and the right one
    to the right."""
    cross_dict = {}
    K = 4*np.pi/3
    angle = angle_armchair(p.mm, p.nn)*np.pi*2/360  # angle in rad
    if half_BZ:
        K_inds = range(1, N+1)
    else:
        K_inds = list(range(-N, 0)) + list(range(1, N+1))
    for K_ind in K_inds:
        # We search for crossings around the projections of the different K valleys
        # onto the k_perp axis
        cosphi = np.abs(np.cos(angle))
        left_ks = np.linspace(K_ind*K*cosphi - search_interval*K, K_ind*K*cosphi, N_points)
        left_crossing = None
        for kperp in left_ks:
            kxy = [tokxky((kperp, kpar), p.mm, p.nn)]
            cond_disp = dispersion(kxy, p, bands=bands)
            E1, E2 = cond_disp[0]
            if np.abs(E1 - E2) < cross_tol and ((E1 + p.mu) < max_E):
                left_crossing = (E1+E2)/2.0
                break
        right_ks = np.linspace(K_ind*K*cosphi, K_ind*K*cosphi + search_interval*K, N_points)
        right_crossing = None
        for kperp in right_ks:
            kxy = [tokxky((kperp, kpar), p.mm, p.nn)]
            cond_disp = dispersion(kxy, p, bands=bands)
            E1, E2 = cond_disp[0]
            if np.abs(E1 - E2) < cross_tol and ((E1 + p.mu) < max_E):
                right_crossing = (E1+E2)/2.0
                break
        cross_dict[K_ind] = (left_crossing, right_crossing)
    return cross_dict

##############################################################################################

def MX2_system(W = 10, L = 4, spin=False, SC=False, lambda_M = 0.0836, lambda_X = 0.0556, sys_type='lead',
               plot_sys=False, finalize=False, orb_mag=False):
    """Make an MX2 armchair lead, a finite system or a bulk system.
    
    Uses the 2D lattice that does NOT separate XA and XB atoms.
    
    Allows to include an orbital magnetic Peierls phase in the in-plane hopping
    between X orbitals.
    
    Parameters:
    ---------------------------------------------
    syst_type: string
               The type of system to make: 'lead' for an armchair lead,
                                           'finite' for a finite rectangular system,
                                           'bulk' for a bulk system with 2D translational
                                                  invariance.
    W: integer
       Width of the system along x (perpendicular to the armchair edge), units of the lattice
       constant.
    L: integer
       Width of the system along y (parallel to the armchair edge), units of the lattice constant.
       Only relevant if syst_type='finite'.
                                            """
    # Define the MX2 lattice.
    # Two atoms (M and X) per unit cell
    # Number of orbitals on M and X
    norbs = [5, 6]
    if spin:
        norbs = [2*N for N in norbs]
        if SC:
            norbs = [2*N for N in norbs]
    MX2 = kwant.lattice.general([(1, 0), (-1/2, np.sqrt(3)/2)],  # Bravais lattice vectors a1 and a2
                                [(0, 0), (1/2, 1/(2*np.sqrt(3)))], # Atom coordinates in a unit cell, M and X
                                norbs=norbs) # Number of orbitals per atom type, M and X 
    M, X = MX2.sublattices
    
    def armchair_ribbon_shape(site):
        x, y = site.pos
        return (0 <= x < W)

    def square(site):
        x, y = site.pos
        return (0 <= x < W) and (0 <= y < L)
    
    # Spin-orbit interaction
    if spin:
        SOI_Ham = H.SOI_Ham(lambda_M = lambda_M, lambda_X = lambda_X)
        M_SOI, X_SOI = rp.SOI_atomic_basis(SOI_Ham)

    def onsite_M(site, Ex, Ey, Delta, mu):
        H0 = rp.M_ons - mu*np.eye(5)
        if spin:
            H0 = np.kron(pauli.s0, H0) + M_SOI + np.kron(Ex*pauli.sx + Ey*pauli.sy, np.eye(5))
            if SC:
                return block_diag(H0, -H0.conj()) + H.SC_coupling(Delta=Delta, N=5)
            else:
                return H0
        else:
            return H0

    def onsite_X(site, Ex, Ey, Delta, mu):
        H0 = rp.X_ons - mu*np.eye(6)
        if spin:
            H0 = np.kron(pauli.s0, H0) + X_SOI + np.kron(Ex*pauli.sx + Ey*pauli.sy, np.eye(6))
            if SC:
                return block_diag(H0, -H0.conj()) + H.SC_coupling(Delta=Delta, N=6)
            else:
                return H0
        else:
            return H0

    # Make a builder for a 2D system with a minimal unit cell.
    bulk_sym = kwant.TranslationalSymmetry(MX2.vec((1, 0)), MX2.vec((0, 1)))   # For 2D bulk trans inv
    bulk = kwant.Builder(bulk_sym)
    
    # Onsites
    bulk[[M(0, 0)]] = onsite_M #((0, 0), spin=spin) #rp.M_ons
    bulk[[X(0, 0)]] = onsite_X #((0, 0), spin=spin) #rp.X_ons
    
    # Hoppings
    # There are 5 orbitals on M and 6 on X, so e.g. the hopping from M to X is 6x5
    # (without spin or SC)
    def hop(site1, site2, Ex, hop_mat, orb_mag=False):
        if orb_mag:
            hop_mat = peierls_XX(hop_mat, Ex)
        if spin:
            hop0 = np.kron(pauli.s0, hop_mat)
            if SC:
                return block_diag(hop0, -hop0.conj())
            else:
                return hop0
        else:
            return hop_mat
        
    # Peierls substitution for XX in-plane hopping
    # for a magnetic field along x.
    def peierls_XX(hop_mat, Ex):
        """Ex in eV! """
        UX = rp.UX
        hop_mat = (UX.T.conj().dot(hop_mat).dot(UX)).astype(complex)
        Bx = Ex*const.e/const.muB   # Ex in eV, Bx in T
        lBinv = np.sqrt(const.e*Bx/const.hbar)*1e-10  # in Å^-1
        p_phase = np.sqrt(3)/4 * TB.dxx*TB.a*lBinv**2
        AA_block = hop_mat[:3, :3]*np.exp(1j*p_phase)
        hop_mat[:3, :3] = AA_block
        BB_block = hop_mat[3:, 3:]*np.exp(-1j*p_phase)
        hop_mat[3:, 3:] = BB_block
        hop_mat = UX.dot(hop_mat).dot(UX.T.conj())
        return hop_mat
    
    
    # M to X, nearest neighbour
    NN_XM_hop_1 = ((0, 0), X, M)
    bulk[[kwant.builder.HoppingKind(*NN_XM_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                   Ex, rp.NN_T_XM_1)

    NN_XM_hop_2 = ((-1, 0), X, M)
    bulk[[kwant.builder.HoppingKind(*NN_XM_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                   Ex, rp.NN_T_XM_2)

    NN_XM_hop_3 = ((-1, -1), X, M)
    bulk[[kwant.builder.HoppingKind(*NN_XM_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                   Ex, rp.NN_T_XM_3)

    # M to M, nearest neighbour
    NN_MM_hop_1 = ((1, 0), M, M)
    bulk[[kwant.builder.HoppingKind(*NN_MM_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                   Ex, rp.NN_T_MM_1)

    NN_MM_hop_2 = ((1, 1), M, M)
    bulk[[kwant.builder.HoppingKind(*NN_MM_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                   Ex, rp.NN_T_MM_2)

    NN_MM_hop_3 = ((0, 1), M, M)
    bulk[[kwant.builder.HoppingKind(*NN_MM_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                   Ex, rp.NN_T_MM_3)

    # X to X, nearest neighbour
    NN_XX_hop_1 = ((1, 0), X, X)
    bulk[[kwant.builder.HoppingKind(*NN_XX_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                   Ex, rp.NN_T_XX_1)

    NN_XX_hop_2 = ((1, 1), X, X)
    bulk[[kwant.builder.HoppingKind(*NN_XX_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                   Ex, rp.NN_T_XX_2, orb_mag=orb_mag)

    NN_XX_hop_3 = ((0, 1), X, X)
    bulk[[kwant.builder.HoppingKind(*NN_XX_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                   Ex, rp.NN_T_XX_3, orb_mag=orb_mag)

    # M to X, next nearest neighbour
    NNN_XM_hop_1 = ((0, -1), X, M)
    bulk[[kwant.builder.HoppingKind(*NNN_XM_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                    Ex, rp.NNN_T_XM_1)

    NNN_XM_hop_2 = ((-2, -1), X, M)
    bulk[[kwant.builder.HoppingKind(*NNN_XM_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                    Ex, rp.NNN_T_XM_2)

    NNN_XM_hop_3 = ((0, 1), X, M)
    bulk[[kwant.builder.HoppingKind(*NNN_XM_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                    Ex, rp.NNN_T_XM_3)
    
    # Use the bulk system as a template to construct other geometries.
    if sys_type == 'bulk':
        syst = bulk
    elif sys_type == 'lead':
        # Along the armchair edge - we need a double unit cell.
        sym = bulk_sym.subgroup((2, 4))
        syst = kwant.Builder(sym)
        added_sites = syst.fill(bulk, armchair_ribbon_shape,
                                (0, 0), max_sites=float('inf'))
    elif sys_type == 'finite':
        syst = kwant.Builder()
        added_sites = syst.fill(bulk, square,
                                (0, 0), max_sites=float('inf'))
    else:
        raise ValueError('Invalid system type specified.')
    
    def family_colors(site):
        return 0 if site.family == M else 1

    def hopping_lw(site1, site2):
        return 0.04 if site1.family == site2.family else 0.1
    
    if plot_sys:
        kwant.plot(syst, site_color=family_colors, site_lw=0.1, hop_lw=hopping_lw, colorbar=False, fig_size=(10, 8));
        
    if finalize:
        return syst.finalized()
    else:
        return syst
    
    
def MX2_systems_3D(W = 10, L = 4, spin=False, SC=False, lambda_M = 0.0836, lambda_X = 0.0556, sys_type='bulk',
                   plot_sys=False, finalize=False, orb_mag=False):
    """Make an MX2 armchair lead, a finite system or a bulk system.
    
    Uses the 3D lattice that separates XA and XB atoms.
    
    Allows to include an orbital magnetic Peierls phase due to an in-plane Zeeman field
    along x.
    
    Parameters:
    ---------------------------------------------
    syst_type: string
               The type of system to make: 'lead' for an armchair lead,
                                           'finite' for a finite rectangular system,
                                           'bulk' for a bulk system with 2D translational
                                                  invariance.
    W: integer
       Width of the system along x (perpendicular to the armchair edge), units of the lattice
       constant.
    L: integer
       Width of the system along y (parallel to the armchair edge), units of the lattice constant.
       Only relevant if syst_type='finite'.
                                            """

    UX = rp.UX   # Unitary that changes to the new atomic basis with XA and XB separate.
    # UX only acts on the X orbitals.
    
    # Number of orbitals per atom.
    norbs = [5, 3, 3]    # M, XA, XB
    if spin:
        norbs = [2*N for N in norbs]
        if SC:
            norbs = [2*N for N in norbs]
    # We use the separation a between identical atoms as the lattice constant.
    # The separation between stacked XA and XB is d.
    d_over_a = TB.dxx/TB.a
    MX2 = kwant.lattice.general([(1, 0, 0), (-1/2, np.sqrt(3)/2, 0)],      # Bravais lattice vectors a1 and a2
                                [(0, 0, 0),                                # Coordinate of M in unit cell
                                 (1/2, 1/(2*np.sqrt(3)), 0.5*d_over_a),    # Coordinate of X_A
                                 (1/2, 1/(2*np.sqrt(3)), -0.5*d_over_a)],  # Coordinate of X_B
                                norbs=norbs) # Number of orbitals per atom type, M, XA, XB
    M, XA, XB = MX2.sublattices

    # Spin-orbit interaction
    if spin:
        SOI_Ham = H.SOI_Ham(lambda_M = lambda_M, lambda_X = lambda_X)
        # A and B atoms are separated in the 3D lattice
        M_SOI, XA_SOI, XB_SOI = rp.SOI_atomic_basis(SOI_Ham, XAXB=True)
        
    def armchair_ribbon_shape(site):
        x, y, z = site.pos
        return (0 <= x < W)

    def square(site):
        x, y, z = site.pos
        return (0 <= x < W) and (0 <= y < L)

    def onsite_M(site, Ex, Ey, Delta, mu):
        H0 = rp.M_ons - mu*np.eye(5)
        if spin:
            H0 = np.kron(pauli.s0, H0) + M_SOI + np.kron(Ex*pauli.sx + Ey*pauli.sy, np.eye(5))
            if SC:
                return block_diag(H0, -H0.conj()) + H.SC_coupling(Delta=Delta, N=5)
            else:
                return H0
        else:
            return H0

    def onsite_XA(site, Ex, Ey, Delta, mu, pot):
        """Onsite for XA"""
        # X_ons is in the 11 band TB basis. Change to the atomic basis that separates
        # A and B X atoms, and pick out the 3x3 onsite block for XA.
        H0 = UX.T.conj().dot(rp.X_ons).dot(UX)[:3, :3] + pot*np.eye(3) - mu*np.eye(3)
        if spin:
            H0 = np.kron(pauli.s0, H0) + XA_SOI + np.kron(Ex*pauli.sx + Ey*pauli.sy, np.eye(3))
            if SC:
                return block_diag(H0, -H0.conj()) + H.SC_coupling(Delta=Delta, N=3)
            else:
                return H0
        else:
            return H0

    def onsite_XB(site, Ex, Ey, Delta, mu, pot):
        """Onsite for XB"""
        # X_ons is in the 11 band TB basis. Change to the atomic basis that separates
        # A and B X atoms, and pick out the 3x3 onsite block for XB.
        H0 = UX.T.conj().dot(rp.X_ons).dot(UX)[3:, 3:] - pot*np.eye(3) - mu*np.eye(3)
        if spin:
            H0 = np.kron(pauli.s0, H0) + XB_SOI + np.kron(Ex*pauli.sx + Ey*pauli.sy, np.eye(3))
            if SC:
                return block_diag(H0, -H0.conj()) + H.SC_coupling(Delta=Delta, N=3)
            else:
                return H0
        else:
            return H0

    # Make a builder for a 2D system with a minimal unit cell.
    bulk_sym = kwant.TranslationalSymmetry(MX2.vec((1, 0)), MX2.vec((0, 1)))   # For 2D bulk trans inv
    bulk = kwant.Builder(bulk_sym)

    # Onsites
#     print(M(0,0).pos, XA(0,0).pos, XB(0,0).pos)
#     print(M(0,0).family, XA(0,0).family, XB(0,0).family)
#     print(M(0,0).tag, XA(0,0).tag, XB(0,0).tag)
    bulk[M(0, 0)] = onsite_M #((0, 0), spin=spin) #rp.M_ons
    bulk[XA(0, 0)] = onsite_XA #((0, 0), spin=spin) #rp.X_ons
    bulk[XB(0, 0)] = onsite_XB

    # Hoppings
    # There are 5 orbitals on M, 3 on XA and 3 on XB, so e.g. the hopping from M to XA is 6x3
    # (without spin or SC)
    # Hoppings are from site2 to site1.
    def hop(site1, site2, Ex, hop_mat, orb_mag=False):
        if orb_mag:
            # Include Peierls substitution
            hop_mat = hop_mat*peierls_phase(site1, site2, Ex)
        if spin:
            hop0 = np.kron(pauli.s0, hop_mat)
            if SC:
                return block_diag(hop0, -hop0.conj())
            else:
                return hop0
        else:
            return hop_mat
        
    def peierls_phase(site1, site2, Ex):
        x1, y1, z1 = site1.pos  # Destination of hop
        x2, y2, z2 = site2.pos  # Source of hop
        Bx = Ex*const.e/const.muB   # Ex in eV, Bx in T
        # Lattice parameters are scaled in terms of the lattice
        # constant a, which is in Å.
        lBinv = np.sqrt(const.e*Bx/const.hbar)*1e-10*TB.a  # a/lB
        dy = y1 - y2
        dz = z1 - z2
        phase = lBinv**2 * dy * (z2 + 0.5*dz)
        return np.exp(1j*phase)
        

    ##### M to M, nearest neighbour - unchanged. ######################
    NN_MM_hop_1 = ((1, 0), M, M)
    bulk[[kwant.builder.HoppingKind(*NN_MM_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2, Ex,
                                                                                   rp.NN_T_MM_1, orb_mag=orb_mag)
    NN_MM_hop_2 = ((1, 1), M, M)
    bulk[[kwant.builder.HoppingKind(*NN_MM_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                   Ex, rp.NN_T_MM_2, orb_mag=orb_mag)
    NN_MM_hop_3 = ((0, 1), M, M)
    bulk[[kwant.builder.HoppingKind(*NN_MM_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                   Ex, rp.NN_T_MM_3, orb_mag=orb_mag)

    ##### X to X hoppings ####################################
    # XA to XB for stacked atoms, directly above each other.
    XA_to_XB_stacked = ((0, 0), XB, XA)
    T_XA_to_XB_stacked = UX.T.conj().dot(rp.X_ons).dot(UX)[3:, :3] # Hopping matrix
    bulk[[kwant.builder.HoppingKind(*XA_to_XB_stacked)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                        Ex, T_XA_to_XB_stacked,
                                                                                        orb_mag=orb_mag)
    # Other X to X hoppings from the 2D model.
    # X to X, nearest neighbour
    # Along delta_1 direction.
    NN_T_XX_1_sep = UX.T.conj().dot(rp.NN_T_XX_1).dot(UX)  # Separate XA and XB atoms.
    NN_XAXA_hop_1 = ((1, 0), XA, XA)
    bulk[[kwant.builder.HoppingKind(*NN_XAXA_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NN_T_XX_1_sep[:3, :3],
                                                                                     orb_mag=orb_mag)
    NN_XBXA_hop_1 = ((1, 0), XB, XA)
    bulk[[kwant.builder.HoppingKind(*NN_XBXA_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NN_T_XX_1_sep[3:, :3],
                                                                                     orb_mag=orb_mag)
    NN_XAXB_hop_1 = ((1, 0), XA, XB)
    bulk[[kwant.builder.HoppingKind(*NN_XAXB_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NN_T_XX_1_sep[:3, 3:],
                                                                                     orb_mag=orb_mag)
    NN_XBXB_hop_1 = ((1, 0), XB, XB)
    bulk[[kwant.builder.HoppingKind(*NN_XBXB_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NN_T_XX_1_sep[3:, 3:],
                                                                                     orb_mag=orb_mag)

    # Along delta_2 direction.
    NN_T_XX_2_sep = UX.T.conj().dot(rp.NN_T_XX_2).dot(UX)  # Separate XA and XB atoms.
    NN_XAXA_hop_2 = ((1, 1), XA, XA)
    bulk[[kwant.builder.HoppingKind(*NN_XAXA_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NN_T_XX_2_sep[:3, :3],
                                                                                     orb_mag=orb_mag)
    NN_XBXA_hop_2 = ((1, 1), XB, XA)
    bulk[[kwant.builder.HoppingKind(*NN_XBXA_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NN_T_XX_2_sep[3:, :3],
                                                                                     orb_mag=orb_mag)
    NN_XAXB_hop_2 = ((1, 1), XA, XB)
    bulk[[kwant.builder.HoppingKind(*NN_XAXB_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NN_T_XX_2_sep[:3, 3:],
                                                                                     orb_mag=orb_mag)
    NN_XBXB_hop_2 = ((1, 1), XB, XB)
    bulk[[kwant.builder.HoppingKind(*NN_XBXB_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NN_T_XX_2_sep[3:, 3:],
                                                                                     orb_mag=orb_mag)

    # Along delta_3 direction.
    NN_T_XX_3_sep = UX.T.conj().dot(rp.NN_T_XX_3).dot(UX)  # Separate XA and XB atoms.
    NN_XAXA_hop_3 = ((0, 1), XA, XA)
    bulk[[kwant.builder.HoppingKind(*NN_XAXA_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NN_T_XX_3_sep[:3, :3],
                                                                                     orb_mag=orb_mag)
    NN_XBXA_hop_3 = ((0, 1), XB, XA)
    bulk[[kwant.builder.HoppingKind(*NN_XBXA_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NN_T_XX_3_sep[3:, :3],
                                                                                     orb_mag=orb_mag)
    NN_XAXB_hop_3 = ((0, 1), XA, XB)
    bulk[[kwant.builder.HoppingKind(*NN_XAXB_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NN_T_XX_3_sep[:3, 3:],
                                                                                     orb_mag=orb_mag)
    NN_XBXB_hop_3 = ((0, 1), XB, XB)
    bulk[[kwant.builder.HoppingKind(*NN_XBXB_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NN_T_XX_3_sep[3:, 3:],
                                                                                     orb_mag=orb_mag)

    #### M to X, nearest neighbour ####################################
    # Along delta_4 direction.
    NN_T_XM_1_sep = UX.T.conj().dot(rp.NN_T_XM_1)  # Separate XA and XB atoms
    NN_XAM_hop_1 = ((0, 0), XA, M)
    bulk[[kwant.builder.HoppingKind(*NN_XAM_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                    Ex, NN_T_XM_1_sep[:3, :],
                                                                                    orb_mag=orb_mag)
    NN_XBM_hop_1 = ((0, 0), XB, M)
    bulk[[kwant.builder.HoppingKind(*NN_XBM_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                    Ex, NN_T_XM_1_sep[3:, :],
                                                                                    orb_mag=orb_mag)

    # Along delta_6 direction.
    NN_T_XM_2_sep = UX.T.conj().dot(rp.NN_T_XM_2)  # Separate XA and XB atoms
    NN_XAM_hop_2 = ((-1, 0), XA, M)
    bulk[[kwant.builder.HoppingKind(*NN_XAM_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                    Ex, NN_T_XM_2_sep[:3, :],
                                                                                    orb_mag=orb_mag)
    NN_XBM_hop_2 = ((-1, 0), XB, M)
    bulk[[kwant.builder.HoppingKind(*NN_XBM_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                    Ex, NN_T_XM_2_sep[3:, :],
                                                                                    orb_mag=orb_mag)

    # Along delta_5 direction.
    NN_T_XM_3_sep = UX.T.conj().dot(rp.NN_T_XM_3)  # Separate XA and XB atoms
    NN_XAM_hop_3 = ((-1, -1), XA, M)
    bulk[[kwant.builder.HoppingKind(*NN_XAM_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                    Ex, NN_T_XM_3_sep[:3, :],
                                                                                    orb_mag=orb_mag)
    NN_XBM_hop_3 = ((-1, -1), XB, M)
    bulk[[kwant.builder.HoppingKind(*NN_XBM_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                    Ex, NN_T_XM_3_sep[3:, :],
                                                                                    orb_mag=orb_mag)

    #### M to X, next nearest neighbour ####################################
    # Along delta_7 direction.
    NNN_T_XM_1_sep = UX.T.conj().dot(rp.NNN_T_XM_1)  # Separate XA and XB atoms
    NNN_XAM_hop_1 = ((0, -1), XA, M)
    bulk[[kwant.builder.HoppingKind(*NNN_XAM_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NNN_T_XM_1_sep[:3, :],
                                                                                     orb_mag=orb_mag)
    NNN_XBM_hop_1 = ((0, -1), XB, M)
    bulk[[kwant.builder.HoppingKind(*NNN_XBM_hop_1)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NNN_T_XM_1_sep[3:, :],
                                                                                     orb_mag=orb_mag)

    # Along delta_9 direction.
    NNN_T_XM_2_sep = UX.T.conj().dot(rp.NNN_T_XM_2)  # Separate XA and XB atoms
    NNN_XAM_hop_2 = ((-2, -1), XA, M)
    bulk[[kwant.builder.HoppingKind(*NNN_XAM_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NNN_T_XM_2_sep[:3, :],
                                                                                     orb_mag=orb_mag)
    NNN_XBM_hop_2 = ((-2, -1), XB, M)
    bulk[[kwant.builder.HoppingKind(*NNN_XBM_hop_2)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NNN_T_XM_2_sep[3:, :],
                                                                                     orb_mag=orb_mag)

    # Along delta_8 direction.
    NNN_T_XM_3_sep = UX.T.conj().dot(rp.NNN_T_XM_3)  # Separate XA and XB atoms
    NNN_XAM_hop_3 = ((0, 1), XA, M)
    bulk[[kwant.builder.HoppingKind(*NNN_XAM_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NNN_T_XM_3_sep[:3, :],
                                                                                     orb_mag=orb_mag)
    NNN_XBM_hop_3 = ((0, 1), XB, M)
    bulk[[kwant.builder.HoppingKind(*NNN_XBM_hop_3)]] = lambda site1, site2, Ex: hop(site1, site2,
                                                                                     Ex, NNN_T_XM_3_sep[3:, :],
                                                                                     orb_mag=orb_mag)

    # Use the bulk system as a template to construct other geometries.
    if sys_type == 'bulk':
        syst = bulk
    elif sys_type == 'lead':
        # Along the armchair edge - we need a double unit cell.
        sym = bulk_sym.subgroup((2, 4))
        syst = kwant.Builder(sym)
        added_sites = syst.fill(bulk, armchair_ribbon_shape,
                                (0, 0, 0), max_sites=float('inf'))
    elif sys_type == 'finite':
        syst = kwant.Builder()
        added_sites = syst.fill(bulk, square,
                                (0, 0, 0), max_sites=float('inf'))
    else:
        raise ValueError('Invalid system type specified.')

    def family_colors(site):
        return 0 if site.family == M else 1

    def hopping_lw(site1, site2):
        return 0.04 if site1.family == site2.family else 0.1

    if plot_sys:
        kwant.plot(syst, site_color=family_colors, site_lw=0.1, hop_lw=hopping_lw, colorbar=False, fig_size=(10, 8));

    if finalize:
        return syst.finalized()
    else:
        return syst
    
    
def make_AC_lead(plot_sys=False, SC=True, spin=True, finalize=True, orb_mag=False, three_d=False,
                R1=2, R2=2):
    """Construct an armchair unit cell with 2D translational invariance,
    wrapped around the y (armchair) direction but retaining kwant translational
    invariance along the x direction. This system can be used to compute the density
    of states at an armchair edge. """
    
    # Minimal unit cell of bulk, which couples distant unit cells
    if not three_d:
        # Do not separate XA and XB atoms.
        bulk = MX2_system(spin=spin, SC=SC, sys_type='bulk', plot_sys=False, finalize=False, orb_mag=orb_mag)
    else:
        bulk = MX2_systems_3D(spin=spin, SC=SC, sys_type='bulk', plot_sys=False, finalize=False, orb_mag=orb_mag)
    # We need to make a larger unit cell to compute stuff with Kwant.
    # We base the new unit cell on the minimal cell.
    sym = bulk.symmetry
    full_bulk = kwant.Builder(sym.subgroup((R1, 0), (R2, 2*R2)))
    added_sites = full_bulk.fill(bulk, lambda site: True,
                                 bulk.sites(), max_sites=float('inf'))
    # Wrap the y direction - along the armchair edge
    lead = kwant.wraparound.wraparound(full_bulk, keep=0)
    if plot_sys:
        kwant.plot(lead)
    if finalize:
        return lead.finalized()
    else:
        return lead
    
    
def rec_lattice_vectors(wrapped, three_d=False):
    """Get the reciprocal lattice vectors.
    If we use a 3D lattice (three_d = True), we ignore
    the lowest row of the resulting matrix (the nonexistant
    z coordinates of the reciprocal vectors)."""
    # columns of B are lattice vectors
    B = np.array(wrapped._wrapped_symmetry.periods).T
    # columns of A are reciprocal lattice vectors
    A = B.dot(np.linalg.inv(B.T.dot(B)))
    if not three_d:
        return A
    else:
        return A[:-1, :]


def momentum_to_lattice(k, A):
    """Columns of A are the reciprocal lattice vectors in units of 2pi.
       k is an array of momenta in the orthogonal basis (kx and ky)
       
       Returns the momenta along the reciprocal lattice directions"""
    k, residuals = scipy.linalg.lstsq(A, k)[:2]
    if np.any(abs(residuals) > 1e-7):
        raise RuntimeError("Requested momentum doesn't correspond"
                            " to any lattice momentum.")
    return k
    

def extend_unit_cell(lead_builder):
    """Takes a kwant.Builder with 1D translational symmetry and hopping to distant unit cells,
    and returns a new kwant.Builder with the same symmetry but a larger unit cell, such that only
    neighbouring cells are coupled. """
    
    sym = lead_builder.symmetry
    H = lead_builder.H
    # Get the range of the hopping
    try:
        hop_range = max(abs(sym.which(hopping[1])[0])
                        for hopping in lead_builder.hoppings())
    except ValueError:  # if there are no hoppings max() will raise
        hop_range = 0
    # Make the new lead builder with an extended unit cell
    if hop_range > 1:
        new_lead = kwant.Builder(sym.subgroup((hop_range,)))
        new_lead.fill(lead_builder, lambda site: True,
                      lead_builder.sites(), max_sites=float('inf'))
        new_lead_builder = new_lead
        sym = new_lead_builder.symmetry
        new_hop_range = max(abs(sym.which(hopping[1])[0])
                            for hopping in new_lead_builder.hoppings())
        # Make sure we have the correct hopping range
        assert new_hop_range == 1
        H = new_lead_builder.H
        if not new_lead_builder.H:
            raise ValueError('Lead contains no sites.')
    return new_lead_builder

########################################################################

def surface_ldos(flead, params, erg, eta = 5*1e-6, error=False):
    """Compute the density of states at the edge of a half-plane with a single armchair edge,
    using an eigenmode decomposition.
    
    This implementation is not numerically stable."""
    h, t = flead.cell_hamiltonian(params=params), flead.inter_cell_hopping(params=params)
    one = np.eye(len(h))
    zer = np.zeros_like(h)
    t = np.hstack([t, zer[:, t.shape[1]:]])
    # Modes
    A = np.bmat([[-t, zer], [zer, one]])
    B = np.bmat([[h-(erg+1j*eta)*np.eye(len(h)), t.T.conj()], [one, zer]])
    evals, evecs = scipy.linalg.eig(A, B)
    # The small broadening makes all modes evanescent, and we pick
    # out the right movers.
    right_movers = np.abs(evals) < 1
    U, lamU = evecs[:len(evecs)//2, right_movers], evecs[len(evecs)//2:, right_movers]
    # Issue here is stability - the condition number of U is huge
    if error:
        eps = np.finfo(float).eps
        cond = np.linalg.cond(U)
        G0 = scipy.linalg.inv((erg+1j*eta)*np.eye(len(h)) - h - t.T.conj().dot(lamU).dot(scipy.linalg.inv(U)))
        return -1/np.pi*np.imag(np.trace(G0)), cond*eps
    else:
        G0 = scipy.linalg.inv((erg+1j*eta)*np.eye(len(h)) - h - t.T.conj().dot(lamU).dot(scipy.linalg.inv(U)))
        return -1/np.pi*np.imag(np.trace(G0))


def surface_ldos_stable(flead, params, erg, eta = 5*1e-6, error=False):
    """Compute the density of states at the edge of a half-plane with a single armchair edge,
    using a Schur factorization of the mode eigenvalue problem.
    
    This implementation is numerically stable."""
    
    h, t = flead.cell_hamiltonian(params=params), flead.inter_cell_hopping(params=params)
    one = np.eye(len(h))
    zer = np.zeros_like(h)
    t = np.hstack([t, zer[:, t.shape[1]:]])
    A = np.bmat([[-t, zer], [zer, one]])
    B = np.bmat([[h-(erg+1j*eta)*np.eye(len(h)), t.T.conj()], [one, zer]])
    # The small broadening makes all modes evanescent, and we pick
    # out the right movers (eigenvalues inside the unit circle)
    AA, BB, alpha, beta, Q, Z = scipy.linalg.ordqz(A, B, sort='iuc', output='complex')
    Zs = Z.shape
    Z11 = Z[:Zs[0]//2, :Zs[1]//2]
    Z21 = Z[Zs[0]//2:, :Zs[1]//2]
    if error:
        eps = np.finfo(float).eps
        cond = np.linalg.cond(Z11)
        G0 = scipy.linalg.inv((erg+1j*eta)*np.eye(len(h)) - h - t.T.conj().dot(Z21).dot(scipy.linalg.inv(Z11)))
        return -1/np.pi*np.imag(np.trace(G0)), eps*cond
    else:
        G0 = scipy.linalg.inv((erg+1j*eta)*np.eye(len(h)) - h - t.T.conj().dot(Z21).dot(scipy.linalg.inv(Z11)))
        return -1/np.pi*np.imag(np.trace(G0))
    

def modes(h_cell, h_hop, tol=1e6):
    """Compute the eigendecomposition of a translation operator of a lead.

    Adapted from kwant.physics.leads.modes such that it returns the
    eigenvalues.

    Parameters:
    ----------
    h_cell : numpy array, real or complex, shape (N, N) The unit cell
        Hamiltonian of the lead unit cell.
    h_hop : numpy array, real or complex, shape (N, M)
        The hopping matrix from a lead cell to the one on which self-energy
        has to be calculated (and any other hopping in the same direction).
    tol : float
        Numbers and differences are considered zero when they are smaller
        than `tol` times the machine precision.

    Returns
    -------
    ev : numpy array
        Eigenvalues of the translation operator in the form lambda=exp(i*k).
    """
    m = h_hop.shape[1]
    n = h_cell.shape[0]

    if (h_cell.shape[0] != h_cell.shape[1] or
            h_cell.shape[0] != h_hop.shape[0]):
        raise ValueError("Incompatible matrix sizes for h_cell and h_hop.")

    # Note: np.any(h_hop) returns (at least from numpy 1.6.1 - 1.8-devel)
    #       False if h_hop is purely imaginary
    if not (np.any(h_hop.real) or np.any(h_hop.imag)):
        v = np.empty((0, m))
        return (kwant.physics.PropagatingModes(np.empty((0, n)), np.empty((0,)),
                                               np.empty((0,))),
                kwant.physics.StabilizedModes(np.empty((0, 0)),
                                              np.empty((0, 0)), 0, v))

    # Defer most of the calculation to helper routines.
    matrices, v, extract = kwant.physics.leads.setup_linsys(
        h_cell, h_hop, tol, None)
    ev = kwant.physics.leads.unified_eigenproblem(*(matrices + (tol,)))[0]
    return ev


def slowest_evan_mode(lead, params):
    """Find the slowest decaying (evanescent) mode.

    It uses an adapted version of the function kwant.physics.leads.modes,
    in such a way that it returns the eigenvalues of the translation operator
    (lamdba = e^ik). The imaginary part of the wavevector k, is the part that
    makes it decay. The inverse of this Im(k) is the size of a Majorana bound
    state. The norm of the eigenvalue that is closest to one is the slowest
    decaying mode. Also called decay length.

    Parameters:
    -----------
    lead : kwant.builder.InfiniteSystem object
        The finalized infinite system.
    p : tools.SimpleNamespace object
        A simple container that is used to store Hamiltonian parameters.

    Returns:
    --------
    majorana_length : float
        The length of the Majorana.
    """
    h, t = lead.cell_hamiltonian(params=params), lead.inter_cell_hopping(params=params)
    ev = modes(h, t)
    # index of the slowest mode by closenest to the unit circle
    idx = np.abs(np.abs(ev) - 1).argmin()

#     majorana_length = np.abs(a / np.log(ev[idx]).real)
#     majorana_length = np.abs(p.a / np.log(np.abs(ev[idx])))
    majorana_length = np.abs(1 / np.log(np.abs(ev[idx]))) # In units of the lattice constant
    return majorana_length

