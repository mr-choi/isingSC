# Tight binding parameters for transition metal dichalcogenides
# Taken from http://journals.aps.org/prb/pdf/10.1103/PhysRevB.92.205108
# Fang et al., PRB 92, 205108 (2015)

import numpy as np

# Helper functions, to keep indexing similar to the paper
# when declaring the tight binding Hamiltonian matrix elements
def TB(alpha, i, j):
    return TB_par[alpha-1, i, j]

def delta(alpha):
    return deltas[alpha-1]

a = 3.18   # Lattice constant [Å]
dxx = 3.13 # Spacing between top/bottom X atoms [Å]

####### Lattice vectors ######################################
# Assume that the momenta are scaled in terms of the lattice constant a
a1 = np.array([1, 0]) # x and y components
a2 = np.array([-0.5, np.sqrt(3.)/2])
# List of the delta vectors: delta[i] is the vector delta_(i-1)
deltas = [a1,
          a1+a2,
          a2,
          -(2*a1+a2)/3.,
          (a1+2*a2)/3.,
          (a1-a2)/3.,
          -2*(a1+2*a2)/3.,
          2*(2*a1+a2)/3.,
          2*(a2-a1)/3.]

# Reciprocal lattice vectors, units of lattice constant
b1 = 2*np.pi*np.array([1, 1/np.sqrt(3.)])
b2 = np.array([0, 4*np.pi/np.sqrt(3.)])
# K point
Kp = (2*b1 - b2)/3.

##############################################################

# Tight binding parameters
#### Independent parameters - change these for different materials ########
#---- MoS2 ---------------
# [eV]
epsilon = [1.0688,
           1.0688,
           -0.7755,
           -1.2902,
           -1.2902,
           -0.1380,
           0.0874,
           0.0874,
           -2.8949,
           -1.9065,
           -1.9065]
epsilon = [1.0349,
           1.0349,
           -0.9573,
           -1.3937,
           -1.3937,
           -0.1667,
           0.0984,
           0.0984,
           -3.3642,
           -2.1820,
           -2.1820]
# [eV]
TB_par = np.zeros((6, 11, 11))
TB_par[0, 0, 0] = -0.1395 #-0.2069
TB_par[0, 1, 1] = 0.0129 #0.0323
TB_par[0, 2, 2] = -0.2171 #-0.1739
TB_par[0, 3, 3] = 0.9763 #0.8651
TB_par[0, 4, 4] = -0.1985 #-0.1872
TB_par[0, 5, 5] = -0.3330 #-0.2979
TB_par[0, 6, 6] = 0.3190 #0.2747
TB_par[0, 7, 7] = -0.5837 #-0.5581
TB_par[0, 8, 8] = -0.2399 #-0.1916
TB_par[0, 9, 9] = 1.0470 #0.9122
TB_par[0, 10, 10] = 0.0029 #0.0059
TB_par[0, 2, 4] = -0.0912 #-0.0679
TB_par[0, 5, 7] = 0.4233 #0.4096
TB_par[0, 8, 10] = -0.0377 #0.0075
TB_par[0, 0, 1] = -0.2321 #-0.2562
TB_par[0, 2, 3] = -0.0797 #-0.0995
TB_par[0, 3, 4] = -0.0920 #-0.0705
TB_par[0, 5, 6] = -0.1250 #-0.1145
TB_par[0, 6, 7] = -0.2456 #-0.2487
TB_par[0, 8, 9] = 0.1857 #0.1063
TB_par[0, 9, 10] = -0.1027 #-0.0385
TB_par[4, 3, 0] = -0.7744 #-0.7883
TB_par[4, 2, 1] = -1.4014 #-1.3790
TB_par[4, 4, 1] = 2.0858 #2.1584
TB_par[4, 8, 5] = -0.8998 #-0.8836
TB_par[4, 10, 5] = -0.9044 #-0.9402
TB_par[4, 9, 6] = 1.4030 #1.4114
TB_par[4, 8, 7] = -0.8548 #-0.9535
TB_par[4, 10, 7] = 0.5711 #0.6517
TB_par[5, 8, 5] = -0.0676 #-0.0686
TB_par[5, 10, 5] = -0.1608 #-0.1498
TB_par[5, 8, 7] = -0.2618 #-0.2205
TB_par[5, 10, 7] = -0.2424 #-0.2451
#------------------------------

#############################################################

# Other dependent parameters
# Last line of equation A1 in the paper
TB_par[3, 8, 5] = TB_par[4, 8, 5]
TB_par[3, 9, 5] = -np.sqrt(3.)/2*TB_par[4, 10, 5]
TB_par[3, 10, 5] = -0.5*TB_par[4, 10, 5]

def TB_comb_1(alpha, beta, gamma, TB_par):
    """alpha, beta and gamma should be the same indices as in the paper, i.e. not shifted"""
    # Equation A1 in the paper
    alpha -= 1
    beta -= 1
    if gamma is not None:
        gamma -= 1
    TB_par[1, alpha, alpha] = 0.25*TB_par[0, alpha, alpha] + 0.75*TB_par[0, beta, beta]
    TB_par[1, beta, beta] = 0.75*TB_par[0, alpha, alpha] + 0.25*TB_par[0, beta, beta]
    if gamma is not None:
        TB_par[1, gamma, gamma] = TB_par[0, gamma, gamma]
        TB_par[1, gamma, beta] = np.sqrt(3.)/2*TB_par[0, gamma, alpha] - 0.5*TB_par[0, gamma, beta]
        TB_par[2, gamma, beta] = -np.sqrt(3.)/2*TB_par[0, gamma, alpha] - 0.5*TB_par[0, gamma, beta]
    TB_par[1, alpha, beta] = np.sqrt(3.)/4*(TB_par[0, alpha, alpha] - TB_par[0, beta, beta]) - TB_par[0, alpha, beta]
    TB_par[2, alpha, beta] = -np.sqrt(3.)/4*(TB_par[0, alpha, alpha] - TB_par[0, beta, beta]) - TB_par[0, alpha, beta]
    if gamma is not None:
        TB_par[1, gamma, alpha] = 0.5*TB_par[0, gamma, alpha] + np.sqrt(3.)/2*TB_par[0, gamma, beta]
        TB_par[2, gamma, alpha] = 0.5*TB_par[0, gamma, alpha] - np.sqrt(3.)/2*TB_par[0, gamma, beta]
    return TB_par

def TB_comb_2(alpha, alphap, beta, betap, gammap, TB_par):
    """alpha, beta and gamma should be the same indices as in the paper, i.e. not shifted"""
    # Equation A2 in the paper, excluding the last line
    alpha -= 1
    alphap -= 1
    beta -= 1
    betap -= 1
    gammap -= 1
    TB_par[3, alphap, alpha] = 0.25*TB_par[4, alphap, alpha] + 0.75*TB_par[4, betap, beta]
    TB_par[3, betap, beta] = 0.75*TB_par[4, alphap, alpha] + 0.25*TB_par[4, betap, beta]
    TB_par[3, betap, alpha] = -np.sqrt(3.)/4*TB_par[4, alphap, alpha] + np.sqrt(3.)/4*TB_par[4, betap, beta]
    TB_par[3, alphap, beta] = -np.sqrt(3.)/4*TB_par[4, alphap, alpha] + np.sqrt(3.)/4*TB_par[4, betap, beta]
    TB_par[3, gammap, alpha] = -np.sqrt(3.)/2*TB_par[4, gammap, beta]
    TB_par[3, gammap, beta] = -0.5*TB_par[4, gammap, beta]
    return TB_par

# Add parameters of Eq. A1
# List of indices (alpha, beta, gamma)
indices1 = []
indices1.append((1, 2, None))
indices1.append((4, 5, 3))
indices1.append((7, 8, 6))
indices1.append((10, 11, 9))
for (alpha, beta, gamma) in indices1:
    TB_par = TB_comb_1(alpha, beta, gamma, TB_par)
    
# Add parameters of Eq. A2
# List of indices (alpha, alphap, beta, betap, gammap)
indices2 = [(1, 4, 2, 5, 3),
            (7, 10, 8, 11, 9)]
for (alpha, alphap, beta, betap, gammap) in indices2:
    TB_par = TB_comb_2(alpha, alphap, beta, betap, gammap, TB_par)
