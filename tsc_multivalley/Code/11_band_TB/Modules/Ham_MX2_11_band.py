# See PHYSICAL REVIEW B 92, 205108 (2015)

import numpy as np
import itertools as it

from numpy import cos, sin
from scipy.linalg import block_diag

import Modules.pauli as pauli
from Modules.TB_parameters import TB, delta, epsilon

# Chiral operator
C = np.kron(np.kron(pauli.sy, pauli.sx), np.diag([-1 for _ in range(5)] + [1 for _ in range(6)]))
# Mirror symmetry M2, x -> -x
M2 = np.kron(np.kron(pauli.sz, pauli.sx), np.diag([-1, 1, 1, -1, 1, 1, -1, 1, 1, -1, 1]))
# 2D particle-hole symmetry, kx, ky -> -kx, -ky
P = np.kron(pauli.sx, np.eye(22))


def energies(kx, ky, p, extended_hopping=True, spin=True, SC=False):
    """Return the energies at the point (kx, ky), in units of
    the lattice constant a. """
    Ham = full_Hamiltonian(kx, ky, p, extended_hopping=extended_hopping, spin=spin, SC=SC)
    return np.linalg.eigh(Ham)[0]
    
def eigvecs(kx, ky, p, extended_hopping=True, spin=True, SC=False):
    """Return the eigenvectors at the point (kx, ky), in units of
    the lattice constant a."""
    Ham = full_Hamiltonian(kx, ky, p, extended_hopping=extended_hopping, spin=spin, SC=SC)
    return np.linalg.eigh(Ham)[1]

def Ham(kx, ky, extended_hopping=True):
    """Orbital Bloch Hamiltonian at momentum kx, ky. """
    H = np.zeros((11, 11), dtype=complex)
    k = np.array([kx, ky])
    # Indices
    inds = (list(zip(range(11), range(11))) +
           [(2,4), (5,7), (8,10),
            (0,1), (2,3), (3,4), (5,6), (6,7), (8,9), (9,10),
            (2,0), (4,0), (3,1), (9,5), (8,6), (10,6), (9,7),
            (3,0), (2,1), (4,1), (8,5), (10,5), (9,6), (8,7), (10,7),
            (8,5), (10,5), (9,5), (8,7), (8,6), (9,6), (10,6), (10,7),
            ])
    # Eliminate duplicates
    inds = list(set(inds))
    for i, j in inds:
        H[i, j] += H_matrix_element(i, j, k, extended_hopping=extended_hopping)
        if i != j:
            H[j, i] += H[i, j].conj()
    return H
    

def H_matrix_element(i, j, k, extended_hopping=True):
    """Construct the matrix elements of the Bloch Hamiltonian"""
    
    # Matrix elements of the minimal TB Hamiltonian.
    # Diagonal elements
    ele = 0
    if i == j:
        ele += (epsilon[i] + 2*TB(1, i, i)*cos(k.dot(delta(1))) + 
                2*TB(2, i, i)*(cos(k.dot(delta(2))) + cos(k.dot(delta(3))))
               )
        
    # MM and XX, even under M2
    if (i+1, j+1) in [(3,5), (6,8), (9,11)]:
        ele += (2*TB(1, i, j)*cos(k.dot(delta(1))) +
                TB(2, i, j)*(np.exp(-1j*k.dot(delta(2))) + np.exp(-1j*k.dot(delta(3)))) +
                TB(3, i, j)*(np.exp(1j*k.dot(delta(2))) + np.exp(1j*k.dot(delta(3))))
               )
    
    # MM and XX, odd under M2
    if (i+1, j+1) in [(1,2), (3,4), (4,5), (6,7), (7,8), (9,10), (10,11)]:
        ele += (-2j*TB(1, i, j)*sin(k.dot(delta(1))) + 
                TB(2, i, j)*(np.exp(-1j*k.dot(delta(2))) - np.exp(-1j*k.dot(delta(3)))) +
                TB(3, i, j)*(-np.exp(1j*k.dot(delta(2))) + np.exp(1j*k.dot(delta(3))))
               )

    # MX (and XM), even under M2
    if (i+1, j+1) in [(3,1), (5,1), (4,2), (10,6), (9,7), (11,7), (10,8)]:
        ele += TB(4, i, j)*(np.exp(1j*k.dot(delta(4))) - np.exp(1j*k.dot(delta(6))))

    # MX (and XM), odd under M2
    if (i+1, j+1) in [(4,1), (3,2), (5,2), (9,6), (11,6), (10,7), (9,8), (11,8)]:
        ele += (TB(4, i, j)*(np.exp(1j*k.dot(delta(4))) + np.exp(1j*k.dot(delta(6)))) + 
                TB(5, i, j)*np.exp(1j*k.dot(delta(5)))
               )
    
    # Corrections due to extended hopping, between M and next-nearest X atoms
    if extended_hopping:
        if (i+1, j+1) == (9, 6):
            ele += TB(6, i, j)*(np.exp(1j*k.dot(delta(7))) + np.exp(1j*k.dot(delta(8))) +
                                np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) == (11, 6):
            ele += TB(6, i, j)*(np.exp(1j*k.dot(delta(7))) - 0.5*np.exp(1j*k.dot(delta(8))) -
                                0.5*np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) == (10, 6):
            ele += np.sqrt(3.)/2*TB(6, 11-1, 6-1)*(-np.exp(1j*k.dot(delta(8))) + np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) == (9, 8):
            ele += TB(6, i, j)*(np.exp(1j*k.dot(delta(7))) - 0.5*np.exp(1j*k.dot(delta(8))) -
                                0.5*np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) == (9, 7):
            ele += np.sqrt(3.)/2*TB(6, 9-1, 8-1)*(-np.exp(1j*k.dot(delta(8))) + np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) == (10, 7):
            ele += 0.75*TB(6, 11-1, 8-1)*(np.exp(1j*k.dot(delta(8))) + np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) in [(11,7), (10,8)]:
            ele += np.sqrt(3.)/4*TB(6, 11-1, 8-1)*(np.exp(1j*k.dot(delta(8))) - np.exp(1j*k.dot(delta(9))))
        if (i+1, j+1) == (11, 8):
            ele += TB(6, i, j)*(np.exp(1j*k.dot(delta(7))) + 0.25*np.exp(1j*k.dot(delta(8))) +
                                0.25*np.exp(1j*k.dot(delta(9))))
    return ele

def full_Hamiltonian(kx, ky, p, extended_hopping=True, spin=True, SC=False):
    """Bloch Hamiltonian at (kx, ky) with everything included: orbital, spin and SC. """
    H = Ham(kx, ky, extended_hopping=extended_hopping) - p.mu*np.eye(11)
    if spin:
        H_SOI = SOI_Ham(lambda_M=p.lambda_M, lambda_X=p.lambda_X)
        H_Z = Zeeman_Ham(Ex=p.Ex, Ey=p.Ey)
        H = np.kron(pauli.s0, H) + H_SOI + H_Z
        # For now, only include SC if spin is included as well
        if SC:
            H_neg_k = Ham(-kx, -ky, extended_hopping=extended_hopping) - p.mu*np.eye(11)
            H_neg_k = np.kron(pauli.s0, H_neg_k) + H_SOI + H_Z
            H = block_diag(H, -H_neg_k.conj()) + SC_coupling(Delta=p.Delta)
    return H

def SOI_Ham(lambda_M = 0.0836, lambda_X = 0.0556):
    """Hamiltonian for SOI. Default parameters are for MoS2."""
    # Block of even spin up, even spin up
    Meeuu = np.zeros((11, 11), dtype=complex)
    Meeuu[6, 7] = 1j*lambda_M
    Meeuu[7, 6] = Meeuu[6, 7].conj()
    Meeuu[9, 10] = -0.5j*lambda_X
    Meeuu[10, 9] = Meeuu[9, 10].conj()
    # Block of even spin down, even spin down
    Meedd = -Meeuu
    # Block of odd spin up, odd spin up
    Moouu = np.zeros((11, 11), dtype=complex)
    Moouu[0, 1] = -0.5j*lambda_M
    Moouu[1, 0] = Moouu[0, 1].conj()
    Moouu[3, 4] = -0.5j*lambda_X
    Moouu[4, 3] = Moouu[3, 4].conj()
    # Block of odd spin down, odd spin down
    Moodd = -Moouu
    # Block of even spin up, odd spin down
    Meoud = np.zeros((11, 11), dtype=complex)
    Meoud[5, 0] = -np.sqrt(3.)/2*lambda_M 
    Meoud[5, 1] = 1j*np.sqrt(3.)/2*lambda_M
    Meoud[6, 0] = -0.5j*lambda_M
    Meoud[6, 1] = 0.5*lambda_M
    Meoud[7, 0] = 0.5*lambda_M
    Meoud[7, 1] = 0.5j*lambda_M
    Meoud[8, 3] = -0.5*lambda_X
    Meoud[8, 4] = 0.5j*lambda_X
    Meoud[9, 2] = 0.5*lambda_X
    Meoud[10, 2] = -0.5j*lambda_X
    # Block of odd spin down, even spin up
    Moedu = Meoud.T.conj()
    # Block of even spin down, odd spin up
    Meodu = np.zeros((11, 11), dtype=complex)
    Meodu[5, 0] = np.sqrt(3.)/2*lambda_M
    Meodu[5, 1] = 1j*np.sqrt(3.)/2*lambda_M
    Meodu[6, 0] = -0.5j*lambda_M
    Meodu[6, 1] = -0.5*lambda_M
    Meodu[7, 0] = -0.5*lambda_M
    Meodu[7, 1] = 0.5j*lambda_M
    Meodu[8, 3] = 0.5*lambda_X
    Meodu[8, 4] = 0.5j*lambda_X
    Meodu[9, 2] = -0.5*lambda_X
    Meodu[10, 2] = -0.5j*lambda_X
    # Block of odd spin up, even spin down
    Moeud = Meodu.T.conj()
    
    # Construct the SOI Hamiltonian
    H_SOI = block_diag(Moouu + Meeuu, Moodd + Meedd)
    H_SOI[:11, 11:] = Moeud + Meoud
    H_SOI[11:, :11] = Moedu + Meodu
    
    return H_SOI

def SC_coupling(Delta=0.2e-3, N=11):
    """Returns the Hamiltonian that describes the superconducting coupling
    in the BdG Hamiltonian. Delta is the pairing potential in eV.
    N is the number of orbitals without spin."""
    # The inner Kronecker product is the SC coupling in spin space, the outer one
    # between the electron and hole blocks
    SC_Ham = np.kron(pauli.sy, np.kron(pauli.sy, np.eye(N)))
    return Delta*SC_Ham

def Zeeman_Ham(Ex=0, Ey=0, N=11):
    """Zeeman Hamiltonian for an in-plane magnetic field. Ex and Ez in eV.
    N is the number of basis orbitals (11 spinless), included for testing."""
    return np.kron(Ex*pauli.sx + Ey*pauli.sy, np.eye(N))