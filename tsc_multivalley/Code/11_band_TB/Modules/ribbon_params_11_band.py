import numpy as np
import Modules.TB_parameters as Tpar
from Modules.TB_parameters import TB
import Modules.Ham_MX2_11_band as Ham
import Modules.pauli as pauli

#======== Onsites =========================
##### M atom ##########
M_ons = np.array([[Tpar.epsilon[0], 0, 0, 0, 0],
                  [0, Tpar.epsilon[1], 0, 0, 0],
                  [0, 0, Tpar.epsilon[5], 0, 0],
                  [0, 0, 0, Tpar.epsilon[6], 0],
                  [0, 0, 0, 0, Tpar.epsilon[7]]
                 ])
##### X atom ##########
X_ons = np.array([[Tpar.epsilon[2], 0, 0, 0, 0, 0],
                  [0, Tpar.epsilon[3], 0, 0, 0, 0],
                  [0, 0, Tpar.epsilon[4], 0, 0, 0],
                  [0, 0, 0, Tpar.epsilon[8], 0, 0],
                  [0, 0, 0, 0, Tpar.epsilon[9], 0],
                  [0, 0, 0, 0, 0, Tpar.epsilon[10]]
                 ])

#======== Hoppings ========================
##### From M to X, NN hoppings. #########
# Along the vector delta_4
NN_T_XM_1 = np.array([[TB(4, 2, 0), TB(4, 2, 1), 0, 0, 0],
                      [TB(4, 3, 0), TB(4, 3, 1), 0, 0, 0],
                      [TB(4, 4, 0), TB(4, 4, 1), 0, 0, 0],
                      [0, 0, TB(4, 8, 5), TB(4, 8, 6), TB(4, 8, 7)],
                      [0, 0, TB(4, 9, 5), TB(4, 9, 6), TB(4, 9, 7)],
                      [0, 0, TB(4, 10, 5), TB(4, 10, 6), TB(4, 10, 7)]
                     ])
# Along the vector delta_6
NN_T_XM_2 = np.array([[-TB(4, 2, 0), TB(4, 2, 1), 0, 0, 0],
                      [TB(4, 3, 0), -TB(4, 3, 1), 0, 0, 0],
                      [-TB(4, 4, 0), TB(4, 4, 1), 0, 0, 0],
                      [0, 0, TB(4, 8, 5), -TB(4, 8, 6), TB(4, 8, 7)],
                      [0, 0, -TB(4, 9, 5), TB(4, 9, 6), -TB(4, 9, 7)],
                      [0, 0, TB(4, 10, 5), -TB(4, 10, 6), TB(4, 10, 7)]
                     ])
# Along the vector delta_5
NN_T_XM_3 = np.array([[0, TB(5, 2, 1), 0, 0, 0],
                      [TB(5, 3, 0), 0, 0, 0, 0],
                      [0, TB(5, 4, 1), 0, 0, 0],
                      [0, 0, TB(5, 8, 5), 0, TB(5, 8, 7)],
                      [0, 0, 0, TB(5, 9, 6), 0],
                      [0, 0, TB(5, 10, 5), 0, TB(5, 10, 7)]
                     ])
##### From M to X, NNN hoppings. #########
# Along the vector delta_7
NNN_T_XM_3 = np.array([[0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0],
                       [0, 0, TB(6, 8, 5), 0, TB(6, 8, 7)],
                       [0, 0, 0, 0, 0],
                       [0, 0, TB(6, 10, 5), 0, TB(6, 10, 7)]
                      ])
# Along the vector delta_9
NNN_T_XM_1 = np.array([[0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0],
                       [0, 0, TB(6, 8, 5), np.sqrt(3)/2.*TB(6, 8, 7), -1./2*TB(6, 8, 7)],
                       [0, 0, np.sqrt(3)/2.*TB(6, 10, 5), 3./4*TB(6, 10, 7), -np.sqrt(3)/4.*TB(6, 10, 7)],
                       [0, 0, -1./2*TB(6, 10, 5), -np.sqrt(3)/4.*TB(6, 10, 7), 1/4.*TB(6, 10, 7)]
                      ])
# Along the vector delta_8
NNN_T_XM_2 = np.array([[0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0],
                       [0, 0, TB(6, 8, 5), -np.sqrt(3)/2.*TB(6, 8, 7), -1./2*TB(6, 8, 7)],
                       [0, 0, -np.sqrt(3)/2.*TB(6, 10, 5), 3./4*TB(6, 10, 7), np.sqrt(3)/4.*TB(6, 10, 7)],
                       [0, 0, -1./2*TB(6, 10, 5), np.sqrt(3)/4.*TB(6, 10, 7), 1/4.*TB(6, 10, 7)]
                      ])
##### From M to M, NN hoppings. #########
# Along the vector delta_1
NN_T_MM_1 = np.array([[TB(1, 0, 0), TB(1, 0, 1), 0, 0, 0],
                      [-TB(1, 0, 1), TB(1, 1, 1), 0, 0, 0],
                      [0, 0, TB(1, 5, 5), TB(1, 5, 6), TB(1, 5, 7)],
                      [0, 0, -TB(1, 5, 6), TB(1, 6, 6), TB(1, 6, 7)],
                      [0, 0, TB(1, 5, 7), -TB(1, 6, 7), TB(1, 7, 7)]
                     ])
# Along the vector delta_2
NN_T_MM_2 = np.array([[TB(2, 0, 0), TB(2, 0, 1), 0, 0, 0],
                      [-TB(3, 0, 1), TB(2, 1, 1), 0, 0, 0],
                      [0, 0, TB(2, 5, 5), TB(2, 5, 6), TB(2, 5, 7)],
                      [0, 0, -TB(3, 5, 6), TB(2, 6, 6), TB(2, 6, 7)],
                      [0, 0, TB(3, 5, 7), -TB(3, 6, 7), TB(2, 7, 7)]
                     ])
# Along the vector delta_3
NN_T_MM_3 = np.array([[TB(2, 0, 0), -TB(2, 0, 1), 0, 0, 0],
                      [TB(3, 0, 1), TB(2, 1, 1), 0, 0, 0],
                      [0, 0, TB(2, 5, 5), -TB(2, 5, 6), TB(2, 5, 7)],
                      [0, 0, TB(3, 5, 6), TB(2, 6, 6), -TB(2, 6, 7)],
                      [0, 0, TB(3, 5, 7), TB(3, 6, 7), TB(2, 7, 7)]
                     ])
##### From X to X, NN hoppings. #########
# Along the vector delta_1
NN_T_XX_1 = np.array([[TB(1, 2, 2), TB(1, 2, 3), TB(1, 2, 4), 0, 0, 0],
                      [-TB(1, 2, 3), TB(1, 3, 3), TB(1, 3, 4), 0, 0, 0],
                      [TB(1, 2, 4), -TB(1, 3, 4), TB(1, 4, 4), 0, 0, 0],
                      [0, 0, 0, TB(1, 8, 8), TB(1, 8, 9), TB(1, 8, 10)],
                      [0, 0, 0, -TB(1, 8, 9), TB(1, 9, 9), TB(1, 9, 10)],
                      [0, 0, 0, TB(1, 8, 10), -TB(1, 9, 10), TB(1, 10, 10)]
                     ])
# Along the vector delta_2
NN_T_XX_2 = np.array([[TB(2, 2, 2), TB(2, 2, 3), TB(2, 2, 4), 0, 0, 0],
                      [-TB(3, 2, 3), TB(2, 3, 3), TB(2, 3, 4), 0, 0, 0],
                      [TB(3, 2, 4), -TB(3, 3, 4), TB(2, 4, 4), 0, 0, 0],
                      [0, 0, 0, TB(2, 8, 8), TB(2, 8, 9), TB(2, 8, 10)],
                      [0, 0, 0, -TB(3, 8, 9), TB(2, 9, 9), TB(2, 9, 10)],
                      [0, 0, 0, TB(3, 8, 10), -TB(3, 9, 10), TB(2, 10, 10)]
                     ])
# Along the vector delta_3
NN_T_XX_3 = np.array([[TB(2, 2, 2), -TB(2, 2, 3), TB(2, 2, 4), 0, 0, 0],
                      [TB(3, 2, 3), TB(2, 3, 3), -TB(2, 3, 4), 0, 0, 0],
                      [TB(3, 2, 4), TB(3, 3, 4), TB(2, 4, 4), 0, 0, 0],
                      [0, 0, 0, TB(2, 8, 8), -TB(2, 8, 9), TB(2, 8, 10)],
                      [0, 0, 0, TB(3, 8, 9), TB(2, 9, 9), -TB(2, 9, 10)],
                      [0, 0, 0, TB(3, 8, 10), TB(3, 9, 10), TB(2, 10, 10)]
                     ])
#=========================================

##### Separate X p-orbitals into top and bottom layers ########
# Tight binding orbitals are even/odd linear combinations of p orbitals
# on the X atoms in top and bottom layers. Separate into constituents
# by layer.
# Block structure in the new basis is
#  [AA, AB
#   BA, BB]
# where A and B are top and bottom layers.
# UX acts only in the subspace of X orbitals.
# H_new_basis = U.T.conj().dot(H).dot(U)
UX = 1/np.sqrt(2) * np.array([[1, 0, 0, 1, 0, 0],
                              [0, 1, 0, 0, 1, 0],
                              [0, 0, 1, 0, 0, 1],
                              [1, 0, 0, -1, 0, 0],
                              [0, -1, 0, 0, 1, 0],
                              [0, 0, -1, 0, 0, 1]]).T


##### Construct the spin orbit interaction in the Kwant atomic basis ########

def basis_change_mat():
    """This matrix changes from the odd-even basis in the paper to the [M, X] ordered
       basis that we use in Kwant. """
    perm_inds = [(0,0), (1,1), (2,5), (3,6), (4,7), (5,2), (6,3), (7,4), (8,8), (9,9), (10,10)]
    basis_change = np.zeros((11, 11), dtype=complex)
    for (i, j) in perm_inds:
        basis_change[i,j] = 1
    return basis_change


def SOI_atomic_basis(SOI_Ham, XAXB=False):
    """Return the onsite SOI couplings for the M and X atoms. Input
    is the SOI Hamiltonian in the original 11 band basis.
    
    XAXB specifies whether or not the basis separates XA and XB atoms.
    """
    
    # Change to the new basis
    U = np.kron(pauli.s0, basis_change_mat())
    nSOI = U.dot(SOI_Ham).dot(U.T.conj())
    
    # Pick out relevant parts of the SOI Hamiltonian.
    # All blocks not taken here (those that couple orbitals
    # on different atoms) vanish.
    MuMu = nSOI[:5, :5]
    MuMd = nSOI[:5, 11:16]
    MdMu = nSOI[11:16, 0:5]
    MdMd = nSOI[11:16, 11:16]
    M_SOI = np.bmat([[MuMu, MuMd], [MdMu, MdMd]])
    assert np.allclose(M_SOI, M_SOI.T.conj())
    
    XuXu = nSOI[5:11, 5:11]
    XuXd = nSOI[5:11, 16:]
    XdXu = nSOI[16:, 5:11]
    XdXd = nSOI[16:, 16:]
    X_SOI = np.bmat([[XuXu, XuXd], [XdXu, XdXd]])
    assert np.allclose(X_SOI, X_SOI.T.conj())
    
    if not XAXB:
        # If XA and XB atoms are not separated, we're done.
        return M_SOI, X_SOI
    else:
        # If XA and XB atoms are separated, we need SOI
        # for each atom. The M block remains unchanged.
        nUX = np.kron(pauli.s0, UX)
        nX_SOI = nUX.T.conj().dot(X_SOI).dot(nUX) # Separate A and B atoms.
        # Onsite SOI of XA atom
        AuAu = nX_SOI[:3, :3]
        AuAd = nX_SOI[:3, 6:9]
        AdAu = nX_SOI[6:9, :3]
        AdAd = nX_SOI[6:9, 6:9]
        XA_SOI = np.bmat([[AuAu, AuAd], [AdAu, AdAd]])
        # Onsite SOI of XB atom
        BuBu = nX_SOI[3:6, 3:6]
        BuBd = nX_SOI[3:6, 9:12]
        BdBu = nX_SOI[9:12, 3:6]
        BdBd = nX_SOI[9:12, 9:12]
        XB_SOI = np.bmat([[BuBu, BuBd], [BdBu, BdBd]])
        # The atoms are identical, so their onsite SOI should be the same
        assert np.allclose(XA_SOI, XB_SOI)
        assert np.allclose(XA_SOI, XA_SOI.T.conj())
        # Also check possible SOI hopping terms between A and B atoms.
        # Blocks of SOI hopping from A to B
        BuAu = nX_SOI[3:6, :3]
        BuAd = nX_SOI[3:6, 6:9]
        BdAu = nX_SOI[9:12, :3]
        BdAd = nX_SOI[9:12, 6:9]
        # Blocks of SOI hopping from B to A
        AuBu = nX_SOI[:3, 3:6]
        AuBd = nX_SOI[:3, 9:12]
        AdBu = nX_SOI[6:9, 3:6]
        AdBd = nX_SOI[6:9, 9:12]
        SOI_hop_blocks = [BuAu, BuAd, BdAu, BdAd, AuBu, AuBd, AdBu, AdBd]
        # All the blocks vanish, so we can neglect hopping due to SOI between
        # the A and B atoms.
        assert all([np.allclose(block, 0) for block in SOI_hop_blocks])
        return M_SOI, XA_SOI, XB_SOI
        
        
        
        