# -*- coding: utf-8 -*-
import numpy as np;
import scipy.linalg as linalg;
import scipy.optimize as optimize;
import matplotlib.pyplot as plt;
import json;
from datetime import datetime;
from multiprocessing import Pool;

def brentGlobalMinimize(f, a, b, M, tol):
    """
    Globally minimize `f` between `a` and `b`, given the second derivative of `f` is at most `M`.
    the tolerance `tol` is a small positive number that indicates how precisely the minimum has to be pinpointed.
    """
    fa = f(a); fb = f(b);
    (fmin, xmin) = (fb, b) if fb < fa else (fa, a);
    if M <= 0: return xmin;
    (fa2, a2) = (fa, a);
    while a2 < b:
        a3 = np.minimum(b, a2 + np.sqrt(2*(fa2 - fmin + tol)/M));
        fa3 = f(a3);
        if fa3 < fmin: (fmin, xmin) = (fa3, a3);
        Pbottom = (16*(a2-a3)*(2*(-a3*fa2 + a2*fa3) + (a2 - a3)*a2*a3*M) - 3*(2*(fa2 - fa3) + (a3**2 - a2**2)*M)**2/M)/(32*(a2-a3)**2);
        while Pbottom < fmin - tol:
            a3 = (a2 + a3)/2;
            fa3 = f(a3);
            if fa3 < fmin: (fmin, xmin) = (fa3, a3);
            Pbottom = (16*(a2-a3)*(2*(-a3*fa2 + a2*fa3) + (a2 - a3)*a2*a3*M) - 3*(2*(fa2 - fa3) + (a3**2 - a2**2)*M)**2/M)/(32*(a2-a3)**2);
        (fa2, a2) = (fa3, a3);
    return xmin;

class TMDmonolayerSC:
    def __init__(self, material="MoS2"):
        """
        Initialize TMD monolayer by retrieving meterial parameters from json data file.
        """
        # Open file:
        with open("material_params.json") as f:
            kpparams = json.load(f)[material]

        # Store them in class variables:
        self.a = kpparams["A"];
        self.b = kpparams["B"];
        self.c = kpparams["C"];
        self.d = kpparams["D"];

    def e(self, kx, ky):
        """Spin independent part of normal phase Hamiltonian."""
        return self.a*(kx*kx + ky*ky);

    def f(self, kx, ky, eta):
        """Spin dependent part of normal phase Hamiltonian."""
        return eta*self.b + eta*self.c*(kx*kx + ky*ky) + self.d*(kx**3 - 3*kx*ky**2);

    def HBdG(self, kx, ky, eta, s, mu):
        """
        The positive eigenvalues of the BdG Hamiltonian when external field and pairing is set to zero.
        """
        return np.abs(np.abs(self.e(kx, ky) - mu) + s*np.abs(self.f(kx, ky, eta)));

    def find_kmax(self, Emax):
        """Guess a `kmax` given the maximum energy."""
        k0max = np.sqrt((Emax + np.abs(self.b))/(self.a - self.c));
        return k0max*np.sqrt(1 - 2*k0max*self.d/(self.a - self.c));

    def create_kgrid(self, muMax, hwcMax, res):
        """Generate a grid of `k` points with resolution `res`."""
        kmax = self.find_kmax(muMax + hwcMax);
        kxs = kys = np.linspace(0.0, kmax, res);
        return np.meshgrid(kxs, kys, indexing='ij');

    def choose_ks(self, kxgrid, kygrid, mu, hwc):
        """
        Obtain the appropriate `ks` from the grid.
        """
        withinCutoffUp = self.HBdG(kxgrid, kygrid, 1, 1, mu) <= hwc;
        withinCutoffDown = self.HBdG(kxgrid, kygrid, 1, -1, mu) <= hwc;
        extraBounds = np.logical_and(kygrid >= 0, kygrid <= kxgrid*np.sqrt(3));
        
        nearFermiUp = np.logical_and(withinCutoffUp, extraBounds);
        nearFermiDown = np.logical_and(withinCutoffDown, extraBounds);
        return np.array([kxgrid[nearFermiUp], kygrid[nearFermiUp]]).transpose(), np.array([kxgrid[nearFermiDown], kygrid[nearFermiDown]]).transpose();

    def generate_ks(self, mu, hwc, res):
        """Generate appropriate ks given the chemcial potential and cutoff."""
        kxgrid, kygrid = self.create_kgrid(mu, hwc, res);
        return self.choose_ks(kxgrid, kygrid, mu, hwc);

    def Vint(self, mu, ksplus, ksmin, Delta):
        """Calculate the attractive interaction between Cooper pairs."""
        spf = self.HBdG(ksplus[:,0], ksplus[:,1], 1, 1, mu);
        smf = self.HBdG(ksmin[:,0], ksmin[:,1], 1, -1, mu);
        return 4 / ( np.sum(1/np.sqrt(spf**2 + Delta**2)) + np.sum(1/np.sqrt(smf**2 + Delta**2)) );

    def FreeEnergy(self, mu, ksplus, ksmin, Delta, Hext, T, V):
        """Obtain the free energy times the interaction V."""
        # Spin (in)dependent energies in case of s = +1:
        Esqplus = (self.e(ksplus[:,0], ksplus[:,1]) - mu)**2;
        Fsqplus = (self.f(ksplus[:,0], ksplus[:,1], 1))**2;
        # Spin (in)dependent energies in case of s = -1:
        Esqmin = (self.e(ksmin[:,0], ksmin[:,1]) - mu)**2;
        Fsqmin = (self.f(ksmin[:,0], ksmin[:,1], 1))**2;
        # Eigenvalues:
        sqrtplus = np.sqrt(Esqplus + Fsqplus + Hext**2 + Delta**2 + 2*np.sqrt(Esqplus*(Fsqplus + Hext**2) + Delta**2 * Hext**2));
        sqrtmin = np.sqrt(Esqmin + Fsqmin + Hext**2 + Delta**2 - 2*np.sqrt(Esqmin*(Fsqmin + Hext**2) + Delta**2 * Hext**2));
        # For very small temperatures, make use of the zero temperature expression to avoid overflow errors:
        plusfilter = sqrtplus >= 2*T*7e2;
        minfilter = sqrtmin >= 2*T*7e2;
        return Delta**2 - V*np.sum(sqrtplus[plusfilter])/2 - V*np.sum(sqrtmin[minfilter])/2 - \
               V*T*np.sum(np.log(2*np.cosh(sqrtplus[np.logical_not(plusfilter)]/(2*T)))) - V*T*np.sum(np.log(2*np.cosh(sqrtmin[np.logical_not(minfilter)]/(2*T))));

    def findDelta(self, mu, ksplus, ksmin, Delta0, Hext, T, V, reltol=1e-11):
        """
        Find the superconducting pairing that globally minimizes the free energy using Brents global minimization algorithm.
        `reltol` is the relative tolerance parameter that should be passed into the global minimization algorithm.
        """
        def F(Delta):
            return self.FreeEnergy(mu, ksplus, ksmin, Delta, Hext, T, V);

        ks = np.array(list(set(tuple(x) for x in ksmin).difference(tuple(x) for x in ksplus)));
        absE = np.abs(self.e(ks[:,0], ks[:,1]) - mu);
        upperBoundDeriv = 2 + V*np.sum(1/absE)/2;
        return brentGlobalMinimize(F, 0.0, Delta0, upperBoundDeriv, reltol);

    def findBc(self, mu, Delta0, ksplus, ksmin, V, T=0.0, minfactor=0.0, maxfactor=100.0, reltol=1e-11, transition=1e-6):
        """
        Find the critical field.
        `maxfactor` is the maximum number times `Delta0` that the critical field can be.
        `reltol` is the relative tolerance parameter that should be passed into the global minimization algorithm.
        `transition` indicates that the superconducting phase is considered vanished when `Delta` is dropped at `transition * Delta0`. 
        
        """
        def theDeltaMinSmall(Hext):
            return self.findDelta(mu, ksplus, ksmin, Delta0, Hext, T, V, reltol=reltol) - Delta0*transition;
        if theDeltaMinSmall(0.0) <= 0.0: return 0.0;
        else: return optimize.brentq(theDeltaMinSmall, minfactor*Delta0, maxfactor*Delta0);

    def Bcgrid(self, muMin, muMax, Delta0Min, Delta0Max, res, T=0.0, nMu=50, nDelta0=50, hwcDeltaRatio=20.0, minfactor=0.0, maxfactor=100.0, reltol=1e-11, transition=1e-2, pauliUnits=True):
        """
        Obtain an array of critical fields as function of `mu` and `Delta0` at fixed temperature.
        `nMu` and `nDelta0` indicate the number of data point along the `mu` and `Delta0` axis.
        `hwcDeltaRatio` indicates how many times larger `hwc` should be with respect to `Delta0`.
        `PauliUnits` indicates whether the grid is contains `Bc` in units of Pauli limit. Otherwise it will return the raw value.
        The other optional parameters are passed into the `findBc` method.
        """
        mus = np.linspace(muMin, muMax, nMu);
        Delta0s = np.linspace(Delta0Min, Delta0Max, nDelta0);
        mugrid, Delta0grid = np.meshgrid(mus, Delta0s, indexing='ij');
        Bcgrid = np.zeros((nMu, nDelta0));

        for i in range(nMu):
            mu = mus[i];
            for j in range(nDelta0):
                print("Currently evaluating at ({},{})...".format(i,j), end='\r');
                Delta0 = Delta0s[j];
                hwc = Delta0 * hwcDeltaRatio;
                ksplus, ksmin = self.generate_ks(mu, hwc, res);
                V = self.Vint(mu, ksplus, ksmin, Delta0);
                Bcgrid[i,j] = self.findBc(mu, Delta0, ksplus, ksmin, V, T=T, minfactor=minfactor, maxfactor=maxfactor, reltol=reltol, transition=transition);

        if pauliUnits:
            Bcgrid = np.sqrt(2)*Bcgrid/Delta0grid;
        return Bcgrid, mugrid, Delta0grid;

    def Bcgrid_constDelta0(self, muMin, muMax, Tmin, Tmax, res, Delta0=1e-3, nMu=50, nT=50, hwcDeltaRatio=20.0, minfactor=0.0, maxfactor=100.0, reltol=1e-11, transition=1e-2, pauliUnits=True):
        """
        Obtain an array of critical fields as function of `mu` and `T` at fixed `Delta0`.
        `nMu` and `nT` indicate the number of data point along the `mu` and `T` axis.
        `hwcDeltaRatio` indicates how many times larger `hwc` should be with respect to `Delta0`.
        `PauliUnits` indicates whether the grid is contains `Bc` in units of Pauli limit. Otherwise it will return the raw value.
        The other optional parameters are passed into the `findBc` method.
        """
        mus = np.linspace(muMin, muMax, nMu);
        Ts = np.linspace(Tmin, Tmax, nT);
        mugrid, Tgrid = np.meshgrid(mus, Ts, indexing='ij');
        Bcgrid = np.zeros((nMu, nT));

        hwc = Delta0 * hwcDeltaRatio;

        for i in range(nMu):
            mu = mus[i];
            ksplus, ksmin = self.generate_ks(mu, hwc, res);
            V = self.Vint(mu, ksplus, ksmin, Delta0);
            for j in range(nT):
                print("Currently evaluating at ({},{})".format(i,j), end="\r");
                T = Ts[j];
                Bcgrid[i,j] = self.findBc(mu, Delta0, ksplus, ksmin, V, T=T, minfactor=minfactor, maxfactor=maxfactor, reltol=reltol, transition=transition);

        if pauliUnits:
            Bcgrid = np.sqrt(2)*Bcgrid/Delta0;
        return Bcgrid, mugrid, Tgrid;
