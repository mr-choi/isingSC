import hpc05;
import TMDmonolauerSuperconductor;
import numpy as np;

def HcDiagram_clusters(self, muMin, muMax, Delta0Min, Delta0Max, res, hwcDeltaRatio=15.0, nMu=32, nDelta0=32, nCores=100, maxfactor=100.0):
        "Plot the critical field diagram by searcing from which field delta will vanish."
        mus, dmu = np.linspace(muMin, muMax, nMu, retstep=True);
        Delta0s, dDelta0 = np.linspace(Delta0Min, Delta0Max, nDelta0, retstep=True);
        mugrid, Delta0grid = np.meshgrid(mus, Delta0s, indexing='ij');
        muflatgrid = np.reshape(mugrid, (nMu*nDelta0,)); Delta0flatgrid = np.reshape(Delta0grid, (nMu*nDelta0,));

        # Method to run parallel:
        def calcHc(mu, Delta0):
            hwc = Delta0 * hwcDeltaRatio;
            ksplus, ksmin = self.generate_ks(mu, hwc, res);
            V = self.Vint(mu, ksplus, ksmin, Delta0);
            try:
                return np.log( np.sqrt(2)*self.findHc(mu, Delta0, ksplus, ksmin, V, maxfactor=maxfactor)/Delta0 );
            except:
                return float('nan');

        client, dview, lview = hpc05.start_remote_and_connect(nCores, folder="~/isingsc/code");
        async_obj = lview.map_async(calcHc, muflatgrid, Delta0flatgrid);
        async_obj.wait_interactive();
        Hcs = np.reshape(async_obj.get(), (nMu, nDelta0));
        hpc05.kill_remote_ipcluster();

        # Plot the figure:
        fig = plt.figure();
        ax = fig.add_subplot(111, xlim=[muMin*1000,muMax*1000], ylim=[Delta0Min*1000,Delta0Max*1000]);
        ax.tick_params(labelsize=17);
        ax.set_xlabel("Chemical potential [meV]", size=17);
        ax.set_ylabel(r"$\Delta_0$ [meV]", size=17);
        muExtgrid, Delta0Extgrid = np.meshgrid( np.append(mus, muMax + dmu) - dmu/2, np.append(Delta0s, Delta0Max + dDelta0) - dDelta0/2, indexing='ij');
        quadmesh = ax.pcolormesh(muExtgrid*1000, Delta0Extgrid*1000, Hcs, rasterized=True, antialiased=True);
        cbar = fig.colorbar(quadmesh, ax=ax);
        #quadcontourmesh = ax.contourf(mugrid*1000, Delta0grid*1000, Hcs);
        #cbar = fig.colorbar(quadcontourmesh, ax=ax);
        cbar.ax.tick_params(labelsize=17);
        cbar.set_label(r"$\log(H_c / H_p)$", size=17);
        return fig;

def HcDiagramAbsolute_clusters(self, muMin, muMax, Delta0Min, Delta0Max, res, hwcDeltaRatio=15.0, nMu=32, nDelta0=32, nCores=100, maxfactor=100.0, T=0.0):
        "Plot the critical field diagram by searching from which field delta will vanish. This version plots the actual critical field"
        mus, dmu = np.linspace(muMin, muMax, nMu, retstep=True);
        Delta0s, dDelta0 = np.linspace(Delta0Min, Delta0Max, nDelta0, retstep=True);
        mugrid, Delta0grid = np.meshgrid(mus, Delta0s, indexing='ij');
        muflatgrid = np.reshape(mugrid, (nMu*nDelta0,)); Delta0flatgrid = np.reshape(Delta0grid, (nMu*nDelta0,));

        # Method to run parallel:
        def calcHc(mu, Delta0):
            hwc = Delta0 * hwcDeltaRatio;
            ksplus, ksmin = self.generate_ks(mu, hwc, res);
            V = self.Vint(mu, ksplus, ksmin, Delta0);
            try:
                return 1000*self.findHc(mu, Delta0, ksplus, ksmin, V, maxfactor=maxfactor, T=T);
            except:
                return float('nan');

        client, dview, lview = hpc05.start_remote_and_connect(nCores, folder="~/isingsc/code");
        async_obj = lview.map_async(calcHc, muflatgrid, Delta0flatgrid);
        async_obj.wait_interactive();
        Hcs = np.reshape(async_obj.get(), (nMu, nDelta0));
        hpc05.kill_remote_ipcluster();

        # Dump the critical fields in a json file
        with open("results/Hcs_{}.json".format(datetime.now()), 'w+') as f:
            json.dump(Hcs.tolist(), f);

        # Plot the figure:
        fig = plt.figure();
        ax = fig.add_subplot(111, xlim=[muMin*1000,muMax*1000], ylim=[Delta0Min*1000,Delta0Max*1000]);
        ax.tick_params(labelsize=17);
        ax.set_xlabel("Chemical potential [meV]", size=17);
        ax.set_ylabel(r"$\Delta_0$ [meV]", size=17);
        muExtgrid, Delta0Extgrid = np.meshgrid( np.append(mus, muMax + dmu) - dmu/2, np.append(Delta0s, Delta0Max + dDelta0) - dDelta0/2, indexing='ij');
        quadmesh = ax.pcolormesh(muExtgrid*1000, Delta0Extgrid*1000, Hcs, rasterized=True, antialiased=True);
        cbar = fig.colorbar(quadmesh, ax=ax);
        #quadcontourmesh = ax.contourf(mugrid*1000, Delta0grid*1000, Hcs);
        #cbar = fig.colorbar(quadcontourmesh, ax=ax);
        cbar.ax.tick_params(labelsize=17);
        cbar.set_label(r"$\mu_B H_c$ [meV]", size=17);
        return fig;