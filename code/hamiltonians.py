import json;
import numpy as np;

def FangkpHamiltonian(material="MoS2"):
    """
    Fang et al.'s k.p Hamiltonian (Phys. Rev. B 92, 205108 (2015)) with paramters for specified material.

    Parameters:
    -----------

    k - array_like with 3 elements
        The k vector with respect to K points.
        k[0] and k[1] represent kx and ky and k[2] represents eta.

    material - string
        The material for which the parameters should be retrieved.

    Returns:
    --------
    A callable Hamiltonian suitable for the Superconductor class.
    """
    # Retrieve k.p parameters and store them in variables:
    with open("data/fitparams/Fang_k.p.json") as f:
        kpparams = json.load(f)[material];

    Eg = kpparams["Eg"];
    t1 = kpparams["t1"];
    f2 = kpparams["f2"];
    f3 = kpparams["f3"];
    t3w = kpparams["t3w"];
    l_vb = kpparams["l_vb"];
    l_cb = kpparams["l_cb"];
    a = kpparams["a"];

    # Define the Pauli matrices + identity:
    I = np.array([[1, 0], [0, 1]]);
    X = np.array([[0, 1], [1, 0]]);
    Y = np.array([[0, -1j], [1j, 0]]);
    Z = np.array([[1, 0], [0, -1]]);

    # 1st order effective mass conduction band:
    mcb1 = 1/(a**2 * (f2+f3));

    # The Hamiltonian:
    def Ham(q):
        k = np.sqrt(q[0]**2 + q[1]**2);
        h = (k**2 / (2*mcb1))*I + l_cb*q[2]*Z + (t1**2*k**2*a**2 + 2*q[2]*t1*t3w*a**3*(q[0]**3 - 3*q[0]*q[1]**2))/Eg * (I + (l_vb-l_cb)*Z/Eg);
        return h;
    return Ham;
