import kwant;
import json;
#import tinyarray as ta;
import numpy as np;
import scipy.linalg as linalg;

def getLiuHamiltonian(material="MoS2", fitmethod="GGA"):
    """Get the Block diagonalized TB Hamiltonian given in Liu et al. Phys. Rev. B 88, 085433 (2013).

    Parameters:
    -----------
    material: string
        The material for which tight binding parameters have to be acquired (default: "MoS2").

    fitmethod: string
        The method of the DFT calculations to which the tight binding parameters have to be fitted (default: "GGA").

    Returns:
    --------
    The Hamiltonian as a callable function with alpha = kx*a/2, and beta = sqrt(3)*ky/2 as parameters (see also Liu et al.).
    """
    # Retrieve TB parameters and store them in variables:
    with open("data/fitparams/Liu" + fitmethod + ".json") as f:
        TBparams = json.load(f)[material];

    e1 = TBparams["e1"];
    e2 = TBparams["e2"];
    t0 = TBparams["t0"];
    t1 = TBparams["t1"];
    t2 = TBparams["t2"];
    t11 = TBparams["t11"];
    t12 = TBparams["t12"];
    t22 = TBparams["t22"];
    r0 = TBparams["r0"];
    r1 = TBparams["r1"];
    r2 = TBparams["r2"];
    r11 = TBparams["r11"];
    r12 = TBparams["r12"];
    u0 = TBparams["u0"];
    u1 = TBparams["u1"];
    u2 = TBparams["u2"];
    u11 = TBparams["u11"];
    u12 = TBparams["u12"];
    u22 = TBparams["u22"];

    # Define the Hamiltonian:
    def H(alpha, beta, g):
        # Onsite and the first order hoppings:
        h0 = 2*t0*(np.cos(2*alpha) + 2*np.cos(alpha)*np.cos(beta)) + e1;
        h1 = -2*np.sqrt(3)*t2*np.sin(alpha)*np.sin(beta) + 2j*t1*(np.sin(2*alpha) + np.sin(alpha)*np.cos(beta));
        h2 = 2*t2*(np.cos(2*alpha) - np.cos(alpha)*np.cos(beta)) + 2j*np.sqrt(3)*t1*np.cos(alpha)*np.sin(beta);
        h11 = 2*t11*np.cos(2*alpha) + (t11 + 3*t22)*np.cos(alpha)*np.cos(beta) + e2;
        h22 = 2*t22*np.cos(2*alpha) + (t22 + 3*t11)*np.cos(alpha)*np.cos(beta) + e2;
        h12 = np.sqrt(3)*(t22 - t11)*np.sin(alpha)*np.sin(beta) + 4j*t12*np.sin(alpha)*(np.cos(alpha) - np.cos(beta));

        # Add the SNN:
        h0 += 2*r0*(2*np.cos(3*alpha)*np.cos(beta) + np.cos(2*beta));
        h1 += 2*(r1 + r2)*np.sin(3*alpha)*np.sin(beta) + 2j*(r1 - r2)*np.sin(3*alpha)*np.cos(beta);
        h2 += -2/np.sqrt(3)*(r1 + r2)*(np.cos(3*alpha)*np.cos(beta) - np.cos(2*beta)) + 2j/np.sqrt(3)*np.sin(beta)*(r1 - r2)*(np.cos(3*alpha) + 2*np.cos(beta));
        h11 += 4*r11*np.cos(3*alpha)*np.cos(beta) + 2*(r11 + np.sqrt(3)*r12)*np.cos(2*beta);
        h22 += 2*r11*(2*np.cos(3*alpha)*np.cos(beta) + np.cos(2*beta)) + 2/np.sqrt(3)*r12*(4*np.cos(3*alpha)*np.cos(beta) - np.cos(2*beta));
        h12 += 4*r12*np.sin(3*alpha)*np.sin(beta);

        # add the TNN:
        h0 += 2*u0*(np.cos(4*alpha) + 2*np.cos(2*alpha)*np.cos(2*beta));
        h1 += -2*np.sqrt(3)*u2*np.sin(2*alpha)*np.sin(2*beta) + 2j*u1*(np.sin(4*alpha) + np.sin(2*alpha)*np.cos(2*beta));
        h2 += 2*u2*(np.cos(4*alpha) - np.cos(2*alpha)*np.cos(2*beta)) + 2j*np.sqrt(3)*u1*np.cos(2*alpha)*np.sin(2*beta);
        h11 += 2*u11*np.cos(4*alpha) + (u11 + 3*u22)*np.cos(2*alpha)*np.cos(2*beta);
        h22 += 2*u22*np.cos(4*alpha) + (u22 + 3*u11)*np.cos(2*alpha)*np.cos(2*beta);
        h12 += np.sqrt(3)*(u22 - u11)*np.sin(2*alpha)*np.sin(2*beta) + 4j*u12*np.sin(2*alpha)*(np.cos(2*alpha) - np.cos(2*beta));

        # Include SOI and return the Hamiltonian
        H0 = np.array([[h0, h1, h2], [np.conj(h1), h11, h12], [np.conj(h2), np.conj(h12), h22]]);
        Lz = np.array([[0,0,0],[0,0,1j],[0,-1j,0]]);
        Z = np.array([[1,0],[0,-1]]);
        return np.kron(np.eye(2), H0) + g*np.kron(Z, Lz);

    # Return the function:
    return H;

def newLiuTMDmonolayer(material="MoS2", fitmethod="GGA"):
    """This creates a normal phase TMD monolayer based on the 3-band TB model in Liu et al. Phys. Rev. B 88, 085433 (2013).

    Parameters:
    -----------
    material: string
        The material for which tight binding parameters have to be acquired (default: "MoS2").

    fitmethod: string
        The method of the DFT calculations to which the tight binding parameters have to be fitted (default: "GGA").

    Returns:
    --------
    A finalized kwant system.
    """
    # Retrieve TB parameters and store them in variables:
    with open("data/fitparams/Liu" + fitmethod + ".json") as f:
        TBparams = json.load(f)[material];

    e1 = TBparams["e1"];
    e2 = TBparams["e2"];
    t0 = TBparams["t0"];
    t1 = TBparams["t1"];
    t2 = TBparams["t2"];
    t11 = TBparams["t11"];
    t12 = TBparams["t12"];
    t22 = TBparams["t22"];
    r0 = TBparams["r0"];
    r1 = TBparams["r1"];
    r2 = TBparams["r2"];
    r11 = TBparams["r11"];
    r12 = TBparams["r12"];
    u0 = TBparams["u0"];
    u1 = TBparams["u1"];
    u2 = TBparams["u2"];
    u11 = TBparams["u11"];
    u12 = TBparams["u12"];
    u22 = TBparams["u22"];

    # Lattice of the system (note that we only have to consider orbitals of the transition metal according to Liu et al.):
    latt = kwant.lattice.triangular();

    # Building infinite 2D TMD monolayer system:
    sym = kwant.TranslationalSymmetry(latt.vec((1,0)), latt.vec((0,1)));
    TMDmonolayer = kwant.Builder(sym);

    # The hoppings to be considered:
    nn1 = kwant.HoppingKind((1,0), latt);
    nn2 = kwant.HoppingKind((0,1), latt);
    nn3 = kwant.HoppingKind((-1,1), latt);
    snn1 = kwant.HoppingKind((2,-1), latt);
    snn2 = kwant.HoppingKind((-1,2), latt);
    snn3 = kwant.HoppingKind((-1,-1), latt);
    tnn1 = kwant.HoppingKind((2,0), latt);
    tnn2 = kwant.HoppingKind((0,2), latt);
    tnn3 = kwant.HoppingKind((-2,2), latt);

    # Filling in values of TB parameters in the system:
    # Onsite Hamiltonian:
    onsite = np.array([[e1, 0, 0],
                      [0, e2, 0],
                      [0, 0, e2]]);

    # Nearest neighbors:
    # NN hopping 1:
    firstNN1 = np.array([[t0, t1, t2],
                        [-t1, t11, t12],
                        [t2, -t12, t22]]);

    # NN hopping 2:
    h01 = (np.sqrt(3)*t2 + t1)/2;
    h10 = (np.sqrt(3)*t2 - t1)/2;
    h02 = (np.sqrt(3)*t1 - t2)/2;
    h20 = (-np.sqrt(3)*t1 - t2)/2;
    h11 = (3*t22 + t11)/4;
    h12 = np.sqrt(3)*(t11 - t22)/4 - t12;
    h21 = np.sqrt(3)*(t11 - t22)/4 + t12;
    h22 = (3*t11 + t22)/4;

    firstNN2 = np.array([[t0, h01, h02],
                        [h10, h11, h12],
                        [h20, h21, h22]]);

    # NN hopping 3:
    h01 = (-np.sqrt(3)*t2 - t1)/2;
    h10 = (-np.sqrt(3)*t2 + t1)/2;
    h02 = (np.sqrt(3)*t1 - t2)/2;
    h20 = (-np.sqrt(3)*t1 - t2)/2;
    h11 = (3*t22 + t11)/4;
    h12 = np.sqrt(3)*(t22 - t11)/4 + t12;
    h21 = np.sqrt(3)*(t22 - t11)/4 - t12;
    h22 = (3*t11 + t22)/4;

    firstNN3 = np.array([[t0, h01, h02],
                        [h10, h11, h12],
                        [h20, h21, h22]]);

    # Second nearest neighbors
    # SNN hopping 1:
    h01 = r1;
    h10 = r2;
    h02 = -r1/np.sqrt(3);
    h20 = -r2/np.sqrt(3);
    h11 = r11;
    h12 = r12;
    h21 = r12;
    h22 = r22 = r11 + 2*r12/np.sqrt(3);

    secondNN1 = np.array([[r0, h01, h02],
                         [h10, h11, h12],
                         [h20, h21, h22]]);

    # SNN hopping 2:
    h01 = 0;
    h10 = 0;
    h02 = 2*r1/np.sqrt(3);
    h20 = 2*r2/np.sqrt(3);
    h11 = (3*r22 + r11)/4 + np.sqrt(3)*r12/2;
    h12 = 0;
    h21 = 0;
    h22 = (3*r11 + r22)/4 - np.sqrt(3)*r12/2;

    secondNN2 = np.array([[r0, h01, h02],
                         [h10, h11, h12],
                         [h20, h21, h22]]);

    # SNN hopping 3:
    h01 = -r1;
    h10 = -r2;
    h02 = -r1/np.sqrt(3);
    h20 = -r2/np.sqrt(3);
    h11 = (3*r22 + r11)/4 - np.sqrt(3)*r12/2;
    h12 = -r12;
    h21 = -r12;
    h22 = (3*r11 + r22)/4 + np.sqrt(3)*r12/2;

    secondNN3 = np.array([[r0, h01, h02],
                         [h10, h11, h12],
                         [h20, h21, h22]]);

    # Third nearest neighbors:
    # TNN hopping 1:
    thirdNN1 = np.array([[u0, u1, u2],
                        [-u1, u11, u12],
                        [u2, -u12, u22]]);

    # TNN hopping 2:
    h01 = (np.sqrt(3)*u2 + u1)/2;
    h10 = (np.sqrt(3)*u2 - u1)/2;
    h02 = (np.sqrt(3)*u1 - u2)/2;
    h20 = (-np.sqrt(3)*u1 - u2)/2;
    h11 = (3*u22 + u11)/4;
    h12 = np.sqrt(3)*(u11 - u22)/4 - u12;
    h21 = np.sqrt(3)*(u11 - u22)/4 + u12;
    h22 = (3*u11 + u22)/4;

    thirdNN2 = np.array([[u0, h01, h02],
                        [h10, h11, h12],
                        [h20, h21, h22]]);

    # TNN hopping 3:
    h01 = (-np.sqrt(3)*u2 - u1)/2;
    h10 = (-np.sqrt(3)*u2 + u1)/2;
    h02 = (np.sqrt(3)*u1 - u2)/2;
    h20 = (-np.sqrt(3)*u1 - u2)/2;
    h11 = (3*u22 + u11)/4;
    h12 = np.sqrt(3)*(u22 - u11)/4 + u12;
    h21 = np.sqrt(3)*(u22 - u11)/4 - u12;
    h22 = (3*u11 + u22)/4;

    thirdNN3 = np.array([[u0, h01, h02],
                        [h10, h11, h12],
                        [h20, h21, h22]]);

    # Define onsite hamiltonian with spin-orbit interaction:
    def onsiteSOI(pos, g):
        Lz = np.array([[0,0,0],[0,0,1j],[0,-1j,0]]);
        Z = np.array([[1,0],[0,-1]]);
        return np.kron(np.eye(2), onsite) + g*np.kron(Z, Lz);

    # Filling in the values:
    TMDmonolayer[latt(0,0)] = onsiteSOI;
    TMDmonolayer[nn1] = np.kron(np.eye(2), firstNN1);
    TMDmonolayer[nn2] = np.kron(np.eye(2), firstNN2);
    TMDmonolayer[nn3] = np.kron(np.eye(2), firstNN3);
    TMDmonolayer[snn1] = np.kron(np.eye(2), secondNN1);
    TMDmonolayer[snn2] = np.kron(np.eye(2), secondNN2);
    TMDmonolayer[snn3] = np.kron(np.eye(2), secondNN3);
    TMDmonolayer[tnn1] = np.kron(np.eye(2), thirdNN1);
    TMDmonolayer[tnn2] = np.kron(np.eye(2), thirdNN2);
    TMDmonolayer[tnn3] = np.kron(np.eye(2), thirdNN3);

    # Wrap around, finalize the system and return:
    return kwant.wraparound.wraparound(TMDmonolayer, coordinate_names=("1", "2")).finalized();

def newFangTMDmonolayer(material="MoS2", GWcorrection=False, includeSOI=False):
    """This creates a normal phase TMD monolayer based on Fang et al. Phys. Rev. B 92, 205108 (2015)

    Parameters:
    -----------
    material: string
        The material for which tight binding parameters have to be acquired (default: "MoS2").

    GWcorrection:
        Correct the fit parameters such that it agrees with GW calculations instead of DFT calculations (only available for MoS2 and disabled by default).

    includeSOI: boolean
        Whether or not to include spin-orbit interaction in the Hamiltonian (False by default).

    Returns:
    --------
    A finalized kwant system.
    """
    # Introduce correction factors if GWcorrection is enabled:
    if GWcorrection and material=="MoS2":
        deM = 0.3642;
        deX = -0.2512;
        MM_NN = 1.4209;
        XX_NN = 1.1738;
        XM_NN = 1.0773;
        XM_SNN = 1.1871;
    else:
        deM = 0;
        deX = 0;
        MM_NN = 1;
        XX_NN = 1;
        XM_NN = 1;
        XM_SNN = 1;

    # Retrieve TB parameters and store them in variables:
    with open("data/fitparams/Fang11band.json") as f:
        TBparams = json.load(f)[material];

    e1 = e2 = TBparams["e1"]+deM;
    e3 = TBparams["e3"]+deX;
    e4 = e5 = TBparams["e4"]+deX;
    e6 = TBparams["e6"]+deM;
    e7 = e8 = TBparams["e7"]+deM;
    e9 = TBparams["e9"]+deX;
    e10 = e11 = TBparams["e10"]+deX;
    t11 = TBparams["t11"]*MM_NN;
    t22 = TBparams["t22"]*MM_NN;
    t33 = TBparams["t33"]*XX_NN;
    t44 = TBparams["t44"]*XX_NN;
    t55 = TBparams["t55"]*XX_NN;
    t66 = TBparams["t66"]*MM_NN;
    t77 = TBparams["t77"]*MM_NN;
    t88 = TBparams["t88"]*MM_NN;
    t99 = TBparams["t99"]*XX_NN;
    t1010 = TBparams["t1010"]*XX_NN;
    t1111 = TBparams["t1111"]*XX_NN;
    t35 = TBparams["t35"]*XX_NN;
    t68 = TBparams["t68"]*MM_NN;
    t911 = TBparams["t911"]*XX_NN;
    t12 = TBparams["t12"]*MM_NN;
    t34 = TBparams["t34"]*XX_NN;
    t45 = TBparams["t45"]*XX_NN;
    t67 = TBparams["t67"]*MM_NN;
    t78 = TBparams["t78"]*MM_NN;
    t910 = TBparams["t910"]*XX_NN;
    t1011 = TBparams["t1011"]*XX_NN;
    u41 = TBparams["u41"]*XM_NN;
    u32 = TBparams["u32"]*XM_NN;
    u52 = TBparams["u52"]*XM_NN;
    u96 = TBparams["u96"]*XM_NN;
    u116 = TBparams["u116"]*XM_NN;
    u107 = TBparams["u107"]*XM_NN;
    u98 = TBparams["u98"]*XM_NN;
    u118 = TBparams["u118"]*XM_NN;
    r96 = TBparams["r96"]*XM_SNN;
    r116 = TBparams["r116"]*XM_SNN;
    r98 = TBparams["r98"]*XM_SNN;
    r118 = TBparams["r118"]*XM_SNN;
    lambdaM = TBparams["lambdaM"];
    lambdaX = TBparams["lambdaX"];

    # Create the lattice of the system:
    latt = kwant.lattice.honeycomb();
    X, M = latt.sublattices;

    # Build infinite 2D monolayer system:
    sym = kwant.TranslationalSymmetry(latt.vec((1,0)), latt.vec((0,1)));
    TMDmonolayer = kwant.Builder(sym);

    # The hoppings to be considered:
    NN_MM1 = kwant.HoppingKind((1,0), M, M); # delta1
    NN_MM2 = kwant.HoppingKind((0,1), M, M); # delta2
    NN_MM3 = kwant.HoppingKind((-1,1), M, M); # delta3
    NN_XX1 = kwant.HoppingKind((1,0), X, X); # delta1
    NN_XX2 = kwant.HoppingKind((0,1), X, X); # delta2
    NN_XX3 = kwant.HoppingKind((-1,1), X, X); # delta3
    NN_XM1 = kwant.HoppingKind((0,0), X, M); # delta5
    NN_XM2 = kwant.HoppingKind((0,-1), X, M); # delta4
    NN_XM3 = kwant.HoppingKind((1,-1), X, M); # delta6
    SNN_XM1 = kwant.HoppingKind((1,-2), X, M); # delta7
    SNN_XM2 = kwant.HoppingKind((1,0), X, M); # delta8
    SNN_XM3 = kwant.HoppingKind((-1,0), X, M); # delta9

    # The onsite Hamiltonians for the M and X atoms respectively:
    HMonsite = np.diag(np.array([e1, e2, e6, e7, e8]));
    HXonsite = np.diag(np.array([e3, e4, e5, e9, e10, e11]));

    # The M-M hoppings:
    # Hopping #1:
    odd = np.array([[t11, -t12],
                    [t12, t22]]);
    even = np.array([[t66, -t67, t68],
                     [t67, t77, -t78],
                     [t68, t78, t88]]);
    HMM1 = linalg.block_diag(odd, even);

    # Hopping #2:
    h11 = (3*t22 + t11)/4;
    h12 = np.sqrt(3)*(t11 - t22)/4 + t12;
    h21 = np.sqrt(3)*(t11 - t22)/4 - t12;
    h22 = (3*t11 + t22)/4;
    odd = np.array([[h11, h12],
                    [h21, h22]]);

    h11 = t66;
    h12 = (-t67 + np.sqrt(3)*t68)/2;
    h21 = (t67 + np.sqrt(3)*t68)/2;
    h13 = (-t68 - np.sqrt(3)*t67)/2;
    h31 = (-t68 + np.sqrt(3)*t67)/2;
    h22 = (3*t88 + t77)/4;
    h23 = np.sqrt(3)*(t77 - t88)/4 + t78;
    h32 = np.sqrt(3)*(t77 - t88)/4 - t78;
    h33 = (3*t77 + t88)/4;
    even = np.array([[h11, h12, h13],
                     [h21, h22, h23],
                     [h31, h32, h33]]);

    HMM2 = linalg.block_diag(odd, even);

    # Hopping #3:
    h11 = (3*t22 + t11)/4;
    h12 = np.sqrt(3)*(t22 - t11)/4 - t12;
    h21 = np.sqrt(3)*(t22 - t11)/4 + t12;
    h22 = (3*t11 + t22)/4;
    odd = np.array([[h11, h12],
                    [h21, h22]]);

    h11 = t66;
    h12 = (t67 - np.sqrt(3)*t68)/2;
    h21 = (-t67 - np.sqrt(3)*t68)/2;
    h13 = (-t68 - np.sqrt(3)*t67)/2;
    h31 = (-t68 + np.sqrt(3)*t67)/2;
    h22 = (3*t88 + t77)/4;
    h23 = np.sqrt(3)*(t88 - t77)/4 - t78;
    h32 = np.sqrt(3)*(t88 - t77)/4 + t78;
    h33 = (3*t77 + t88)/4;
    even = np.array([[h11, h12, h13],
                     [h21, h22, h23],
                     [h31, h32, h33]]);

    HMM3 = linalg.block_diag(odd, even);

    # The X-X hoppings:
    # Hopping #1:
    odd = np.array([[t33, -t34, t35],
                    [t34, t44, -t45],
                    [t35, t45, t55]]);
    even = np.array([[t99, -t910, t911],
                     [t910, t1010, -t1011],
                     [t911, t1011, t1111]]);
    HXX1 = linalg.block_diag(odd, even);

    # Hopping #2:
    h11 = t33;
    h12 = (-t34 + np.sqrt(3)*t35)/2;
    h21 = (t34 + np.sqrt(3)*t35)/2;
    h13 = (-t35 - np.sqrt(3)*t34)/2;
    h31 = (-t35 + np.sqrt(3)*t34)/2;
    h22 = (3*t55 + t44)/4;
    h23 = np.sqrt(3)*(t44 - t55)/4 + t45;
    h32 = np.sqrt(3)*(t44 - t55)/4 - t45;
    h33 = (3*t44 + t55)/4;
    odd = np.array([[h11, h12, h13],
                    [h21, h22, h23],
                    [h31, h32, h33]]);

    h11 = t99;
    h12 = (-t910 + np.sqrt(3)*t911)/2;
    h21 = (t910 + np.sqrt(3)*t911)/2;
    h13 = (-t911 - np.sqrt(3)*t910)/2;
    h31 = (-t911 + np.sqrt(3)*t910)/2;
    h22 = (3*t1111 + t1010)/4;
    h23 = np.sqrt(3)*(t1010 - t1111)/4 + t1011;
    h32 = np.sqrt(3)*(t1010 - t1111)/4 - t1011;
    h33 = (3*t1010 + t1111)/4;
    even = np.array([[h11, h12, h13],
                     [h21, h22, h23],
                     [h31, h32, h33]]);

    HXX2 = linalg.block_diag(odd, even);

    # Hopping #3:
    h11 = t33;
    h12 = (t34 - np.sqrt(3)*t35)/2;
    h21 = (-t34 - np.sqrt(3)*t35)/2;
    h13 = (-t35 - np.sqrt(3)*t34)/2;
    h31 = (-t35 + np.sqrt(3)*t34)/2;
    h22 = (3*t55 + t44)/4;
    h23 = np.sqrt(3)*(t55 - t44)/4 - t45;
    h32 = np.sqrt(3)*(t55 - t44)/4 + t45;
    h33 = (3*t44 + t55)/4;
    odd = np.array([[h11, h12, h13],
                    [h21, h22, h23],
                    [h31, h32, h33]]);

    h11 = t99;
    h12 = (t910 - np.sqrt(3)*t911)/2;
    h21 = (-t910 - np.sqrt(3)*t911)/2;
    h13 = (-t911 - np.sqrt(3)*t910)/2;
    h31 = (-t911 + np.sqrt(3)*t910)/2;
    h22 = (3*t1111 + t1010)/4;
    h23 = np.sqrt(3)*(t1111 - t1010)/4 - t1011;
    h32 = np.sqrt(3)*(t1111 - t1010)/4 + t1011;
    h33 = (3*t1010 + t1111)/4;
    even = np.array([[h11, h12, h13],
                     [h21, h22, h23],
                     [h31, h32, h33]]);

    HXX3 = linalg.block_diag(odd, even);

    # The X-M hoppings:
    # Hopping #1:
    odd = np.array([[0, u32],
                    [u41, 0],
                    [0, u52]]);
    even = np.array([[u96, 0, u98],
                     [0, u107, 0],
                     [u116, 0, u118]]);
    HXM1 = linalg.block_diag(odd, even);

    # Hopping #2:
    h11 = -np.sqrt(3)*u32/2;
    h12 = -u32/2;
    h21 = (u41 + 3*u52)/4;
    h22 = np.sqrt(3)*(u52 - u41)/4;
    h31 = np.sqrt(3)*(u52 - u41)/4;
    h32 = (3*u41 + u52)/4;
    odd = np.array([[h11, h12],
                    [h21, h22],
                    [h31, h32]]);

    h11 = u96;
    h12 = -np.sqrt(3)*u98/2;
    h13 = -u98/2;
    h21 = -np.sqrt(3)*u116/2;
    h22 = (u107 + 3*u118)/4;
    h23 = np.sqrt(3)*(u118 - u107)/4;
    h31 = -u116/2;
    h32 = np.sqrt(3)*(u118 - u107)/4;
    h33 = (3*u107 + u118)/4;
    even = np.array([[h11, h12, h13],
                     [h21, h22, h23],
                     [h31, h32, h33]]);

    HXM2 = linalg.block_diag(odd, even);

    # Hopping #3:
    h11 = np.sqrt(3)*u32/2;
    h12 = -u32/2;
    h21 = (u41 + 3*u52)/4;
    h22 = np.sqrt(3)*(u41 - u52)/4;
    h31 = np.sqrt(3)*(u41 - u52)/4;
    h32 = (3*u41 + u52)/4;
    odd = np.array([[h11, h12],
                    [h21, h22],
                    [h31, h32]]);

    h11 = u96;
    h12 = np.sqrt(3)*u98/2;
    h13 = -u98/2;
    h21 = np.sqrt(3)*u116/2;
    h22 = (u107 + 3*u118)/4;
    h23 = np.sqrt(3)*(u107 - u118)/4;
    h31 = -u116/2;
    h32 = np.sqrt(3)*(u107 - u118)/4;
    h33 = (3*u107 + u118)/4;
    even = np.array([[h11, h12, h13],
                     [h21, h22, h23],
                     [h31, h32, h33]]);

    HXM3 = linalg.block_diag(odd, even);

    # Additional SNN X-M hoppings:
    odd = np.zeros((3,2)); # The SNN X-M do not involve the odd hoppings.
    # Hopping #1:
    even = np.array([[r96, 0, r98],
                     [0, 0, 0],
                     [r116, 0, r118]]);
    H2XM1 = linalg.block_diag(odd, even);

    # Hopping #2:
    even = np.array([[r96, -np.sqrt(3)*r98/2, -r98/2],
                     [-np.sqrt(3)*r116/2, 3*r118/4, np.sqrt(3)*r118/4],
                     [-r116/2, np.sqrt(3)*r118/4, r118/4]]);
    H2XM2 = linalg.block_diag(odd, even);

    # Hopping #3:
    even = np.array([[r96, np.sqrt(3)*r98/2, -r98/2],
                     [np.sqrt(3)*r116/2, 3*r118/4, -np.sqrt(3)*r118/4],
                     [-r116/2, -np.sqrt(3)*r118/4, r118/4]]);
    H2XM3 = linalg.block_diag(odd, even);

    # Include SOI if enabled:
    if includeSOI:
        # The spin x, y, and z matrices:
        Sx = np.array([[0, 1], [1, 0]])/2;
        Sy = np.array([[0, -1j], [1j, 0]])/2;
        Sz = np.array([[1, 0], [0, -1]])/2;

        # Angular momentum matrices for hybridized p orbitals:
        Lpx = [[0, 0, 0, 0, 0, 1j],
               [0, 0, 0, 0, 0, 0],
               [0, 0, 0, -1j, 0, 0],
               [0, 0, 1j, 0, 0, 0],
               [0, 0, 0, 0, 0, 0],
               [-1j, 0, 0, 0, 0, 0]];
        Lpy = [[0, 0, 0, 0, -1j, 0],
               [0, 0, 0, 1j, 0, 0],
               [0, 0, 0, 0, 0, 0],
               [0, -1j, 0, 0, 0, 0],
               [1j, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0]];
        Lpz = [[0, 0, 0, 0, 0, 0],
               [0, 0, -1j, 0, 0, 0],
               [0, 1j, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, -1j],
               [0, 0, 0, 0, 1j, 0]];

        # Angular momentum matrices for d orbitals:
        Ldx = [[0, 0, 0, 1j, 0],
               [0, 0, -1j*np.sqrt(3), 0, -1j],
               [0, 1j*np.sqrt(3), 0, 0, 0],
               [-1j, 0, 0, 0, 0],
               [0, 1j, 0, 0, 0]];
        Ldy = [[0, 0, 1j*np.sqrt(3), 0, -1j],
               [0, 0, 0, -1j, 0],
               [-1j*np.sqrt(3), 0, 0, 0, 0],
               [0, 1j, 0, 0, 0],
               [1j, 0, 0, 0, 0]];
        Ldz = [[0, -1j, 0, 0, 0],
               [1j, 0, 0, 0, 0],
               [0, 0, 0, 0, 0],
               [0, 0, 0, 0, 2j],
               [0, 0, 0, -2j, 0]];

        # Include the SOI at the onsite energies:
        HXonsite = np.kron(np.eye(2), HXonsite) + lambdaX * ( np.kron(Sx, Lpx) + np.kron(Sy, Lpy) + np.kron(Sz, Lpz) );
        HMonsite = np.kron(np.eye(2), HMonsite) + lambdaM * ( np.kron(Sx, Ldx) + np.kron(Sy, Ldy) + np.kron(Sz, Ldz) );
        HMM1 = np.kron(np.eye(2), HMM1);
        HMM2 = np.kron(np.eye(2), HMM2);
        HMM3 = np.kron(np.eye(2), HMM3);
        HXX1 = np.kron(np.eye(2), HXX1);
        HXX2 = np.kron(np.eye(2), HXX2);
        HXX3 = np.kron(np.eye(2), HXX3);
        HXM1 = np.kron(np.eye(2), HXM1);
        HXM2 = np.kron(np.eye(2), HXM2);
        HXM3 = np.kron(np.eye(2), HXM3);
        H2XM1 = np.kron(np.eye(2), H2XM1);
        H2XM2 = np.kron(np.eye(2), H2XM2);
        H2XM3 = np.kron(np.eye(2), H2XM3);

    # Put the calculated hopping integrals in the grid:
    TMDmonolayer[X(0,0)] = HXonsite;
    TMDmonolayer[M(0,0)] = HMonsite;
    TMDmonolayer[NN_MM1] = HMM1;
    TMDmonolayer[NN_MM2] = HMM2;
    TMDmonolayer[NN_MM3] = HMM3;
    TMDmonolayer[NN_XX1] = HXX1;
    TMDmonolayer[NN_XX2] = HXX2;
    TMDmonolayer[NN_XX3] = HXX3;
    TMDmonolayer[NN_XM1] = HXM1;
    TMDmonolayer[NN_XM2] = HXM2;
    TMDmonolayer[NN_XM3] = HXM3;
    TMDmonolayer[SNN_XM1] = H2XM1;
    TMDmonolayer[SNN_XM2] = H2XM2;
    TMDmonolayer[SNN_XM3] = H2XM3;

    # Wrap around, finalize the system and return:
    return kwant.wraparound.wraparound(TMDmonolayer, coordinate_names=("1", "2")).finalized();

def getFangkpHamiltonian(material="MoS2", eta=1, includeSOI=False):
    """Obtain the k.p Hamiltonian from Fang et al. Phys. Rev. B 92, 205108 (2015).

    Parameters:
    ----------
    material: string
        The material for which the k.p parameters have to be acquired (default = "MoS2").

    eta: +1 or -1
        Whether to use +K or -K point respectively (+K by default).

    includeSOI: boolean
        Whether or not to include spin-orbit interaction (False by default).

    Returns:
    --------
    The Hamiltonian as a callable function of kx and ky, the crystal momenta with respect to the eta*K point.
    """
    # Check if eta is +1 or -1:
    if not(eta==1 or eta==-1):
        raise ValueError("'eta' should be either +1 or -1.");

    # Retrieve k.p parameters and store them in variables:
    with open("data/fitparams/Fang_k.p.json") as f:
        kpparams = json.load(f)[material];

    Eg = kpparams["Eg"];
    t1 = kpparams["t1"];
    f2 = kpparams["f2"];
    f3 = kpparams["f3"];
    t3w = kpparams["t3w"];
    l_vb = kpparams["l_vb"];
    l_cb = kpparams["l_cb"];
    a = kpparams["a"];

    # Define the Pauli matrices + identity:
    I = np.array([[1, 0], [0, 1]]);
    X = np.array([[0, 1], [1, 0]]);
    Y = np.array([[0, -1j], [1j, 0]]);
    Z = np.array([[1, 0], [0, -1]]);

    # Define the Hamiltonian:
    def H(kx, ky):
        h = Eg/2*(I+Z) + t1*a*(eta*kx*X + ky*Y) + (a**2)*(kx**2 + ky**2)*(f2*I + f3*Z) + t3w*(a**2)*((kx**2 - ky**2)*X - 2*eta*kx*ky*Y);
        if includeSOI:
            hsoi = eta*np.kron(Z, l_vb*(I-Z)/2 + l_cb*(I+Z)/2);
            h = np.kron(I, h) + hsoi;
        return h;
    # Return this function:
    return H;
