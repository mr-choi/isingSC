import numpy as np;
import scipy.linalg as linalg;
import scipy.optimize as optimize;
import matplotlib.pyplot as plt;

# The normal phase Hamiltonian with no external field:
def Hnormal(kx, ky):
    return np.array([[kx*kx + ky*ky, 0], [0, kx*kx + ky*ky]]);

# The superconducting BdG Hamiltonian:
def HBdG(kx, ky, Delta, Hx, Hy, Hz, mu):
    return np.array([[kx*kx + ky*ky - mu + Hz, Hx - 1j*Hy, 0.0, -Delta],
                             [Hx + 1j*Hy, kx*kx + ky*ky - mu - Hz, Delta, 0.0],
                             [0.0, Delta, -kx*kx - ky*ky + mu - Hz, -Hx - 1j*Hy],
                             [-Delta, 0.0, -Hx + 1j*Hy, -kx*kx - ky*ky + mu + Hz]]);

# A generator for k points given chemical potential and energy width:
def generate_ks(mu, hwc, res):
    kmax = np.sqrt(mu + hwc);
    kxs = kys = np.linspace(-kmax, kmax, res);
    KXs, KYs = np.meshgrid(kxs, kys, indexing='ij');
    nearFermi = np.logical_and(KXs*KXs + KYs*KYs <= mu + hwc, KXs*KXs + KYs*KYs >= mu - hwc);
    ks = np.array([KXs[nearFermi], KYs[nearFermi]]).transpose();
    return ks;

# Find interaction strength at given chemical potential and k's:
def Vint(mu, ks, Delta):
    Vinverse = 0;
    ksqs = ks[:,0]*ks[:,0] + ks[:,1]*ks[:,1];
    return 2 / np.sum(1/np.sqrt((ksqs - mu)**2 + Delta**2));

def FreeEnergy(mu, ks, Delta, Hext, T, V):
    ksqs = ks[:,0]*ks[:,0] + ks[:,1]*ks[:,1];
    if T == 0.0:
        return Delta**2 / V - np.sum(np.abs(Hext + np.sqrt((ksqs - mu)**2 + Delta**2)) + np.abs(Hext - np.sqrt((ksqs - mu)**2 + Delta**2)))/2;
    else:
        return Delta**2 / V - T * np.sum( np.log(2*np.cosh((Hext + np.sqrt((ksqs - mu)**2 + Delta**2))/(2*T))) + np.log(2*np.cosh((Hext - np.sqrt((ksqs - mu)**2 + Delta**2))/(2*T))) );

def findHc(mu, Delta, hwc, res):
    ks = generate_ks(mu, hwc, res);
    V = Vint(mu, ks, Delta);
    def PhiNminusPhiS(Hext):
        return FreeEnergy(mu, ks, 0.0, Hext, 0.0, V) - FreeEnergy(mu, ks, Delta, Hext, 0.0, V);
    return optimize.brentq(PhiNminusPhiS, 0.0, 1.0*Delta);

def findTc(mu, Delta, hwc, res):
    ks = generate_ks(mu, hwc, res);
    V = Vint(mu, ks, Delta);
    absE = np.abs(ks[:,0]*ks[:,0] + ks[:,1]*ks[:,1] - mu);
    def critTeq(T):
        return np.sum(np.tanh(absE/(2*T))/absE)/2 - 1/V;
    return optimize.brentq(critTeq, 0.001*Delta, 1.0*Delta);

def findDelta(mu, ks, Delta0, Hext, T, V):
    def F(Delta):
        return FreeEnergy(mu, ks, Delta, Hext, T, V);
    return optimize.differential_evolution(F, [(0.0, 1.05*Delta0)]).x[0];

def phaseDiagram(mu, Delta0, hwc, res, nT=32, nHext=32):
    # Base limits on found critical field and temperature:
    Tmax = 0.7*Delta0;
    Hmax = 0.8*Delta0;
    ks = generate_ks(mu, hwc, res);
    V = Vint(mu, ks, Delta0);
    Ts = np.linspace(0.0, Tmax, nT);
    Hs = np.linspace(0.0, Hmax, nHext);
    Tgrid, Hgrid = np.meshgrid(Ts, Hs, indexing='ij');
    phase = np.zeros((nT, nHext));
    for i in range(nT):
        for j in range(nHext):
            print("Processing site ({}, {})...".format(i,j), end="\r");
            Delta = findDelta(mu, ks, Delta0, Hs[j], Ts[i], V);
            phase[i][j] = (5 if Hs[j] > Delta else 1) if Delta > Delta0/100.0 else 8;
    # Plot the figure:
    fig = plt.figure();
    ax = fig.add_subplot(111, xlim=[0,Tmax/Delta0], ylim=[0,Hmax/Delta0]);
    ax.tick_params(labelsize=17);
    ax.set_xlabel("Temperature [$\Delta_0/k_B$]", size=17);
    ax.set_ylabel("External field [$\Delta_0/\mu_B$]", size=17);
    ax.pcolormesh(Tgrid/Delta0, Hgrid/Delta0, phase, cmap=plt.get_cmap("Set1"), vmin=0, vmax=8);
    return fig;
