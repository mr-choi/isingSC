import kwant;
import numpy as np;
import matplotlib.pyplot as plt;
import scipy.linalg as linalg;
from matplotlib.colors import LinearSegmentedColormap, Normalize;

def plotBandStructureTrajectory(syst, ks, labels=None, normMatrix=None, colorMatrix=None, figure=None, linespec='b-', n=500, s=2, **kwargs):
    """Plot the band structure of a 2D system over a specified trajectory.

    Parameters
    ----------
    syst: either a kwant.system.System or a callable.
        A kwant system or an explicit Hamiltonian with ks as arguments.

    ks: array_like containing array_like of numerics.
        A list of all k-points of the trajectory. The array_like of each k represents an N-dimensional vector.

    labels: array_like of strings.
        Optional list of label names for all k-points in the ks list. Raises a ValueError if length does not agree with that of ks.

    normMatrix: N by N matrix
        matrix operation to be aplied before computing the norm of the difference between 2 subsequent k-vectors.
        This is very useful if the unit k-vectors are not orthonormal, which will be assumed if not specified.

    colorMatrix: projection matrix
        Projection matrix for the eigenvectors.
        If the norm of the normalized eigenvector after projection is higher, the line will have a more red color while a lower norm will make the line more blue.
        Raises a ValueError if it is not a projection matrix (P @ P != P) or if the dimensions of the matrix does not agree with the dimensions of the Hamiltonian.

    figure: matplotlib figure
        If specified, plot the band sturcture in the figure instead creating a new one (this will also render the labels parameter unused).

    linespec:
        Line specification for plot.
        This will only be used if no colorMatrix is specified.

    n: integer
        Number of data points in the plot (500 by default).

    s: floating point number
        Size of the markers used in the figure.

    **kwargs: dictionary
        Additional parameters that have to be passed in the Hamiltonian.

    Returns:
    --------
    The figure handle of the plotted figure.
    """

    # Check if ks list length equals that of labels if given
    if labels is not None:
        if len(ks) != len(labels):
            raise ValueError("Length of 'ks' ({}) does not agree with that of 'labels' ({}).".format(len(ks), len(labels)));

    # Check if colorMatrix is a projection matrix and has the right dimensions if specified
    if colorMatrix is not None:
        if np.any(colorMatrix @ colorMatrix != colorMatrix):
            raise ValueError("'colorMatrix' is not a projection matrix.");
        if isinstance(syst, kwant.system.System):
            params = dict(zip(syst._momentum_names, ks[0]), **kwargs);
            H = syst.hamiltonian_submatrix(params=params);
        else:
            H = syst(*ks[0], **kwargs);
        if len(H) != len(colorMatrix):
            raise ValueError("Dimensions of 'colorMatrix' ({}) does not agree with that of the Hamiltonian ({}).".format(len(colorMatrix), len(H)));

    ks = np.array(ks); # Convert ks in numpy array if not done

    # Get culumative length of difference vectors:
    dks = np.diff(ks, axis=0); # vector differences
    if normMatrix is not None: dks = np.tensordot(dks, normMatrix, axes=(1,1));
    ts = np.cumsum(np.linalg.norm(dks, axis=1));
    tmax = ts[-1];

    # Define parameterized function that goes over trajectory:
    def path(t):
        i = np.sum(ts < t);
        tmin = 0 if i==0 else ts[i-1];
        dt = ts[i] - tmin;
        return (t - tmin)/dt * (ks[i+1] - ks[i]) + ks[i];

    # Calculate projection:
    def proj(eigvec):
        # Also assure value is between 0 and 1:
        return np.minimum(1, np.maximum(0, np.linalg.norm(colorMatrix @ eigvec)**2));

    # Compute eigenenergies (and colors if applicable) for different 'times' in the trajectory.
    times = np.linspace(0, tmax, n);
    energies = [];
    colors = [];

    # Find energies and colors
    if isinstance(syst, kwant.system.System):
        for time in times:
            k = path(time);
            params = dict(zip(syst._momentum_names, k), **kwargs);
            H = syst.hamiltonian_submatrix(params=params);
            eigvals, eigvecs = linalg.eigh(H);
            energies.append(eigvals);
            if colorMatrix is not None: colors.append([ proj(eigvec) for eigvec in eigvecs.transpose() ]);
    else:
        for time in times:
            k = path(time);
            H = syst(*k, **kwargs);
            eigvals, eigvecs = linalg.eigh(H);
            energies.append(eigvals);
            if colorMatrix is not None: colors.append([ proj(eigvec) for eigvec in eigvecs.transpose() ]);

    # Plot the band structure:
    # If no figure is specified, create one:
    if figure is None:
        fig = plt.figure();
        ax = fig.add_subplot(111, xticks=[0, *ts], xlim=[0, tmax]);
        if labels is not None: ax.set_xticklabels(labels, size=17);
        ax.grid(True, axis='x');
        ax.tick_params(labelsize=17);
        ax.set_ylabel("Energy [eV]", size=17);
    else: # Else use the specified figure:
        fig = figure;
        ax = fig.gca();
        # I rescale the time such that the band completely fits in the figure:
        (tminnew, tmaxnew) = ax.get_xlim();
        times = (tmaxnew - tminnew)/tmax*times + tminnew;

    # If no colorMatrix is specified, use the plot function:
    if colorMatrix is None:
        ax.plot(times, energies, linespec, markersize=s);
    else: # Else make a scatter for each band, taking into account the projection of the eigenvector for the color:
        colors = np.array(colors);
        energies = np.array(energies);
        cm = LinearSegmentedColormap.from_list("redblue", [(0,0,1),(1,0,0)]);
        for j in range(len(energies[0])):
            ax.scatter(times, energies[:,j], s=s, c=colors[:,j].tolist(), cmap=cm, norm=Normalize(0,1));
    return fig;

def plot2Dcontours(syst, bandnr=0, kxlim=[-1,1], kylim=[-1,1], nx=100, ny=100, levels=None, **kwargs):
    """Plot a contour plot of one of the bands of the 2D band structure.

    Parameters:
    -----------
    syst: either a kwant.system.System or a callable.
        A kwant system or an explicit Hamiltonian with kx, ky as first two arguments.

    bandnr: integer
        The band to be plotted.
        Bands are sorted in ascending order in energies.
        Default band index that will be picked is 0 (the lowest energy band).

    kxlim: list with two floating point numbers.
        The limits of the kx variable.

    kylim: list with two floating point numbers.
        The limits of the ky variable.

    nx: integer
        The number of data points along the kx (100 by default).

    ny: integer
        The number of data points along the ky (100 by default).

    levels: list of floats or None
        List of all the values for which contours have to be plotted.
        If None, the levels are automatically generated by the contour function (default option).

    kwargs: dictionary
        Additional parameters that have to be passed to the Hamilonian.

    Returns:
    --------
    The figure handle of the plotted figure.
    """
    # Generate meshgrids:
    kxs = np.linspace(kxlim[0], kxlim[1], nx);
    kys = np.linspace(kylim[0], kylim[1], ny);
    KX, KY = np.meshgrid(kxs, kys);

    # Calculate eigenenergies of Hamiltonian on the grid:
    energies = np.zeros((nx, ny));
    if isinstance(syst, kwant.system.System):
        for x in range(nx):
            kx = kxs[x];
            for y in range(ny):
                ky = kys[y];
                params = dict(zip(syst._momentum_names, [kx, ky]), **kwargs);
                H = syst.hamiltonian_submatrix(params=params);
                energies[y,x] = linalg.eigh(H, eigvals_only=True)[bandnr];
    else:
        for x in range(nx):
            kx = kxs[x];
            for y in range(ny):
                ky = kys[y];
                H = syst(kx, ky, **kwargs);
                energies[y,x] = linalg.eigh(H, eigvals_only=True)[bandnr];

    # Plot the contour and return the figure:
    fig = plt.figure();
    ax = fig.add_subplot(111, xlim=kxlim, ylim=kylim, aspect="equal");
    ax.tick_params(labelsize=17);
    ax.set_xlabel("$k_x$ [Å$^{-1}$]", size=17);
    ax.set_ylabel("$k_y$ [Å$^{-1}$]", size=17);
    cm = LinearSegmentedColormap.from_list("redblueishblack", [(0.3,0.3,0.4),(0.25,0,0)]);
    cset = ax.contour(KX, KY, energies, levels=levels, cmap=cm);
    ax.clabel(cset, fmt="%1.2f eV", fontsize=17);
    return fig;
