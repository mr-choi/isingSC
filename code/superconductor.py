import numpy as np;
import scipy.linalg as linalg;
import scipy.optimize as optimize;
import matplotlib.pyplot as plt;

spinX = np.array([[0, 1], [1, 0]]);
spinY = np.array([[0, -1j], [1j, 0]]);
spinZ = np.array([[1, 0], [0, -1]]);

class Superconductor:
    """
    Base class for s-wave superconductor.

    Properties:
    -----------
    ks - (N,D) array_like
        Array of N crystal momentum vectors for which the Hamiltonian has to be evaluated.
        The number of columns D implies the number of dimensions of the system.
        If a 1D array is specified, D = 1 is assumed.
        If the system is 0D (e.g. a quantum dot), ks has to be set to None.
    Hnormal - callable
        The normal phase Hamiltonian, that acts on quantum states living in the Hilbert space X × Hn.
        Here, X represents the spin-1/2 Hilbert space while Hn represents an n-orbital Hilbert space.
        The Hamiltonian must thus be an even square array.
    Delta0 - complex number
        The superconducting gap at zero temperature, zero external magnetic field, and zero chemical potential.
    """
    def __init__(self, ks, Hnormal, Delta0):
        # Save the ks and also the number of dimensions:
        self.ks = [None] if (ks is None) else np.array(ks);
        self.ndim = 0 if (ks is None) else (1 if self.ks.ndim==1 else self.ks.shape[1]);
        if (ks is not None and self.ks.ndim==1): self.ks.shape = (len(self.ks), 1);

        # Check if the normal phase Hamiltonian is a callable:
        if not callable(Hnormal):
            raise ValueError("Normal phase Hamiltonian is not callable.");
        # Check dimensions of Hamiltonian:
        if self.ndim > 0:
            k0 = np.zeros((self.ndim));
            row, col = Hnormal(k0).shape;
        else:
            row, col = Hnormal(None).shape;
        if row != col:
            raise ValueError("Hamiltonian must be square matrix (is {} by {}).".format(row, col));
        if row % 2 == 1:
            raise ValueError("Hamiltonian must include spin-1/2 Hilbert space and thus the size ({}) cannot be odd.".format(row));
        # Save the normal phase Hamiltonian and also the number of orbitals.
        self.Hnormal = Hnormal;
        self.norbs = row // 2;

        # Save the zero temperature and magnetic field superconducting gap:
        self.Delta0 = 1.0*Delta0;

        # Construct the Bogoliubov-De Gennes (BdG) Hamiltonian:
        def HBdG(k, Delta, Hext, mu):
            # Some definitions:
            minusk = None if (k is None) else -np.array(k);
            Hzeeman = np.kron(spinX*Hext[0] + spinY*Hext[1] + spinZ*Hext[2], np.eye(self.norbs));
            # The blocks of the Hamiltonian:
            tl = Hnormal(k) - mu*np.eye(2*self.norbs) + Hzeeman;
            tr = np.kron(-1j*spinY, Delta*np.eye(self.norbs));
            bl = np.kron(1j*spinY, np.conj(Delta)*np.eye(self.norbs));
            br = -np.conj(Hnormal(minusk) - mu*np.eye(2*self.norbs) + Hzeeman);
            return np.block([[tl, tr], [bl, br]]);
        self.HBdG = HBdG;

        # Find the isotropic interaction energy using the gap at zero temperature, magnetic field and chemical potential:
        sum_part = 0.0 + 0j;
        for k in self.ks:
            # Obtain the eigenvalues and eigenvectors of the BdG Hamiltonian:
            energies, U = linalg.eigh(self.HBdG(k, self.Delta0, np.array([0,0,0]), 0.0));

            # Evaluate the occupations of the quasiparticles:
            n = (np.sign(energies)+1)/2;

            # The matrix of all averages of pairs of electron creation-annihilation operators can then be evaulated by:
            C = U @ np.diag(n) @ U.conj().T;

            # To obtain the result, add some elements located in the upper-right of the C matrix:
            sum_part += np.trace(C[:self.norbs,-self.norbs:]);
        self.V = -np.real(self.Delta0 / sum_part);

    def Delta(self, T, Hext, mu):
        """
        Find the superconducting gap for the given parameters.
        This is done by minimizing the free energy.

        Parameters:
        -----------
        T - positive real number
            The temperature.
        Hext - array_like containing 3 real numbers
            The external magnetic field.
        mu - real number
            The chemical potential.

        Returns:
        --------
        The superconducting gap.
        """
        phase = self.Delta0 / np.abs(self.Delta0);
        F = lambda x: self.freeEnergy(x*phase, T, Hext, mu);
        bounds = [(0.0, 2*np.abs(self.Delta0))];
        return optimize.differential_evolution(F, bounds).x[0]*phase;

    def freeEnergy(self, Delta, T, Hext, mu):
        """
        Calculate the free energy.

        Paramters:
        ----------
        Delta - complex number
            The superconducting phase.
        T - positive real number
            The temperature
        Hext - array_like containing 3 real numbers
            The external magnetic field.
        mu - real number
            The chemical potential.

        Returns:
        --------
        The free energy
        """
        F = np.abs(Delta)**2 / self.V;
        for k in self.ks:
            # Obtain the eigenvalues of the  BdG Hamiltonian:
            energies = linalg.eigh(self.HBdG(k, Delta, Hext, mu), eigvals_only=True);

            # The free energy contribution is then given by:
            if (T==0):
                F += -np.sum(np.abs(energies))/4;
            else:
                F += -T*np.sum(np.log(2*np.cosh(energies/(2*T))))/2;
        return F;

    def phaseDiagram(self, mu, Tmax, Hextmax, direction=np.array([0,0,1]), numT=32, numHext=32):
        """
        Plot the superconducting gap as function of temperature and magnetic field.

        Parameters:
        -----------
        mu - real number
            The chemical potential.
        Tmax - positive real number
            The upper limit of the temperature in the diagram.
        Hextmax - positive real number
            The upper limit of the magnitude of the external field in the diagram.
        direction - array_like with 3 real numbers
            The direction of the magnetic field.
            The vector will be normalized first.
            By default, the external field is in the positive z-direction.
        numT - positive integer
            number of temperatures for which the self-consistency equation will be sampled (default = 50).
        numHext - positive integer
            number of external magnetic fields for which the self-consistency equation will be sampled (default = 50).

        Returns:
        --------
        fig, the figure object.
        """
        # First normalize direction vector:
        direction = np.array(direction) / np.linalg.norm(direction);

        # Generate grid:
        Ts = np.linspace(0, Tmax, numT);
        Hexts = np.linspace(0, Hextmax, numHext);
        Tgrid, Hextgrid = np.meshgrid(Ts, Hexts, indexing='ij');

        # Vectorize the self-consistent gap equation algorithm:
        Delta = np.vectorize(self.Delta, signature="(),(n),()->()");
        #Deltagrid = np.zeros((numT, numHext));

        # Find the magnitude of the superconducting gap:
        #for i in range(numT):
        #    for j in range(numHext):
        #        Deltagrid[i][j] = np.abs(self.Delta(Tgrid[i,j], Hextgrid[i,j]*direction, mu));
        #        print("evaluating cell ({},{})...".format(i,j));
        Deltagrid = np.abs( Delta(Tgrid, np.tensordot(Hextgrid, direction, axes=0), mu) );

        # Plot the figure:
        fig = plt.figure();
        ax = fig.add_subplot(111, xlim=[0,Tmax], ylim=[0,Hextmax]);
        ax.tick_params(labelsize=17);
        ax.set_xlabel("Temperature [$E_0/k_B$]", size=17);
        ax.set_ylabel("External field [$E_0/\mu_B$]", size=17);
        quadmesh = ax.pcolormesh(Tgrid, Hextgrid, Deltagrid);
        cbar = fig.colorbar(quadmesh, ax=ax);
        cbar.ax.tick_params(labelsize=17);
        cbar.set_label("Superconducting gap [$E_0$]", size=17);
        return fig;

    def criticalT(self, mu):
        """
        Find the critical temperature at zero external field.

        Parameters:
        -----------
        mu - real number
            The chemical potential.

        Returns:
        --------
        The critical temperature.
        """
        f = lambda T: np.abs(self.Delta(T, [0,0,0], mu)) - np.abs(self.Delta0)/100;
        return optimize.brentq(f, 0.0, 2*np.abs(self.Delta0), rtol=1e-3);

    def criticalHext(self, mu, direction=np.array([0,0,1])):
        """
        Obtain the critical field at zero temperature.

        Parameters:
        -----------
        mu - real number
            The chemical potential.
        direction - array_like with 3 real numbers
            The direction of the magnetic field.
            The vector will be normalized first.
            By default, the external field is in the positive z-direction.

        Returns:
        --------
        The crital field.
        """
        direction = np.array(direction) / np.linalg.norm(direction);
        f = lambda Hext: np.abs(self.Delta(0.0, direction*Hext, mu)) - np.abs(self.Delta0)/100;
        return optimize.brentq(f, 0.0, 10*np.abs(self.Delta0), rtol=1e-3);

    def plotCriticals(self, mu_min, mu_max, n=16, direction=np.array([0,0,1])):
        """
        Plot the critical temperature and field as a function of the chemical potential, in units of the gap at zero temperature and field.

        Parameters:
        -----------
        mu_min - real number
            Lower bound chemical potential.
        mu_max - real number
            Upper bound chemical potential.
        n -positive integer
            Number of chemcial potentials to be evaluatated (default = 16).
        direction - array_like with 3 real numbers
            The direction of the magnetic field.
            The vector will be normalized first.
            By default, the external field is in the positive z-direction.

        Returns:
        --------
        fig, the figure object.
        """
        # Normalize the direction vector:
        direction = np.array(direction) / np.linalg.norm(direction);

        # Evaluate Delta0's and critcal temperature and fields for all chemical potentials:
        mus = np.linspace(mu_min, mu_max, n);
        Delta0s = []; critTs = []; critHexts = [];
        for mu in mus:
            Delta0s.append(np.abs(self.Delta(0.0, np.array([0,0,0]), mu)));
            critTs.append(self.criticalT(mu));
            critHexts.append(self.criticalHext(mu, direction));

        # Convert the lists into arrays:
        Delta0s = np.array(Delta0s);
        critTs = np.array(critTs);
        critHexts = np.array(critHexts);

        # Calculate the critical field and temperature with respect to the gap at zero field and temperature:
        cT = critTs / Delta0s;
        cHext = critHexts / Delta0s;

        # Plot the figure:
        fig = plt.figure();
        ax = fig.add_subplot(111, xlim=[mu_min, mu_max]);
        ax.tick_params(labelsize=17);
        ax.set_xlabel("Chemical potential [a.u.]", size=17);
        ax.plot(mus, cT, 'r-');
        ax.plot(mus, cHext, 'b-');
        return fig;
