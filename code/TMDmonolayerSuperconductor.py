import numpy as np;
import scipy.linalg as linalg;
import scipy.optimize as optimize;
import matplotlib.pyplot as plt;
import json;
import hpc05;
from datetime import datetime;

def brentGlobalMinimize(f, a, b, M, tol):
    "Globally minimize f between a and b, given the second derivative of f is at most M."
    fa = f(a); fb = f(b);
    (fmin, xmin) = (fb, b) if fb < fa else (fa, a);
    if M <= 0: return xmin;
    (fa2, a2) = (fa, a);
    while a2 < b:
        a3 = np.minimum(b, a2 + np.sqrt(2*(fa2 - fmin + tol)/M));
        fa3 = f(a3);
        if fa3 < fmin: (fmin, xmin) = (fa3, a3);
        Pbottom = (16*(a2-a3)*(2*(-a3*fa2 + a2*fa3) + (a2 - a3)*a2*a3*M) - 3*(2*(fa2 - fa3) + (a3**2 - a2**2)*M)**2/M)/(32*(a2-a3)**2);
        while Pbottom < fmin - tol:
            a3 = (a2 + a3)/2;
            fa3 = f(a3);
            if fa3 < fmin: (fmin, xmin) = (fa3, a3);
            Pbottom = (16*(a2-a3)*(2*(-a3*fa2 + a2*fa3) + (a2 - a3)*a2*a3*M) - 3*(2*(fa2 - fa3) + (a3**2 - a2**2)*M)**2/M)/(32*(a2-a3)**2);
        (fa2, a2) = (fa3, a3);
    return xmin;

class TMDmonolayerSC:
    def __init__(self, material="MoS2"):
        "Initialize TMD monolayer by retrieving parameters"
        # Open file:
        with open("data/fitparams/Wang_k.p.json") as f:
            kpparams = json.load(f)[material]

        # Store them in class variables:
        self.a = kpparams["A"];
        self.b = kpparams["B"];
        self.c = kpparams["C"];
        self.d = kpparams["D"];

    def e(self, kx, ky):
        "Spin independent part of normal phase Hamiltonian."
        return self.a*(kx*kx + ky*ky);

    def f(self, kx, ky, eta):
        "Spin dependent part of normal phase Hamiltonian."
        return eta*self.b + eta*self.c*(kx*kx + ky*ky) + self.d*(kx**3 - 3*kx*ky**2);

    def Hnormal(self, kx, ky, eta, s):
        "Normal phase Hamiltonian of the eta-K point and the spin s band."
        return self.e(kx, ky) + s*self.f(kx, ky, eta);

    def HnormalAbsVal(self, kx, ky, eta, s, mu):
        "Normal phase Hamiltonian of the eta-K point and the spin s band, but absolute values taken for spin-(in)dependent part separately and the expression as a whole."
        return np.abs(np.abs(self.e(kx, ky) - mu) + s*np.abs(self.f(kx, ky, eta)));

    def find_kmax(self, Emax):
        "Guess a kmax given the maximum energy."
        k0max = np.sqrt((Emax - self.b)/(self.a - self.c));
        return k0max*np.sqrt(1 - 2*k0max*self.d/(self.a - self.c));

    def create_kgrid(self, muMax, hwcMax, res):
        "Generate a grid of k points."
        kmax = self.find_kmax(muMax + hwcMax);
        kxs = kys = np.linspace(-kmax, kmax, res);
        etas = np.array([-1, 1]);
        return np.meshgrid(kxs, kys, etas, indexing='ij');

    def choose_ks(self, kxgrid, kygrid, etagrid, mu, hwc):
        "Obtain the appropriate ks from the grid."
        nearFermiUp = self.HnormalAbsVal(kxgrid, kygrid, etagrid, 1, mu) <= hwc;
        nearFermiDown = self.HnormalAbsVal(kxgrid, kygrid, etagrid, -1, mu) <= hwc;
        return np.array([kxgrid[nearFermiUp], kygrid[nearFermiUp], etagrid[nearFermiUp]]).transpose(), np.array([kxgrid[nearFermiDown], kygrid[nearFermiDown], etagrid[nearFermiDown]]).transpose();

    def generate_ks(self, mu, hwc, res):
        "Generate appropriate ks given the chemcial potential and cutoff."
        kxgrid, kygrid, etagrid = self.create_kgrid(mu, hwc, res);
        return self.choose_ks(kxgrid, kygrid, etagrid, mu, hwc);

    def displayFilteredGrid(self, mu, hwc, res):
        "Display the grid points after filtering giving chemical potential energy range."
        kxgrid, kygrid, etagrid = self.create_kgrid(mu, hwc, res);
        kmax = self.find_kmax(mu + hwc);
        filterUp = self.HnormalAbsVal(kxgrid, kygrid, etagrid, 1, mu) <= hwc;
        filterDown = self.HnormalAbsVal(kxgrid, kygrid, etagrid, -1, mu) <= hwc;
        # Plot and return figure
        fig = plt.figure();
        ax = fig.add_subplot(111, xlim=[-kmax,kmax], ylim=[-kmax,kmax]);
        ax.tick_params(labelsize=17);
        ax.set_xlabel(r"$k_x$ [Å$^{-1}$]", size=17);
        ax.set_ylabel(r"$k_y$ [Å$^{-1}$]", size=17);
        ax.pcolormesh(kxgrid[:,:,1], kygrid[:,:,1], 1 - 5*filterUp[:,:,1] + 4*filterDown[:,:,1], cmap=plt.get_cmap("Set1"), vmin=0, vmax=8, rasterized=True, antialiased=True);
        return fig;

    def Vint(self, mu, ksplus, ksmin, Delta):
        "Interaction between particles and holes."
        spf = self.HnormalAbsVal(ksplus[:,0], ksplus[:,1], ksplus[:,2], 1, mu);
        smf = self.HnormalAbsVal(ksmin[:,0], ksmin[:,1], ksmin[:,2], -1, mu);
        return 4 / ( np.sum(1/np.sqrt(spf**2 + Delta**2)) + np.sum(1/np.sqrt(smf**2 + Delta**2)) );

    def FreeEnergy(self, mu, ksplus, ksmin, Delta, Hext, T, V):
        "The free energy."
        # Spin (in)dependent energies in case of s = +1:
        Esqplus = (self.e(ksplus[:,0], ksplus[:,1]) - mu)**2;
        Fsqplus = (self.f(ksplus[:,0], ksplus[:,1], ksplus[:,2]))**2;
        # Spin (in)dependent energies in case of s = -1:
        Esqmin = (self.e(ksmin[:,0], ksmin[:,1]) - mu)**2;
        Fsqmin = (self.f(ksmin[:,0], ksmin[:,1], ksmin[:,2]))**2;
        # Eigenvalues:
        sqrtplus = np.sqrt(Esqplus + Fsqplus + Hext**2 + Delta**2 + 2*np.sqrt(Esqplus*(Fsqplus + Hext**2) + Delta**2 * Hext**2));
        sqrtmin = np.sqrt(Esqmin + Fsqmin + Hext**2 + Delta**2 - 2*np.sqrt(Esqmin*(Fsqmin + Hext**2) + Delta**2 * Hext**2));
        plusfilter = sqrtplus >= 2*T*7e2;
        minfilter = sqrtmin >= 2*T*7e2;
        return Delta**2 / V - np.sum(sqrtplus[plusfilter])/2 - np.sum(sqrtmin[minfilter])/2 - \
               T*np.sum(np.log(2*np.cosh(sqrtplus[np.logical_not(plusfilter)]/(2*T)))) - T*np.sum(np.log(2*np.cosh(sqrtmin[np.logical_not(minfilter)]/(2*T))));

    def dFdDelta(self, mu, ksplus, ksmin, Delta, Hext, V, deltaPrefactor=True):
        "The derivative of the free energy with respect to Delta at zero temperature."
        # Spin (in)dependent energies in case of s = +1:
        Esqplus = (self.e(ksplus[:,0], ksplus[:,1]) - mu)**2;
        Fsqplus = (self.f(ksplus[:,0], ksplus[:,1], ksplus[:,2]))**2;
        # Spin (in)dependent energies in case of s = -1:
        Esqmin = (self.e(ksmin[:,0], ksmin[:,1]) - mu)**2;
        Fsqmin = (self.f(ksmin[:,0], ksmin[:,1], ksmin[:,2]))**2;
        # Eigenvalues:
        sqrtsqrtplus = np.sqrt(Esqplus*(Fsqplus + Hext**2) + Delta**2 * Hext**2);
        sqrtsqrtmin = np.sqrt(Esqmin*(Fsqmin + Hext**2) + Delta**2 * Hext**2);
        sqrtplus = np.sqrt(Esqplus + Fsqplus + Hext**2 + Delta**2 + 2*sqrtsqrtplus);
        sqrtmin = np.sqrt(Esqmin + Fsqmin + Hext**2 + Delta**2 - 2*sqrtsqrtmin);
        # Result:
        result = 2 / V - np.sum((1 + Hext**2/sqrtsqrtplus) / sqrtplus)/2 - np.sum((1 - Hext**2/sqrtsqrtmin) / sqrtmin)/2;
        return Delta*result if deltaPrefactor else result;

    def findHc(self, mu, Delta0, ksplus, ksmin, V, maxfactor=100.0, T=0.0):
        "Find the critical field."
        def theDeltaMinSmall(Hext):
            return self.findDelta(mu, ksplus, ksmin, Delta0, Hext, T, V) - Delta0/1e6;
        return optimize.brentq(theDeltaMinSmall, 0.0*Delta0, maxfactor*Delta0);

    def approxHc(self, mu, Delta0, ksplus, ksmin, V, maxfactor=100.0):
        "Approximate the critical field by means of necessary conditions."
        # Necessary condition #1: positive second derivative at Delta = 0.
        def condition1(Hext):
            return self.FreeEnergy(mu, ksplus, ksmin, Delta0/1000.0, Hext, 0.0, V) - self.FreeEnergy(mu, ksplus, ksmin, 0.0, Hext, 0.0, V);
        # Necessary condition #2: free energy at Delta0 is lower than normal phase free energy.
        def condition2(Hext):
            return self.FreeEnergy(mu, ksplus, ksmin, Delta0, Hext, 0.0, V) - self.FreeEnergy(mu, ksplus, ksmin, 0.0, Hext, 0.0, V);
        return np.maximum(optimize.brentq(condition1, 0.0*Delta0, maxfactor*Delta0), optimize.brentq(condition2, 0.0*Delta0, maxfactor*Delta0));

    def findTc(self, mu, Delta, ksplus, ksmin, V):
        "Find the critical temperature."
        spf = self.HnormalAbsVal(ksplus[:,0], ksplus[:,1], ksplus[:,2], 1, mu);
        smf = self.HnormalAbsVal(ksmin[:,0], ksmin[:,1], ksmin[:,2], -1, mu);
        def critTeq(T):
            return np.sum(np.tanh(spf/(2*T))/spf) + np.sum(np.tanh(smf/(2*T))/smf) - 4/V;
        return optimize.brentq(critTeq, 0.001*Delta, 1.0*Delta);

    def findDelta(self, mu, ksplus, ksmin, Delta0, Hext, T, V):
        "Find the actual pairing strength that minimizes the free energy."
        def F(Delta):
            return self.FreeEnergy(mu, ksplus, ksmin, Delta, Hext, T, V);
        #DeltaLower = optimize.minimize_scalar(F, bounds=(0.0*Delta0, 0.5*Delta0), method='bounded', options={'xatol': 1e-3*Delta0}).x;
        #DeltaUpper = optimize.minimize_scalar(F, bounds=(0.5*Delta0, 1.0*Delta0), method='bounded', options={'xatol': 1e-3*Delta0}).x;
        #FLower = self.FreeEnergy(mu, ksplus, ksmin, DeltaLower, Hext, T, V);
        #FUpper = self.FreeEnergy(mu, ksplus, ksmin, DeltaUpper, Hext, T, V);
        #return DeltaLower if FLower < FUpper else DeltaUpper;
        #return optimize.differential_evolution(F, [(0.0, 1.0*Delta0)]).x[0];
        absEplus = np.abs(self.e(ksplus[:,0], ksplus[:,1]) - mu);
        absEmin = np.abs(self.e(ksmin[:,0], ksmin[:,1]) - mu);
        upperBoundDeriv = 2/V + np.sum(1/absEmin)/2 - np.sum(1/absEplus)/2;
        return brentGlobalMinimize(F, 0.0, Delta0, upperBoundDeriv, 1e-11/V);

    def plotFreeEnergy(self, mu, Delta0, hwc, res, Hext, T, nDeltas=200, DeltaMax=1):
        "Plot the free energy as a function of Delta at specified temperature and external field."
        ksplus, ksmin = self.generate_ks(mu, hwc, res);
        V = self.Vint(mu, ksplus, ksmin, Delta0);
        Deltas = np.linspace(0, Delta0*DeltaMax, nDeltas);
        Fs = [];
        for Delta in Deltas:
            Fs.append(self.FreeEnergy(mu, ksplus, ksmin, Delta, Hext, T, V));
        plt.plot(Deltas / Delta0, np.array(Fs));
        minimalDelta = self.findDelta(mu, ksplus, ksmin, Delta0, Hext, T, V);
        plt.axvline(x=minimalDelta/Delta0, color="k", linestyle="dotted");

    def plotdFdDelta(self, mu, Delta0, hwc, res, Hext, nDeltas=200, DeltaMax=1, DeltaPrefactor=True):
        "Plot the derivative of the free energy as a function of Delta at specified external field and assuming T = 0."
        ksplus, ksmin = self.generate_ks(mu, hwc, res);
        V = self.Vint(mu, ksplus, ksmin, Delta0);
        Deltas = np.linspace(0, Delta0*DeltaMax, nDeltas);
        dFs = [];
        for Delta in Deltas:
            dFs.append(self.dFdDelta(mu, ksplus, ksmin, Delta, Hext, V, DeltaPrefactor));
        plt.plot(Deltas / Delta0, np.array(dFs));

    def plotd2FdDelta2(self, mu, Delta0, hwc, res, Hext, nDeltas=200, DeltaMax=1):
        "Plot the second derivative of the free energy as a function of Delta at specified external field and assuming T = 0."
        ksplus, ksmin = self.generate_ks(mu, hwc, res);
        V = self.Vint(mu, ksplus, ksmin, Delta0);
        Deltas = np.linspace(0, Delta0*DeltaMax, nDeltas);
        dDelta = Delta0 / nDeltas / 2;
        dFs = [];
        for Delta in Deltas:
            dFs.append( (self.dFdDelta(mu, ksplus, ksmin, Delta + dDelta, Hext, V) - self.dFdDelta(mu, ksplus, ksmin, Delta - dDelta, Hext, V)) / dDelta / 2 );
        plt.plot(Deltas / Delta0, np.array(dFs));

    def nodalPoints(self, mu, Delta, Hext):
        "The number of nodal points in the superconductor at the specified parameters."
        k0 = np.sqrt(-self.b / self.c);
        kmin = np.sqrt(k0**2 + self.d*k0**3 / self.c);
        kmax = np.sqrt(k0**2 - self.d*k0**3 / self.c);
        nodalPoints = 0;
        if Delta > Hext: return 0;
        gapClosingE = np.sqrt(Hext**2 - Delta**2);
        if self.a*kmin**2 < mu - gapClosingE and mu - gapClosingE < self.a*kmax**2: nodalPoints += 6;
        if self.a*kmin**2 < mu + gapClosingE and mu + gapClosingE < self.a*kmax**2: nodalPoints += 6;
        return nodalPoints;

    def plotGapSize(self, mu, Delta0, hwc, res, Hext, T, theta):
        "Plot of the lower eigenenergies."
        k0 = np.sqrt(-self.b / self.c);
        kmin = np.sqrt(k0**2 + self.d*k0**3 / self.c);
        kmax = np.sqrt(k0**2 - self.d*k0**3 / self.c);
        ksplus, ksmin = self.generate_ks(mu, hwc, res);
        V = self.Vint(mu, ksplus, ksmin, Delta0);
        Delta = self.findDelta(mu, ksplus, ksmin, Delta0, Hext, T, V);
        print("Superconducting pairing in units of Delta0: " + str(Delta/Delta0));
        k = np.linspace(kmin - 0.0025, kmax + 0.0025, 1000);
        Esq = (self.e(k*np.cos(theta), k*np.sin(theta)) - mu)**2;
        Fsq = self.f(k*np.cos(theta), k*np.sin(theta), 1)**2;
        emin = np.sqrt(Esq + Fsq + Hext**2 + Delta**2 - 2*np.sqrt(Esq*(Fsq + Hext**2) + Delta**2 * Hext**2));
        # Plot the diagram:
        fig = plt.figure();
        ax = fig.add_subplot(111, ylim=[-2,2]);
        ax.tick_params(labelsize=17);
        ax.set_xlabel(r"$k_x$", size=17);
        ax.set_ylabel(r"Energy state [$\Delta_0$]", size=17);
        ax.plot(k, -emin/Delta0, "b-", k, emin/Delta0, "r-");
        return fig;

    def DeltaDiagram(self, mu, Delta0, hwc, res, nT=32, nHext=32):
        "Plot the Delta diagram."
        ksplus, ksmin = self.generate_ks(mu, hwc, res);
        V = self.Vint(mu, ksplus, ksmin, Delta0);
        # Base limits on found critical field and temperature:
        Tmax = 1.1*self.findTc(mu, Delta0, ksplus, ksmin, V);
        print("Critical temperature = {} Delta0".format(Tmax/(1.1*Delta0)));
        Hmax = 1.1*self.approxHc(mu, Delta0, ksplus, ksmin, V);
        print("Critical field = {} Delta0".format(Hmax/(1.1*Delta0)));
        Ts = np.linspace(0.0, Tmax, nT);
        Hs = np.linspace(0.0, Hmax, nHext);
        Tgrid, Hgrid = np.meshgrid(Ts, Hs, indexing='ij');
        Deltagrid = np.zeros((nT, nHext));
        for i in range(nT):
            for j in range(nHext):
                print("Calculating pairing at site ({}, {})...".format(i,j), end="\r");
                Deltagrid[i][j] = self.findDelta(mu, ksplus, ksmin, Delta0, Hs[j], Ts[i], V);
        # Plot the figure:
        fig = plt.figure();
        ax = fig.add_subplot(111, xlim=[0,Tmax/Delta0], ylim=[0,Hmax/Delta0]);
        ax.tick_params(labelsize=17);
        ax.set_xlabel("Temperature [$\Delta_0/k_B$]", size=17);
        ax.set_ylabel("External field [$\Delta_0/\mu_B$]", size=17);
        quadmesh = ax.pcolormesh(Tgrid/Delta0, Hgrid/Delta0, Deltagrid/Delta0, rasterized=True, antialiased=False);
        cbar = fig.colorbar(quadmesh, ax=ax);
        cbar.ax.tick_params(labelsize=17);
        cbar.set_label("Superconducting gap [$\Delta_0$]", size=17);
        return fig;

    def phaseDiagram(self, mu, Delta0, hwc, res, nT=32, nHext=32):
        "Plot the phase diagram."
        ksplus, ksmin = self.generate_ks(mu, hwc, res);
        V = self.Vint(mu, ksplus, ksmin, Delta0);
        # Base limits on found critical field and temperature:
        Tmax = 1.1 * Delta0 * np.exp(0.5772156649) / np.pi;
        Hmax = 1.1 * self.findHc(mu, Delta0, ksplus, ksmin, V);
        print("Critical field = {} Hp".format(np.sqrt(2)*Hmax/(1.1*Delta0)));
        Ts = np.linspace(0.0, Tmax, nT);
        Hs = np.linspace(0.0, Hmax, nHext);
        Tgrid, Hgrid = np.meshgrid(Ts, Hs, indexing='ij');
        phase = np.zeros((nT, nHext));
        colors = [1,2,5];
        for i in range(nT):
            for j in range(nHext):
                print("\rProcessing site ({}, {})...".format(i,j), end="");
                Delta = self.findDelta(mu, ksplus, ksmin, Delta0, Hs[j], Ts[i], V);
                nodalPoints = self.nodalPoints(mu, Delta, Hs[j]);
                phase[i][j] = colors[nodalPoints // 6] if Delta > Delta0/1000.0 else 8;
        # Plot the figure:
        fig = plt.figure();
        ax = fig.add_subplot(111, xlim=[0,Tmax/np.exp(0.5772156649)*np.pi/Delta0], ylim=[0,np.sqrt(2)*Hmax/Delta0]);
        ax.tick_params(labelsize=17);
        ax.set_xlabel("Temperature [$T_c$]", size=17);
        ax.set_ylabel("External field [$H_p$]", size=17);
        ax.pcolormesh(Tgrid/np.exp(0.5772156649)*np.pi/Delta0, np.sqrt(2)*Hgrid/Delta0, phase, cmap=plt.get_cmap("Set1"), vmin=0, vmax=8, rasterized=True, antialiased=True);
        return fig;

    def HcDiagram(self, muMin, muMax, Delta0Min, Delta0Max, res, hwcDeltaRatio=15.0, nMu=32, nDelta0=32, nCores=100, maxfactor=100.0):
        "Plot the critical field diagram by searching from which field delta will vanish."
        mus, dmu = np.linspace(muMin, muMax, nMu, retstep=True);
        Delta0s, dDelta0 = np.linspace(Delta0Min, Delta0Max, nDelta0, retstep=True);
        mugrid, Delta0grid = np.meshgrid(mus, Delta0s, indexing='ij');
        muflatgrid = np.reshape(mugrid, (nMu*nDelta0,)); Delta0flatgrid = np.reshape(Delta0grid, (nMu*nDelta0,));

        # Method to run parallel:
        def calcHc(mu, Delta0):
            hwc = Delta0 * hwcDeltaRatio;
            ksplus, ksmin = self.generate_ks(mu, hwc, res);
            V = self.Vint(mu, ksplus, ksmin, Delta0);
            try:
                return np.log( np.sqrt(2)*self.findHc(mu, Delta0, ksplus, ksmin, V, maxfactor=maxfactor)/Delta0 );
            except:
                return float('nan');

        client, dview, lview = hpc05.start_remote_and_connect(nCores, folder="~/isingsc/code");
        async_obj = lview.map_async(calcHc, muflatgrid, Delta0flatgrid);
        async_obj.wait_interactive();
        Hcs = np.reshape(async_obj.get(), (nMu, nDelta0));
        hpc05.kill_remote_ipcluster();

        # Plot the figure:
        fig = plt.figure();
        ax = fig.add_subplot(111, xlim=[muMin*1000,muMax*1000], ylim=[Delta0Min*1000,Delta0Max*1000]);
        ax.tick_params(labelsize=17);
        ax.set_xlabel("Chemical potential [meV]", size=17);
        ax.set_ylabel(r"$\Delta_0$ [meV]", size=17);
        muExtgrid, Delta0Extgrid = np.meshgrid( np.append(mus, muMax + dmu) - dmu/2, np.append(Delta0s, Delta0Max + dDelta0) - dDelta0/2, indexing='ij');
        quadmesh = ax.pcolormesh(muExtgrid*1000, Delta0Extgrid*1000, Hcs, rasterized=True, antialiased=True);
        cbar = fig.colorbar(quadmesh, ax=ax);
        #quadcontourmesh = ax.contourf(mugrid*1000, Delta0grid*1000, Hcs);
        #cbar = fig.colorbar(quadcontourmesh, ax=ax);
        cbar.ax.tick_params(labelsize=17);
        cbar.set_label(r"$\log(H_c / H_p)$", size=17);
        return fig;

    def HcDiagramApprox(self, muMin, muMax, Delta0Min, Delta0Max, res, hwcDeltaRatio=15.0, nMu=32, nDelta0=32):
        "Plot the critical field diagram by searching from which field delta will vanish."
        mus = np.linspace(muMin, muMax, nMu);
        Delta0s = np.linspace(Delta0Min, Delta0Max, nDelta0);
        mugrid, Delta0grid = np.meshgrid(mus, Delta0s, indexing='ij');
        Hcs = np.zeros((nMu, nDelta0));
        for i in range(nMu):
            kxgrid, kygrid, etagrid = self.create_kgrid(mus[i], Delta0Max*hwcDeltaRatio, res);
            for j in range(nDelta0):
                print("Processing site ({}, {})...".format(i,j), end="\r");
                hwc = Delta0s[j]*hwcDeltaRatio;
                ksplus, ksmin = self.choose_ks(kxgrid, kygrid, etagrid, mus[i], hwc);
                V = self.Vint(mus[i], ksplus, ksmin, Delta0s[j]);
                Hcs[i][j] = np.log( np.sqrt(2)*self.approxHc(mus[i], Delta0s[j], ksplus, ksmin, V)/Delta0s[j] );
        # Plot the figure:
        fig = plt.figure();
        ax = fig.add_subplot(111, xlim=[muMin*1000,muMax*1000], ylim=[Delta0Min*1000,Delta0Max*1000]);
        ax.tick_params(labelsize=17);
        ax.set_xlabel("Chemical potential [meV]", size=17);
        ax.set_ylabel(r"$\Delta_0$ [meV]", size=17);
        quadmesh = ax.pcolormesh(mugrid*1000, Delta0grid*1000, Hcs, rasterized=True, antialiased=False);
        cbar = fig.colorbar(quadmesh, ax=ax);
        cbar.ax.tick_params(labelsize=17);
        cbar.set_label(r"$\log(H_c / H_p)$", size=17);
        return fig;

    def HcDiagramAbsolute(self, muMin, muMax, Delta0Min, Delta0Max, res, hwcDeltaRatio=15.0, nMu=32, nDelta0=32, nCores=100, maxfactor=100.0, T=0.0):
        "Plot the critical field diagram by searching from which field delta will vanish. This version plots the actual critical field"
        mus, dmu = np.linspace(muMin, muMax, nMu, retstep=True);
        Delta0s, dDelta0 = np.linspace(Delta0Min, Delta0Max, nDelta0, retstep=True);
        mugrid, Delta0grid = np.meshgrid(mus, Delta0s, indexing='ij');
        muflatgrid = np.reshape(mugrid, (nMu*nDelta0,)); Delta0flatgrid = np.reshape(Delta0grid, (nMu*nDelta0,));

        # Method to run parallel:
        def calcHc(mu, Delta0):
            hwc = Delta0 * hwcDeltaRatio;
            ksplus, ksmin = self.generate_ks(mu, hwc, res);
            V = self.Vint(mu, ksplus, ksmin, Delta0);
            try:
                return 1000*self.findHc(mu, Delta0, ksplus, ksmin, V, maxfactor=maxfactor, T=T);
            except:
                return float('nan');

        client, dview, lview = hpc05.start_remote_and_connect(nCores, folder="~/isingsc/code");
        async_obj = lview.map_async(calcHc, muflatgrid, Delta0flatgrid);
        async_obj.wait_interactive();
        Hcs = np.reshape(async_obj.get(), (nMu, nDelta0));
        hpc05.kill_remote_ipcluster();

        # Dump the critical fields in a json file
        with open("data/results/Hcs_{}.json".format(datetime.now()), 'w+') as f:
            json.dump(Hcs.tolist(), f);

        # Plot the figure:
        fig = plt.figure();
        ax = fig.add_subplot(111, xlim=[muMin*1000,muMax*1000], ylim=[Delta0Min*1000,Delta0Max*1000]);
        ax.tick_params(labelsize=17);
        ax.set_xlabel("Chemical potential [meV]", size=17);
        ax.set_ylabel(r"$\Delta_0$ [meV]", size=17);
        muExtgrid, Delta0Extgrid = np.meshgrid( np.append(mus, muMax + dmu) - dmu/2, np.append(Delta0s, Delta0Max + dDelta0) - dDelta0/2, indexing='ij');
        quadmesh = ax.pcolormesh(muExtgrid*1000, Delta0Extgrid*1000, Hcs, rasterized=True, antialiased=True);
        cbar = fig.colorbar(quadmesh, ax=ax);
        #quadcontourmesh = ax.contourf(mugrid*1000, Delta0grid*1000, Hcs);
        #cbar = fig.colorbar(quadcontourmesh, ax=ax);
        cbar.ax.tick_params(labelsize=17);
        cbar.set_label(r"$\mu_B H_c$ [meV]", size=17);
        return fig;

    def phaseDiagramZeroT(self, muMin, muMax, Hmax, Delta0, hwc, res, nMu=100, nHext=100, nCores=100):
        "Plot the phase diagram for zero temperature."
        # Base limits of field on critical field in the middle of the region:
        mus = np.linspace(muMin, muMax, nMu);
        Hs = np.linspace(0.0, Hmax, nHext);
        mugrid, Hgrid = np.meshgrid(mus, Hs, indexing='ij');
        muflatgrid = np.reshape(mugrid, (nMu*nHext,)); Hflatgrid = np.reshape(Hgrid, (nMu*nHext,));
        colors = [1,2,5];

        # Method to run parallel:
        def detPhase(mu, Hext):
            ksplus, ksmin = self.generate_ks(mu, hwc, res);
            V = self.Vint(mu, ksplus, ksmin, Delta0);
            Delta = self.findDelta(mu, ksplus, ksmin, Delta0, Hext, 0.08617343*2/1000.0, V);
            nodalPoints = self.nodalPoints(mu, Delta, Hext);
            return colors[nodalPoints // 6] if Delta > Delta0/1000.0 else 8;

        client, dview, lview = hpc05.start_remote_and_connect(nCores, folder="~/isingsc/code");
        async_obj = lview.map_async(detPhase, muflatgrid, Hflatgrid);
        async_obj.wait_interactive();
        phase = np.reshape(async_obj.get(), (nMu, nHext));
        hpc05.kill_remote_ipcluster();

        # Plot the figure:
        fig = plt.figure();
        ax = fig.add_subplot(111, xlim=[muMin*1000,muMax*1000], ylim=[0,Hmax*1000]);
        ax.tick_params(labelsize=17);
        ax.set_xlabel(r"$\mu$ [meV]", size=17);
        ax.set_ylabel(r"$\mu_B H_{ext}$ [meV]", size=17);
        ax.pcolormesh(mugrid*1000, Hgrid*1000, phase, cmap=plt.get_cmap("Set1"), vmin=0, vmax=8, rasterized=True, antialiased=True);
        return fig;