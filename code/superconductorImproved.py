import numpy as np;
import scipy.linalg as linalg;
import scipy.optimize as optimize;
import matplotlib.pyplot as plt;

pauliX = np.array([[0.0, 1.0], [1.0, 0.0]]);
pauliY = np.array([[0.0, -1j], [1j, 0.0]]);
pauliZ = np.array([[1.0, 0.0], [0.0, -1.0]]);

class Superconductor:
    """
    Base class for s-wave superconductor.

    Properties:
    -----------
    Hnormal - the normal phase Hamiltonian with k dependence and living in Hilbert space spin-1/2 × norbs.
    kgrid - A numpy grid of all k points of the Brillouin zone.
    norbs - the number of orbitals.
    """
    def __init__(self, Hnormal, kgrid, norbs):
        # Values from input:
        self.Hnormal = Hnormal;
        self.kgrid = kgrid;
        self.norbs = norbs;

        # Values derived from input:
        self.nks = len(kgrid[0]);
        self.Hparticle = self.Hnormal(*self.kgrid).transpose(2,0,1);
        self.energies, self.states = linalg.eigh(self.Hparticle);

        particleBlock = np.apply_along_axis(np.diag, axis=1, self.energies);
        holeStates = np.kron(1j*pauliY, np.eye(self.norbs)) @ np.conj(self.states);
        HxBlock = self.states @ np.kron(pauliX, np.eye(self.norbs)) @ self.states.conj().transpose(0,2,1);
        HyBlock = self.states @ np.kron(pauliY, np.eye(self.norbs)) @ self.states.conj().transpose(0,2,1);
        HzBlock = self.states @ np.kron(pauliY, np.eye(self.norbs)) @ self.states.conj().transpose(0,2,1);

        self.SCpairingBlock = self.states @ np.kron(-1j*pauliY, np.eye(self.norbs)) @ holeStates.conj().transpose(0,2,1); # We need to filter the pairing first before we can make

    def filtered_energies(self, mu, hwc):
        """
        Return the energies and states that lie within mu - hwc and mu + hwc.
        """
        energyFilter = np.abs(self.energies - mu) <= hwc;
        return self.energies[energyFilter] - mu;

    def getV(self, energies, Delta):
        """
        Obtain the cooper pair interaction interaction given Delta at zero temperature and field.
        """
        return 2.0 / np.sum( 1.0 / np.sqrt(energies**2 + Delta**2) );

    def getTc(self, mu, hwc, Delta):
        """
        Find the critical temperature.
        """
        energies = self.filtered_energies(mu, hwc);
        V = self.getV(energies, Delta);
        def critTeq(T):
            return np.sum(np.tanh(np.abs(energies)/(2*T)) / np.abs(energies)) / 2 - 1/V;
        optimize.brentq(critTeq, 0.001*Delta, 1.0*Delta);

    def filterDeltaBlock(self, mu, hwc):
        """
        Remove the pairing from the superconducting pairing blocks where the energy is too far away from the chemical potential.
        """
        energyFilter = np.abs(self.energies - mu) <= hwc;
        filteredSCpairingBlock = self.SCpairingBlock;
        filteredSCpairingBlock[energyFilter,:] = 0j;
        return filteredSCpairingBlock;
