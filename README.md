# Ising superconductivity in TMD monolayers

## Research Goal
In this project, I will make a model to calculate the in-plane critical field of a superconducting TMD monolayer.
Additionally, I will try to predict (transport) properties of TMD monolayers.

## Research Plan
First, I will implement models to get the normal band structure of TMD monolayers and verify properties that have been reported earlier.
Second, I will investigate the TMD monolayer in superconducting phase by means of reported models and verify properties.
...

## Working on this project
Configure the project by running

    ./setup
