\chapter{The Löwdin partition method}\label{ch:lowdinpartitionmethod}
Let $\qty{\ket{\psi_n}}_n$ be a basis of orthonormal quantum states, which does not necessarily have to be complete.
Any state in the subspace spanned by this basis can be expressed as
\begin{equation}\label{eq:basis}
    \ket{\psi} = \sum_{n} c_n \ket{\psi_n}.
\end{equation}
I can approximate the eigenstates of an Hamiltonian $\hat{H}$ by using the variational method.
It is known that this method gives a set of equations, given by
\begin{equation}\label{eq:evproblem}
    \sum_{n} \qty(H_{mn} - E \delta_{mn}) c_n = 0\quad\forall m,
\end{equation}
where $H_{mn} = \mel{\psi_m}{\hat{H}}{\psi_n}$, and $\delta_{mn}$ is the Kronecker delta.
The energies $E$ for which non-trivial solutions for $c_n$ occur are the approximate eigenenergies of the Hamiltonian.

In the Löwdin partition method, the basis of orthonormal quantum states can be separated in two classes $A$ and $B$.
Then, I can reduce the eigenvalue problem given in \cref{eq:evproblem}, such that it only uses the orthonormal states in class $A$.
The set of equations will then become
\begin{equation}
    \sum_{n \in A} \qty(H_{mn}^{\mathit{eff}} - E \delta_{mn}) c_n = 0\quad\forall m \in A,
\end{equation}
where the Hamiltonian is replaced by an effective Hamiltonian, whose matrix elements are given by
\begin{equation}\label{eq:effHam}
    H_{mn}^{\mathit{eff}} = H_{mn} + \sum_{\alpha \in B} \frac{H_{m \alpha} H_{\alpha n}}{E - H_{\alpha\alpha}} + \sum_{\alpha \in B} \sum_{\beta \in B \setminus \alpha} \frac{H_{m \alpha} H_{\alpha \beta} H_{\beta n}}{\qty(E - H_{\alpha\alpha})\qty(E - H_{\beta\beta})} + \ldots
\end{equation}
The derivation of this effective Hamiltonian can be found back in Löwdins paper \cite{lowdin_note_1951}.
It is also interesting to note that this is in fact a generalization of perturbation theory leaned in undergraduate quantum mechanics.
Suppose that the Hamiltonian is given by $\hat{H} = \hat{H}_0 + \hat{V}$, where $\hat{V}$ represents a perturbation.
Furthermore, let the basis in \cref{eq:basis} be the eigenstates of the unperturbed Hamiltonian $\hat{H}_0$.
If class $A$ only contains a single non-degenerate eigenstate, then the effective Hamiltonian given in \cref{eq:effHam} can be reduced to the expression for perturbation theory.