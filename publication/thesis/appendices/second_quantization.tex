\chapter{Second quantization}\label{ch:secondquantization}
Second quantization is very useful and convenient to describe many-body systems.
As learned in undergraduate physics, the Hamiltonian of two identical particles must commute with the particle exchange operator $\hat{X}$, i.e. $[\hat{H},\hat{X}] = 0$.
The eigenstates and eigenvalues of the particle exchange operator can be evaluated using
\begin{equation}
    \hat{X}^2 = \hat{I} \rightarrow \hat{X} \ket{\phi_1} \ket{\phi_2} = \ket{\phi_2} \ket{\phi_1} = \pm \ket{\phi_1} \ket{\phi_2},
\end{equation}
where the eigenvalue $-1$ will be used for fermions (half-integer spin particles) and the eigenvalue $+1$ will be used for bosons (integer spin particles).
From this expression, it is clear that two fermions, unlike bosons, cannot occupy the same quantum state, which is also known as Pauli's exclusion principle.
For example, if $\ket{\psi_1}$ and $\ket{\psi_2}$ are two possible single-body eigenstates of the Hamiltonian than the eigenstates of the two body system can be written as
\begin{align}
    \ket{\Psi} &= \frac{1}{\sqrt{2}} \qty(\ket{\psi_1} \ket{\psi_2} + \ket{\psi_2} \ket{\psi_1}) \quad\text{(bosons)},\\
    \ket{\Psi} &= \frac{1}{\sqrt{2}} \qty(\ket{\psi_1} \ket{\psi_2} - \ket{\psi_2} \ket{\psi_1}) \quad\text{(fermions)}.
\end{align}
This can be generalized for systems that have $N$ identical particles.
In that case, swapping any two particles should either give the same wave function (bosons) or minus the original wave function (fermions).
This, however, complicates the wave function of many-body systems significantly, and they have to be expressed like
\begin{align}
    \ket{\Psi} &= \frac{1}{\sqrt{N!}} \sum_{\sigma \in S_N} \prod\limits_{i=1}^{N} \ket{\psi_{\sigma(i)}}  \quad\text{(bosons)},\\
    \ket{\Psi} &= \frac{1}{\sqrt{N!}} \sum_{\sigma \in S_N} \operatorname{sign}(\sigma) \prod\limits_{i=1}^{N} \ket{\psi_{\sigma(i)}} \quad\text{(fermions)},
\end{align}
where $S_N$ represents the set of all possible permutations of $N$ particles and $\operatorname{sign}(\sigma)$ is the sign of the permutation $\sigma$, which is $+1$ for even permutations and $-1$ for odd permutations.

Such states are difficult to work with, in particular when using statistical mechanics in numerical simulations.
In many-body systems, it is way more convenient to keep the number of particles unfixed.
This gives rise to the theory of second quantization.
Instead of expressing the Hamiltonian in terms of matrix elements of all possible many-particle states, which can be huge, the Hamiltonian is expressed in terms of creation and annihilation operators.
An example of such a Hamiltonian is
\begin{equation}
    \hat{H} = \sum_i \epsilon_i \ladderup{a}{i} \ladderdown{a}{i} - t \sum_{i>j} \ladderup{a}{i} \ladderdown{a}{j},
\end{equation}
where $\ladderup{a}{i}$ represents a creation operator of a particle in state $i$ and $\ladderdown{a}{i}$ the corresponding annihilation operator.
The effect of these operators on quantum states can be expressed as
\begin{align}
    \ladderup{a}{i} \ket{n_i} &= \sqrt{n_i+1} \ket{n_i+1},\label{eq:lupeffect} \\
    \ladderdown{a}{i} \ket{n_i} &= \sqrt{n_i} \ket{n_i-1},\label{eq:ldowneffect}
\end{align}
where $\ket{n_i}$ represents a quantum state with $n_i$ particles in state $i$.
These number states live in the so-called Fock space $\mathcal{F}$, which can be expressed as a Kronecker product of Hilbert spaces with all possible number of particles, which are all non-negative integers for bosons and 0 and 1 for fermions.
For fermions, it can sometimes happen that the resulting state should be multiplied with $-1$, but this will be explained later.
From the expressions in \cref{eq:lupeffect,eq:ldowneffect}, one can see that
\begin{equation}
    \ladderup{a}{i} \ladderdown{a}{i} \ket{n_i} = n_i \ket{n_i} = \hat{n_i} \ket{n_i},
\end{equation}
where $\hat{n_i} = \ladderup{a}{i} \ladderdown{a}{i}$ represents a number operator for particles in state $i$.
It is clear form this expression that the number states $\ket{n_i}$ are the eigenstates of this operator and the number of particles in state $i$ is the corresponding eigenvalue.

When working with second quantizations, it is important to know the commutation relations between the creation and annihilation operators.
For bosons, the commutation relations are
\begin{equation}
    [\ladderdown{a}{i}, \ladderdown{a}{j}] = 0, \qquad [\ladderup{a}{i}, \ladderup{a}{j}] = 0, \qquad [\ladderdown{a}{i}, \ladderup{a}{j}] = \delta_{ij},
\end{equation}
where $\delta_{ij}$ is the Kronecker delta.
For fermions these commutation relations are replaced by anticommutation relations given by
\begin{equation}
\{\ladderdown{a}{i}, \ladderdown{a}{j}\} = 0, \qquad \{\ladderup{a}{i}, \ladderup{a}{j}\} = 0, \qquad \{\ladderdown{a}{i}, \ladderup{a}{j}\} = \delta_{ij},
\end{equation}
where the anticommutator is defined as $\{\hat{A},\hat{B}\} = \hat{A}\hat{B} + \hat{B}\hat{A}$.
The commutation and anticommutation relations are consistent with the fact that many-body wave functions must be eigenstates of particle exchange operators.
For bosons, the commutation relations guarantee that it does not matter in what order particles are created or annihilated.
For fermions, on the other hand, a phase factor of $-1$ is needed if the order in creation or annihilation is swapped.
Hence the remark that earlier that the resulting states in \cref{eq:lupeffect,eq:ldowneffect} must sometimes be multiplied with $-1$.
The anticommutation relations of the fermions also support Pauli's exclusion principle as it clear from these that $\qty(\ladderup{a}{i})^2 = 0$, making it impossible to have two fermions in the same state.