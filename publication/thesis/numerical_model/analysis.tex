\section{Analysis of model}\label{sec:analysis_model}
\subsection{Practical input values}
In the numerical model presented in \cref{sec:description_model}, the input parameters are the temperature $T$, the external field $H_{ext}$, the chemical potential $\mu$, the superconducting pairing at zero temperature and magnetic field $\Delta_0$, the cutoff energy $\hbar \omega_D$, and the size of the equally spaced $\vb{k}$-point grid.
The highest relevant temperature (i.e. the critical temperature) is related to $\Delta_0$.
In the weak-coupling limit, it is known that $\Delta_0$ and the critical temperature are related by \cref{eq:universalTc}.
According to Lu et al., the maximum critical temperature of \ce{MoS2} is around \SI{10}{\K} \cite{lu_evidence_2015}.
If the weak-coupling limit can be applied to TMD monolayer superconductors, then $\Delta_0 \approx \SI{1.5}{\meV}$.
According to Peng et al., the Debye energy of \ce{MoS2} is \SI{22.6}{\meV} \cite{peng_thermal_2016}, which looks consistent with the requirement that $\hbar \omega_D \gg \Delta_0$ for weak-coupling superconductors.
In a more recent experiment by Shi et al., it has been found that \ce{MoSe2} has a maximum critical temperature of \SI{7.1}{\K} \cite{shi_superconductivity_2015}, which comes down to a superconducting pairing of $\Delta_0 \approx \SI{1.1}{\meV}$ in the weak-coupling limit.
The Debye energy of \ce{MoSe2} is \SI{15.3}{\meV} according to Peng et al., so it is also arguable that \ce{MoSe2} can be described as a weak-coupling superconductor.
However, in order to conclude whether the Debye energy is high enough for the weak-coupling limit, one need to consider if the value of $\Delta$ is sufficiently converged, which is explained in more detail in the next section.

\subsection{Convergence in the weak coupling limit}\label{subsec:convergence}
In this thesis, the cutoff energy is equal to the Debye energy $\hbar \omega_D$.
In some reports, however, the cutoff energy is set to a finite number of times the Debye energy (e.g. 3 times \cite{szczesniak_theoretical_2014,carbotte_properties_1990}).
In the weak-coupling limit when the cutoff energy energy diverges, the $\Delta$ that minimizes the free energy should converge to a specific value.
This means that in this limit, it should not matter how large the cutoff energy is as long as it is much larger than the maximum superconducting pairing $\Delta_0$.
In \cref{fig:weak-coupling_convergence}, the convergence of $\Delta$ is demonstrated for \ce{MoSe2} at $\mu = \SI{0.15}{\eV}$.
It can been seen that $\Delta$ will indeed converge for large cutoff energies that are several times $\Delta_0$, although noise will be visible if the number $\vb{k}$-points is too low.
This is in particular the case when one approaches the critical field (compare \cref{fig:convergence_mu=0.15_T=0.0_Hext=1.5,fig:convergence_mu=0.15_T=0.0_Hext=2.0}).

\begin{figure}[ht]
    \centering
    \begin{subfigure}[b]{0.48\linewidth}
        \centering
        \includegraphics[height=170px]{numerical_model/figures/{convergence_mu=0.15_T=0.52_Hext=0.0}.pdf}
        \caption{$T = 0.52 \Delta_0$, $H_{ext} = 0 H_p$}
        \label{fig:convergence_mu=0.15_T=0.52_Hext=0.0}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.48\linewidth}
        \centering
        \includegraphics[height=170px]{numerical_model/figures/{convergence_mu=0.15_T=0.0_Hext=1.5}.pdf}
        \caption{$T = 0 \Delta_0$, $H_{ext} = 1.5 H_p$}
        \label{fig:convergence_mu=0.15_T=0.0_Hext=1.5}
    \end{subfigure}

    \begin{subfigure}[b]{0.48\linewidth}
        \centering
        \includegraphics[height=170px]{numerical_model/figures/{convergence_mu=0.15_T=0.0_Hext=2.0}.pdf}
        \caption{$T = 0 \Delta_0$, $H_{ext} = 2.0 H_p$}
        \label{fig:convergence_mu=0.15_T=0.0_Hext=2.0}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.48\linewidth}
        \centering
        \includegraphics[height=170px]{numerical_model/figures/{convergence_mu=0.15_T=0.3_Hext=1.0}.pdf}
        \caption{$T = 0.3 \Delta_0$, $H_{ext} = 1.0 H_p$}
        \label{fig:convergence_mu=0.15_T=0.3_Hext=1.0}
    \end{subfigure}
    \caption{The superconducting pairing $\Delta$ as a function of the cutoff energy for \ce{MoSe2} at $\mu = \SI{0.15}{\eV}$. Calculations are done with a $100 \times 100$ (blue), $250 \times 250$ (yellow), and $500 \times 500$ (green) $\vb{k}$-point grid. Again $H_p = \Delta_0 / \sqrt{2}$ is the Pauli limit.}
    \label{fig:weak-coupling_convergence}
\end{figure}

\subsection{Uncertainty analysis}\label{subsec:uncertainty_analysis}
As shown in \cref{fig:weak-coupling_convergence}, the value of $\Delta$ that minimizes the free energy has an uncertainty depending on the resolution of the $\vb{k}$-point grid.
The question is how high the resolution of the $\vb{k}$-point grid should be in order to make the uncertainty acceptably low.
As it can be very time consuming to calculate the uncertainty of the critical field for every $\Delta_0$ and $\mu$, it is better to have an upper bound of the uncertainty given the upper and lower bound of $\mu$ and $\Delta_0$.
This suggest the following procedure to determine the maximum uncertainty as function of the resolution of the $\vb{k}$-point grid:
for a given material with a given $\mu$ and $\Delta_0$, calculate the critical field for several random cutoff energies $E_D \gg \Delta_0$.
The uncertainty can then be determined by computing the standard deviation of the found critical fields.
This process will be done for all combinations of boundary values of $\mu$ and $\Delta_0$ and in the end, one will simply pick the combination with the highest uncertainty.

The goal now is to find the minimum resolution of the $\vb{k}$-point grid such that the relative uncertainty of the critical field is below a certain value.
In this thesis, it will be assumed that a relative uncertainty of $0.05$ (5\%) is acceptable.
The calculated uncertainties as function of the resolution for \ce{MoSe2} are displayed in \cref{fig:rel_err_Hc}.
This is done by picking 12 random cutoff energies using an uniform distribution on the interval $\qty[15 \Delta_0, 20 \Delta_0]$, and then calculate the relative uncertainty by dividing the standard deviation by the mean.
Comparing \cref{fig:rel_err_mu=0.134_Delta0=0.0001} with the others, it can been seen that the highest uncertainty is at $\mu = \SI{134}{\meV}$ and $\Delta_0 = \SI{0.1}{\meV}$.
It should be noted that this chemical potential is centered within the vanishing region of \ce{MoSe2}.
From \cref{fig:rel_err_mu=0.134_Delta0=0.0001}, it appears that a $1000 \times 1000$ $\vb{k}$-point grid is sufficient in order to obtain a relative uncertainty below 5\% for the calculated critical field of \ce{MoSe2}.
Also for other materials, the highest uncertainty can be found for low $\Delta_0$ and for a chemical potential centered in the vanishing region.
It can be argued that a $600 \times 600$ $\vb{k}$-point grid is sufficient for \ce{MoS2} and a $1200 \times 1200$ $\vb{k}$-point grid is sufficient for \ce{MoTe2} (see \cref{fig:rel_err_other_materials} in \cref{ch:rel_err_other_materials}).

\begin{figure}[ht]
    \centering
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=115px]{numerical_model/figures/{rel_err_mu=0.25_Delta0=0.001}.pdf}
        \caption{$\mu = \SI{250}{\meV}$, $\Delta_0 = \SI{1}{\meV}$}
        \label{fig:rel_err_mu=0.25_Delta0=0.001}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=115px]{numerical_model/figures/{rel_err_mu=0.134_Delta0=0.001}.pdf}
        \caption{$\mu = \SI{134}{\meV}$, $\Delta_0 = \SI{1}{\meV}$}
        \label{fig:rel_err_mu=0.134_Delta0=0.001}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=115px]{numerical_model/figures/{rel_err_mu=0.05_Delta0=0.001}.pdf}
        \caption{$\mu = \SI{50}{\meV}$, $\Delta_0 = \SI{1}{\meV}$}
        \label{fig:rel_err_mu=0.05_Delta0=0.001}
    \end{subfigure}
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=115px]{numerical_model/figures/{rel_err_mu=0.25_Delta0=0.0001}.pdf}
        \caption{$\mu = \SI{250}{\meV}$, $\Delta_0 = \SI{0.1}{\meV}$}
        \label{fig:rel_err_mu=0.25_Delta0=0.0001}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=115px]{numerical_model/figures/{rel_err_mu=0.134_Delta0=0.0001}.pdf}
        \caption{$\mu = \SI{134}{\meV}$, $\Delta_0 = \SI{0.1}{\meV}$}
        \label{fig:rel_err_mu=0.134_Delta0=0.0001}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=115px]{numerical_model/figures/{rel_err_mu=0.05_Delta0=0.0001}.pdf}
        \caption{$\mu = \SI{50}{\meV}$, $\Delta_0 = \SI{0.1}{\meV}$}
        \label{fig:rel_err_mu=0.05_Delta0=0.0001}
    \end{subfigure}
    \caption{The relative uncertainty of the critical field as a function of the resolution for \ce{MoSe2}. A resolution of $n$ means that an $n \times n$ $\vb{k}$-point grid has been used.}
    \label{fig:rel_err_Hc}
\end{figure}

\subsection{Free electron case}
As discussed in \cref{subsec:free2Delectrons}, it is expected that the critical field in case of 2D free electrons should reach the Pauli limit (see \cref{eq:Hc}).
Moreover, one should also expect to find that the critical temperature is given by \cref{eq:universalTc}.
The presented numerical \kdotp model can be reduced to a free electron model by setting $A \approx \SI{3.81}{\eV\per\angstrom\squared}$ and $B = C = D = 0$.
Plots of the critical field and temperature of the model as well as the analytical expressions (\cref{eq:Hc,eq:universalTc} respectively) are shown in \cref{fig:freeElectronCritical}.
During the calculation, $\mu = \SI{0.1}{\eV}$, $E_D = 15\Delta_0$, and the size of the $\vb{k}$-point grid is $500 \times 500$.
It can be seen in this figure that the results of the model are in good agreement with the analytical expressions.

\begin{figure}[ht]
    \centering
    \begin{subfigure}[b]{0.48\linewidth}
        \centering
        \includegraphics[height=170px]{numerical_model/figures/freeElectronsHc}
        \caption{}
        \label{fig:freeElectronsHc}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.48\linewidth}
        \centering
        \includegraphics[height=170px]{numerical_model/figures/freeElectronsTc}
        \caption{}
        \label{fig:freeElectronsTc}
    \end{subfigure}
    \caption{The critical field (a) and temperature (b) as a function of $\Delta_0$ in the free electron case. The red lines are computed using the numerical \kdotp model, while the black dashed lines are the analytical expressions discussed in \cref{sec:basic_theory_sc}. Calculation is done for $\mu = \SI{0.1}{\eV}$, $E_D = 15\Delta_0$, and a $500 \times 500$ $\vb{k}$-point grid.}
    \label{fig:freeElectronCritical}
\end{figure}