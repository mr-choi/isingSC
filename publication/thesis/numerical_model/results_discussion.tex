\section{Results and discussion}\label{sec:results_and_discussion_numerical_model}
\subsection{Critical field}
Using the procedure described in \cref{sec:description_model} and the desired resolutions discussed in \cref{subsec:uncertainty_analysis}, the critical field diagrams are calculated and displayed in \cref{fig:HcDiagram}.
For every chemical potential $\mu$ and maximum superconducting pairing $\Delta_0$, a separate $\vb{k}$-point grid has been generated and a cutoff energy of $15 \Delta_0$ has been used in the calculations.
The white dotted lines in the figure indicate the vanishing region given in \cref{tab:vanishingregion}.
The figures confirm that the critical field will be suppressed when the chemical potential approaches the vanishing region.
Furthermore, it can also be seen that the critical field is lower for lower $\Delta_0$.
However, the critical field with respect to the Pauli limit $H_c / H_p$ is higher for lower $\Delta_0$.
This may be the effect of the SOI, which contributes to the critical field without any $\Delta_0$ dependence.
An other remarkable feature is that the critical field will be completely suppressed to (virtually) the Pauli limit in case of \ce{MoS2}.
This can be explained from the fact that the spin-orbit interaction of \ce{MoS2}, compared to the other materials, is very weak at the bottom of the conduction band.
For \ce{MoSe2} and \ce{MoTe2}, there is a dark strip visible near $\mu = 0$.
This is likely the effect of the higher energetic spin-down band at the $+\mathrm{K}$ point (and the spin-up band at the $-\mathrm{K}$ point) as the strip is located at $\mu = B$ in both cases.

\begin{figure}[ht]
    \centering
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=100px]{numerical_model/figures/HcDiagram_MoS2_abs.pdf}
        \caption{}
        \label{fig:HcDiagram_MoS2_abs.pdf}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=100px]{numerical_model/figures/HcDiagram_MoSe2_abs.pdf}
        \caption{}
        \label{fig:HcDiagram_MoSe2_abs.pdf}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=100px]{numerical_model/figures/HcDiagram_MoTe2_abs.pdf}
        \caption{}
        \label{fig:HcDiagram_MoTe2_abs.pdf}
    \end{subfigure}

    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=100px]{numerical_model/figures/HcDiagram_MoS2.pdf}
        \caption{}
        \label{fig:HcDiagram_MoS2.pdf}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=100px]{numerical_model/figures/HcDiagram_MoSe2.pdf}
        \caption{}
        \label{fig:HcDiagram_MoSe2.pdf}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=100px]{numerical_model/figures/HcDiagram_MoTe2.pdf}
        \caption{}
        \label{fig:HcDiagram_MoTe2.pdf}
    \end{subfigure}
    \caption{Critical field diagrams for (a,d) \ce{MoS2}, (b,e) \ce{MoSe2}, and (c,f) \ce{MoTe2} as function of chemical potential and $\Delta_0$. In accordance with the discussion in \cref{subsec:uncertainty_analysis}, the used resolutions for the calculations are $600 \times 600$, $1000 \times 1000$ and $1200 \times 1200$ respectively. For every $\mu$ and $\Delta_0$, a separate $\vb{k}$-point grid has been generated and the cutoff energy is set to $E_D = 15 \Delta_0$. The white dotted lines indicate vanishing region based on the values in \cref{tab:vanishingregion} and the red solid line indicate the critical chemical potential. The diagrams in (a), (b), and (c) show the actual value of the critical field, while the diagrams in (d), (e), (f) show the log of critical field with respect to the Pauli limit $H_p$.}
    \label{fig:HcDiagram}
\end{figure}

Using the results in \cref{fig:HcDiagram}, one can now make a prediction of the critical field in experiments.
Previous experiments have shown that the critical temperature (and thus $\Delta_0$) depends on the carrier density in the lowest conduction band (and thus the chemical potential).
Thus, for each material, a trajectory through the diagram in \cref{fig:HcDiagram} can be associated with.
Ye et al. and Lu et al.\cite{ye_superconducting_2012,lu_evidence_2015} have shown that \ce{MoS2} will become superconducting if the carrier density in the conduction band exceeds \SI{6e13}{\per\centi\meter\squared}.
If the effect of spin-orbit interaction is negligible, the corresponding chemical potential can be calculated by
\begin{equation}
    \mu \approx A \pi n,
\end{equation}
where $A$ is the \kdotp parameter discussed in \cref{sec:superconductingTMD} and $n$ the carrier density.
Therefore, the minimum chemical potential required to obtain superconducting \ce{MoS2} is given by $\mu_c = \SI{153}{\meV}$, and this will be called the critical chemical potential.
Critical chemical potentials are also indicated in \cref{fig:HcDiagram} with red solid lines.
From Shi et al.\cite{shi_superconductivity_2015}, it can be found that the critical carrier density for \ce{MoSe2} is \SI{8e13}{\per\centi\meter\squared}, which results in a critical chemical potential of $\SI{160}{\meV}$.
However, this value may be overestimated, because the temperature of the experiment did not go below \SI{2}{\K}.
A calculation in a more recent experiment by Miao et al.\cite{miao_emergence_2016} suggests that the critical density is rather roughly \SI{6e13}{\per\centi\meter\squared}, the same value as for \ce{MoS2}.
This implies critical chemical potential of $\SI{120}{\meV}$ which is just below the vanishing region (see also \cref{fig:HcDiagram_MoSe2.pdf}).
Furthermore, Shi et al. suggest that the critical density of \ce{MoTe2} has to be at least \SI{7e13}{\per\centi\meter\squared} as no superconductivity has been found for this maximum carrier density.
Again, this value may be overestimated due to the fact the experiment did not go below \SI{2}{\K}.
If the critical carrier density is the same as for \ce{MoS2} and \ce{MoSe2}, then the corresponding critical chemical potential for \ce{MoTe2} is \SI{116}{\meV}, well below the vanishing region (see \cref{fig:HcDiagram_MoTe2.pdf}).

Due to the fact that the critical chemical potential of \ce{MoS2} is above the vanishing region, one does not expect to see a drop of the critical field for these two materials.
Data from the supplementary data of Ye et al. (figure S3b) also confirm this.
For \ce{MoSe2}, the critical carrier density is seemingly \SI{8e13}{\meV} in Shi et al.'s experiment, which results in a critical chemical potential above the vanishing region.
The supplementary data of the critical field in Shi et al. (figure S5c) also confirms this as there is no drop of critical field visible in the dome.
However, for a lower temperature, it may be possible to get the critical chemical potential below the vanishing region.
In that case, it would be expected to see a suppression of the critical field.

\subsection{Topological phase diagram}

\subsection{Varying chemical potential and critical field, fixed temperature}
The topological phase diagram for \ce{MoTe2} at a fixed temperature of \SI{2}{\K} is shown in \cref{fig:phaseDiagramConstT}.
All the calculations are done for $\Delta_0 = \SI{1}{\meV}$, and $E_D = 15 \Delta_0$.
Furthermore, the resolution of the $\vb{k}$-point grid was $500 \times 500$ for all calculations.
The reason why \SI{2}{\K} has been chosen instead of \SI{0}{\K} is that it is very difficult to accurately determine the transition of superconducting phase to normal phase when at \SI{0}{\K}.
Consistent with figure 1b in Wang et al.\cite{wang_new_2018}, the twelve nodal point topological superconductivity can only be obtained when the chemical potential is inside the vanishing region.
Furthermore, one does need an external field energy $\mu_B H_{ext}$ of approximately \SI{0.8}{\meV} to obtain the topological phase.
The six nodal point topological superconducting phase is visible near the boundaries of the vanishing region.
\Cref{fig:phaseDiagramConstT} also makes clear that only close to the vanishing region, nodal topological superconductivity is present.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\linewidth]{numerical_model/figures/phaseDiagramConstT_MoTe2.pdf}
    \caption{Topological phase diagram of \ce{MoTe2} at \SI{2}{\K}. The possible phases are: trivial superconducting phase (blue); twelve nodal point topological superconducting phase (yellow); six nodal point topological superconducting phase (green); normal phase (gray). All calculations are done with a $500 \times 500$ $\vb{k}$-point grid and for $\Delta_0 = \SI{1}{\meV}$, and $E_D = 15 \Delta_0$. The black dashed lines indicate the vanishing region of \ce{MoTe2}}
    \label{fig:phaseDiagramConstT}
\end{figure}


\subsection{Varying temperature and critical field, fixed chemical potential}
Topological phase diagrams for different materials and chemical potential are shown in \cref{fig:phaseDiagram}.
All the calculations are done for $\Delta_0 = \SI{1}{\meV}$, and $E_D = 15 \Delta_0$.
Furthermore, the resolution of the $\vb{k}$-point grid was $500 \times 500$ for all calculations.
\Cref{fig:phaseDiagramMoS2_mu=0.033,fig:phaseDiagramMoSe2_mu=0.134,fig:phaseDiagramMoTe2_mu=0.159} show the phase diagram of the three materials mentioned in \cref{subsec:kpparameters} in case the chemical potential lies (approximately) in the middle of the vanishing region.
In that case, the trivial topological phase becomes a twelve nodal point topological superconducting phase when the field is sufficiently increased.
This can be explained from figure 1b in Wang et al.\cite{wang_new_2018} or \cref{eq:nodalpointequation}: when the the chemical potential is in the middle of the vanishing region, the material will be in the twelve nodal point topological superconducting phase when
\begin{equation}
    0 < H_{ext}^2 - \Delta\qty(H_{ext})^2 < \qty(\frac{\mu_{max} - \mu_{min}}{2})^2.
\end{equation}
Otherwise, the material is in the trivial superconducting phase when $\Delta > 0$ and in the normal phase when $\Delta = 0$.
Note that $\Delta\qty(H_{ext})$ decreases as the external field increases.
\Cref{fig:phaseDiagramMoS2_mu=0.033,fig:phaseDiagramMoSe2_mu=0.134,fig:phaseDiagramMoTe2_mu=0.159} also show that strength of the spin-orbit interaction has a significant effect on the topological phase diagram.
It can clearly be seen that a nodal topological superconducting phase is much easier to obtain in materials with a higher SOI like \ce{MoTe2}.

In case the chemical potential is not centered in the vanishing region, the material may also be in the six nodal point topological superconducting phase if the external field is enhanced even more (\cref{fig:phaseDiagramMoTe2_mu=0.18}).
However, this does not mean that this phase will always be visible as $\Delta$ may become zero before $\sqrt{H_{ext}^2 - \Delta\qty(H_{ext})^2}$ is large enough.
Once the chemical potential lies outside the vanishing region, it is not possible that the material is in the twelve nodal point topological superconducting phase (\cref{fig:phaseDiagramMoTe2_mu=0.185}).
The material may be in the six nodal point topological phase when
\begin{equation}
    \Delta \mu < \sqrt{H_{ext}^2 - \Delta\qty(H_{ext})^2} < \Delta \mu + \mu_{max} - \mu_{min},
\end{equation}
where $\Delta \mu$ is the nearest distance of the chemical potential from the vanishing region.
As the minimum value of $\Delta \mu$ for nodal topological superconductivity increases faster than the energy of the critical field, no nodal topological superconductivity will be visible when $\Delta \mu$ is high enough (\cref{fig:phaseDiagramMoTe2_mu=0.19}).
Thus, nodal topological superconductivity is only acquirable when the chemical potential is in or close to the vanishing region.

\begin{figure}[ht]
    \centering
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=110px]{numerical_model/figures/{phaseDiagramMoS2_mu=0.033}.pdf}
        \caption{\ce{MoS2}, $\mu = \SI{0.033}{\eV}$.}
        \label{fig:phaseDiagramMoS2_mu=0.033}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=110px]{numerical_model/figures/{phaseDiagramMoSe2_mu=0.134}.pdf}
        \caption{\ce{MoSe2}, $\mu = \SI{0.134}{\eV}$.}
        \label{fig:phaseDiagramMoSe2_mu=0.134}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=110px]{numerical_model/figures/{phaseDiagramMoTe2_mu=0.159}.pdf}
        \caption{\ce{MoTe2}, $\mu = \SI{0.159}{\eV}$.}
        \label{fig:phaseDiagramMoTe2_mu=0.159}
    \end{subfigure}

    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=110px]{numerical_model/figures/{phaseDiagramMoTe2_mu=0.18}.pdf}
        \caption{\ce{MoTe2}, $\mu = \SI{0.18}{\eV}$.}
        \label{fig:phaseDiagramMoTe2_mu=0.18}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=110px]{numerical_model/figures/{phaseDiagramMoTe2_mu=0.185}.pdf}
        \caption{\ce{MoTe2}, $\mu = \SI{0.185}{\eV}$.}
        \label{fig:phaseDiagramMoTe2_mu=0.185}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=110px]{numerical_model/figures/{phaseDiagramMoTe2_mu=0.19}.pdf}
        \caption{\ce{MoTe2}, $\mu = \SI{0.19}{\eV}$.}
        \label{fig:phaseDiagramMoTe2_mu=0.19}
    \end{subfigure}
    \caption{Topological phase diagrams for different materials at different chemical potential. The possible phases are: trivial superconducting phase (blue); twelve nodal point topological superconducting phase (yellow); six nodal point topological superconducting phase (green); normal phase (gray). All calculations are done with a $500 \times 500$ $\vb{k}$-point grid and for $\Delta_0 = \SI{1}{\meV}$, and $E_D = 15 \Delta_0$. $H_p$ denotes the Pauli limit while $T_c$ denotes the critical temperature defined in \cref{eq:universalTc}.}
    \label{fig:phaseDiagram}
\end{figure}