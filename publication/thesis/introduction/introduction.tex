\chapter{Introduction}\label{ch:introduction}

The main purpose of this research is to calculate the in-plane critical field of a superconducting transition metal dichalcogenide (TMD) monolayer by means of a numerical model.
In this thesis, the theory of the TMD monolayer and a numerical model to calculate in-plane critical fields will be explained.
%Although some sections in this thesis can be very technical, I attempt to make the text as accessible as possible for bachelor students who are proficient in solid state physics and want to do a bachelor research project in this area.
%I also provided a summary (in Dutch) that should be accessible for everyone and this summary can be found in \cref{ch:samenvatting_iedereen}.
TMD monolayers are semiconductors that have a structure of the form \ce{MX2} and consist of a transition metal and two chalcogens.
In general, a transition metal is any metal that has a partially filled $d$-shell, but this thesis will only focus on group 6 transition metals like molybdenum (\ce{Mo}) and tungsten (\ce{W}).
%It is also worth to mention that a TMD with a transition metal of a different group likely has different properties.
A chalcogen, on the other hand, is a group 16 atom such as sulfur (\ce{S}) or selenium (\ce{Se}).
Although oxygen (\ce{O}) is technically a chalcogen as well, it will not be considered in this thesis, as it has very different chemical properties compared to other chalcogens.
The crystal structure of group 6 TMD monolayers is displayed in \cref{fig:TMDmonolayer}.
As it can be seen in the 3D figure (\cref{fig:TMDmonolayer_crystal}), the transition metal atoms are sandwiched between two chalcogen layers and each layer has an equal number of atoms (hence \ce{MX2}).
From the top view (\cref{fig:TMDmonolayer_topview}), the structure is very similar to the honeycomb structure of graphene, except it does not have a point inversion symmetry in the bonds due to the alternation between two different atoms.

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{0.95\linewidth}
		\centering
		\includegraphics[height=120px]{introduction/figures/TMDmonolayer_crystal.png}
		\caption{}
		\label{fig:TMDmonolayer_crystal}
	\end{subfigure}
	\begin{subfigure}[b]{0.52\linewidth}
		\centering
		\includegraphics[height=140px]{introduction/figures/TMDmonolayer_topview.png}
		\caption{}
		\label{fig:TMDmonolayer_topview}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.43\linewidth}
		\centering
		\includegraphics[height=140px]{introduction/figures/TMDmonolayer_unit_cell.png}
		\caption{}
		\label{fig:TMDmonolayer_unit_cell}
	\end{subfigure}
	\caption{The crystal structure of a group 6 TMD monolayer with the transition metal in blue and the chalcogens in yellow. (a) 3D figure; (b) top view; (c) single unit cell. The figures are rendered with XCrysDen \cite{kokalj_xcrysdennew_1999}.}
	\label{fig:TMDmonolayer}
\end{figure}

The history and research behind TMD monolayers is closely linked to that of graphene, which has been experimentally discovered by Geim and Novoselov in 2004 \cite{novoselov_electric_2004}.
%Both TMD monolayers and graphene are namely examples of pure 2D crystals, which was believed in the past to be thermally unstable and thus non-existing \cite{peierls_quelques_1935,landau_zur_1937,mermin_crystalline_1968}.
%This notion, however, has been challenged when Geim and Novoselov experimentally discovered graphene in 2004 \cite{novoselov_electric_2004}.
Not much later, Novoselov et al. demonstrated the fabrication of other 2D materials like boron nitride and also some TMD monolayers like \ce{MoS2} by means of a mechanical cleaving technique \cite{novoselov_two-dimensional_2005}.
This technique is also used in follow-up experiments \cite{mak_atomically_2010,splendiani_emerging_2010}.
It should be noted, however, that the technique is not scalable, although research has been done to improve this \cite{magda_exfoliation_2015}.
Since the experimental discovery of graphene, more and more research has been done on TMD monolayers (see also fig. 1 in ref. \cite{choi_recent_2017}).

One important property of TMD monolayer semiconductors is that they have a direct bandgap, which has also been found in experiments \cite{mak_atomically_2010,splendiani_emerging_2010}.
This is different from bulk TMD, which has an indirect bandgap \cite{wilson_transition_1969,bromley_band_1972,mattheiss_band_1973}.
The direct band gap of TMD monolayers makes it suitable for several potential applications, such as field effect transistors \cite{radisavljevic_single-layer_2011} or photo-detectors \cite{lopez-sanchez_ultrasensitive_2013}.
Two other important properties of TMD monolayers are due to the lack of inversion symmetry.
The first one is that the interaction between electrons and circular polarized light is valley-dependent \cite{zeng_valley_2012,yao_valley-dependent_2008}, which has potential for new type of devices called valleytronics \cite{rycerz_valley_2007}.
The other one is the exhibition of Ising spin-orbit interaction (Ising SOI), which fixes the electron spin in the out-of-plane directions \cite{xiao_coupled_2012,kormanyos_monolayer_2013}.
This is different from Rashba spin-orbit interaction where the electron spins are in-plane.

%In this thesis, however, I will focus on a different interesting aspect of some TMD monolayers: Ising superconductivity \cite{lu_evidence_2015}.
%Ising superconductivity has the property that it has a remarkably high in-plane critical magnetic field.
%In general, the critical magnetic field of (conventional) superconductors is limited by the so called Pauli paramagnetic limit.
%Paramagnetism breaks superconductivity because spins in a paramagnet want to be aligned with the magnetic field while conventional superconductors consist of Cooper pairs with opposing spins.
%In TMD monolayers, however, the in-plane critical field will largely exceed this Pauli paramagnetic limit \cite{lu_evidence_2015}.
%Although the main focus is to determine the in-plane critical field of a TMD monolayer I will also investigate more (transport) properties of Ising superconductor as this phenomenon is quite novel (at the time the thesis was written).

This thesis will focus on the superconducting phase of TMD monolayers.
When combining superconductivity with the Ising SOI of TMD monolayers, one will obtain Ising superconductors.
It has been shown in experiments that TMD monolayers have an enhanced in-plane critical field \cite{lu_evidence_2015}.
This can be explained from the fact the Ising SOI interaction keeps the electron spins out-of-plane, effectively protecting the spins from aligning along an external in-plane magnetic field, which would break the superconductivity of the TMD monolayer.
The focus of this thesis will be particularly on TMD monolayers with nodal topological superconductivity, introduced by Wang et al.\cite{wang_new_2018}
The source of this topology is the vanishing SOI at the lowest conduction band near the $\mathrm{K}$ point, which will be discussed in more detail in this thesis.
It is expected that the critical field will be suppressed when the chemical potential crosses the vanishing spin-orbit interaction in the conduction band as a weaker SOC implies a weaker protection.

%Superconductors with Ising SOC (also called Ising superconductors) are very interesting to study in the area of topology.
%For example, Zhou et al. have proposed an idea to create Majorana fermions using a Ising superconductor and a half-metal wire \cite{zhou_ising_2016}, which can be very useful in the area of topological quantum computation %\cite{kitaev_unpaired_2001,bravyi_fermionic_2002}.
%In this thesis, however, the focus will be on nodal topological superconductivity of TMD monolayers, introduced by Wang et al.\cite{wang_new_2018}
%The source of this topology is the vanishing SOI at the conduction band near the $\mathrm{K}$-point, which will be discussed in more detail in this thesis.
%It is expected that the critical field will be suppressed when the chemical potential crosses the vanishing spin-orbit interaction in the conduction band as a weaker SOC implies a weaker protection.

The contents of this thesis are as follows:
\begin{itemize}
    \item in \cref{ch:bandstructure}, the band structure of TMD monolayers in the normal phase will be visualized, which will be needed to explain Ising superconductivity later;
    \item in \cref{ch:isingsuperconductivity}, Ising superconductivity will be explained in more detail using Bogoliubov-De Gennes formalism for conventional superconductors;
    \item in \cref{ch:numerical_model}, a numerical model will be presented to calculate the in-plane critical field for Ising superconducting TMD monolayers, using the concepts introduced in the previous chapter;
    \item finally, a conclusion about my research will be given in \cref{ch:conclusion}.
\end{itemize}