\section{\kdotp method around the $\pm\mathrm{K}$ point}\label{sec:kpmodel}
An interesting feature of the spin splitting of the conduction band in \ce{MoS2} is that the spin-up and spin-down band cross each other near the $\pm\mathrm{K}$ point.
Such a crossing does not always happen in all TMD monolayers.
TMD monolayers with molybdenum as transition metal usually have this crossing, but monolayers with tungsten do not \cite{kosmider_large_2013}.
This crossing, however, is very important later on, as these are the source of nodal topological superconductivity which will be explained in the next chapter.
It is useful later on to have a simple, but accurate expression of the conduction band near the $\pm\mathrm{K}$ point, and this can be done by deriving a \kdotp model.

\subsection{\kdotp model}\label{subsec:k.p_model}
In order to set up a \kdotp model around a symmetry point, a set of eigenstates at that point in reciprocal space is required.
Most models only pick (approximate) eigenstates of the lowest conduction band and highest valence band, and this is also sufficient in this case.
According to the three-band model (\cref{sec:3band}), the eigenstates of the conduction and valence band at the $\eta\mathrm{K}$ point are given by
\begin{align}
    \ket{\psi_{cb}} &= \ket{\dzsq},\\
    \ket{\psi^\eta_{vb}} &= \frac{1}{\sqrt{2}} \qty(\ket{\dxsmys} + i \eta \ket{\dxy}),
\end{align}
respectively, where $\eta$ represents either $+1$ or $-1$.
Using the basis $\qty{\ket{\psi_{cb}}, \ket{\psi^\eta_{vb}}}$, the most general form of the Hamiltonian is given by
\begin{equation}\label{eq:general2by2Hamiltonian}
    \hat{H}_\eta = f_{I,\eta}(k_x, k_y) \hat{I} + f_{X,\eta}(k_x, k_y) \hat{X} + f_{Y,\eta}(k_x, k_y) \hat{Y} + f_{Z,\eta}(k_x, k_y) \hat{Z},
\end{equation}
where $k_x, k_y$ represents the crystal momentum with respect to the $\eta\mathrm{K}$ point, $\hat{I}$ is the identity matrix, and $\hat{X}$, $\hat{Y}$, and $\hat{Z}$ are the Pauli matrices.
The identity matrix and the Pauli matrices namely form a basis for all $2 \times 2$ hermitian matrices.
The \kdotp method approximates the Hamiltonian for small $k_x, k_y$ by treating a deviation from the symmetry point as a perturbation\footnote{It's also nice to mention that this perturbation of the Hamiltonian actually has the form \kdotp (hence the name) where $\vb{k}$ is the crystal momentum with respect to the symmetry point and $\vb{p}$ the momentum operator.}.
$f_{i,\eta}(k_x, k_y)$ in \cref{eq:general2by2Hamiltonian} can therefore be written out as a Taylor series.
In this case, it is sufficient to go up to second order, which means that the most general form of $f_{i,\eta}(k_x, k_y)$ is given by
\begin{equation}\label{eq:kptaylor}
    f_{i,\eta}(k_x, k_y) = a^{i}_0 + a^{i}_{1,x} k_x + a^{i}_{1,y} k_y + a^{i}_{2,xx} k_x^2 + a^{i}_{2,yy} k_y^2 + a^{i}_{2,xy} k_x k_y,
\end{equation}
which means that up to second order, the \kdotp Hamiltonian may be described by 24 parameters.
However, a lot of these parameters will vanish or are related to each other when the symmetries of the Hamiltonian are considered.
At the $\eta\mathrm{K}$ point, the reciprocal space of the TMD monolayer has a $C_{3h}$ point-group symmetry (a subgroup of $D_{3h}$), which only contains the rotation operations and \mirror{xy}.
The rotation symmetries require that
\begin{equation}\label{eq:kprotationsymm}
    \hat{H}_\eta (k_x, k_y) = \rotation{3}^\dagger \hat{H}_\eta \qty(\frac{-k_x - \sqrt{3} k_y}{2}, \frac{- \sqrt{3} k_x + k_y}{2}) \rotation{3}.
\end{equation}
Although the $\eta\mathrm{K}$ point does not have a mirror symmetry and the time reversal symmetry, they do relate the \kdotp Hamiltonian at the $\eta\mathrm{K}$ point with the one at the $-\eta\mathrm{K}$ point.
In particular, the $yz$-plane mirror operation \mirror{2} requires
\begin{equation}\label{eq:kpmirrorsymm}
    \mirror{2} \hat{H}_\eta (k_x, k_y) \mirror{2} = \hat{H}_{-\eta} (-k_x, k_y).
\end{equation}
Furthermore, the time reversal symmetry demands
\begin{equation}\label{eq:kptimerevsymm}
    \timerev^{-1} \hat{H}_\eta (k_x, k_y) \timerev = \hat{H}^*_{-\eta} (-k_x, -k_y).
\end{equation}
In order to find out which parameters in \cref{eq:kptaylor} vanish or are related to each other, it helps to look at the terms of the Taylor series in this equation separately, which will be done in the next series of paragraphs.

\paragraph{The zeroth order}
The most general form of the zeroth order \kdotp Hamiltonian is given by
\begin{equation}
    \hat{H}_\eta^{(0)} = a^{I}_0 \hat{I} + a^{X}_0 \hat{X} + a^{Y}_0 \hat{Y} + a^{Z}_0 \hat{Z}.
\end{equation}
As the \kdotp Hamiltonian has a $C_{3h}$ point-group symmetry, the rotation operator \rotation{3} must commute with $\hat{H}_\eta^{(0)}$.
This also means that the eigenstates of $\hat{H}_\eta^{(0)}$ are also eigenstates of \rotation{3} and the corresponding eigenvalues are given by
\begin{align}
    \rotation{3} \ket{\psi_{cb}} &= \ket{\psi_{cb}}, \\
    \rotation{3} \ket{\psi^\eta_{vb}} &= \exp(2 \pi \eta i /3) \ket{\psi^\eta_{vb}}.
\end{align}
from this, the effect of the rotation operator on the identity (trivial) and the Pauli matrices can be calculated, and are given by
\begin{align}
    \rotation{3}^\dagger \hat{I} \rotation{3} &= \hat{I},\label{eq:beginrotonpauli}\\
    \rotation{3}^\dagger \hat{X} \rotation{3} &= -\frac{1}{2} (\sqrt{3} \eta \hat{Y} + \hat{X}),\\
    \rotation{3}^\dagger \hat{Y} \rotation{3} &= \frac{1}{2} (\sqrt{3} \eta \hat{X} - \hat{Y}),\\
    \rotation{3}^\dagger \hat{Z} \rotation{3} &= \hat{Z}.\label{eq:endrotonpauli}
\end{align}
Using \cref{eq:kprotationsymm}, it is clear that the $\hat{I}$ and $\hat{Z}$ component in the zeroth order expression remains invariant, while it requires for the $\hat{X}$ and $\hat{Y}$ component that
\begin{align}
    a^{X}_0 &= -\frac{1}{2} a^{X}_0 + \frac{\sqrt{3}}{2} \eta a^{Y}_0,\\
    a^{Y}_0 &= -\frac{1}{2} a^{Y}_0 - \frac{\sqrt{3}}{2} \eta a^{X}_0.
\end{align}
Both of these equations can only hold if $a^{X}_0 = a^{Y}_0 = 0$.
Thus, the $C_{3h}$ point-group symmetry of the \kdotp Hamiltonian requires that the zeroth order expression does not have an $\hat{X}$ and $\hat{Y}$ component, but $a^{I}_0$ and $a^{Z}_0$ are free parameters.
Using physical intuition, one can express the zeroth order \kdotp Hamiltonian as
\begin{equation}\label{eq:zerothorderkp}
    \hat{H}_\eta^{(0)} = -\mu \hat{I} + \frac{E_g}{2} (\hat{I} + \hat{Z}),
\end{equation}
where $\mu$ represents the chemical potential and $E_g$ the band gap energy.
It is convenient to set $\mu = 0$ as this term only shifts the eigenenergies of the system, and this will also be done in the remainder of this chapter.

\paragraph{The first order}
The most general form of the first order \kdotp Hamiltonian is given by
\begin{equation}
\hat{H}_\eta^{(1)} = \qty(a^{I}_x k_x + a^{I}_y k_y) \hat{I} + \qty(a^{X}_x k_x + a^{X}_y k_y) \hat{X} + \qty(a^{Y}_x k_x + a^{Y}_y k_y) \hat{Y} + \qty(a^{Z}_x k_x + a^{Z}_y k_y) \hat{Z}.
\end{equation}
Using the rotational symmetry requirement given in \cref{eq:kprotationsymm} and using the relations given in \crefrange{eq:beginrotonpauli}{eq:endrotonpauli}, the $\hat{I}$ component is given by
\begin{equation}
    a^{I}_x k_x + a^{I}_y k_y = \frac{1}{2} \qty[-a^{I}_x \qty(k_x + \sqrt{3} k_y) + a^{I}_y \qty(\sqrt{3} k_x - k_y)].
\end{equation}
The same expression can be obtained for the $\hat{Z}$ by replacing $I$ by $Z$ in the equation.
The equation has to hold for all $k_x$, $k_y$ and this requirement can only be satisfied if $a^{I}_x = a^{I}_y = a^{Z}_x = a^{Z}_y = 0$.
Thus, the first order expression of the \kdotp Hamiltonian has no $\hat{I}$ and $\hat{Z}$ component.
For the $\hat{X}$ component, the following equations from \cref{eq:kprotationsymm} can be derived:
\begin{equation}
    \begin{split}
        a^{X}_x k_x + a^{X}_y k_y &= -\frac{1}{4} \qty[-a^{X}_x \qty(k_x + \sqrt{3} k_y) + a^{X}_y \qty(\sqrt{3} k_x - k_y)] \\
        &+ \frac{\sqrt{3}}{4} \eta \qty[-a^{Y}_x \qty(k_x + \sqrt{3} k_y) + a^{Y}_y \qty(\sqrt{3} k_x - k_y)].
    \end{split}
\end{equation}
And similarly for the $\hat{Y}$ component
\begin{equation}
    \begin{split}
        a^{Y}_x k_x + a^{Y}_y k_y &= -\frac{1}{4} \qty[-a^{Y}_x \qty(k_x + \sqrt{3} k_y) + a^{Y}_y \qty(\sqrt{3} k_x - k_y)] \\
        &- \frac{\sqrt{3}}{4} \eta \qty[-a^{X}_x \qty(k_x + \sqrt{3} k_y) + a^{X}_y \qty(\sqrt{3} k_x - k_y)].
    \end{split}
\end{equation}
Since these equations have to hold for all $k_x$ and $k_y$, separate equations for the $k_x$ and $k_y$ terms can be set up, which gives the following equation in matrix form:
\begin{equation}
    \mqty(-3 & -\sqrt{3} & -\sqrt{3} \eta & 3 \eta \\
         \sqrt{3} & -3 & -3 \eta & -\sqrt{3} \eta \\
         \sqrt{3} \eta & -3 \eta & -3 & -\sqrt{3} \\
         3 \eta & \sqrt{3} \eta & \sqrt{3} & -3) \,
    \mqty(a^{X}_x \\ a^{X}_y \\ a^{Y}_x \\ a^{Y}_y) = \vb{0}.
\end{equation}
The equations in row 2 and row 3 are equivalent as well as the ones in row 1 and row 4.
From the equations in the first two rows, it can be found that
\begin{align}
    a^{X}_x &= \eta a^{Y}_y,\label{eq:kp1sol1} \\
    a^{X}_y &= -\eta a^{Y}_x.\label{eq:kp1sol2}
\end{align}
Together with the finding that the $\hat{I}$ and $\hat{Z}$ component should vanish, the first order Hamiltonian can thus be written as
\begin{equation}
    \hat{H}_\eta^{(1)} = \eta \qty(a^{Y}_y k_x - a^{Y}_x k_y) \hat{X} + \qty(a^{Y}_x k_x + a^{Y}_y k_y) \hat{Y}.
\end{equation}
For the derivation for this Hamiltonian, however, only the rotational symmetry of the Hamiltonian has been considered\footnote{One may ask whether the rotation in the other direction should be considered. However, this will give the exact same result.}.
The Hamiltonian already obeys the time reversal symmetry requirement in \cref{eq:kptimerevsymm}, but the mirror symmetry (\cref{eq:kpmirrorsymm}) also requires that $a^{Y}_x = 0$.
Using physical intuition, the first order Hamiltonian can be written as
\begin{equation}\label{eq:firstorderkp}
    \hat{H}_\eta^{(1)} = t_1 a \qty(\eta k_x \hat{X} + k_y \hat{Y}),
\end{equation}
where $a$ is the lattice constant, the distance between two nearest transition metal or chalcogen atoms, and $t_1$ represents the effective linear coupling between the conduction band and valence band.

The \kdotp Hamiltonian up to first order is sometimes called the massive Dirac fermion model and was introduced by Xiao et al. in 2012 \cite{xiao_coupled_2012}.
It should be noted that the effective linear coupling produces an isotropic band structure around the $\eta\mathrm{K}$ point.
In order to understand this, it should be noted that only the magnitude of the coupling will affect the eigenenergies of the two bands, which is given by $t_1 a \sqrt{k_x^2 + k_y^2} = t_1 |\vb{k}| a$.
Clearly, the magnitude of the coupling only depends on the magnitude of the crystal momentum with respect to the $\eta\mathrm{K}$ point and the band structure is thus isotropic.

\paragraph{The second order}
To explain the second order terms of the \kdotp Hamiltonian, it is useful to rewrite the coefficients of the components as
\begin{equation}
    a^{i}_{2,xx} k_x^2 + a^{i}_{2,yy} k_y^2 + a^{i}_{2,xy} k_x k_y = \alpha^i \qty(k_x^2 + k_y^2) + \beta^i \qty(k_x^2 - k_y^2) + 2 \gamma^i k_x k_y.
\end{equation}
When considering how these terms change under the rotation operation as shown in \cref{eq:kprotationsymm}, one will get
\begin{align}
    k_x^2 + k_y^2 &\rightarrow k_x^2 + k_y^2 = |\vb{k}|^2, \\
    k_x^2 - k_y^2 &\rightarrow \frac{\sqrt{3}}{2} \qty(2 k_x k_y) - \frac{1}{2} \qty(k_x^2 - k_y^2), \\
    2 k_x k_y &\rightarrow -\frac{\sqrt{3}}{2} \qty(k_x^2 - k_y^2) - \frac{1}{2} \qty(2 k_x k_y).
\end{align}
As can been seen from these expressions, the $k_x^2 + k_y^2$ term will remain invariant.
This means this term, analogous to the zeroth order of the \kdotp Hamiltonian, will only appear in the $\hat{I}$ and $\hat{Z}$ component.
The $2 k_x k_y$ and $k_x^2 - k_y^2$ terms, on the other hand, transform exactly like $k_x$ and $k_y$ respectively under the rotation operator.
These terms will thus appear in the $\hat{X}$ and $\hat{Y}$ in a very similar way as in the first order expression of the \kdotp Hamiltonian.
There is a slight difference though: in order to preserve time reversal symmetry given by \cref{eq:kptimerevsymm}, the solution similar to \cref{eq:kp1sol1,eq:kp1sol2} need to be put in a different from:
\begin{align}
    \beta^{Y} &= \eta \gamma^{X}, \\
    \gamma^{Y} &= -\eta \beta^{X}.
\end{align}
After substituting $\beta^{Y}$ and $\gamma^{Y}$ in the general expression of the \kdotp Hamiltonian, and considering the mirror symmetry requirement in \cref{eq:kpmirrorsymm}, it can be found out that $\gamma^{X} = 0$.
Again using physical intuition, the second order terms of the \kdotp Hamiltonian can be expressed as
\begin{equation}\label{eq:secondorderkp}
    \hat{H}_\eta^{(2)} = \frac{\hbar^2 |\vb{k}|^2}{2 m_{cb}^{*(1)}} \frac{\hat{I} + \hat{Z}}{2} + \frac{\hbar^2 |\vb{k}|^2}{2 m_{vb}^{*(1)}} \frac{\hat{I} - \hat{Z}}{2} + t_{3w} a^2 \qty[\qty(k_x^2 - k_y^2) \hat{X} - 2 \eta k_x k_y \hat{Y}],
\end{equation}
where $m_{cb}^{*(1)}$ and $m_{vb}^{*(1)}$ are the first order effective masses of the conduction band and valence band respectively, $a$ is the lattice constant, and $t_{3w}$ is an anisotropic coupling term, which will be explained later in more detail.
The masses are called the first order effective masses as these terms appear in first order perturbation theory.
The second order masses can be obtained by applying second order perturbation theory to the off-diagonal first order \kdotp Hamiltonian, and the actual effective masses can be acquired by using
\begin{equation}\label{eq:ordersoffmass}
    \frac{1}{m_{vb/cb}^{*}} = \frac{1}{m_{vb/cb}^{*(1)}} + \frac{1}{m_{vb/cb}^{*(2)}}.
\end{equation}

One of the first derivation of the second order \kdotp Hamiltonian is given by Rostami et al. in 2013 \cite{rostami_effective_2013}.
The second order terms contribute to phenomena in the \kdotp model that cannot be described by the massive Dirac fermion model.
The first order effective mass terms are the first terms in the \kdotp model that break the particle-hole symmetry of the system, since usually $m_{cb}^{*(1)} \neq -m_{vb}^{*(1)}$.
This is different from the second order effective mass terms, as these terms terms do not break the particle-hole symmetry because
\begin{equation}\label{eq:secondordermass}
    m_{cb}^{*(2)} = -m_{vb}^{*(2)} = \frac{\hbar^2 E_g}{2 t_1^2 a^2}.
\end{equation}
Furthermore, the anisotropic coupling term $t_{3w}$ is the first term that break the isotropic behavior of the system around the $\eta\mathrm{K}$ points.
This is, however, not directly clear on first sight as the magnitude of the coupling seems isotropic:
\begin{equation}\label{eq:trigonalwarpingmagnitude}
    \begin{split}
        \sqrt{\qty(k_x^2 - k_y^2)^2 + 4 k_x^2 k_y^2} &= |\vb{k}|^2 \sqrt{\cos[4](\theta) + \sin[4](\theta) + 2 \sin[2](\theta) \cos[2](\theta)} \\
        &= |\vb{k}|^2 \qty(\cos[2](\theta) + \sin[2](\theta)) = |\vb{k}|^2.
    \end{split}
\end{equation}
However, the phase dependence is different from the linear effective coupling in \cref{eq:firstorderkp} and this will cause triangular shaped contours.

\paragraph{Putting together}
Putting the terms in \cref{eq:zerothorderkp,eq:firstorderkp,eq:secondorderkp} together and setting $\mu = 0$, the final expression for the \kdotp Hamiltonian is given by
\begin{equation}\label{eq:kphamiltoniantotal}
    \begin{split}
        \hat{H}_\eta &= \frac{E_g}{2} \qty(\hat{I} + \hat{Z}) + t_1 a \qty(\eta k_x \hat{X} + k_y \hat{Y}) \\
        &+ \frac{\hbar^2 |\vb{k}|^2}{2 m_{cb}^{*(1)}} \frac{\hat{I} + \hat{Z}}{2} + \frac{\hbar^2 |\vb{k}|^2}{2 m_{vb}^{*(1)}} \frac{\hat{I} - \hat{Z}}{2} + t_{3w} a^2 \qty[\qty(k_x^2 - k_y^2) \hat{X} - 2 \eta k_x k_y \hat{Y}].
    \end{split}
\end{equation}
This second order \kdotp model is the same as the one given in Fang et al.'s paper on the 11-band model (eq. 10) \cite{fang_ab_2015}, but different parameter names are used.
In \cref{tab:kpparamvalues}, the conversion from Fang et al.'s parameters to the parameters in this thesis are shown as well the values for \ce{MoS2}, \ce{MoSe2}, \ce{WS2}, and \ce{WSe2}.
Plots of the band structure near the $\eta\mathrm{K}$ point (without SOI) of \ce{MoS2} and \ce{WSe2} are given in \cref{fig:k.p_results}.
For these plots, the first principle fit parameters from table VI in Fang et al. are used.

\begin{table}[h]
    \centering
    \caption{Conversion from Fang et al.'s \kdotp parameters to parameters described in this text as well as the converted values for \ce{MoS2} and \ce{WSe2}. The energies and couplings are in $\si{\eV}$ while the effective masses are in \si{\planckbar\squared \per\angstrom\squared \per\eV}.}
    \begin{tabular}{llrrrr}
        \toprule
        & & \multicolumn{4}{c}{material} \\
        param. & expression & \ce{MoS2} & \ce{MoSe2} & \ce{WS2} & \ce{WSe2} \\
        \midrule
        $E_g$ & $f_0$ & $1.6735$ & $1.4415$ & $1.8126$ & $1.5455$ \\
        $t_1$ & $f_1$ & $1.1518$ & $0.9560$ & $1.4073$ & $1.1894$ \\
        $m_{cb}^{*(1)}$ & $\hbar^2 \qty[2 a^2 (f_2 + f_3)]^{-1}$ & $3.7744$ & $453.6217$ & $0.3593$ & $0.4050$ \\
        $m_{vb}^{*(1)}$ & $\hbar^2 \qty[2 a^2 (f_2 - f_3)]^{-1}$ & $0.3644$ & $0.4596$ & $0.2865$ & $0.3635$ \\
        $t_{3w}$ & $f_4$ & $-0.0780$ & $-0.0654$ & $-0.0709$ & $-0.0627$ \\
        $\lambda_{vb}$ & $f_5$ & $0.0746$ & $0.0929$ & $0.2153$ & $0.2335$ \\
        $\lambda_{cb}$ & $f_6$ & $-0.0015$ & $-0.0106$ & $0.0148$ & $0.0180$ \\
        \midrule
        $m_{cb}^*$ & see \cref{eq:effmasscb} & $0.0614$ & $0.0715$ & $0.0402$ & $0.0442$ \\
        $m_{vb}^*$ & see \cref{eq:effmassvb} & $-0.0753$ & $-0.0847$ & $-0.0537$ & $-0.0574$ \\
        \bottomrule
    \end{tabular}
    \label{tab:kpparamvalues}
\end{table}

For small $k_x$, $k_y$\footnote{Small $k_x$, $k_y$ implies that the first and second order terms of the  \kdotp Hamiltonian are much smaller than the bandgap.}, the first and second order terms of the \kdotp Hamiltonian can be treated as a perturbation with respect to the zeroth order.
Second order perturbation theory can be used to derive an approximate expression for the energy levels of the conduction band and valence band.
Anisotropic coupling will be neglected first, i.e. $t_{3w} = 0$.
In that case, the energy levels are quite easy to derive:
\begin{align}
    E_{cb} &\approx E_g + \frac{\hbar^2 |\vb{k}|^2}{2 m_{cb}^{*(1)}} + \frac{t_1^2}{E_g} |\vb{k}|^2 a^2, \\
    E_{vb} &\approx \frac{\hbar^2 |\vb{k}|^2}{2 m_{vb}^{*(1)}} - \frac{t_1^2}{E_g} |\vb{k}|^2 a^2.
\end{align}
From these expressions it is also easy to derive the actual effective masses of the conduction band and valence band:
\begin{align}
    \frac{1}{m_{cb}^*} &= \frac{1}{\hbar^2} \eval{\pdv[2]{E_{cb}}{|\vb{k}|}}_{\vb{k}=\vb{0}} = \frac{1}{m_{cb}^{*(1)}} + \frac{2 t_1^2 a^2}{\hbar^2 E_g},\label{eq:effmasscb}\\
    \frac{1}{m_{vb}^*} &= \frac{1}{\hbar^2} \eval{\pdv[2]{E_{vb}}{|\vb{k}|}}_{\vb{k}=\vb{0}} = \frac{1}{m_{vb}^{*(1)}} - \frac{2 t_1^2 a^2}{\hbar^2 E_g}.\label{eq:effmassvb}
\end{align}
Note that these expressions are consistent with \cref{eq:ordersoffmass,eq:secondordermass}.
The numeric values of the effective masses for \ce{MoS2}, \ce{MoSe2}, \ce{WS2}, and \ce{WSe2} are given in \cref{tab:kpparamvalues}.

Now, consider the effect of anisotropic coupling, i.e. $t_{3w} \neq 0$.
In that case, the second order correction to the energy for the conduction band ($+$) and valence band ($-$) is given by
\begin{equation}
    E_{cb/vb}^{(2)} = \pm \frac{t_1^2 |\vb{k}|^2 a^2 + t_{3w}^2 |\vb{k}|^4 a^4 + 2 \eta t_1 t_{3w} |\vb{k}|^3 a^3 \cos(3 \theta)}{E_g},
\end{equation}
where $\theta$ represents the angle of the $\vb{k}$ vector with respect to the $x$-axis.
It can be thus seen in this expression that the anisotropic coupling causes triangular anisotropic energy contours, but as suggested earlier about \cref{eq:trigonalwarpingmagnitude}, this will only be visible if there is an effective linear coupling $t_1$.

\begin{figure}[t]
    \centering
    \begin{subfigure}[b]{0.46\linewidth}
        \centering
        \includegraphics[height=200px]{band_structure/figures/{k.p_MoS2cb}.pdf}
        \caption{}
        \label{fig:k.p_MoS2cb}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.46\linewidth}
        \centering
        \includegraphics[height=200px]{band_structure/figures/{k.p_MoS2vb}.pdf}
        \caption{}
        \label{fig:k.p_MoS2vb}
    \end{subfigure}
    \begin{subfigure}[b]{0.46\linewidth}
        \centering
        \includegraphics[height=200px]{band_structure/figures/{k.p_WSe2cb}.pdf}
        \caption{}
        \label{fig:k.p_WSe2cb}
    \end{subfigure}
    \begin{subfigure}[b]{0.46\linewidth}
        \centering
        \includegraphics[height=200px]{band_structure/figures/{k.p_WSe2vb}.pdf}
        \caption{}
        \label{fig:k.p_WSe2vb}
    \end{subfigure}
    \caption{Contour plots of: (a) the conduction band of \ce{MoS2}; (b) the valence band of \ce{MoS2}; (c) the conduction band of \ce{WSe2}; (d) the valence band of \ce{WSe2}. These plots are exact solutions of the Hamiltonian near the $+\mathrm{K}$ point given in \cref{eq:kphamiltoniantotal}. Note that SOI has not been included. The anisotropic coupling effect is particularly visible in the valence band.}
    \label{fig:k.p_results}
\end{figure}

\subsection{Spin-orbit interaction}\label{subsec:k.p_SOI}
As discussed in the 11-band model, the $z$ component of the spin is (approximately) a good quantum number due to the \mirror{xy} symmetry.
Furthermore, the splitting at the $-\eta\mathrm{K}$ points is reversed with the respect to the splitting at the $\eta\mathrm{K}$ points.
The spin-orbit interaction can therefore be described with the following Hamiltonian:
\begin{equation}\label{eq:Hsoi_k.p}
    \hat{H}_{SOI} = \eta\lambda_{cb} \frac{\hat{I} + \hat{Z}}{2} \hat{S}_z + \eta\lambda_{vb} \frac{\hat{I} - \hat{Z}}{2} \hat{S}_z,
\end{equation}
where $S_z$ is the Pauli-Z matrix acting in the spin space, and $\lambda_{cb}$ and $\lambda_{vb}$ represent the strength of the spin-orbit interaction of the conduction band and valence band respectively.
This is related to the energy splitting at the $\eta\mathrm{K}$ point according to
\begin{equation}
    \lambda_{cb/vb} = \eta \frac{E_{cb/vb,\uparrow} - E_{cb/vb,\downarrow}}{2}.
\end{equation}
Note that $\lambda_{cb}$ can be negative since it is possible that $E_{cb,\uparrow} < E_{cb,\downarrow}$.
The source of this are the $p$-orbitals at the chalcogen atoms, as already demonstrated in the 11-band tight binding model discussed in \cref{sec:11band}.

Using the fit parameters in Fang et al., The band structures near the $+\mathrm{K}$ point of \ce{MoS2} and \ce{WSe2} are visualized in \cref{fig:k.p_SOI}.
It can clearly be seen from these figures that \ce{MoS2} has a crossing of the spin-up and spin-down conduction band, whereas \ce{WSe2} does not have such a crossing.
In order to explain this difference between \ce{MoS2} and \ce{WSe2}, Two things need to be shown: (1) the condition when the spin-up band is lower in energy than the spin-down band at the $+\mathrm{K}$ point, and (2) the fact that the spin-up band seemingly has a lower effective mass than the spin-down band at the $+\mathrm{K}$ point.
The first condition is already explained earlier and this is satisfied when $\lambda_{cb} < 0$.
The second condition is more tricky and requires to apply third order perturbation theory.
To keep the expression simple, the anisotropic coupling term will be ignored\footnote{Anisotropic coupling does not affect the effective mass at the $+\mathrm{K}$ point anyway.}.
Treating the spin-orbit interaction given in \cref{eq:Hsoi_k.p} as an additional perturbation, an approximate expression can be derived for the eigenenergy of the conduction bands using third order perturbation theory and is given by
\begin{equation}\label{eq:thirdorderEnergycb}
    E_{cb,s} \approx E_g + \eta \lambda_{cb} s + \frac{\hbar^2 |\vb{k}|^2}{2 m_{cb}^{*(1)}} + \frac{t_1^2 |\vb{k}|^2 a^2}{E_g} \qty[1 + \frac{\hbar^2 |\vb{k}|^2 \qty(1/2 m_{vb}^{*(1)} - 1/2 m_{cb}^{*(1)}) +  \eta\qty(\lambda_{vb} - \lambda_{cb}) s}{E_g}],
\end{equation}
where $s$ is either $+1$ or $-1$ representing the spin-up band and spin-down band respectively.
The effective mass at the $+\mathrm{K}$ point ($\eta = 1$) of these bands is thus given by
\begin{equation}\label{eq:k.peffmassSOI}
    \frac{1}{m_{cb,s}^*} = \frac{1}{\hbar^2} \eval{\pdv[2]{E_{cb,s}}{|\vb{k}|}}_{\vb{k}=\vb{0},\eta=1} = \frac{1}{m_{cb}^{*(1)}} + \frac{2 t_1^2 a^2}{\hbar^2 E_g} \qty(1 + \frac{\lambda_{vb} - \lambda_{cb}}{E_g} s).
\end{equation}
As discussed in the earlier sections, the spin splitting of the conduction band is smaller than that at the valence band, i.e. $\abs{\lambda_{cb}} < \abs{\lambda_{vb}}$.
Furthermore, the energy splittings of spin-up and spin-down are much smaller than the bandgap energy, i.e. $\abs{\lambda_{cb}}, \abs{\lambda_{vb}} \ll E_g$.
Using this information, it is clear from \cref{eq:k.peffmassSOI} that the effective mass of the spin-up ($s = +1$) band is lower than that of the spin-down band ($s = -1$), regardless whether $\lambda_{cb}$ is positive or negative.
It can thus be concluded that crossing of spin-up and spin-down in the conduction band will occur if $\lambda_{cb} < 0$.

\begin{figure}[ht]
    \centering
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=115px]{band_structure/figures/{k.p_MoS2_SOI}.pdf}
        \caption{}
        \label{fig:k.p_MoS2_SOI}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=115px]{band_structure/figures/{k.p_MoS2_SOIzoom}.pdf}
        \caption{}
        \label{fig:k.p_MoS2_SOIzoom}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=115px]{band_structure/figures/{k.p_WSe2_SOIzoom}.pdf}
        \caption{}
        \label{fig:k.p_WSe2_SOI}
    \end{subfigure}
   \caption{(a) The band structure near the $+\mathrm{K}$ point of \ce{MoS2}. Red indicates spin-up bands while blue indicates spin-down bands; (b) zoom at the minimum of the conduction band of \ce{MoS2} indicated with rectangle in (a); (c) The minimum of the conduction band near the $+\mathrm{K}$ point of \ce{WSe2}. Unlike \ce{MoS2}, there is no vanishing SOI.}
   \label{fig:k.p_SOI}
\end{figure}

It is useful later on to have an effective Hamiltonian of the spin-up and spin-down conduction band when superconductivity will be included in the system.
This can be done by calculating the third order energy of the spin-up band  and spin-down band separately and use them as diagonal elements of the effective Hamiltonian.
As mentioned several times earlier, the $z$-component of the spin is a good quantum number and thus there should be no coupling between these two bands.
Furthermore, it is assumed that $|\vb{k}|$ is so small that all terms of order $\abs{\vb{k}}^4$ or higher can be neglected and this results in an effective Hamiltonian given by
\begin{equation}\label{eq:kpHamiltoniancb}
    \hat{H}_{\mathit{eff},\eta} = E_g + \frac{\hbar^2 |\vb{k}|^2}{2 m_{cb}^{*(1)}} + \eta \lambda_{cb} \hat{S}_z + \frac{t_1^2 |\vb{k}|^2 a^2 + 2 \eta t_1 t_{3w} |\vb{k}|^3 a^3 \cos(3 \theta)}{E_g} \qty[1 + \eta \frac{\lambda_{vb} - \lambda_{cb}}{E_g} \hat{S}_z].
\end{equation}
In literature about Ising superconductivity such as Wang et al.\cite{wang_new_2018}, the spin-independent anisotropic term $2 \eta t_1 t_{3w} |\vb{k}|^3 a^3 \cos(3 \theta)/E_g$ is left out as this term does not significantly change the band structure when comparing with the valence band.
This also simplifies analytical analysis, which will be done in more detail in \cref{sec:superconductingTMD}.

%Such a Hamiltonian can be constructed by using the so-called Löwdin partition method \cite{lowdin_note_1951}, which is also briefly explained in \cref{ch:lowdinpartitionmethod}.
%The tho classes in which the states are partitioned are the conduction band states $\ket{\psi_{cb}, \uparrow}$, $\ket{\psi_{cb}, \downarrow}$ belonging to class $A$ and the valence band states $\ket{\psi_{vb}, \uparrow}$, $\ket{\psi_{vb}, \downarrow}$ belonging to class $B$.
%Using the expression for the effective Hamiltonian given in \cref{eq:effHam} in \cref{ch:lowdinpartitionmethod}, I obtain
%\begin{equation}\label{eq:effHamcb}
%    \hat{H}_{\mathit{eff},\eta} = E_g + \frac{\hbar^2 |\vb{k}|^2}{2 m_{cb}^{*(1)}} + \eta \lambda_{cb} \hat{S}_z + \frac{t_1^2 |\vb{k}|^2 a^2 + 2 \eta t_1 t_{3w} |\vb{k}|^3 a^3 \cos(3 \theta)}{E - \hbar^2 |\vb{k}|^2 / 2 m_{vb}^{*(1)} - \eta \lambda_{vb} \hat{S}_z},
%\end{equation}
%where $E$ represents an eigenvalue of the effective Hamiltonian $\hat{H}_{\mathit{eff},\eta}$, which I do not know a priori.
%This can however be approximated by a process of iteration: substitute $E$ by the expression of $\hat{H}_{\mathit{eff},\eta}$.
%Of course, this cannot be done indefinitely so I need to cut off this process at some moment.
%For my purposes, it is sufficient to use the substitution
%\begin{equation}
%    E \rightarrow E_g + \frac{\hbar^2 |\vb{k}|^2}{2 m_{cb}^{*(1)}} + \eta \lambda_{cb} \hat{S}_z,
%\end{equation}
%so that the fraction term in \cref{eq:effHamcb} becomes
%\begin{equation}\label{eq:fractioneffHam}
%    \frac{t_1^2 |\vb{k}|^2 a^2 + 2 \eta t_1 t_{3w} |\vb{k}|^3 a^3 \cos(3 \theta)}{E_g + \hbar^2 |\vb{k}|^2 / 2 m_{cb}^{*(1)} - \hbar^2 |\vb{k}|^2 / 2 m_{vb}^{*(1)} - \eta (\lambda_{vb} - \lambda_{cb}) \hat{S}_z}.
%\end{equation}
%This fraction can be Taylor approximated by noting that
%\begin{equation}
%    \frac{\hbar^2 |\vb{k}|^2}{2 m_{cb}^{*(1)}} - \frac{\hbar^2 |\vb{k}|^2}{2 m_{vb}^{*(1)}} - \eta (\lambda_{vb} - \lambda_{cb}) \hat{S}_z \ll E_g.
%\end{equation}
%After using a linear approximation ($1/(1-x) \approx 1+x$) for the fraction given in \cref{eq:fractioneffHam} and eliminating all terms that are of order $\abs{\vb{k}}^4$ or higher, The final expression for the effective Hamiltonian will become
%\begin{equation}
%    \hat{H}_{\mathit{eff},\eta} = E_g + \frac{\hbar^2 |\vb{k}|^2}{2 m_{cb}^{*(1)}} + \eta \lambda_{cb} \hat{S}_z + \frac{t_1^2 |\vb{k}|^2 a^2 + 2 \eta t_1 t_{3w} |\vb{k}|^3 a^3 \cos(3 \theta)}{E_g} \qty[1 + \eta \frac{\lambda_{vb} - \lambda_{cb}}{E_g} \hat{S}_z].
%\end{equation}