\section{Three-band tight binding model}\label{sec:3band}
The three-band model, based on work by Liu et al. \cite{liu_three-band_2013}, shows that the \dzsq, \dxy, and \dxsmys orbitals of the transition metal can provide a good description of the 3 bands near the Fermi level of a TMD monolayer.
The \dzsq, \dxy, and \dxsmys orbitals (see \cref{fig:3dorbitals}) are cubic harmonics and these are related to the spherical harmonics encountered in quantum mechanics when deriving the eigenstates of the angular momentum operator:
\begin{align}
    \label{eq:beginSHlist}
	\ket{\dzsq} &= \ket{\Y{0}{2}},\\
	\ket{\dxy} &= \frac{i}{\sqrt{2}} \qty( \ket{\Y{-2}{2}} - \ket{\Y{2}{2}} ),\\
	\ket{\dxsmys} &= \frac{1}{\sqrt{2}}\qty(\ket{\Y{-2}{2}} + \ket{\Y{2}{2}}).
    \label{eq:endSHlist}
\end{align}
One consequence of the so-called three-band approximation is that a triangular lattice (see \cref{fig:3band_realspace}) can be used instead of a honeycomb lattice, as chalcogens atoms do not have to be considered.
The three-band approximation is accurate near the $+\mathrm{K}$ and $-\mathrm{K}$ points if only nearest neighbor hoppings are considered.
If second and third nearest neighbor hoppings are taken into account as well, a good description of the band structure in the entire Brillouin zone can be provided except near the $\mathrm{\Gamma}$ point (see \cref{fig:3band_brillouinzone}).
This is also the limitation of the three-band approximation, since $p$-orbitals of the chalcogens have a non-negligible contribution to the band structure at the $\mathrm{\Gamma}$ point.

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{0.32\linewidth}
		\centering
		\includegraphics[height=151px]{band_structure/figures/dz2-orbital}
		\caption{}
		\label{fig:dz2_orbital}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.32\linewidth}
		\centering
		\includegraphics[height=151px]{band_structure/figures/dxy-orbital}
		\caption{}
		\label{fig:dxy_orbital}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.32\linewidth}
		\centering
		\includegraphics[height=151px]{band_structure/figures/dx2-y2-orbital}
		\caption{}
		\label{fig:dx2-y2_orbital}
	\end{subfigure}
	\caption{Plots of the \dzsq (a), \dxy (b), and \dxsmys (c) orbitals used in the three-band approximation.}
	\label{fig:3dorbitals}
\end{figure}

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{0.48\linewidth}
		\centering
		\def\svgscale{1.35}
		\input{band_structure/figures/3band_realspace.pdf_tex}
		\caption{}
		\label{fig:3band_realspace}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.48\linewidth}
		\centering
		\input{band_structure/figures/3band_brillouinzone.pdf_tex}
		\caption{}
		\label{fig:3band_brillouinzone}
	\end{subfigure}
	\caption{(a) the lattice of the transition metal atoms with primitive vectors $\vb{a}_1$ and $\vb{a}_2$. The nearest neighbors (NN), second nearest neighbors (SNN) and third nearest neighbors (TNN) are the hoppings from the gray center to the red, green and blue dots respectively. The dotted lines indicate the mirror planes \mirror{1}, \mirror{2}, \mirror{3}; (b) visualization of the first Brillouin zone of the transition metal lattice with reciprocal vectors $\vb{b}_1$ and $\vb{b}_2$ and high symmetry points $\mathrm{\Gamma}$, $\mathrm{M}$, $+\mathrm{K}$, and $-\mathrm{K}$.}
	\label{fig:3band}
\end{figure}

\subsection{Onsite Hamiltonian}\label{subsec:onsite}
In order to calculate the band structure, a Python package called Kwant \cite{groth_kwant:_2014} will be used to ease the work.
This package requires to specify the onsite energies of the orbitals and all the possible hopping integrals between two orbitals at different sites.
Hopping integrals will be discussed in \cref{subsec:hoppings} in more detail.
Using the basis $\qty{\ket{\dzsq}, \ket{\dxy}, \ket{\dxsmys}}$, the Hamiltonian of the onsite energies is simple and given by
\begin{equation}\label{eq:onsiteH_unpert}
	\hat{H}_{onsite} = \mqty(\dmat[0]{\varepsilon_1,\varepsilon_2,\varepsilon_2}).
\end{equation}
The \dxy and \dxsmys orbitals have to be degenerate, which can be explained from the fact that a TMD monolayer has a $D_{3h}$ point-group symmetry.
It will become clear later how such a point-group symmetry leads to a degeneracy between these two orbitals.
But first, consider some of the elements of the $D_{3h}$ symmetry group:
\begin{itemize}
	\item $\hat{I}$, the identity operation (i.e. do nothing).
	\item \rotation{3}, a counterclockwise rotation of $2 \pi / 3$ around the $z$-axis (i.e. the axis perpendicular to TMD monolayer).
	\item \mirror{i}, a reflection in the mirror plane indicated by the lines visualized in \cref{fig:3band_realspace}.
\end{itemize}
%Note that under these operations, the crystal of a TMD monolayer will indeed remain invariant.
To understand why symmetries can make quantum states degenerate (in this case, the \dxy and \dxsmys orbitals), let $\hat{O}$ be any operator in the $D_{3h}$ symmetry group.
Because the Hamiltonian of a TMD monolayer has a $D_{3h}$ point-group symmetry, it must commute with $\hat{O}$, i.e. $\comm*{\hat{H}}{\hat{O}} = 0$.
But this may cause degeneracy between quantum states as follows: suppose $\ket{\psi}$ is an eigenstate of the TMD monolayer with eigenenergy $\varepsilon$, then
\begin{equation}\label{eq:degeneracy_by_symmetry}
	\hat{O} \qty(\hat{H} \ket{\psi}) = \hat{O} \qty(\varepsilon \ket{\psi}) = \varepsilon \qty(\hat{O} \ket{\psi}) = \hat{H} \qty(\hat{O} \ket{\psi}),
\end{equation}
where the latter expression is allowed due to the fact that $\comm*{\hat{H}}{\hat{O}} = 0$.
One can see from this expression that if $\ket{\psi}$ is an eigenstate, then $\hat{O} \ket{\psi}$ is an eigenstate as well and it has the same eigenenergy as $\ket{\psi}$.

This can also be applied to the \dxy and \dxsmys orbitals.
First, suppose that $\ket{\dxy}$ is an eigenstate of the Hamiltonian with eigenenergy $\varepsilon_2$.
Applying the operator \mirror{1} to this state gives\footnote{The normalization factor $\frac{1}{\sqrt{2}}$ of the \dxy orbital for convenience during the calculation has been omitted for convenience.}
\begin{equation}\label{eq:mirror1dxy}
	\begin{split}
		\mirror{1} \ket{\dxy} &= i \qty(\mirror{1} \ket{\Y{-2}{2}} - \mirror{1} \ket{\Y{2}{2}}) = i \qty(\exp(-2\pi i / 3) \ket{\Y{2}{2}} - \exp(2\pi i / 3) \ket{\Y{-2}{2}}) \\
		&= \frac{1}{2} \qty[\sqrt{3} \qty(\ket{\Y{-2}{2}} + \ket{\Y{2}{2}}) + i \qty(\ket{\Y{-2}{2}} - \ket{\Y{2}{2}})] = \frac{1}{2} \qty(\sqrt{3} \ket{\dxsmys} + \ket{\dxy}).
	\end{split}
\end{equation}
To evaluate $\mirror{1} \ket{\Y{\pm 2}{2}}$, note that $\ket{\Y{\pm 2}{2}} \propto \exp(\pm 2 i \phi)$ and that $\mirror{1}: \phi \rightarrow \pi/3 - \phi$, so that indeed $\mirror{1} \ket{\Y{\pm 2}{2}} = \exp(\pm 2 \pi i /3) \ket{\Y{\mp 2}{2}}$.
From what has been shown earlier in \cref{eq:degeneracy_by_symmetry}, the state $\mirror{1} \ket{\dxy}$ also has to be an eigenstate with eigenenergy $\varepsilon_2$.
Because $\ket{\dxy}$ and $\mirror{1} \ket{\dxy}$ are degenerate states, any linear combination of these two states is also an eigenstate with eigenenergy $\varepsilon_2$.
But this means that
\begin{equation}
	\frac{1}{\sqrt{3}} \qty(2 \mirror{1} \ket{\dxy} - \ket{\dxy}) = \ket{\dxsmys}
\end{equation}
is also an eigenstate with eigenenergy $\varepsilon_2$, which completes the proof that the \dxy and \dxsmys orbitals have to be degenerate.
Note that a different symmetry operation could have been chosen, such as \rotation{3}, as long as the operation is in $D_{3h}$ and results in a linear combination of \dxy and \dxsmys orbitals.
Also note that the \dzsq orbital is not degenerate with the \dxy and \dxsmys orbitals, as there is no operation in $D_{3h}$ that transforms a \dxy or \dxsmys into a superposition with a \dzsq orbital.

\subsection{Hopping integrals}\label{subsec:hoppings}
The hopping integrals considered by Liu et al. are all nearest neighbors (NN), second nearest neighbors (SNN), or third nearest neighbors (TNN).
For simplicity, these overlaps are defined as
\begin{align}\label{eq:beginhoppinglist}
	t_{ij} &= \mel{\phi_i \qty(\vb{r})}{\hat{H}}{\phi_j \qty(\vb{r} - \vb{a}_1)},\\
	r_{ij} &= \mel{\phi_i \qty(\vb{r})}{\hat{H}}{\phi_j \qty(\vb{r} - 2\vb{a}_1 + \vb{a}_2)},\\
	u_{ij} &= \mel{\phi_i \qty(\vb{r})}{\hat{H}}{\phi_j \qty(\vb{r} - 2\vb{a}_1)},
	\label{eq:endhoppinglist}
\end{align}
where $\ket{\phi_0} = \ket{\dzsq}$, $\ket{\phi_1} = \ket{\dxy}$, and $\ket{\phi_2} = \ket{\dxsmys}$, and these definitions will also be used in the remainder of this section about the three-band model.
These parameters only define the NN, SNN and TNN hopping integrals for one specific direction.
The main question remains what the hopping integrals in the other directions are.
It turns out that these depend on the hopping integrals defined in \crefrange{eq:beginhoppinglist}{eq:endhoppinglist} due to the $D_{3h}$ point-group symmetry of the Hamiltonian.
The case for $t_{00}$ is simple, all NN hoppings should have the same energy due to the circular symmetry of the \dzsq orbital (see also \cref{fig:dz2_orbital}).
The same applies for the SNN hoppings ($r_{00}$), or TNN hoppings ($u_{00}$) as only \dzsq orbitals are involved.
All the other hopping integrals, however, involve \dxy and/or \dxsmys orbitals and from their plots (\cref{fig:dxy_orbital,fig:dx2-y2_orbital}), it is clear that these integrals may be different for different NN, SNN or TNN.
There are too many hopping intergrals to work them all out, so only some examples are covered to give an idea how these integrals can be calculated.
The results of all the other hopping integrals will be tabulated in \cref{tab:hoppingintegralsNN,tab:hoppingintegralsSNN}.
It is also worth to mention that once all the NN hopping integrals are found, all the TNN hopping integrals can easily found by analogy: one simply has to replace $t$ by $u$, $\vb{a}_1$ by $2\vb{a}_1$, and $\vb{a}_2$ by $2\vb{a}_2$.

\begin{table}[ht]
	\centering
	\caption{Overview of all NN hopping integrals in terms of the expressions in \crefrange{eq:beginhoppinglist}{eq:endhoppinglist}. Note that TNN hopping integrals can be found by replacing $t$ with $u$, $\vb{a}_1$ with $2\vb{a}_1$, and $\vb{a}_2$ with $2\vb{a}_2$.}
	\begin{tabular}{llll}
		\toprule
		& \multicolumn{3}{c}{neighbor positions} \\
		\cmidrule{2-4}
		hopping orbitals & $\pm \vb{a}_1$ & $\pm \vb{a}_2$ & $\pm\qty(\vb{a}_2 - \vb{a}_1)$  \\
		\cmidrule{1-1}
		\dzsq $\rightarrow$ \dzsq & $t_{00}$ & $t_{00}$ & $t_{00}$ \\
		\dzsq $\rightarrow$ \dxy & $t_{01}$ & $\frac{\sqrt{3} t_{02} \pm t_{01}}{2}$ & $-\frac{\sqrt{3} t_{02} \pm t_{01}}{2}$ \\
		\dzsq $\rightarrow$ \dxsmys & $t_{02}$ & $\frac{\pm\sqrt{3} t_{01} - t_{02}}{2}$ & $\frac{\pm\sqrt{3} t_{01} - t_{02}}{2}$ \\
		\dxy $\rightarrow$ \dxy & $t_{11}$ & $\frac{3t_{22} + t_{11}}{4}$ & $\frac{3t_{22} + t_{11}}{4}$ \\
		\dxy $\rightarrow$ \dxsmys & $t_{12}$ & $\frac{\sqrt{3}}{4} \qty(t_{11} - t_{22}) \mp t_{12}$ & $\frac{\sqrt{3}}{4} \qty(t_{22} - t_{11}) \pm t_{12}$ \\
		\dxsmys $\rightarrow$ \dxsmys & $t_{22}$ & $\frac{3 t_{11} + t_{22}}{4}$ & $\frac{3 t_{11} + t_{22}}{4}$ \\
		\bottomrule
	\end{tabular}
	\label{tab:hoppingintegralsNN}
\end{table}

\begin{table}[ht]
	\centering
	\caption{Overview of all SNN hopping integrals in terms of the expressions in \crefrange{eq:beginhoppinglist}{eq:endhoppinglist}. The hopping integrals in the opposite directions are the same except that $r_{01}$ should be replaced by $r_{10}$.}
	\begin{tabular}{llll}
		\toprule
		& \multicolumn{3}{c}{neighbor positions} \\
		\cmidrule{2-4}
		hopping orbitals & $2 \vb{a}_1 - \vb{a}_2$ & $2 \vb{a}_2 - \vb{a}_1$ & $-\vb{a}_1 - \vb{a}_2$  \\
		\cmidrule{1-1}
		\dzsq $\rightarrow$ \dzsq & $r_{00}$ & $r_{00}$ & $r_{00}$ \\
		\dzsq $\rightarrow$ \dxy & $r_{01}$ & $0$ & $-r_{01}$ \\
		\dzsq $\rightarrow$ \dxsmys & $-r_{01}/\sqrt{3}$ & $\frac{2 r_{01}}{\sqrt{3}}$ & $-\frac{r_{01}}{\sqrt{3}}$ \\
		\dxy $\rightarrow$ \dxy & $r_{11}$ & $\frac{3 r_{22} + r_{11}}{4} + \frac{1}{2}\sqrt{3} r_{12}$ & $\frac{3 r_{22} + r_{11}}{4} - \frac{1}{2}\sqrt{3} r_{12}$ \\
		\dxy $\rightarrow$ \dxsmys & $r_{12}$ & $0$ & $-r_{12}$ \\
		\dxsmys $\rightarrow$ \dxsmys & $r_{22} = r_{11} + \frac{2 r_{12}}{\sqrt{3}}$ & $\frac{3 r_{11} + r_{22}}{4} - \frac{1}{2}\sqrt{3} r_{12}$ & $\frac{3 r_{11} + r_{22}}{4} + \frac{1}{2}\sqrt{3} r_{12}$ \\
		\bottomrule
	\end{tabular}
	\label{tab:hoppingintegralsSNN}
\end{table}

In this section, the hopping between two orbitals with a relative displacement $\vb{a}$ is defined as
\begin{equation}
	h_{ij}\qty(\vb{a}) = \mel{\phi_i\qty(\vb{r})}{\hat{H}}{\phi_j\qty(\vb{r} - \vb{a})}.
\end{equation}
The first example that will be worked out is the hopping integral $h_{12}\qty(\vb{a}_2)$.
In order to do that, one can exploit the $D_{3h}$ point-group symmetry of the Hamiltonian.
Suppose that $\hat{O}$ is a symmetry operation in $D_{3h}$, then the Hamiltonian can be rewritten as
\begin{equation}
	\hat{H} = \hat{O}^{-1} \hat{O} \hat{H} = \hat{O}^{-1} \hat{H} \hat{O},
\end{equation}
as $\comm*{\hat{H}}{\hat{O}} = 0$.
What can be seen from this expression is that one can always sandwich the Hamiltonian between a symmetry operator in $D_{3h}$ and its inverse.
By making a smart choice for $\hat{O}$, one can then express $h_{12}\qty(\vb{a}_2)$ in terms of the hopping integrals given in \crefrange{eq:beginhoppinglist}{eq:endhoppinglist}.
As the center of the \dxsmys orbital is at $\vb{a}_2$ while the center of the known hopping integrals are at $\vb{a}_1$, it makes sense to choose $\hat{O} = \mirror{1}$, so that the ket part of $h_{12}\qty(\vb{a}_2)$ becomes
\begin{equation}
	\mirror{1} \ket{\dxsmys\qty(\vb{r} - \vb{a}_2)} = \frac{1}{2} \qty(\sqrt{3} \ket{\dxy\qty(\vb{r} - \vb{a}_1)} - \ket{\dxsmys\qty(\vb{r} - \vb{a}_1)}),
\end{equation}
which can be derived in a similar way as the mirror operation on the \dxy orbital (see \cref{eq:mirror1dxy}).
Using the fact that a reflection operation is unitary and equals its inverse (i.e. $\mirror{1} = \mirror{1}^{-1} = \mirror{1}^\dagger$), the bra part of $h_{12}\qty(\vb{a}_2)$ evaluates to
\begin{equation}
	\qty[\mirror{1} \ket{\dxy\qty(\vb{r})}]^\dagger = \frac{1}{2} \qty(\bra{\dxsmys(\vb{r})} \sqrt{3} + \bra{\dxy(\vb{r})}).
\end{equation}
Putting together then gives
\begin{equation}
	h_{12}\qty(\vb{a}_2) = \frac{\sqrt{3}}{4} \qty(t_{11} - t_{22}) + \frac{3 t_{21} - t_{12}}{4},
\end{equation}
where $t_{21} = \mel{\dxsmys\qty(\vb{r})}{\hat{H}}{\dxy\qty(\vb{r} - \vb{a}_1)}$ still needs to be evaluated as it turns out to be a dependent hopping integral.
To derive an expression for $t_{21}$, first make use of the \mirror{2} symmetry of the Hamiltonian so that
\begin{equation}
	t_{21} = \mel{\dxsmys\qty(\vb{r})}{\mirror{2}\hat{H}\mirror{2}}{\dxy\qty(\vb{r} - \vb{a}_1)} = -\mel{\dxsmys\qty(\vb{r})}{\hat{H}}{\dxy\qty(\vb{r} + \vb{a}_1)}.
\end{equation}
Next, the translational symmetry of the Hamiltonian can be used and the corresponding operator is defined as $\transl{\vb{a}} \ket{\psi\qty(\vb{r})} = \ket{\psi\qty(\vb{r} + \vb{a})}$.
As the inverse of $\transl{\vb{a}}$ is simply $\transl{-\vb{a}}$ and the lattice is periodic in $\vb{a}_1$, $t_{21}$ can be rewritten as
\begin{equation}
	t_{21} = -\mel{\dxsmys\qty(\vb{r})}{\transl{\vb{a}_1} \hat{H} \transl{-\vb{a}_1}}{\dxy\qty(\vb{r} + \vb{a}_1)} = -\mel{\dxsmys\qty(\vb{r} - \vb{a}_1)}{\hat{H}}{\dxy\qty(\vb{r})}.
\end{equation}
But from this, the complex conjugate of $t_{12}$ can be recognized, and since all the hopping integrals in Liu et al. are real, one will obtain $t_{21} = -t_{12}$, so that the final expression will become
\begin{equation}
	h_{12}\qty(\vb{a}_2) = \frac{\sqrt{3}}{4} \qty(t_{11} - t_{22}) - t_{12}.
\end{equation}

An analysis similar to that of $h_{12}\qty(\vb{a}_2)$ can be done for $h_{12}\qty(\vb{a}_2 - \vb{a}_1)$, but instead of the \mirror{1} operation, the $\rotation{3}^{-1}$ operation need to be used instead.
The bra and ket terms will thus become
\begin{align}
	\rotation{3}^{-1} \ket{\dxsmys\qty(\vb{r} - \vb{a}_2 + \vb{a}_1)} &= \frac{1}{2} \qty(\sqrt{3} \ket{\dxy\qty(\vb{r} - \vb{a}_1)} - \ket{\dxsmys\qty(\vb{r} - \vb{a}_1)}),\\
	\bra{\dxy\qty(\vb{r})} \rotation{3} &= -\frac{1}{2} \qty(\bra{\dxsmys\qty(\vb{r})} \sqrt{3} + \bra{\dxy\qty(\vb{r})}),
\end{align}
so that
\begin{equation}
	h_{12}\qty(\vb{a}_2 -\vb{a}_1) = \frac{\sqrt{3}}{4} \qty(t_{22} - t_{11}) + t_{12}.
\end{equation}
The hopping integrals to the other nearest neighbors can also be found with this analysis by using the appropriate symmetry operations.
However, by doing a similar derivation as the one for $t_{21}$, it turns out that the hopping integral in a given direction is the same as the one in the opposite direction except for a minus sign in front of $t_{01}$ and $t_{12}$.

The calculation of the SNN hoppings is a bit different from the calculation of the NN and the TNN hoppings.
In case of NN hoppings, $t_{01}$ and $t_{10}$ (idem for $t_{02}$ and $t_{20}$) are dependent hopping integrals due to the \mirror{2} symmetry operation, while $t_{01}$ and $t_{02}$ (idem for $t_{10}$ and $t_{20}$) are independent.
For SNN hoppings (where $t$ is replaced by $r$), however, this is the other way around and the dependence between $r_{01}$ and $r_{02}$ (and between $r_{10}$ and $r_{20}$) can be found by means of the $\mirror{3}$ symmetry operation:
\begin{align}
	r_{02} &= -\frac{r_{01}}{\sqrt{3}},\\
	r_{20} &= -\frac{r_{10}}{\sqrt{3}}.
\end{align}
It should also be remarked that in Liu et al. the parameter $r_2$ should be the value for $r_{10}$ in order to reproduce their results\footnote{The statement that $r_2 = r_{20}$ in Liu et al.'s erratum \cite{liu_erratum:_2014} is therefore wrong as well.}.
The reason why a hopping integral $r_{22}$ is `missing' in Liu et al. (compare the NN and TNN case) is because it is not an independent parameter.
In order to see that, consider the hopping integrals $r_{12}$ and $r_{21}$.
Due to the \mirror{3} symmetry of the lattice,
\begin{align}
	r_{12} &= \frac{\sqrt{3}}{4}\qty(r_{22} - r_{11}) + \frac{3 r_{21} - r_{12}}{4},\label{eq:r12}\\
	r_{21} &= \frac{\sqrt{3}}{4}\qty(r_{22} - r_{11}) + \frac{3 r_{12} - r_{21}}{4}.\label{eq:r21}
\end{align}
It can easily be verified (by subtracting \cref{eq:r21} from \cref{eq:r12}) that the only way that both equations can hold, is when $r_{12} = r_{21}$.
This, however, also means that either $r_{22}$ or $r_{11}$ must be a dependent hopping integral.
In Liu et al. this is chosen to be $r_{22}$ and after some calculation, this hopping integral is given by
\begin{equation}
	r_{22} = r_{11} + \frac{2 r_{12}}{\sqrt{3}}.
\end{equation}

\subsection{Spin-orbit interaction}
One of interactions in the solid that will be very important later in the thesis is the spin-orbit interaction (abbreviated as SOI in the rest of this thesis).
This can be described as an atomic \LdotS-like perturbation to the Hamiltonian.
Implementing this in the three-band model in Kwant is not too difficult.
Using the basis $\qty{\ket{\dzsq, \uparrow}, \ket{\dxy, \uparrow}, \ket{\dxsmys, \uparrow}, \ket{\dzsq, \downarrow}, \ket{\dxy, \downarrow}, \ket{\dxsmys, \downarrow}}$, the onsite Hamiltonian need to be replaced by
\begin{equation}\label{eq:onsiteHfull}
    \hat{H}_{onsite} = \hat{I}_2 \otimes \hat{H}_{0} + \lambda \qty( \hat{S}_x \otimes \hat{L}_x  + \hat{S}_y \otimes \hat{L}_y + \hat{S}_z \otimes \hat{L}_z),
\end{equation}
where $\hat{I}_2$ is a $2 \times 2$ identity matrix and $\hat{H}_{0}$ is the unperturbed onsite Hamiltonian given by \cref{eq:onsiteH_unpert}.
Next step is to evaluate $\hat{L}_x$, $\hat{L}_y$, and $\hat{L}_z$ operators for the given basis.
Note that the cubic harmonics can be expressed in terms of the spherical harmonics given in \crefrange{eq:beginSHlist}{eq:endSHlist}, which are eigenvectors of $\hat{L}_z$.
After some calculation, it can be found that $\hat{L}_x = \hat{L}_y = 0$ and that\footnote{The $\hbar$ is omitted for convenience as it can also be put into the $\lambda$.}
\begin{equation}
    \hat{L}_z = \mqty(0 & 0 & 0 \\ 0 & 0 & 2i \\ 0 & -2i & 0),
\end{equation}
so that \cref{eq:onsiteHfull} can be simplified to
\begin{equation}\label{eq:onsiteHworkedOut}
    \hat{H}_{onsite} = \mqty(\hat{H}_{0} + \frac{\lambda}{2} \hat{L}_z  & 0  \\ 0 & \hat{H}_{0} - \frac{\lambda}{2} \hat{L}_z).
\end{equation}
Note that the Hamiltonian is block diagonal with respect to the spin Hilbert space, which makes the spin in the $z$-direction good quantum numbers.
%\Cref{eq:onsiteHworkedOut} is therefore consistent with the statement in the introduction that TMD monolayers have Ising SOI: the spin splitting is perpendicular to the surface.

\subsection{Results and discussion}\label{subsec:3bandresults}
The resulting band structure of the three-band model for \ce{MoS2} is given in \cref{fig:3bandstructure}.
To this end, the fitted parameters of the generalized-gradient approximation (GGA) for \ce{MoS2} from Liu et al. are used.
According to Liu et al., the strength of the SOI ($\lambda$) is \SI{0.073}{\eV} for \ce{MoS2}.
It can be seen that the three-band model implemented in Kwant (\cref{fig:3band_unperturbed}) visually agrees with the band structure in Liu et al.
Furthermore, the splitting along a trajectory through a $+\mathrm{K}$ point is opposite to that through a $-\mathrm{K}$ point.
This confirms the statement in the introduction that TMD monolayers have Ising SOI.
The band structure with SOI of the 3-band model has two remarkable properties: (1) there is no splitting in any band along the $\mathrm{\Gamma}$-${\mathrm{M}}$ trajectory, and (2) there is no splitting in the conduction band at the $\pm\mathrm{K}$ point.
The former can be explained by means of symmetry arguments, but the latter is not in agreement with expectation \cite{cheiwchanchamnangij_quasiparticle_2012,zeng_low-frequency_2012}.

\begin{figure}[ht]
    \centering
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=111px]{band_structure/figures/3bandstructure_unperturbed}
        \caption{}
        \label{fig:3band_unperturbed}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=111px]{band_structure/figures/3bandstructure_SOI_Kplus}
        \caption{}
        \label{fig:3band_Kplus}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.32\linewidth}
        \centering
        \includegraphics[height=111px]{band_structure/figures/3bandstructure_SOI_Kmin}
        \caption{}
        \label{fig:3band_Kmin}
    \end{subfigure}
    \caption{The 3-band structure of \ce{MoS2} from Liu et al.'s paper: (a) no SOI; (b and C) SOI interaction with the trajectory through a $+\mathrm{K}$ point (b) or $-\mathrm{K}$ point (c). The red lines indicate spin-up bands, the blue lines indicate spin-down bands.}
    \label{fig:3bandstructure}
\end{figure}

To explain why there is no splitting along the $\mathrm{\Gamma}$-${\mathrm{M}}$ trajectory, recall that if $\comm*{\hat{H}}{\hat{O}} = 0$ and $\ket{\psi}$ is an eigenstate of $\hat{H}$, then $\hat{O}\ket{\psi}$ is also an eigenstate with same eigenenergy as $\ket{\psi}$.
For the $\mathrm{M}$ and $\mathrm{\Gamma}$ point, the time reversal operator $\timerev$ can be used.
It should be noted that $\timerev$ is an antilinear and antiunitary operator, which requires caution as computation with such operators is a bit different than with unitary and linear operators.
%A short explanation of how the time reversal operator works is given in \cref{ch:timerev}.
Both the tight-binding Hamiltonian $\hat{H}(\vb{k})$ and the spin-orbit Hamiltonian are symmetric under time reversal, i.e. $\comm*{\hat{H}}{\timerev} = 0$.
This also implies that for the tight-binding Hamiltonian
\begin{equation}
    \hat{H}(\vb{k}) = \timerev^{-1} \hat{H}(\vb{k}) \timerev = \hat{H}^*(-\vb{k}).
\end{equation}
Furthermore, the time reversal operator flips crystal momentum and spin so that
\begin{equation}\label{eq:timerevonstate}
    \timerev \ket{\psi} = \timerev \ket{\vb{k}, \uparrow\downarrow} = \exp(i \varphi) \ket{-\vb{k}, \downarrow\uparrow},
\end{equation}
where $\varphi$ is some phase.
The states $\ket{\psi}$ and $\timerev \ket{\psi}$ are not only degenerate, but also always orthogonal due to fact that the spin is a half-integer.
All states are therefore at least twofold degenerate and this is also known as Kramer's degeneracy theorem.
From \cref{eq:timerevonstate}, it is clear that if $\vb{k}$ and $-\vb{k}$ represent equivalent crystal momenta, then there will be no spin splitting at crystal momentum $\vb{k}$.
It is then straightforward to see that the $\mathrm{\Gamma}$ and $\mathrm{M}$ points have no spin splitting, namely if $\vb{k}$ is such a point, then $-\vb{k}$ is such a point as well.
However, in order to explain that there is no splitting along the $\mathrm{\Gamma}$-${\mathrm{M}}$ trajectory, one need the mirror symmetry along that trajectory.
If $\vb{k}$ is at the trajectory, then $\mirror{i} \ket{\vb{k}, \uparrow\downarrow} = \exp(i \varphi) \ket{\vb{k}, \downarrow\uparrow}$ and thus there is no spin-splitting along the $\mathrm{\Gamma}$-${\mathrm{M}}$ trajectory due to the mirror symmetry.

To explain why there is no splitting of the conduction band in the three-band model at the $\pm\mathrm{K}$ point, note that an eigenstate at point $\vb{k}$ in reciprocal space can be expressed as
\begin{equation}
    \ket{\psi(\vb{k}),\uparrow\downarrow} = \alpha(\vb{k}) \ket{\Y{-2}{2},\uparrow\downarrow} + \beta(\vb{k}) \ket{Y^0_2,\uparrow\downarrow} + \gamma(\vb{k}) \ket{\Y{2}{2},\uparrow\downarrow}.
\end{equation}
For such states, the first order energies of the SOI is given by
\begin{equation}
    \ev{\hat{L}_z \hat{S}_z}{\psi(\vb{k}),\uparrow\downarrow} = \pm \qty(|\gamma(\vb{k})|^2 - |\alpha(\vb{k})|^2).
\end{equation}
It is clear from this expression that there will be no splitting if $|\gamma(\vb{k})| = |\alpha(\vb{k})|$, which is for example the case for the \dzsq, \dxy and \dxsmys orbitals.
From fig. 2 in Liu et al.'s paper, it can be seen that the eigenstate of the conduction band at the $\pm\mathrm{K}$ point is $\ket{\dzsq}$, and this will not be split by SOI.
It can also be derived analytically using eqs. (15) to (18) from Liu et al. that $\mel{\dzsq}{\hat{H}(\vb{k})}{\dxy} = \mel{\dzsq}{\hat{H}(\vb{k})}{\dxsmys} = 0$ if $\vb{k}$ is a $\pm\mathrm{K}$ point.
This means that the $\ket{\dzsq}$ state is decoupled from the other orbitals and thus $\ket{\dzsq}$ is one of the eigenstates at the $\pm\mathrm{K}$ point.
As mentioned earlier, however, there is some splitting expected in the conduction band and this will only be visible when the $p$-orbitals of the chalcogen atoms are included, which will be shown later in the 11-band model.
This splitting at the conduction band will also be relevant later in this thesis, as this is needed to describe the vanishing SOI in the conduction band.