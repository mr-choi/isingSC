\section{11-band tight binding model}\label{sec:11band}
As discussed in the previous section, the three-band model does not reproduce the expected SOI splitting in the conduction band at the $\pm\mathrm{K}$ point.
Therefore, the more accurate 11-band model of Fang et al.\cite{fang_ab_2015} will be used instead.
It has already been explained in detail how to calculate dependent hopping integrals using symmetries in \cref{sec:3band}, so it will not be done again in this section.

\subsection{Orbitals and hopping integrals}\label{subsec:11bandorbitals}
The orbitals that are included in the 11-band model are the 5 $d$-orbitals of the transition metal and the 3 $p$-orbitals of both the top and the bottom chalcogen atoms (remember \cref{fig:TMDmonolayer_crystal} in the introduction).
The 5 $d$-orbitals are \dzsq, \dxy, \dxsmys, \dxz and \dyz, and the last two orbitals are, just like the other 3 discussed in the three-band model, cubic harmonics that are related to the spherical harmonics according to
\begin{align}
    \ket{\dxz} &= \frac{1}{\sqrt{2}} \qty(\ket{\Y{-1}{2}} - \ket{\Y{1}{2}}),\\
    \ket{\dyz} &= \frac{i}{\sqrt{2}} \qty(\ket{\Y{-1}{2}} + \ket{\Y{1}{2}}).
\end{align}
The two chalcogen atoms each have a \px, \py and \pz orbital that are related to the spherical harmonics according to
\begin{align}
    \ket{\px} &= \frac{1}{\sqrt{2}} \qty(\ket{\Y{-1}{1}} - \ket{\Y{1}{1}}),\\
    \ket{\py} &= \frac{i}{\sqrt{2}} \qty(\ket{\Y{-1}{1}} + \ket{\Y{1}{1}}),\\
    \ket{\pz} &= \ket{\Y{0}{1}}.
\end{align}
It is convenient to hybridize the $p$ orbitals of the top and bottom layer as follows:
\begin{align}
    \ket{\px^{o}} &= \frac{1}{\sqrt{2}} \qty(\ket{\px^A} - \ket{\px^B}),\quad
    \ket{\py^{o}} = \frac{1}{\sqrt{2}} \qty(\ket{\py^A} - \ket{\py^B}),\quad
    \ket{\pz^{o}} = \frac{1}{\sqrt{2}} \qty(\ket{\pz^A} + \ket{\pz^B}), \\
    \ket{\px^{e}} &= \frac{1}{\sqrt{2}} \qty(\ket{\px^A} + \ket{\px^B}),\quad
    \ket{\py^{e}} = \frac{1}{\sqrt{2}} \qty(\ket{\py^A} + \ket{\py^B}),\quad
    \ket{\pz^{e}} = \frac{1}{\sqrt{2}} \qty(\ket{\pz^A} - \ket{\pz^B}),
\end{align}
where $A$ and $B$ indicate the the top and bottom layer respectively.
These hybridized $p$ orbitals are eigenstates of the mirror operation in the $xy$-plane \mirror{xy}, which will be useful later on.
The two chalcogen atoms in the top and bottom layer can effectively be replaced with one atom in the transition metal layer, and these effective atoms have these 6 hybridized orbitals.

The 11-band model takes the following hoppings into account:
\begin{itemize}
    \item All nearest neighbor hoppings between two transition metal atoms (NN \ce{M}-\ce{M}).
    \item All nearest neighbor hoppings between two chalcogen atoms (NN \ce{X}-\ce{X}).
    \item All nearest neighbor hoppings between a chalcogen atom and a transition metal atom (NN \ce{X}-\ce{M}).
    \item Some second nearest neighbor hoppings between a chalcogen atom and a transition metal atom (SNN \ce{X}-\ce{M}).
    This is done for improved accuracy.
\end{itemize}
The number of possible hopping directions times the number of orbitals in the model gives a lot of parameters.
Fortunately, most of these parameters can be eliminated by considering the \mirror{xy} symmetry of the Hamiltonian.
To this end, one has to check whether the orbitals correspond to an eigenvalue of $1$ or $-1$ under the \mirror{xy} operation.
An eigenvalue of $1$ means that the orbital is even under \mirror{xy} while an eigenvalue of $-1$ means that the orbital is odd under \mirror{xy}.
It can be found out that the \dzsq, \dxy, \dxsmys, $\px^e$, $\py^e$, and $\pz^e$ orbitals are even under \mirror{xy}, while the \dxz, \dyz, $\px^o$, $\py^o$, and $\pz^o$ orbitals are odd under \mirror{xy}.
The key point is that there is no hopping between an even and an odd orbital.
In order to see this, let $\ket{\phi_i(\vb{r})}$ and $\ket{\phi_j(\vb{r} - \vb{a})}$ be an even and odd orbital respectively.
Note that the hopping direction $\vb{a}$ lies in the $xy$-plane so that this will not be affected when \mirror{xy} is applied.
However, this means that
\begin{equation}
    \mel{\phi_i(\vb{r})}{\hat{H}}{\phi_j(\vb{r} - \vb{a})} = \mel{\phi_i(\vb{r})}{\mirror{xy}\hat{H}\mirror{xy}}{\phi_j(\vb{r} - \vb{a})} = -\mel{\phi_i(\vb{r})}{\hat{H}}{\phi_j(\vb{r} - \vb{a})},
\end{equation}
and the only way this can hold is when the hopping integral is $0$.
Therefore, only hopping integrals between two even orbitals or two odd orbitals have to be considered, and this reduces the amount of parameters significantly.

\subsection{Spin-orbit interaction}\label{subsec:11bandSOI}
Just like in the three-band model, an \LdotS-like, momentum-independent SOI is used.
It should be kept in mind, however, that the strength of the interaction at the transition metal is different than at the chalcogen atoms.
The spin orbit Hamiltonian can thus be expressed like
\begin{equation}
    \hat{H}_{SOI} = \qty(\lambda_M \vb{\hat{L}}_M + \lambda_X \vb{\hat{L}}_X^A + \lambda_X \vb{\hat{L}}_X^B) \vdot \vb{\hat{S}}.
\end{equation}
Unlike in the three-band model, there will be an $x$ and $y$ component of the angular momentum, which suggest that the $z$ component of the spin may not be a good quantum number.
However, it will turn out that spin in the $z$-component will be (approximately) a good quantum number, and one can thus still classify the interaction as Ising SOC.
In order to understand why this happens, first note that coupling of spin-up and spin-down due to the $x$ and $y$-component of the angular momentum can only happen between the states $\ket{\Y{m}{l}, \uparrow\downarrow}$ and $\ket{\Y{m \pm 1}{l}, \downarrow\uparrow}$\footnote{It is also worth to mention that this is also the reason why the three-band model did not have an $x$ and $y$-component of angular momentum.}.
These two states, however, are not both even or both odd under the \mirror{xy} symmetry operation, as discussed in \cref{subsec:11bandorbitals}, which means that there cannot be any coupling between these two states.
Now consider orbitals $\ket{\psi_e}$ and $\ket{\psi_o}$ that are even and odd respectively under the \mirror{xy} operation.
Let the energy difference of these orbitals (without SOI) be $\Delta\varepsilon(\vb{k})$ at crystal momentum $\vb{k}$.
Furthermore, suppose that the matrix elements of the $x$ and $y$-component of the SOI is given by $ \lambda \mel{\psi_e,\uparrow}{\hat{L}_{x} \hat{S}_{x} + \hat{L}_{y} \hat{S}_{y}}{\psi_o,\downarrow} = t_1(\vb{k})$ and $ \lambda \mel{\psi_e,\downarrow}{\hat{L}_{x} \hat{S}_{x} + \hat{L}_{y} \hat{S}_{y}}{\psi_o,\uparrow} = t_2(\vb{k})$.
The $z$ component of the SOI will cause a splitting $t_3((\vb{k}))$ and $t_4(\vb{k})$ in the spin space for the even and odd orbital respectively.
Using the basis $\qty{\ket{\psi_e,\uparrow}, \ket{\psi_o,\uparrow}, \ket{\psi_e,\downarrow}, \ket{\psi_o,\downarrow}}$ The Hamiltonian with SOI interaction will thus become
\begin{equation}
\hat{H} = \mqty(\Delta\varepsilon(\vb{k})/2 + t_3(\vb{k})/2 & 0 & 0 & t_1 \\
0 & -\Delta\varepsilon(\vb{k})/2 + t_4(\vb{k})/2 & t_2^* & 0 \\
0 & t_2 & \Delta\varepsilon(\vb{k})/2 - t_3(\vb{k})/2 & 0 \\
t_1^* & 0 & 0 & -\Delta\varepsilon(\vb{k})/2 - t_4(\vb{k})/2).
\end{equation}
From this expression, it can be concluded that the $z$ component of the spin is approximately a good quantum number if $\abs{t_1(\vb{k})},\abs{t_2(\vb{k})} \ll \abs{\Delta\varepsilon(\vb{k}) \pm t_{3,4}(\vb{k})}$.
This is the case for the most part of the band structure as $\Delta\varepsilon(\vb{k})$ can be in the order of multiple \si{eV}, while the strength of the SOI is only in order of \SIrange{50}{80}{\meV} for \ce{MoS2}.
The only way mixing of spin-up and spin-down may occur, is when even and odd bands would cross each other if there would be no SOI.
In fact, the spin orbit-interaction will cause small anticrossings between such bands and at these anticrossings, mixing of spin-up and spin-down will occur.

\subsection{Results and discussion}\label{subsec:11bandresults}
The results of the 11-band model for \ce{MoS2} are displayed in \cref{fig:11bandstructure}.
The used fit parameters are based on the DFT calculations, and can be found in table VII of Fang et al.'s paper.
The strength of the SOI can also be found and is given in table VIII.
The resulting band structure in \cref{fig:11band_orbitalmixing} is calculated with Kwant, and is in agreement with the one in Fang et al.'s paper (fig. 3).
The band structure with SOI is displayed in \cref{fig:11band_SOI}.
As discussed in \cref{subsec:11bandSOI}, mixing of spin-up and spin-down states is not visible in the figure, except where an even and odd orbital would cross each other (see \cref{fig:11band_SOIanticrossing}).

\begin{figure}[ht]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \includegraphics[height=200px]{band_structure/figures/11bandstructure_orbitalmixing}
        \caption{}
        \label{fig:11band_orbitalmixing}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \includegraphics[height=200px]{band_structure/figures/11bandstructure_SOI}
        \caption{}
        \label{fig:11band_SOI}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \includegraphics[height=190px]{band_structure/figures/11bandstructure_SOIanticrossing}
        \caption{}
        \label{fig:11band_SOIanticrossing}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \includegraphics[height=190px]{band_structure/figures/11bandstructure_SOIzoom}
        \caption{}
        \label{fig:11band_SOIzoom}
    \end{subfigure}
    \caption{The 11-band structure of \ce{MoS2} from Fang et al.'s paper. The used fit parameters are from the DFT method. (a) no SOI. Red-colored bands have $p$ orbitals as eigenstate, whereas blue-colored bands have $d$ orbitals as eigenstate. Purple-colored bands have a mix of $p$ and $d$ orbitals as eigenstate; (b) SOI with trajectory through a $+\mathrm{K}$ point. The red lines indicate spin-up bands, the blue lines indicate spin-down bands; (c) zoom of an anticrossing of two filled bands, indicated with the green box in (b); (d) zoom of the conduction band at the $+\mathrm{K}$ point, indicated with a black rectangle in (b).}
    \label{fig:11bandstructure}
\end{figure}

In the three-band model, it has been shown that there was no splitting in the conduction band at the $+\mathrm{K}$ point due to the fact that the \dzsq orbital is decoupled from the \dxy and \dxsmys orbitals.
In the 11-band model, these is a small splitting of the conduction band (see \cref{fig:11band_SOIzoom}).
In first order, this splitting cannot be caused by the other two $d$ orbitals (\dxz and \dyz), as these states have to be decoupled from the \dzsq orbital due to the \mirror{xy} symmetry.
Thus, the origin of the splitting (in first order) have to be the $p$ orbitals of the chalcogen atoms.
In table IV of Fang et al.'s paper, it indeed turns out that the eigenstate of the lowest conduction band has a small contribution from the $p$ orbitals of the chalcogen atoms:
\begin{equation}
    \ket{\psi_c} \approx 0.9154 i \ket{\dzsq} + 0.4026 \ket{p_{-1}^e},
\end{equation}
where $\ket{p_{-1}^e} = \qty(\ket{\px^e} - i \ket{\py^e})/\sqrt{2}$.
From this expression, the splitting of the spin-up and spin-down band can be approximated with first-order perturbation theory:
\begin{equation}
    \varepsilon_{\uparrow} - \varepsilon_{\downarrow} \approx \lambda_X (0.4026)^2 \qty(\ev{\LdotS}{p_{-1}^e, \uparrow} - \ev{\LdotS}{p_{-1}^e, \downarrow}) \approx \SI{-0.009}{\eV},
\end{equation}
which is a small splitting compared to that of the valence band $\varepsilon_{\uparrow} - \varepsilon_{\downarrow} \approx \SI{0.15}{\eV}$ (see also figure 4 of Fang et al.).