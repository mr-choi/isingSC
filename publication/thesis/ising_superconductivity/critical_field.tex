\section{Critical magnetic field}
In the previous section, it has been shown that there is an universal relation of the critical temperature: \cref{eq:universalTc} applies for any time-reversal symmetric normal phase Hamiltonian in the weak-coupling limit.
For the critical magnetic field, however, there is no such universal relation, and it depends on the SOI of the material what the critical field will be.
In this section, the critical field of a simple 2D free electron model will be discussed first

\subsection{2D free electron model}\label{subsec:free2Delectrons}
The normal phase Hamiltonian of the 2D free electron model without external field is given by
\begin{equation}
    \hat{H}_N = \qty(\frac{\hbar^2 k^2}{2 m_e} - \mu) \hat{\sigma}_{0}.
\end{equation}
When an external field is applied to the system, the two spin states will be split, which is also known as the Zeeman effect.
The Hamiltonian due to this external field is given by
\begin{equation}
    \hat{H}_{Zeeman} = \frac{1}{2} \mu_B g_e \vb*{\hat{\sigma}} \vdot \vb{H}_{ext} \approx \mu_B \vb*{\hat{\sigma}} \vdot \vb{H}_{ext},
\end{equation}
where $\vb*{\hat{\sigma}} = [\hat{\sigma}_x, \hat{\sigma}_y, \hat{\sigma}_z]^T$, $\mu_B$ is the Bohr magneton and $g_e \approx 2$ is the gyromagnetic ratio of the electron spin.
Plugging the sum of the normal phase Hamiltonian and the Zeeman Hamiltonian in the BdG Hamiltonian (\cref{eq:HBdG}) gives
\begin{equation}
    \hat{H}_{BdG} = \mqty[\frac{\hbar^2 k^2}{2 m_e} - \mu + \mu_B H_z & \mu_B \qty(H_x - i H_y) & 0 & -\Delta \\
    \mu_B \qty(H_x + i H_y) & \frac{\hbar^2 k^2}{2 m_e} - \mu - \mu_B H_z & \Delta & 0 \\
    0 & \Delta^* & -\frac{\hbar^2 k^2}{2 m_e} + \mu - \mu_B H_z & \mu_B \qty(-H_x - i H_y) \\
    -\Delta^* & 0 & \mu_B \qty(-H_x + i H_y) & -\frac{\hbar^2 k^2}{2 m_e} + \mu + \mu_B H_z]
\end{equation}
The eigenvalues can be derived analytically and are given by
\begin{equation}
    \varepsilon_{\vb{k}} = \pm \qty| \mu_B H_{ext} \pm \sqrt{\qty[\frac{\hbar^2 k^2}{2 m_e} - \mu]^2 + \qty|\Delta|^2} |,
\end{equation}
where $H_{ext} = \sqrt{H_x^2 + H_y^2 + H_z^2}$.
Plugging these eigenvalues in the expression of the free energy (\cref{eq:freeEnergy}) results in
\begin{equation}\label{eq:2DfreeElectronFreeEnergy}
    \Phi = \frac{\abs{\Delta}^2}{V} - k_B T \sum_{\vb{k}, s=\pm 1} \log(2 \cosh(\frac{\mu_B H_{ext} + s \sqrt{\qty[\frac{\hbar^2 k^2}{2 m_e} - \mu]^2 + \qty|\Delta|^2}}{2 k_B T})).
\end{equation}
Taking the derivative with respect to $|\Delta|$ and equating this to zero gives the following expression
\begin{equation}
    \pdv{\Phi}{|\Delta|} = \frac{2}{V} - \frac{1}{2} \sum_{\vb{k}, s=\pm 1} \tanh(\frac{\mu_B H_{ext} + s \sqrt{\qty[\frac{\hbar^2 k^2}{2 m_e} - \mu]^2 + \qty|\Delta|^2}}{2 k_B T}) \frac{s}{\sqrt{\qty[\frac{\hbar^2 k^2}{2 m_e} - \mu]^2 + \qty|\Delta|^2}} = 0,
\end{equation}
and converting summation into integration gives
\begin{equation}\label{eq:DeltaMinimization2DFree}
    \frac{1}{V} = \frac{1}{4} \sum_{s=\pm 1} \int\limits_{-\hbar \omega_D}^{\hbar \omega_D} \tanh(\frac{\mu_B H_{ext} + s \sqrt{\xi^2 + \qty|\Delta|^2}}{2 k_B T}) \frac{s \mathcal{N}_s(\xi) \dd\xi}{\sqrt{\xi^2 + \qty|\Delta|^2}}.
\end{equation}
In the weak-coupling limit, \cref{eq:weakcouplinglimit,eq:universalTc} can be recovered from this equation.

The critical field $H_c$ at zero temperature with respect to the pairing at zero temperature and field $\abs{\Delta_0}$ depends on the type of system.
In case of the 2D free electron model, if $T = 0$, $\Delta = \Delta_0$, \cref{eq:DeltaMinimization2DFree} will reduce to \cref{eq:zeroTandHextCase} as long as $\mu_B H_{ext} < \abs{\Delta_0}$.
This suggests that at zero temperature, the free energy has an extrema at $\Delta = \Delta_0$ as long as $\mu_B H_{ext} < \abs{\Delta_0}$.
However, this does not always mean that the global minimum of the free energy is at that point: the normal phase $\Delta = 0$ may minimize the free energy as well.
Therefore, in order to find the critical field at zero temperature, one need to compare the free energy of the superconducting phase with that of the normal phase.
At the critical field, the difference in free energy should vanish so that
\begin{equation}\label{eq:freeEnergyDifference}
    \Phi_{N} - \Phi_{S} = \frac{\mathcal{N}_{tot}(0)}{4} \sum_{s=\pm 1} \int\limits_{-\hbar\omega_D}^{\hbar\omega_D} \qty( \abs{\mu_B H_{c} + s \sqrt{\xi^2 + \qty|\Delta_0|^2}} - \abs{\mu_B H_{c} + s \abs{\xi}} ) \dd\xi - \frac{\abs{\Delta_0}^2}{V} = 0.
\end{equation}
Assuming that $\mu_B H_{c} < \abs{\Delta_0} < \hbar \omega_D$, working out the integral gives
\begin{equation}
    \frac{2 \abs{\Delta_0}^2}{\mathcal{N}_{tot}(0) V} = \hbar \omega_D \sqrt{\qty(\hbar \omega_D)^2 + \abs{\Delta_0}^2} + \abs{\Delta_0}^2 \log(\frac{\hbar \omega_D + \sqrt{\qty(\hbar \omega_D)^2 + \abs{\Delta_0}^2}}{\abs{\Delta_0}}) - \qty(\mu_B H_{c})^2 - \qty(\hbar \omega_D)^2.
\end{equation}
In the weak-coupling limit, $2 / \mathcal{N}_{tot}(0) V$ can be substituted using \cref{eq:weakcouplinglimit}.
As $\abs{\Delta_0} \ll \hbar \omega_D$ in the weak-coupling limit, the expression can be written as a Taylor series.
When $\hbar \omega_D \to \infty$, the Taylor series will reduce to
\begin{equation}\label{eq:Hc}
    \mu_B H_c = \frac{\abs{\Delta_0}}{\sqrt{2}}.
\end{equation}
Substituting the $\abs{\Delta_0}$ for the expression in \cref{eq:universalTc} gives
\begin{equation}\label{eq:HcTc}
    \mu_B H_c \approx 1.247 k_B T_c.
\end{equation}
This expression of the critical field is in agreement with that of Clogston \cite{clogston_upper_1962}, and this is commonly known as the Pauli paramagnetic limit.

\subsection{Superconducting TMD monolayers}\label{sec:superconductingTMD}
The BdG formalism can also be applied to the \kdotp model of the TMD monolayer in \cref{sec:kpmodel}.
The effective \kdotp Hamiltonian given in \cref{eq:kpHamiltoniancb} can be rewritten as
\begin{equation}\label{eq:effHamTMDsimplified}
    \hat{H}_{\mathit{eff},\eta}(\vb{k}) = A k^2 + \qty[\eta B + \eta C k^2 + \qty(k_x^3 - 3 k_x k_y^2) D] \hat{S}_z = A k^2 + f(\vb{k},\eta,B,C,D) \hat{S}_z,
\end{equation}
where $k = \sqrt{k_x^2 + k_y^2}$ and $A$, $B$, $C$, and $D$ are the material-dependent parameters that can be determined using \cref{eq:kpHamiltoniancb}.
As stated at the end of \cref{subsec:k.p_SOI}, the spin-independent trigonal warping term can be left out to simplify analysis and this is done here as well.

Deriving the superconducting gap as well as the critical field and temperature can be done in a very similar way as in the 2D free electron model in \cref{subsec:free2Delectrons}.
The BdG Hamiltonian of a TMD monolayer is given by
\begin{equation}
    \hat{H}_{BdG}(\vb{k}) = \mqty[\hat{H}_{\mathit{eff},\eta}(\vb{k}) + \hat{H}_{Zeeman} - \mu \hat{\sigma}_0 & -i \Delta \hat{\sigma}_y \\ i \Delta^* \hat{\sigma}_y & -\hat{H}^{*}_{\mathit{eff},-\eta}(-\vb{k}) - \hat{H}^{*}_{Zeeman} + \mu \hat{\sigma}_0].
\end{equation}
The eigenvalues can be calculated analytically if the field has no component perpendicular to the TMD monolayer, i.e. it is a fully in-plane field.
In that case the eigenvalues are given by
\begin{equation}\label{eq:eigvalsBdGTMDmonolayer}
    \varepsilon_{\vb{k}} = \pm \sqrt{\xi_{\vb{k}}^2 + f_{\vb{k}}^2 + \qty(\mu_B H_{ext})^2 + \abs{\Delta}^2 \pm 2 \sqrt{\xi_{\vb{k}}^2 \qty[f_{\vb{k}}^2 + \qty(\mu_B H_{ext})^2] + \abs{\Delta}^2 \qty(\mu_B H_{ext})^2}},
\end{equation}
where $\xi_{\vb{k}} = A k^2 - \mu$ and $f_{\vb{k}} = f(\vb{k},\eta,B,C,D)$ from \cref{eq:effHamTMDsimplified}.

Just as for the 2D free electron model, an expression for the free energy can be derived and minimized it by setting the derivative of the free energy with respect to the superconducting gap to zero.
When the temperature and external field are set to zero, one can derive that
\begin{equation}\label{eq:Delta0TMDmonolayer}
    \frac{2}{V} = \frac{1}{2} \sum\limits_{\vb{k},s \in S_{\vb{k}}} \frac{1}{\sqrt{\qty(|\xi_{\vb{k}}| + s|f_{\vb{k}}|)^2 + \abs{\Delta_0}^2}} = \frac{1}{2} \int\limits_{-\hbar\omega_D}^{\hbar\omega_D} \frac{\mathcal{N}_{tot}(\xi) \dd\xi}{\sqrt{\xi^2 + \abs{\Delta_0}^2}},
\end{equation}
where $S_{\vb{k}}$ can be either a set with $-1$ and $+1$ as elements or $-1$ as only element.
It depends on the energies in the normal phase which set should be picked as these may not lie too far away from the chemical potential.
When converting the sum in \cref{eq:Delta0TMDmonolayer} into an integral, one will recover \cref{eq:zeroTandHextCase}.
Similarly the expression for the critical temperature will be
\begin{equation}
    \frac{2}{V} = \frac{1}{2} \sum\limits_{\vb{k},s \in S_{\vb{k}}} \frac{1}{\abs{|\xi_{\vb{k}}| + s|f_{\vb{k}}|}} \tanh(\frac{\abs{|\xi_{\vb{k}}| + s|f_{\vb{k}}|}}{2 k_B T_c}) = \frac{1}{2} \int\limits_{-\hbar\omega_D}^{\hbar\omega_D}  \tanh(\frac{\abs{\xi}}{2 k_B T_c}) \frac{\mathcal{N}_{tot}(\xi) \dd\xi}{\abs{\xi}},
\end{equation}
which is the same equation as \cref{eq:zeroTCase} when the sum will be properly converted into an integral.

%The calculation of the critical field is not so easy and even with a simple \kdotp model, it is not feasible to do this analytically.
%However, a lower bound of the critical field can be found using two necessary conditions for $\Delta = 0$ minimizing the free energy:
%\begin{enumerate}
%    \item The second derivative of the free energy with respect to $\Delta$ should be non-negative at $\Delta = 0$.
%    \item The free energy at $\Delta=0$ should be lower than at $\Delta=\Delta_0$.
%\end{enumerate}
%For TMD monolayers, the mathematical expression for condition 1 is given by
%\begin{equation}\label{eq:secondderivativeCondition1}
%    \eval{\pdv[2]{\Phi}{\abs{\Delta}}}_{\abs{\Delta}=0} = \frac{2}{V} - \frac{1}{2} \sum_{\vb{k},s \in S_{\vb{k}}} \frac{1}{\abs{\abs{\xi_{\vb{k}}} + s \sqrt{f_{\vb{k}}^2 + H_{c}^2}}} \qty[ 1 + \frac{H_{c}^2 s}{\abs{\xi_{\vb{k}}} \sqrt{f_{\vb{k}}^2 + H_{c}^2}}] \ge 0.
%\end{equation}
%In case the spin-orbit interaction is absent ($f_{\vb{k}} = 0$), the equation can be simplified to
%\begin{equation}
%    \frac{1}{2} \sum_{\vb{k},s \in S_{\vb{k}}} \frac{\operatorname{sgn}\qty(\abs{\xi_{\vb{k}}} + s H_c)}{\abs{\xi_{\vb{k}}}} \le \frac{2}{V}.
%\end{equation}
%Working this expression out in the weak-coupling limit gives
%\begin{equation}
%    H_c \ge \frac{\Delta_0}{2},
%\end{equation}
%which is consistent with the Pauli limit $H_c = \Delta_0 / \sqrt{2}$, although the lower bound from condition 1 for the critical field is obviously lower than the actual critical field.
%In absence of spin-orbit interaction, the mathematical expression for condition 2 is equivalent to \cref{eq:freeEnergyDifference}.
%This means that, according to condition 2, the lower bound for the critical field will be same as the actual critical field.