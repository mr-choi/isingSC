\section{Topological phase}\label{sec:topologicalphase}
The possibility of topological superconductivity in TMD monolayers has recently been described by Wang et al\cite{wang_new_2018}.
A non-trivial topological superconducting phase will occur if there are $\vb{k}$ points (also called nodal points) in the superconductor where the energy gap between particle and hole excitations vanish.
In normal superconductors without any external field, this does not occur as it is known that in that case the gap between particle and hole excitations is at least $2|\Delta|$.
The gap could be closed by means of an external field, but making the field too large will break superconductivity.
In case of the 2D free electron model, closing this gap at zero temperature is not possible as the required field strength exceeds the Pauli limit.
Ising superconductors, on the other hand, have an enhanced critical field which increases the likelihood to find topological phases.
In terms of BdG formalism, the nodal points can be found at the $\vb{k}$ points where the eigenvalues become zero.
Using the result of the \kdotp model, nodal points are found when
\begin{equation}
    \xi_{\vb{k}}^2 + f_{\vb{k}}^2 + \qty(\mu_B H_{ext})^2 + \abs{\Delta}^2 - 2 \sqrt{\xi_{\vb{k}}^2 \qty[f_{\vb{k}}^2 + \qty(\mu_B H_{ext})^2] + \abs{\Delta}^2 \qty(\mu_B H_{ext})^2} = 0.
\end{equation}
And solving this equation for $\xi_{\vb{k}}^2$ gives
\begin{equation}
    \xi_{\vb{k}}^2 = \qty(\mu_B H_{ext})^2 + f_{\vb{k}}^2 - \abs{\Delta}^2 \pm 2 i \abs{\Delta} \abs{f_{\vb{k}}}.
\end{equation}
In order to have real solutions, either $\Delta$ or $f_{\vb{k}}$ should be zero.
However, $\Delta = 0$ implies that there is no superconductivity so in order get topological superconductivity, the spin-orbit splitting term $f_{\vb{k}}$ should vanish.
This means that the nodal points should satisfy the following two equations:
\begin{align}
    \eta B + \eta C k^2 + D k^3 \cos(3\theta) = 0,\label{eq:SOIvanishes} \\
    A k^2 = \mu \pm \sqrt{\qty(\mu_B H_{ext})^2 - \abs{\Delta}^2}.
\end{align}
These equations are equivalent to equations (4a) and (4b) in Wang et al\cite{wang_new_2018}.

With the first equation, a minimum and maximum of $k$ can be calculated at which nodal points can be calculated.
The $k^3$ term makes solving exactly tricky, but using the method described by Wang et al., one can approximate \cref{eq:SOIvanishes} as
\begin{equation}
    k^2 = -\frac{B}{C} - \frac{\eta D \cos(3\theta)}{C} k^3 \approx -\frac{B}{C} - \frac{\eta D \cos(3\theta)}{C} \qty(-\frac{B}{C})^{3/2}.
\end{equation}
Defining $k_0^2 = -B/C > 0$ and noting that $D/C < 0$, the minimum and maximum of $k^2$ are given by
\begin{align}
    k_{min}^2 = k_0^2 \qty(1 + \frac{D}{C} k_0),\label{eq:kmin}\\
    k_{max}^2 = k_0^2 \qty(1 - \frac{D}{C} k_0).\label{eq:kmax}
\end{align}
This means that nodal points can be found when
\begin{equation}\label{eq:nodalpointequation}
    A k_{min}^2 = \mu_{min} < \mu \pm \sqrt{\qty(\mu_B H_{ext})^2 - \abs{\Delta}^2} < A k_{max}^2 = \mu_{max}.
\end{equation}
In Wang et al., a distinction is made when the equation holds for both $+$ and $-$ or only for one of the two signs.
These will be called the twelve and six nodal point topological superconducting phase respectively.
The region of the chemical potential where SOI vanishes is given by $(\mu_{min}, \mu_{max})$ and will also be called the vanishing region.
As mentioned in the introduction, it is expected that the critical field is suppressed in this region.