\section{Basic theory on superconductivity}\label{sec:basic_theory_sc}
The source of superconductivity in many-body systems is a small attractive interaction between electrons due to electron-phonon interaction \cite{frohlich_theory_1950}.
Such a weak attraction causes electron pairing near the Fermi surface and these electron pairs have a lower energy than two separated electrons near the Fermi surface.
An attractive interaction thus causes the Fermi sphere to break down as it is higher in energy than paired electrons.
According to the BCS theory \cite{bardeen_theory_1957}, the pairing should be between electrons with opposite crystal momentum and spin in order to minimize the energy.
These pairs also known as Cooper pairs.
Furthermore, it is assumed that the attractive interaction is isotropic and thus independent of the crystal momentum of the electrons.
However, it should also be noted that the attractive interaction is only between electrons that are close to the Fermi level due to the fact that phonons can carry only a limited amount of energy called the Debye energy.
Cooper pairs and isotropic interaction are the foundation of conventional $s$-wave superconductivity this will also be applied to Ising superconductors.

\subsection{Hamiltonian}\label{subsec:bdgham}
The Hamiltonian of a superconductor consist of two parts: the normal phase Hamiltonian and the Hamiltonian for the attractive interaction.
Using the two postulates of $s$-wave superconductivity mentioned in the beginning of this section, the Hamiltonian can be expressed like
\begin{equation}\label{eq:originalHamiltonian}
    \begin{split}
        \hat{H} &= \sum_{\vb{k}} \sum_{\sigma', n'; \sigma, n} \qty(\mel{\sigma', n'}{\hat{H}_N\qty(\vb{k})}{\sigma, n} - \mu \delta_{\sigma',\sigma} \delta_{n',n}) \ladderup{c}{\vb{k} \sigma' n'} \ladderdown{c}{\vb{k} \sigma n}
        - V \sum_{\vb{k}, \vb{l}, n', n} \ladderup{c}{\vb{k} \uparrow n'} \ladderup{c}{-\vb{k} \downarrow n'} \ladderdown{c}{-\vb{l} \downarrow n} \ladderdown{c}{\vb{l} \uparrow n}\\
        &= \hat{H}_{0} + \hat{H}_{int},
    \end{split}
\end{equation}
where $\mu$ is the chemical potential, $V$ is the isotropic attractive interaction, and $\ladderup{c}{\vb{k} \sigma n}$ ($\ladderdown{c}{\vb{k} \sigma n}$) is the creation (annihilation) operator for an electron with crystal momentum $\vb{k}$, spin $\sigma$ and orbital $n$.
Note that the summation is only over states that lie close enough to the chemical potential, due to the fact that phonons only have a maximum amount of energy to couple Cooper pairs.
The second term of the Hamiltonian represents the interaction between two different Cooper pairs.
Due to the fact that this term has 4 creation/annihilation operators, it is not possible to exactly solve the Schrödinger equation, i.e. finding the eigenvalues and eigenvectors of the Hamiltonian.
However, a mean field approximation can be performed, so that this term only has 2 creation/annihilation operators left, which makes diagonalization of the Hamiltonian possible.
To this end, define
\begin{equation}
    b_{\vb{k}} = \expval{\sum_{n} \ladderdown{c}{-\vb{k} \downarrow n} \ladderdown{c}{\vb{k} \uparrow n}}.
\end{equation}
The trick is then to add and subtract $b_{\vb{k}}$ from the $\sum_{n} \ladderdown{c}{-\vb{k} \downarrow n} \ladderdown{c}{\vb{k} \uparrow n}$ terms in the interaction Hamiltonian.
The idea is that $\sum_{n} \ladderdown{c}{-\vb{k} \downarrow n} \ladderdown{c}{\vb{k} \uparrow n}$ only slightly deviates from it's mean given by $b_{\vb{k}}$, so that $\qty(\sum_{n} \ladderdown{c}{-\vb{k} \downarrow n} \ladderdown{c}{\vb{k} \uparrow n} - b_{\vb{k}})^2$ is negligible.
The interaction term of the Hamiltonian after mean field approximation is thus
\begin{equation}
    \hat{H}_{int} = - V \sum_{\vb{k}, \vb{l}} \qty( b_{\vb{k}}^{*} \qty[\sum_{n} \ladderdown{c}{-\vb{k} \downarrow n} \ladderdown{c}{\vb{k} \uparrow n}] + b_{\vb{l}} \qty[\sum_{n} \ladderup{c}{\vb{l} \uparrow n} \ladderup{c}{-\vb{l} \downarrow n}] - b_{\vb{k}}^{*} b_{\vb{l}} ).
\end{equation}
Next step is to define the superconducting pairing\footnote{The superconducting pairing may be a complex value, but since only a single bulk TMD monolayer will be studied, the phase will not be interesting.} as
\begin{equation}\label{eq:selfConsistentGapEq}
    \Delta = V \sum_{\vb{l}} b_{\vb{l}},
\end{equation}
so that the interaction Hamiltonian can be simplified further to
\begin{equation}
    \hat{H}_{int} = - \sum_{\vb{k},n} \qty( \Delta^* \ladderdown{c}{-\vb{k} \downarrow n} \ladderdown{c}{\vb{k} \uparrow n} + \Delta \ladderup{c}{\vb{k} \uparrow n} \ladderup{c}{-\vb{k} \downarrow n} - \Delta b_{\vb{k}}^{*} ).
\end{equation}
Using the commutation relations for electron creation/annihilation operators, the normal phase and interaction Hamiltonian can be rewritten as
\begin{align}
    \hat{H}_0 &= \frac{1}{2}\sum_{\vb{k}} \sum_{\sigma', n'; \sigma, n} \qty(\mel{\sigma', n'}{\hat{H}_N\qty(\vb{k})}{\sigma, n} - \mu \delta_{\sigma',\sigma} \delta_{n',n}) \qty(\ladderup{c}{\vb{k} \sigma' n'} \ladderdown{c}{\vb{k} \sigma n} - \ladderdown{c}{\vb{k} \sigma n} \ladderup{c}{\vb{k} \sigma' n'} + \delta_{\sigma',\sigma} \delta_{n',n}) \\
    \hat{H}_{int} &= \frac{1}{2} \sum_{\vb{k},n} \qty[ \Delta^* \qty(\ladderdown{c}{\vb{k} \uparrow n} \ladderdown{c}{-\vb{k} \downarrow n} - \ladderdown{c}{-\vb{k} \downarrow n} \ladderdown{c}{\vb{k} \uparrow n}) + \Delta \qty(\ladderup{c}{-\vb{k} \downarrow n} \ladderup{c}{\vb{k} \uparrow n} - \ladderup{c}{\vb{k} \uparrow n} \ladderup{c}{-\vb{k} \downarrow n}) + 2 \Delta b_{\vb{k}}^{*} ],
\end{align}
and that the total Hamiltonian can be rewritten as
\begin{equation}
    \hat{H} = \frac{1}{2} \sum_{\vb{k}} \ladderup{C}{\vb{k}} \hat{H}_{BdG} \ladderdown{C}{\vb{k}} + \frac{\Tr[\hat{H}_N - \mu \hat{I}]}{2} +  \frac{\abs{\Delta}^2}{V},
\end{equation}
where
\begin{equation}
    \ladderdown{C}{\vb{k}} = \qty[\ladderdown{c}{\vb{k} \uparrow 1}, \ldots, \ladderdown{c}{\vb{k} \uparrow n}, \ladderdown{c}{\vb{k} \downarrow 1}, \ldots, \ladderdown{c}{\vb{k} \downarrow n},
    \ladderup{c}{-\vb{k} \uparrow 1}, \ldots, \ladderup{c}{-\vb{k} \uparrow n}, \ladderup{c}{-\vb{k} \downarrow 1}, \ldots, \ladderup{c}{-\vb{k} \downarrow n}]^T,
\end{equation}
and
\begin{equation}\label{eq:HBdG}
    \hat{H}_{BdG}(\vb{k}) = \mqty[\hat{H}_N(\vb{k}) - \mu \hat{\sigma}_0 \otimes \hat{I}_n & -i \Delta \hat{\sigma}_y \otimes \hat{I}_n \\ i \Delta^* \hat{\sigma}_y \otimes \hat{I}_n & -\hat{H}^*_N(-\vb{k}) + \mu \hat{\sigma}_0 \otimes \hat{I}_n],
\end{equation}
which is also known as the Bogoliubov-de Gennes (BdG) Hamiltonian.
Further, $\hat{\sigma}_0$ and $\hat{\sigma}_y$ are the identity matrix and Pauli-y matrix in spin space respectively, and $I_n$ is the identity matrix acting in the orbital space.

The next step is to diagonalize the BdG Hamiltonian.
By calculating the eigenvalues and eigenvectors of the BdG Hamiltonian, the BdG Hamiltonian can be rewritten as
\begin{equation}
    \hat{H}_{BdG}(\vb{k}) = \hat{U}_{\vb{k}} \hat{D}_{BdG}(\vb{k}) \hat{U}_{\vb{k}}^\dagger,
\end{equation}
where $\hat{U}$ is a matrix with all normalized eigenvectors in the columns and $\hat{D}_{BdG}$ is a diagonal matrix with all the corresponding eigenvalues.
The total Hamiltonian can then be rewritten as
\begin{equation}
    \hat{H} = \frac{1}{2} \sum_{\vb{k}} \ladderup{P}{\vb{k}} \hat{D}_{BdG}(\vb{k}) \ladderdown{P}{\vb{k}} + \frac{\Tr[\hat{H}_N - \mu \hat{I}]}{2} + \frac{\abs{\Delta}^2}{V} = \frac{1}{2} \sum_{\vb{k}, i} \varepsilon_{\vb{k},i} \ladderup{p}{\vb{k}, i} \ladderdown{p}{\vb{k}, i} + \frac{\Tr[\hat{H}_N - \mu \hat{I}]}{2} +  \frac{\abs{\Delta}^2}{V},
\end{equation}
where $\ladderdown{P}{\vb{k}} = \hat{U}_{\vb{k}}^\dagger \ladderdown{C}{\vb{k}}$, and $\ladderup{p}{\vb{k},i}$ ($\ladderdown{p}{\vb{k},i}$) is the new creation (annihilation) operator for quasi-particles and quasi-holes in the superconductor, which are sometimes called Bogoliubons.
As the basis of the original BdG Hamiltonian in \cref{eq:HBdG} contains both particles and holes, so does the diagonalized Hamiltonian.
In order to find the ground state and the exited states of the Hamiltonian, this has to be kept in mind: if a quasi-particle in stat $i$ is present, then there is no quasi-hole in state $i$.
However, if a creation operator for a quasi-particle is known, then the creation operator for the corresponding hole can be found using the particle-hole symmetry of the BdG Hamiltonian, which says
\begin{equation}
    \hat{\mathcal{P}} \hat{H}_{BdG}(\vb{k}) \hat{\mathcal{P}} = - \hat{H}_{BdG} (-\vb{k}),
\end{equation}
where, in this case, $\hat{\mathcal{P}} = \tau_x \hat{\mathcal{K}}$, with $\tau_x$ the Pauli-x matrix in particle-hole space and $\hat{\mathcal{K}}$ the complex conjugation operator.
So if the creation operators create a quasi-particle with energy $\varepsilon_{\vb{k},i} > 0$, then there are creation operators $\hat{\mathcal{P}}\ladderup{P}{\vb{-k}}$ that create the corresponding quasi-hole with energy $-\varepsilon_{\vb{k},i}$.
Therefore, in order to find the ground state, all the quasi-hole creation operators should be converted in to quasi-particle creation operators using the fermion commutation relation $\{\hat{p}, \hat{q}\} = 1$.
The total Hamiltonian with only the quasi-particle creation operators is thus
\begin{equation}\label{eq:finalham}
    \hat{H} = \sum_{\varepsilon_{\vb{k},i} > 0} \varepsilon_{\vb{k},i} \qty(\ladderup{p}{\vb{k}, i} \ladderdown{p}{\vb{k}, i} - \frac{1}{2}) + \frac{\Tr[\hat{H}_N - \mu \hat{I}]}{2} + \frac{\abs{\Delta}^2}{V}.
\end{equation}
From this expression, it can be found that the ground state energy of the superconductor is given by\footnote{The chemical potential $\mu$ should not be included when finding the ground state. It is only there for convenience the free energy will be calculated later on.}
\begin{equation}
    E_g = \frac{\Tr[\hat{H}_N]}{2} + \frac{\abs{\Delta}^2}{V} -\frac{1}{2} \sum_{\varepsilon_{\vb{k},i} > 0} \varepsilon_{\vb{k},i}.
\end{equation}
Furthermore, the excitation energies of the superconductor are $\varepsilon_{\vb{k},i} > |\Delta|$.
Unlike in normal conductors, where excitation energies are infinitely small, superconductors will have hardly any scattering (and therefore zero resistance) as a minimum energy of $|\Delta|$ is required to perturb the ground state of the superconductor.

\subsection{Obtaining the weak-coupling limit}
In order to find the superconducting pairing $\Delta$, the free energy needs to be minimized.
The obtained superconducting pairing should also be consistent with the self-consistent gap equation (\cref{eq:selfConsistentGapEq}).
This equation, however, may give non-trivial solutions ($\Delta \neq 0$), which do not globally minimize the free energy.
On the other hand, the self-consistent gap equation can be used when the isotropic interaction $V$ needs to be calculated, if $\Delta_0$, the superconducting pairing at zero temperature and field, is known.
Using the definitions specified earlier in \cref{subsec:bdgham}, the expression of $b_{\vb{k}}$ can be rewritten as
\begin{equation}
    b_{\vb{k}} = -\expval{\sum_{n} \ladderdown{c}{\vb{k} \uparrow n} \ladderdown{c}{-\vb{k} \downarrow n}} = -\expval{\Tr_{\text{topright}}\qty[\ladderdown{C}{\vb{k}}\ladderup{C}{\vb{k}}]} = -\Tr_{\text{topright}}\qty[\hat{U}_{\vb{k}} \expval{\ladderdown{P}{\vb{k}}\ladderup{P}{\vb{k}}} \hat{U}_{\vb{k}}^{\dagger}],
\end{equation}
where $\Tr_{\text{topright}}$ indicates that the trace should be applied to the top right $n \times n$ submatrix of $\ladderdown{C}{\vb{k}}\ladderup{C}{\vb{k}}$, which is a $4n \times 4n$ matrix.
To evaluate $\expval{\ladderdown{P}{\vb{k}}\ladderup{P}{\vb{k}}}$, note that the diagonal matrix elements are simply unity minus the average occupation of the quasi-particles or quasi-holes, which can be calculated using the Fermi-Dirac distribution.
On the other hand, the off-diagonal elements should be zero as eigenstates of the BdG Hamiltonian do not contribute to the average of a creation-annihilation pair of two different states.
The matrix elements of $\expval{\ladderdown{P}{\vb{k}}\ladderup{P}{\vb{k}}}$ are thus given by
\begin{equation}
    \begin{split}
        \expval{\ladderdown{p}{\vb{k},i} \ladderup{p}{\vb{k},j}} &= \begin{cases}
        0 &\quad\text{if } i \neq j \\
        1 - \frac{1}{\exp(-\frac{\varepsilon_{\vb{k},i}}{k_B T}) + 1} &\quad\text{if } i = j
        \end{cases} \\
        &=
        \begin{cases}
        0 &\quad\text{if } i \neq j \\
        \frac{1}{\exp(\frac{\varepsilon_{\vb{k},i}}{k_B T}) + 1} &\quad\text{if } i = j
        \end{cases},
    \end{split}
\end{equation}
where $k_B$ is the Boltzmann constant and $T$ is the temperature.
A problem of using the self-consistent gap equation to calculate $V$, however, is that it does not look efficient: only the diagonal of a small upper-right part of the matrix is actually used.
Fortunately, there is a better way to get $V$ given $\Delta_0$, but this requires to minimize the free energy as well and the method will be explained later in this section.

From the final Hamiltonian in \cref{eq:finalham}, the grand partition function can be derived and is given by
\begin{equation}
    Z_{gr} = \exp(-\qty[ \frac{\Tr[\hat{H}_N - \mu \hat{I}]}{2} + \frac{\abs{\Delta}^2}{V} ] / k_B T) \prod_{\varepsilon_{\vb{k},i} > 0} 2 \cosh(\frac{\varepsilon_{\vb{k},i}}{2 k_B T}).
\end{equation}
From this, the free energy\footnote{Since the Hamiltonian describes an open system with constant $T$ and $\mu$, it also also known the grand potential, but in the literature, it is usually called the (Landau) free energy.} can then simply be calculated by
\begin{equation}
    \Phi = - k_B T \log(Z_{gr}) = \frac{\Tr[\hat{H}_N - \mu \hat{I}]}{2} + \frac{\abs{\Delta}^2}{V} - k_B T \sum_{\varepsilon_{\vb{k},i} > 0} \log(2 \cosh(\frac{\varepsilon_{\vb{k},i}}{2 k_B T})).
\end{equation}
For the purpose of finding a $\Delta$ that minimizes the free energy the trace term $\Tr[\hat{H}_N - \mu \hat{I}]/2$ can be left out as it does not depend on $\Delta$.
Furthermore, as $\cosh$ is an even function, it simpler to sum over all eigenvalues and divide the result by two.
This is equivalent as the particle-hole symmetry requires that if there is a quasi-particle with energy $\varepsilon$, then there is a quasi-hole with energy $-\varepsilon$.
The expression for the free energy will thus become
\begin{equation}\label{eq:freeEnergy}
    \Phi = \frac{\abs{\Delta}^2}{V} - \frac{1}{2} k_B T \sum_{\vb{k},i} \log(2 \cosh(\frac{\varepsilon_{\vb{k},i}}{2 k_B T})).
\end{equation}
For low temperatures, $\cosh(\varepsilon_{\vb{k},i} / 2 k_B T) \approx \exp(\qty|\varepsilon_{\vb{k},i}| / 2 k_B T)/2.$ so that the free energy in that limit becomes
\begin{equation}\label{eq:freeEnergyLowT}
    \Phi \approx \frac{\abs{\Delta}^2}{V} - \sum_{\vb{k},i} \frac{\qty|\varepsilon_{\vb{k},i}|}{4}.
\end{equation}
This is also the exact value of the free energy if $T = 0$.

As mentioned earlier, the free energy expression can be used to find $V$ given the superconducting pairing $\Delta_0$ in case of zero temperature and zero field.
To this end, the free energy (using the expression for low temperatures \cref{eq:freeEnergyLowT}) needs to be minimized by taking the derivative of \cref{eq:freeEnergy} with respect to $\abs{\Delta}$.
This gives the expression
\begin{equation}\label{eq:generalminfreeEnergyZeroFieldandT}
    \eval{\pdv{\Phi}{\abs{\Delta}}}_{\abs{\Delta} = \abs{\Delta_0}} = \frac{2 \abs{\Delta}}{V} - \frac{1}{4} \sum_{\vb{k},i} \eval{\pdv{\abs{\varepsilon_{\vb{k},i}}}{|\Delta|}}_{\abs{\Delta} = \abs{\Delta_0}} = 0.
\end{equation}
This still requires to find an expression for $\eval{\pdv*{\abs{\varepsilon_{\vb{k},i}}}{|\Delta|}}_{\abs{\Delta} = \abs{\Delta_0}}$.
Fortunately, because of the absence of a magnetic field, the normal phase Hamiltonian is time reversal symmetric and this gives a relation between the normal phase eigenstates for opposite $\vb{k}$, as
\begin{equation}
    \hat{H}_N(\vb{k}) = \timerev^{-1} \hat{H}_N(-\vb{k}) \timerev.
\end{equation}
The time reversal operator is given by
\begin{equation}
    \timerev = \qty(i \hat{\sigma}_y \otimes \hat{I}_n) \hat{\mathcal{K}}.
\end{equation}
Thus, if there is a quasi-particle in the normal phase with crystal momentum $\vb{k}$, then the time reversal symmetry requires there is a quasi-particle with opposite momentum and spin with the same energy.
But these quasi-particles form a Cooper pair together, which means that the BdG Hamiltonian can be block diagonalized such that the blocks look like
\begin{equation}
    \hat{H}_{BdG}\qty(\vb{k},n) = \mqty(\xi_{\vb{k},n} & \Delta e^{i \varphi}  \\ \Delta^* e^{-i \varphi} & -\xi_{\vb{k},n}),
\end{equation}
where $\xi_{\vb{k},n}$ represents the the $n$th eigenvalue of the normal phase\footnote{This eigenvalue is the energy of the $n$th band with respect to the chemical potential $\mu$.} at crystal momentum $\vb{k}$, and $\varphi$ is some phase.
The eigenvalues of this block are given by
\begin{equation}
    \varepsilon_{\vb{k},n} = \pm \sqrt{\xi_{\vb{k},n}^2 + \abs{\Delta}^2},
\end{equation}
so that \cref{eq:generalminfreeEnergyZeroFieldandT} can be rewritten as
\begin{equation}\label{eq:V_sumexpr}
    \frac{1}{V} = \frac{1}{4} \sum_{\vb{k},n} \frac{1}{\sqrt{\xi_{\vb{k},n}^2 + \abs{\Delta_0}^2}}.
\end{equation}
From this expression, that it is easier to find $V$ in terms of $\Delta_0$ than with the method explained in the beginning of this section, as only the eigenvalues of the normal phase Hamiltonian are required in addition to $\Delta_0$.
The sum over $\vb{k}$ in the expression can also be converted into an integral as the possible crystal momenta form a continuum in large systems and this gives
\begin{equation}
    \frac{1}{V} = \frac{1}{4} \sum_{n} \int\limits_{-\hbar \omega_D}^{\hbar \omega_D} \frac{\mathcal{N}_n\qty(\xi) \dd\xi}{\sqrt{\xi^2 + \abs{\Delta_0}^2}},
\end{equation}
where $\mathcal{N}_n\qty(\xi)$ represents the density of states (DOS) for the $n$th band, and $\omega_D$ is the Debye frequency.
As explained earlier, the attractive interaction between Cooper pairs is due to electron-phonon interaction and the maximum energy of the phonons $\hbar \omega_D$ determines the cutoff energy of the integral.
Assuming that the density of states of all the bands are roughly constant in the interval $\qty[-\hbar \omega_D, \hbar \omega_D]$, the integral can also be evaluated so that
\begin{equation}\label{eq:zeroTandHextCase}
    \frac{1}{\mathcal{N}_{tot}\qty(0) V} = \frac{1}{4} \int\limits_{-\hbar \omega_D}^{\hbar \omega_D} \frac{\dd\xi}{\sqrt{\xi^2 + \abs{\Delta_0}^2}} = \frac{1}{2} \sinh[-1](\frac{\hbar \omega_D}{\abs{\Delta_0}}),
\end{equation}
where $\mathcal{N}_{tot}\qty(0) = \sum_{n} \mathcal{N}_n\qty(0)$ represents the total density of states at the chemical potential.
In most superconductors $\mathcal{N}_{tot}\qty(0) V \ll 1$, which is also known as the weak-coupling limit.
In that case, it can be found that
\begin{equation}\label{eq:weakcouplinglimit}
    \abs{\Delta_0} = 2 \hbar \omega_D \exp(-\frac{2}{\mathcal{N}_{tot}(0) V}).
\end{equation}
This equation holds for any (time reversal symmetric) system with conventional $s$-wave superconductivity.
Note that the weak-coupling limit can also be achieved by imposing $\abs{\Delta_0} \ll \hbar \omega_D$.

\subsection{Universal critical temperature}
At finite temperatures, the general free energy expression (\cref{eq:freeEnergy}) should be used instead.
Minimizing the free energy by taking the derivative with respect to $|\Delta|$ gives
\begin{equation}\label{eq:generalMinDelta}
    \frac{1}{V} = \frac{1}{4} \sum_{n} \int\limits_{-\hbar \omega_D}^{\hbar \omega_D} \tanh(\frac{\sqrt{\xi^2 + \qty|\Delta|^2}}{2 k_B T}) \frac{\mathcal{N}_n\qty(\xi) \dd\xi}{\sqrt{\xi^2 + \qty|\Delta|^2}}.
\end{equation}
Using this expression, an universal expression for the critical temperature of all conventional $s$-wave superconductors can be derived.
At the critical temperature $T_c$, the superconducting pairing $\Delta$ should become zero.
Again assuming that the density of states of all the bands are roughly constant in the interval $\qty[-\hbar \omega_D, \hbar \omega_D]$, one can rewrite the expression as
\begin{equation}\label{eq:zeroTCase}
    \frac{2}{\mathcal{N}_{tot}\qty(0) V} = \frac{1}{2} \int\limits_{-\hbar \omega_D}^{\hbar \omega_D} \tanh(\frac{\abs{\xi}}{2 k_B T_c}) \frac{\dd\xi}{\abs{\xi}} = \ln(\frac{2 \hbar \omega_D}{\pi k_B T_c}) + \gamma,
\end{equation}
where $\gamma = 0.577\ldots$ is the so-called Euler's constant.
Working out further gives
\begin{equation}\label{eq:universalTc}
    k_B T_c = \frac{2 e^{\gamma}}{\pi} \hbar \omega_D e^{-\frac{2}{\mathcal{N}_{tot}(0) V}} \approx \frac{\abs{\Delta_0}}{1.764},
\end{equation}
where the weak-coupling limit (\cref{eq:weakcouplinglimit}) has been used in the last step.
This equation fixes the ratio between the critical temperature and the superconducting pairing in the weak-coupling limit and is valid for any material with conventional $s$-wave pairing as long as $\mathcal{N}_{tot}\qty(0) V \ll 1$.

One may be concerned whether the $\Delta$ that minimizes the free energy given by equation \cref{eq:generalMinDelta} converges when the Debye energy diverges.
By carefully analyzing the equation, it can be found that this is indeed the case and the analysis is given in \cref{ch:convergence_Delta_minimizes_FreeEnergy}.