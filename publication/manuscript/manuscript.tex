\documentclass[twocolumn, 10pt, aps, floatfix, showpacs, prb, citeautoscript]{revtex4-1}

% Input, output and font packages:
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

% Language:
\usepackage[english]{babel}
\usepackage{csquotes}

% Page geometry:
\usepackage[margin=2cm]{geometry}

% Math related packages:
\usepackage{amsmath,amsfonts,amssymb,mathtools}

% Units and uncertainties:
\usepackage[separate-uncertainty,range-units=single,range-phrase=--]{siunitx}
\DeclareSIUnit{\diracconstant}{\ensuremath{\hbar}}

% Graphics:
\usepackage{graphicx,float}
\usepackage[font=small,labelfont=bf]{caption}
\usepackage{subcaption}

% Lipsum text and placeholder images:
\usepackage{lipsum,mwe}

% Neat tables
\usepackage{booktabs}

% Useful shorthand math for physicists:
\usepackage{physics}

% Chemcial formulas
\usepackage[version=4]{mhchem}

% Hyperlinks and clever referencing:
\usepackage[colorlinks]{hyperref}
\usepackage{cleveref}

% Space after argumentless costum commands
\usepackage{xspace}

% Additional useful macros:
\newcommand{\kdotp}{\ensuremath{\vb{k}\vdot\vb{p}}\xspace}
\newcommand{\Heff}{\ensuremath{H_{\text{eff}}}\xspace}
\newcommand{\HBdG}{\ensuremath{H_{\text{BdG}}}\xspace}

\begin{document}
    %\title{In-plane critical field of superconducting transition metal dichalcogenide monolayers with momentum dependent Ising spin-orbit coupling.}
    \title{In-plane critical field due to momentum dependent Ising spin-orbit coupling in superconducting transition metal dichalcogenide monolayers.}

    \author{Kevin Choi}
    \affiliation{Kavli Institute of Nanoscience, Delft University of Technology, P.~O.~Box 4056, 2600 GA Delft, The Netherlands}

    \author{Lin Wang}
    \affiliation{Kavli Institute of Nanoscience, Delft University of Technology, P.~O.~Box 4056, 2600 GA Delft, The Netherlands}

    \begin{abstract}
        Superconducting transition metal dichalcogenide (TMD) monolayers exhibit an enhanced in-plane critical field with respect to the Pauli limit due to Ising spin-orbit coupling (SOC).
        Using a mean field calculation, we calculated the in-plane critical field of \ce{MoS2}, \ce{MoSe2}, \ce{MoTe2} and \ce{WS2} monolayers.
        We find for all materials that the in-plane critical field is larger when the chemical potential is located in a region with stronger SOC.
        Consequently, the in-plane critical field is greatly suppressed when the chemical potential is located in a region where the SOC vanishes.
        Our results show that the vanishing region of \ce{MoS2} is located far below the critical chemical potential required to induce superconductivity.
        Therefore, a suppression of the enhancement of the in-plane critical field cannot be observed for \ce{MoS2} in experiments.
        On the other hand, the critical chemical potential of \ce{MoSe2} and \ce{MoTe2} is located closely below the vanishing region, and we predict that a suppression of the in-plane critical field can be observed for these materials.
        Our work shows the importance of the effect of momentum dependence of Ising SOC to the suppression of the in-plane critical field.
    \end{abstract}

    \maketitle

    \section{Introduction}
    Transition metal dichalcogenide (TMD) monolayers are a family of two-dimensional materials that are composed of one layer of transition metal (e.g. \ce{Mo}, \ce{W}) atoms sandwiched between two layers of chalcogen atoms (e.g. \ce{S}, \ce{Se}, \ce{Te}).
    Both the transition metal layer and the two chalcogen layers have their atoms arranged in a triangular lattice, and form together a honeycomb lattice from top view~\cite{bromley_band_1972}.
    The normal phase band structure shows that TMD monolayers are semiconductors with a direct bandgap at the $\pm K$ point~\cite{mak_atomically_2010,ross_electrical_2013,splendiani_emerging_2010,ruppert_optical_2014,zeng_optical_2013,tongay_thermally_2012}.
    Due to the lack of inversion symmetry, the lowest conduction and highest valence bands of TMD monolayers also exhibit effective Zeeman field that is perpendicular to the plane and has an opposite orientation at opposite crystal momenta~\cite{xiao_coupled_2012,zhu_giant_2011}.
    This so-called Ising spin-orbit coupling (SOC) has applications for spintronic~\cite{wolf_spintronics:_2001,husain_spin_2018} and valleytronic~\cite{rycerz_valley_2007,mak_control_2012,zeng_valley_2012,cao_valley-selective_2012} devices.

    Over the recent years, it has been shown in experiments that several TMD monolayers like \ce{MoS2}, \ce{MoSe2}, \ce{MoTe2} and \ce{WS2} are superconducting for sufficiently large electron densities in the conduction band~\cite{taniguchi_electric-field-induced_2012,miao_emergence_2016,costanzo_gate-induced_2016,zheliuk_monolayer_2017,ye_superconducting_2012,shi_superconductivity_2015,lu_evidence_2015,lu_full_2018,saito_superconductivity_2016,saito_gate-induced_2016} with a critical temperature up to \SI{10.8}{\K} for \ce{MoS2}~\cite{ye_superconducting_2012,lu_evidence_2015}.
    Furthermore, it has been demonstrated that \ce{MoS2} and \ce{WS2} have a high in-plane critical field that exceed several times the Pauli limit~\cite{lu_evidence_2015,lu_full_2018,saito_superconductivity_2016,saito_gate-induced_2016}.
    For conventional superconductors in the absence of SOC, the critical magnetic field is equal to the Pauli limit~\cite{clogston_upper_1962,chandrasekhar_note_1962}, when the orbital effects are neglected.
    However, for superconducting TMD monolayers, the Ising SOC pins electron spins along the out-of-plane direction such that an in-plane magnetic field cannot effectively break the Cooper pairs~\cite{lu_evidence_2015,saito_superconductivity_2016}.

    For molybdenum dichalcogenide (\ce{MoX2}) monolayers, unlike tungsten dichalcogenide (\ce{WX2}) monolayers, the lowest conduction band has a region near the $\pm K$ point where the spin splitting vanishes~\cite{kosmider_large_2013,liu_three-band_2013,kormanyos_k.p_2015}.
    These materials are interesting as they offer a platform for nodal topological superconductivity~\cite{wang_platform_2018}.
    Because smaller effective fields imply less protection of Cooper pairs from in-plane fields, it is expected that the enhancement of the in-plane critical field in superconducting \ce{MoX2} monolayers is suppressed.
    Nevertheless, experiments show that the in-plane critical field of \ce{MoS2} significantly exceeds the Pauli limit~\cite{lu_evidence_2015,saito_superconductivity_2016}.
    As far as we know, there are no experiments that report an enhancement of the in-plane critical field for \ce{MoSe2} and \ce{MoTe2}.

    In this work, we show with a mean field calculation that the in-plane critical field is higher when the chemical potential is located within a region with larger SOC.
    In particular, the in-plane critical field is greatly suppressed when the chemical potential is located in the vanishing region of \ce{MoX2} monolayers.
    We find that the vanishing region of \ce{MoS2} is located much lower than the critical chemical potential required to induce superconductivity in \ce{MoS2} monolayers, which means that no suppression of the enhancement of the in-plane critical field can be observed for \ce{MoS2}.
    On the other hand, the critical chemical potential of \ce{MoSe2} and \ce{MoTe2} lie closely below vanishing region of these materials and we therefore expect that a suppression will be detected in future experiments.

    \section{Model}
    In the basis $\qty[c^{\phantom{\dag}}_{\eta \vb{k} \uparrow}, c^{\phantom{\dag}}_{\eta \vb{k} \downarrow}, c^{\dag}_{-\eta -\vb{k} \uparrow}, c^{\dag}_{-\eta -\vb{k} \downarrow}]$ near the $\eta K$ point, the lowest conduction band of superconducting TMD monolayers with $s$-wave pairing and an in-plane ($xy$-plane) external magnetic field is described by the Bogoliubov-De Gennes (BdG) Hamiltonian given by~\cite{wang_platform_2018}
    \begin{equation}\label{eq:BdG}
        \begin{split}
            \HBdG\qty(\vb{k}, \eta) &= \qty(\frac{\hbar^2 k^2}{2 m^*} - \mu) \tau_z \\
            &+ \qty[\lambda \eta + A_1 k^2 \eta + A_2 \qty(k_x^3 - 3 k_x k_y^2)] \sigma_z \\
            &+ B_{x} \sigma_x \tau_z + B_{y} \sigma_y + \Delta \tau_y \sigma_y,
        \end{split}
    \end{equation}
    where $m^*$, $\lambda$, $A_1$, $A_2$ are material-dependent parameters, $\eta = \pm 1$ is the valley index, $\vb{k}$ is the crystal momentum with respect to the $\eta K$ point, $\mu$ is the chemical potential, $B_{x}$ and $B_{y}$ are the $x$ and $y$ component of the external magnetic field respectively, $\Delta$ is the superconducting pairing, and $\sigma_i$ and $\tau_i$ ($i \in \qty{x,y,z}$) are the Pauli matrices acting in the spin and particle-hole space respectively.
    The material-dependent parameters $m^*$, $\lambda$, $A_1$ and $A_2$ for \ce{MoS2}, \ce{MoSe2}, \ce{MoTe2}, and \ce{WS2} are derived from \kdotp models of Fang et al.~\cite{fang_ab_2015} and Kormányos et al.~\cite{kormanyos_k.p_2015} using the Löwdin partition method~\cite{lowdin_note_1951,wang_electron_2014}, and are given in \cref{tab:materialProperties}.

    \begin{table}[ht]
        \caption{The \kdotp parameters ($m^*$, $\lambda$, $A_1$, $A_2$), the lower and upper bound of the vanishing region ($\mu_{min}$, $\mu_{max}$), and the critical chemical potential ($\mu_c$) for the materials \ce{MoS2}, \ce{MoSe2}, \ce{MoTe2}, and \ce{WS2}. $m_e$ denotes the electron rest mass. The \kdotp parameters of \ce{MoTe2} as well as $m^*$ of \ce{MoSe2} are based on Kormányos et al.~\cite{kormanyos_k.p_2015}, while the other \kdotp
             parameters are based on Fang et al.~\cite{fang_ab_2015}}
        \begin{tabular}{lrrrr}
            \toprule
            & \ce{MoS2} & \ce{MoSe2} & \ce{MoTe2} & \ce{WS2} \\
            \midrule
            $m^*$ [$m_e$] & $0.4675$ & $0.60$ & $0.62$ & $0.3063$ \\
            $\lambda$ [\si{\eV}] & $-0.0015$ & $-0.0106$ & $-0.0180$ & $0.0148$ \\
            $A_1$ [\si{\eV\angstrom\squared}] & $0.3645$ & $0.5018$ & $0.6964$ & $1.2222$ \\
            $A_2$ [\si{\eV\angstrom\cubed}] & $-0.1570$ & $-0.2279$ & $-0.6575$ & $-0.3916$ \\
            $\mu_{1}$ [\si{\meV}] & $32.4$ & $125.3$ & $134.7$ & - \\
            $\mu_{2}$ [\si{\meV}] & $34.3$ & $143.0$ & $182.9$ & - \\
            $\mu_{c}$ [\si{\meV}] & $154$ & $120$ & $116$ & $78.2$ \\
        $T_{c,\mathrm{max}}$ [\si{\K}] & $10.8$~\cite{ye_superconducting_2012} & $7.1$~\cite{shi_superconductivity_2015} & $2.8$~\cite{shi_superconductivity_2015} & $3.15$~\cite{lu_full_2018} \\
            \bottomrule
        \end{tabular}
        \label{tab:materialProperties}
    \end{table}

    Using the eigenvalues of the BdG Hamiltonian $\varepsilon_{\vb{k}, \eta, j}$ in \cref{eq:BdG}, we determine the value of $\Delta$ by globally minimizing the free energy of the superconducting TMD monolayer given by~\cite{he_magnetic_2018}
    \begin{equation}\label{eq:freeEnergy}
        \Phi = \frac{\Delta^2}{U} - \frac{1}{2 \beta} \sum_{\vb{k}, \eta, j} \log(2 \cosh(\frac{\beta \varepsilon_{\vb{k}, \eta, j}}{2})).
    \end{equation}
    Here, $U$ is the attractive interaction, which is determined by solving $\eval{\pdv{\Phi}{\Delta}}_{\Delta = \Delta_0} = 0$ for $U$, where $\Delta_0$ indicates the maximum superconducting pairing at zero temperature and field.
    The in-plane critical field $B_{c}$ is determined by examining for what field the value of $\Delta$ will vanish.
    Details of our calculation is provided in the Supplementary information.

    \section{Results}

    \subsection{Critical field at zero temperature}
    The calculated in-plane critical field $B_c$ as a function of chemical potential and $\Delta_0$ for \ce{MoSe2}, \ce{MoTe2}, \ce{MoS2}, and \ce{WS2} at \SI{0}{\K} are shown in \cref{fig:zeroT_ratio}.
    The white dotted vertical lines in the plots for \ce{MoSe2}, \ce{MoTe2}, and \ce{MoS2} indicate the vanishing region of these materials. 
    The lower $\mu_{1}$ and upper $\mu_{2}$ bound of the vanishing region of these materials are also tabulated in \cref{tab:materialProperties} and are derived from the \kdotp parameters using~\cite{wang_platform_2018}
    \begin{equation}
        \mu_{1,2} = \frac{\hbar^2 k_0^2}{2 m^*} \qty(1 \pm \frac{A_2}{A_1} k_0),
    \end{equation}
    where $k_0 = \sqrt{-\tfrac{\lambda}{A_1}}$.
    The vertical green line in the plots indicate the critical chemical potential $\mu_{c}$ of the material, which is defined as the minimum chemical potential required in order to obtain superconductivity.
    The values of the critical chemical potential for all materials are also tabulated in \cref{tab:materialProperties}.
    These values are related to the minimum carrier density required in experiments in order to obtain superconductivity, which is approximately \SI{6e13}{\per\centi\meter\squared} for all \ce{MoX2} materials~\cite{ye_superconducting_2012,lu_evidence_2015,shi_superconductivity_2015} and approximately \SI{2e13}{\per\centi\meter\squared} for \ce{WS2}~\cite{lu_full_2018}.

    We see from \cref{fig:MoSe2_zeroT_ratio,fig:MoTe2_zeroT_ratio} that the critical chemical potential for \ce{MoSe2} and \ce{MoTe2} are located just below the vanishing region.
    We therefore expect that a suppression of the enhancement of the in-plane critical field can be detected in future experiments.
    On the other hand, the critical chemical potential of \ce{MoS2} (\cref{fig:MoS2_zeroT_ratio}) is far above the vanishing region, which implies that no suppression of the enhancement of the in-plane critical field is expected in experiments.
    Nevertheless, the possibility to induce superconductivity in low carrier density \ce{MoS2} monolayers by means of the proximity effect must be kept into consideration.
    Our results show that the enhancement of the in-plane critical field for \ce{MoTe2} is not so much suppressed in the vanishing region compared to \ce{MoS2} and \ce{MoSe2}, which we ascribe to the relative large values of $A_1$ and $A_2$ for \ce{MoTe2}.
    For \ce{WS2} (\cref{fig:WS2_zeroT_ratio}), we do not have a suppression of the enhancement of the in-plane critical field due to the absence of a vanishing region, although the enhancement is higher for larger chemical potential due to larger spin splitting.
    To summarize, all our results show that the enhancement of the in-plane critical field is larger when the chemical potential in in a region with larger SOC.

    \begin{figure}[ht]
        \centering
        \begin{subfigure}[b]{0.45\linewidth}
            \centering
            \includegraphics[height=80px]{figures/MoSe2_zeroT_ratio}
            \caption{\ce{MoSe2}}
            \label{fig:MoSe2_zeroT_ratio}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.45\linewidth}
            \centering
            \includegraphics[height=80px]{figures/MoTe2_zeroT_ratio}
            \caption{\ce{MoTe2}}
            \label{fig:MoTe2_zeroT_ratio}
        \end{subfigure}

        \begin{subfigure}[b]{0.45\linewidth}
            \centering
            \includegraphics[height=80px]{figures/MoS2_zeroT_ratio}
            \caption{\ce{MoS2}}
            \label{fig:MoS2_zeroT_ratio}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.45\linewidth}
            \centering
            \includegraphics[height=80px]{figures/WS2_zeroT_ratio}
            \caption{\ce{WS2}}
            \label{fig:WS2_zeroT_ratio}
        \end{subfigure}
        \caption{The in-plane critical fields with respect to the Pauli limit as a function of chemical potential and $\Delta_0$ for (a) \ce{MoSe2}, (b) \ce{MoTe2}, (c) \ce{MoS2}, and (d) \ce{WS2} at \SI{0}{\K}. The white dotted vertical lines indicate the vanishing region (if present) and the vertical green line in the plots indicate the critical chemical potential.}
        \label{fig:zeroT_ratio}
    \end{figure}

    \subsection{Critical field at fixed $\Delta_0$}
    The temperature and chemical potential dependence of the calculated in-plane critical field with respect to the Pauli limit for \ce{MoSe2}, \ce{MoTe2}, \ce{MoS2}, and \ce{WS2} are shown in \cref{fig:finiteT}.
    The values for $\Delta_0$ in \cref{fig:finiteT} are different for each material and fixed to the half of the maximum possible value of $\Delta_0$ for that material.
    In the weak-coupling limit, the maximum value of $\Delta_{0,\mathrm{max}}$ can be derived from the maximum critical temperature $T_{c,\mathrm{max}}$ by using the universal relation given by $T_{c,\mathrm{max}} \approx \tfrac{\Delta_{0,\mathrm{max}}}{1.764 k_B}$~\cite{tinkham_introduction_1996}.
    The values of $T_{c,\mathrm{max}}$ are based on experiments and tabulated with corresponding references in \cref{tab:materialProperties}.
    Similar to \cref{fig:zeroT_ratio}, the vanishing region for all \ce{MoX2} materials and the critical chemical potential for all 4 materials are indicated with white dotted vertical lines and a vertical green line respectively.
    
    The calculated in-plane critical field in shows that the field decreases while the temperature increases and eventually becomes zero at half of the maximum critical temperature, which means that the numerical calculation is consistent with the universal relation between $T_c$ and $\Delta_0$ for weak-coupling superconductors.
    We also conclude from \cref{fig:finiteT} that our findings for zero temperature also apply for finite temperatures.
    Like in \cref{fig:zeroT_ratio}, we see that the the in-plane critical field is larger when the chemical potential is in a region with larger SOC.

    \begin{figure}[ht]
        \centering
        \begin{subfigure}[b]{0.45\linewidth}
            \centering
            \includegraphics[height=80px]{figures/MoSe2_finiteT}
            \caption{\ce{MoSe2},\\$\Delta_0 \approx \SI{0.54}{\meV}$}
            \label{fig:MoSe2_finiteT}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.45\linewidth}
            \centering
            \includegraphics[height=80px]{figures/MoTe2_finiteT}
            \caption{\ce{MoTe2},\\$\Delta_0 \approx \SI{0.21}{\meV}$}
            \label{fig:MoTe2_finiteT}
        \end{subfigure}
        
        \begin{subfigure}[b]{0.45\linewidth}
            \centering
            \includegraphics[height=80px]{figures/MoS2_finiteT}
            \caption{\ce{MoS2},\\$\Delta_0 \approx \SI{0.82}{\meV}$}
            \label{fig:MoS2_finiteT}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.45\linewidth}
            \centering
            \includegraphics[height=80px]{figures/WS2_finiteT}
            \caption{\ce{WS2},\\$\Delta_0 \approx \SI{0.24}{\meV}$}
            \label{fig:WS2_finiteT}
        \end{subfigure}
        \caption{The in-plane critical fields with respect to the Pauli limit as a function of chemical potential and temperature for (a) \ce{MoSe2}, (b) \ce{MoTe2}, (c) \ce{MoS2}, and (d) \ce{WS2} at \SI{0}{\K}. The fixed value of $\Delta_0$ of the material is equal to half of the maximum value of $\Delta_0$ found in experiments. The white dotted vertical lines indicate the vanishing region (if present) and the vertical green line in the plots indicate the critical chemical potential.}
        \label{fig:finiteT}
    \end{figure}

    \section{Discussion}
    Our calculation provides insight how the vanishing region in the lowest conduction band of \ce{MoX2} monolayers affect the in-plane critical as a function of the chemical potential.
    In literature, suppression of the in-plane critical field has been ascribed to external effects such as intervalley scattering due to disorder~\cite{ilic_enhancement_2017} and Rashba SOC due because of ripples in the sample~\cite{huertas-hernando_spin-orbit_2006}. 
    Our work emphasizes that also the vanishing region of \ce{MoX2} monolayers may cause a suppression of the in-plane critical field.
    Unlike disorder and sample ripples, the vanishing region is intrinsic to \ce{MoX2} monolayers and cannot be mitigated by improving sample quality.
    This effect of the vanishing region on the in-plane critical field is an important finding, because earlier models of superconducting TMD monolayers used to determine the in-plane critical field do not consider the presence of a vanishing region in the lower conduction band~\cite{ilic_enhancement_2017,lu_evidence_2015,liu_microscopic_2019}.

    \appendix

    \bibliographystyle{apsrev4-1}
    \bibliography{Manuscript}

\end{document}